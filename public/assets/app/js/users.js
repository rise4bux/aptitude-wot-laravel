$(document).ready(function () {
    var college_value;
    // front side validation
    $('#f_name').on('keyup',function () {
        if($('#f_name').val() != '') {
            $('#f_nameError').hide();
        } else {
            $('#f_nameError').show();
        }
    });
    $('#address').on('keyup',function () {
        if($('#address').val() != '') {
            $('#addressError').hide();
        } else {
            $('#addressError').show();
        }
    });
    $('#interest').on('keyup',function () {
        if($('#interest').val() != '') {
            $('#interestError').hide();
        } else {
            $('#interestError').show();
        }
    });
    $('#l_name').on('keyup',function () {
        if($('#l_name').val() != '') {
            $('#l_nameError').hide();
        } else {
            $('#l_nameError').show();
        }
    });
    $('#enrollment').on('keyup',function () {
        if(($('#enrollment').val()).length >= 6) {
            $('#enrollmentError').hide();
        } else {
            $('#enrollmentError').show();
        }
    });
    $('#email').on('keyup', function() {
        var email = $(this).val();
        console.log(email);
        var emailExp = new RegExp(/^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i);
        if(emailExp.test(email)) {
            $('#emailError').hide();
        } else {
            $('#emailError').show();
        }
    });
    $('#mobile_no').on('keyup', function() {
        var value = $(this).val();
        if(value == '') {
            $('#mobile_noError').show();
        } else if (value.length != 10) {
            $('#mobile_noError').show();
        } else {
            $('#mobile_noError').hide();
        }
    });
    $('#password').on('keyup',function () {
        $('#passwordError').hide();
    });
    $('#openings').on('change', function() {
            $('#openingsError').hide();
    });
    $('#college').on('change', function() {
            $('#collegeError').hide();
    });
    $('#future_studies').on('change', function() {
        $('#future_studiesError').hide();
    });
    $('#stream').on('change', function() {
        $('#streamError').hide();
    });
    $('#year').on('change', function() {
        $('#yearError').hide();
    });
    $('#university').on('change', function() {
        $('#universityError').hide();
        $('#college option').remove();
        var value = $(this).val();
        $.ajax({
            url: '/admin/users/ajaxCollege/'+value,
            type: 'GET',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                $("#college").append('<option value="">Select</option>');
                $.each(response.data, function (key, value) {
                    $("#college").append('<option value=' + value.id + '>' + value.name + '</option>');
                });
            },
        });
    });

    // show/hide password
    $(document).on('click','.toggle-password', function() {
        var id = $(this).siblings('input').attr('id');
        if($('#'+id).attr('type') == 'password') {
            $('#'+id).attr('type','text');
        } else {
            $('#'+id).attr('type','password');
        }
    });
    // create user
    $(document).on('click', '#adduser', function () {
        var form = $("#user_form")[0];
        var data = new FormData(form);
        $.ajax({
            url: 'users/register',
            data: data,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                if(response['status'] == 'success') {
                    window.location.reload();
                }
                },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors.errors, function (key, value) {
                    $('#'+key+'Error').show();
                    $('#'+key+'Error').html(value);
                });
            }
        });
    });

    $(document).on('click','#addCategoryButton',function () {
        $('#exampleModalLongInner').modal();
        clear();
        hideError();
    });
    $(document).on('click','#cancel',function(){
        clear();
        hideError();
    });
    $(document).on('click','#closeModal',function(){
        clear();
        hideError();
    });

});

// hide error
function hideError() {
    $('#addressError').hide();
    $('#interestError').hide();
    $('#passwordError').hide();
    $('#mobile_noError').hide();
    $('#emailError').hide();
    $('#enrollmentError').hide();
    $('#l_nameError').hide();
    $('#f_nameError').hide();
    $('#collegeError').hide();
    $('#openingsError').hide();
    $('#streamError').hide();
    $('#yearError').hide();
    $('#universityError').hide();
    $('#future_studiesError').hide();
    $('.save').attr('id','adduser').attr('onclick','').html('Add');
}

// clear form value
function clear() {
    $('#college option').remove();
    $("#college").append('<option value="">Select</option>');
    $('#cvFile').val('');
    $('#f_name').val('');
    $('#l_name').val('');
    $('#enrollment').val('');
    $('#email').val('');
    $('#mobile_no').val('');
    $('#interest').val('');
    $('#address').val('');
    $('#college').val($('#college option:first').val());
    $('#openings').val($('#openings option:first').val());
    $('#password').val('').show();
    $('#stream').val($('#stream option:first').val());
    $('#year').val($('#year option:first').val());
    $('#university').val($('#university option:first').val());
    $('#future_studies').val($('#future_studies option:first').val());
    $('#exampleModalLongTitle').html('Add new user');
};

// update user data
function updateUser(id) {
    var form = $("#user_form")[0];
    var data = new FormData(form);
    $.ajax({
        url:'users/update-user/'+id,
        dataType:'json',
        type:'post',
        data:data,
        contentType: false,
        cache: false,
        processData: false,
        success:function () {
            window.location.reload();
        },
        error: function (xhr) {
            var errors = xhr.responseJSON;
            $.each(errors.errors, function (key, value) {
                $('#'+key+'Error').show();
                $('#'+key+'Error').html(value);
            });
        }
    })
};

// edit user data
 function editUser(id) {
    $.ajax({
        url:'users/edit-user/'+id,
        dataType:'json',
        type:'get',
        success:function (res) {
            $('#form-errors').children().remove();
            $('#exampleModalLongTitle').html('Update user details')
            $('#f_name').val(res.data.first_name);
            $('#l_name').val(res.data.last_name);
            $('#enrollment').val(res.data.enrollment_no);
            $('#email').val(res.data.email);
            $('#address').val(res.data.address);
            $('#interest').val(res.data.other_interest);
            var college_value = res.data.college_id;
            var openings_value = res.data.openings;
            var university_value = res.data.university_id;
            var stream_value = res.data.education_stream;
            var year_value = res.data.year;
            var study_value = res.data.future_studies;
            $(`#openings option[value='${openings_value}']`).prop('selected', true);
            $(`#stream option[value='${stream_value}']`).prop('selected', true);
            $(`#university option[value='${university_value}']`).prop('selected', true);
            $('#college option').remove();
            $.ajax({
                url: '/admin/users/ajaxCollege/'+university_value,
                type: 'GET',
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    $("#college").append('<option value="">Select</option>');
                    $.each(response.data, function (key, value) {
                        if(value.id == college_value) {
                            $("#college").append('<option value=' + value.id + ' selected>' + value.name + '</option>');
                        }else {
                            $("#college").append('<option value=' + value.id + '>' + value.name + '</option>');
                        }

                    });
                },
            });
            $(`#future_studies option[value='${study_value}']`).prop('selected', true);
            $(`#year option[value='${year_value}']`).prop('selected', true);
            $('#mobile_no').val(res.data.mobile_no);
            $('#roles').find('#'+res.data.roles[0].name).attr('selected','selected');
            $('.save').attr('id','updateuser').attr('onclick',`updateUser(`+res.data.id+`)`).html('Update');
            // $(`#college option[value='${college_value}']`).prop('selected', true);
        }
    })
}

// delete user data
 function deleteUser(id){
    $.ajax({
        url:"users/delete-user",
        dataType:"json",
        type:"post",
        data:{"id": id},
        success:function(res){

            window.location.reload();
        }
    })
}

// click on delete button show confrimation modal
$(document).on('click', '.delete-button-action', function (e) {
    $('#myModal').modal();
    $('#delete_btn').attr('onclick',`deleteUser(`+$(this).data('id')+`)`);
});
