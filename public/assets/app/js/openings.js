$(document).ready(function () {
    // change the opening status
    $(document).on('click', '.eligibalClass', function() {
        var id = $(this).attr('id');
        var status;
        if($(this).hasClass('checked')) {
            $(this).removeClass('checked');
            $(this).addClass('unchecked');
            status = 0;
        } else {
            $(this).removeClass('unchecked');
            $(this).addClass('checked');
            status = 1;
        }
        $.ajax({
            url:'/admin/openings/status',
            type: 'POST',
            data:{ id:id, status :status },
            success: function (response) {

            },
        });
    });

    $('#name').on('keyup',function () {
        if($('#name').val() != '') {
            $('#openingsError').hide();
        } else {
            $('#openingsError').show();
        }
    });
    $('#examTime').on('keyup', function() {
        var value = $('#examTime').val();
        if(value != '') {
            $('#examError').hide();
        } else {
            $('#examError').show();
        }

    });

    // create Opening
    $(document).on('click', '#addButton',function () {
        var form = $("#create_form")[0];
        var data = new FormData(form);
        $.ajax({
            url: 'openings/save',
            data: data,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                if(response['status'] == 'success') {
                    window.location.reload();
                }
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors.errors, function (key, value) {
                    if(key == 'name') {
                        $('#openingsError').show();
                        $('#openingsError').html(value);
                    }  else if(key == 'examTime') {
                        $('#examError').show();
                        $('#examError').html(value);
                    }
                });

            }
        });
    });

    // show opening form in popup modal
    $(document).on('click','#addCollegeButton',function () {
        clear();
        $('.save').attr('id','addButton').attr('onclick','').html('Add');
    });
    $(document).on('click','#cancel',function(){
        clear();
    });
});

    // clear data value from form and hide error
    function clear() {
        $('.questionClass').hide();
        $('#openingsError').hide();
        $('#examError').hide();
        $('#easyError').hide();
        $('#mediumError').hide();
        $('#hardError').hide();
        $('#name').val('');
        $('#form-errors').html('');
        $('#status').val($('#status option:first').val());
        $('#examTime').val('');
        $('#exampleModalLongTitle').html('Add New Opening')
    }

    // edit opening data
    function editData(id) {
        clear();
        $('.questionClass').show();
        $.ajax({
            url: 'openings/edit/' + id,
            dataType: 'json',
            type: 'get',
            success: function (response) {
                $('#exampleModalLongTitle').html('Update Opening Name');
                $('#name').val(response.data.name);
                $('#examTime').val(response.data.examTime);
                status_value = response.data.status;
                $(`#status option[value='${status_value}']`).prop('selected', true);
                $('.save').attr('id', 'updateData').attr('onclick', `updateData(` + response.data.id + `)`).html('Update');
            }
        })
        $.ajax({
            url:'openings/questions/'+id,
            dataType: 'json',
            type: 'get',
            success: function(response) {
                $('#opening_id').val(response.data.id);
                $('#total').text(response.data.total);
                $('#easyTotal').text(response.data.easy);
                $('#mediumTotal').text(response.data.medium);
                $('#hardTotal').text(response.data.hard);
                $('#easy').val(response.data.easy);
                $('#medium').val(response.data.medium);
                $('#hard').val(response.data.hard);
            }
        });
    }

    // delete opening data
    function deleteData(id) {
        $.ajax({
            url: 'openings/delete',
            dataType: 'json',
            type: 'post',
            data: {'id': id},
            success: function (res) {
                window.location.reload();
            }
        })
    }

    // click on delete button and show confirmation popup modal
    $(document).on('click', '.delete-button-action', function (e) {
        $('#myModal').modal();
        $('#delete_btn').attr('onclick',`deleteData(`+$(this).data('id')+`)`);
    });

    // update opening data
    function updateData(id) {
        var form = $("#create_form")[0];
        var data = new FormData(form);
        $.ajax({
            url:'openings/update/'+id,
            dataType:'json',
            type:'post',
            data:data,
            contentType: false,
            cache: false,
            processData: false,
            success:function () {
                window.location.reload();
            },
            error: function (xhr) {
                $('#easyError').hide();
                $('#mediumError').hide();
                $('#hardError').hide();
                var errors = xhr.responseJSON;
                $.each(errors.errors, function (key, value) {
                    console.log(key);
                    if(key == 'name') {
                        $('#openingsError').show();
                        $('#openingsError').html(value);
                    } else if(key == 'examTime') {
                        $('#examError').show();
                        $('#examError').html(value);
                    } else if (key == 'easy') {
                        $('#easyError').show();
                        $('#easyError').html(value);
                    }  else if (key == 'medium') {
                        $('#mediumError').show();
                        $('#mediumError').html(value);
                    }  else if (key == 'hard') {
                        $('#hardError').show();
                        $('#hardError').html(value);
                    }
                });
            }
        })
    }


