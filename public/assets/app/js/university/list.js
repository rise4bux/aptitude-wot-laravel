$(document).ready(function () {
    // change university status (Active/Inactive)
    $(document).on('click', '.eligibalClass', function() {
        var id = $(this).attr('id');
        var status;
        if($(this).hasClass('checked')) {
            $(this).removeClass('checked');
            $(this).addClass('unchecked');
            status = '0';
        } else {
            $(this).removeClass('unchecked');
            $(this).addClass('checked');
            status = '1';
        }
        $.ajax({
            url:'/admin/university/status',
            type: 'POST',
            data:{ id:id, status :status },
            success: function (response) {

            },
        });
    });

    // show/hide error
    $('#name').on('keyup',function () {
        $('#nameError').hide();
    });

    // create university
    $(document).on('click', '#addUni', function () {
        var form = $("#create_form")[0];
        var data = new FormData(form);
        $.ajax({
            url: 'university/store',
            data: data,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                window.location.reload();
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors.errors, function (key, value) {
                    if(key == 'name') {
                        $('#nameError').show();
                        $('#nameError').html(value);
                    }
                });
            }
        });
    });

    // show create university popup modal and reset form value and hide error message
    $(document).on('click','#addButton',function () {
        clear();
        $('#nameError').hide();
        $('.save').attr('id','addUni').attr('onclick','').html('Add');
    });
    $(document).on('click','#cancel',function(){
        $('#nameError').hide();
        clear();
    });
});

// clear form value
function clear() {
    $('#name').val('');
    $('#status').val($('#status option:first').val())
    $('#exampleModalLongTitle').html('Add New University');
    $('#namError').hide();
}

// update university data
function updateUniversity(id) {
    var form = $("#create_form")[0];
    var data = new FormData(form);
    $.ajax({
        url:'university/update-university/'+id,
        dataType:'json',
        type:'post',
        data:data,
        contentType: false,
        cache: false,
        processData: false,
        success:function () {
            window.location.reload();
        },
        error: function (xhr) {
            var errors = xhr.responseJSON;
            $.each(errors.errors, function (key, value) {
                if(key == 'name') {
                    $('#nameError').show();
                    $('#nameError').html(value);
                }
            });
        }

    })
}

// edit university data
function editUniversity(id) {
    clear();
    $.ajax({
        url: 'university/edit-university/' + id,
        dataType: 'json',
        type: 'get',
        success: function (response) {
            $('#exampleModalLongTitle').html('Update University');
            $('#name').val(response.data.name);
            $('#status').val(response.data.status);
            $('.save').attr('id', 'updaUniversity').attr('onclick', `updateUniversity(` + response.data.id + `)`).html('Update');
        }
    })
}

// delete university data
function deleteUniversity(id) {
    $.ajax({
        url: "university/delete-university",
        dataType: "json",
        type: "post",
        data: {"id": id},
        success: function (res) {
            window.location.reload();
        }
    })
}

// click on delete button show confirmation popup modal
$(document).on('click', '.delete-button-action', function (e) {
    $('#myModal').modal();
    $('#delete_btn').attr('onclick',`deleteUniversity(`+$(this).data('id')+`)`);
});
