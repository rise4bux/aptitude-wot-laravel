$(document).ready(function () {
    // change question status (Active/Inactive)
    $(document).on('click', '.eligibalClass', function() {
        var id = $(this).attr('id');
        var status;
        if($(this).hasClass('checked')) {
            $(this).removeClass('checked');
            $(this).addClass('unchecked');
            status = 0;
        } else {
            $(this).removeClass('unchecked');
            $(this).addClass('checked');
            status = 1;
        }
        $.ajax({
            url:'/admin/questions/status',
            type: 'POST',
            data:{ id:id, status :status },
            success: function (response) {

            },
        });
    });
});

// click on delete button show confirmation modal
$(document).on('click', '.delete-button-action', function (e) {
    $('#myModal').modal();
    $('#delete_btn').attr('href','questions/delete-question/' + $(this).data('id'));
});
