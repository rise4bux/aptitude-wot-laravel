
$(document).ready(function () {
    // Change the college status (Active/Inactive)
    $(document).on('click', '.eligibalClass', function() {
        var id = $(this).attr('id');
        var status;
        if($(this).hasClass('checked')) {
            $(this).removeClass('checked');
            $(this).addClass('unchecked');
            status = '0';
        } else {
            $(this).removeClass('unchecked');
            $(this).addClass('checked');
            status = '1';
        }
        $.ajax({
            url:'/admin/college/status',
            type: 'POST',
            data:{ id:id, status :status },
            success: function (response) {

            },
        });
    });

    // hide/show error message
    $('#name').on('keyup',function () {
        $('#nameError').hide();
    });
    $('#city').on('keyup',function () {
        $('#cityError').hide();
    });
    $('#university_id').on('change', function() {
        if($('#university_id').val() == '') {
            $('#universityError').show();
        } else {
            $('#universityError').hide();
        }
    });

    // create college and if error then show the error
    $(document).on('click', '#addButton', function () {
        var form = $("#create_form")[0];
        var data = new FormData(form);
        $.ajax({
            url: 'college/store',
            data: data,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                if(response['status'] == 'success') {
                    window.location.reload();
                }
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors.errors, function (key, value) {
                    if(key == 'name') {
                        $('#nameError').show();
                        $('#nameError').html(value);
                    }
                    if(key == 'city') {
                        $('#cityError').show();
                        $('#cityError').html(value);
                    }
                    if(key == 'university_id') {
                        $('#univercityError').show();
                        $('#univercityError').html(value);
                    }
                });
            }
        });
    });
    // click on create college button hide and clear form error and data
    $(document).on('click','#addCollegeButton',function () {
        clear();
        hideError();
        $('.save').attr('id','addButton').attr('onclick','').html('Add');
    });
    // click on cancel college button hide and clear form error and data
    $(document).on('click','#cancel',function(){
        clear();
        hideError();
    });
});

    // reset form
    function clear() {
        $('#name').val('');
        $('#city').val('');
        $('#form-errors').html('');
        $('#status').val($('#status option:first').val());
        $('#university_id').val($('#university_id option:first').val());
        $('#exampleModalLongTitle').html('Add New College')
    }

    // Hide error
    function hideError() {
        $('#cityError').hide();
        $('#nameError').hide();
        $('#universityError').hide();
    }

    // edit college data
    function editData(id) {
        clear();
        $.ajax({
            url: 'college/edit/' + id,
            dataType: 'json',
            type: 'get',
            success: function (response) {
                $('#exampleModalLongTitle').html('Update College');
                $('#name').val(response.data.name);
                $('#city').val(response.data.city);
                $('#status').val(response.data.status);
                $('#university_id').val(response.data.university_id);
                $('.save').attr('id', 'updateData').attr('onclick', `updateData(` + response.data.id + `)`).html('Update');
            }
        })
    }

    // delete college
    function deleteData(id) {
        $.ajax({
            url: "college/delete",
            dataType: "json",
            type: "post",
            data: {"id": id},
            success: function (res) {
                window.location.reload();
            }
        })
    }

    // show confirmation modal for delete college
    $(document).on('click', '.delete-button-action', function (e) {
        $('#myModal').modal();
        $('#delete_btn').attr('onclick',`deleteData(`+$(this).data('id')+`)`);
    });

    // update college data
    function updateData(id) {
        var form = $('#create_form')[0];
        var data = new FormData(form);
        $.ajax({
            url:'college/update/'+id,
            dataType:'json',
            type:'post',
            data:data,
            contentType: false,
            cache: false,
            processData: false,
            success:function () {
                window.location.reload();
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors.errors, function (key, value) {
                    if(key == 'name') {
                        $('#nameError').show();
                        $('#nameError').html(value);
                    }
                    else if(key == 'city') {
                        $('#cityError').show();
                        $('#cityError').html(value);
                    }
                    else if(key == 'university_id') {
                        $('#univercityError').show();
                        $('#univercityError').html(value);
                    }
                });
            }

        })
    }

