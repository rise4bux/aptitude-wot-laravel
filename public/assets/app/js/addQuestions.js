$(document).ready(function () {
    // create new option
    $('#addOptionButton').click(function () {
        var clone = $('#optionGroup').clone();
        $(clone).attr('id', '');
        clone.find('#phaseCount').html(parseInt(count)+1);
        $(clone).find('#option').attr('id', 'option' + count).attr('name','options['+count+'][option]');
        $(clone).find('textarea').attr('name','options['+count+'][option]').addClass('kt-ckeditor-'+count)
        var current = count;
        $(clone).find('input[type=radio]').attr('name','options['+count+'][is_correct]')
        $(clone).show();
        $('#option-container').append(clone);
        count++;
        ClassicEditor
            .create( document.querySelector( '.kt-ckeditor-'+current ), {

                toolbar: {
                    items: [
                        'heading',
                        '|',
                        'bold',
                        'italic',
                        'link',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'indent',
                        'outdent',
                        '|',
                        'imageUpload',
                        'blockQuote',
                        'insertTable',
                        'mediaEmbed',
                        'undo',
                        'redo'
                    ]
                },
                language: 'en',
                image: {
                    toolbar: [
                        'imageTextAlternative',
                        'imageStyle:full',
                        'imageStyle:side'
                    ]
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells'
                    ]
                },
                licenseKey: '',

            } )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something gone wrong!' );
                console.error( 'Please, report the following error in the https://github.com/ckeditor/ckeditor5 with the build id and the error stack trace:' );
                console.warn( 'Build id: ia47t6hg9pi0-8o65j7c6blw0' );
                console.error( error );
            } );

    });

    // select radio button
    $(document).on('click','.radioSelect',function(){
        $('.radioSelect').val(0).prop('checked',false);
            $(this).val(1).prop('checked',true);
        });

    });

var KTCkeditor = function () {
    // Private functions
    var demos = function () {
        ClassicEditor
            .create( document.querySelector( '.titleeditor' ), {

                toolbar: {
                    items: [
                        'heading',
                        '|',
                        'bold',
                        'italic',
                        'link',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'indent',
                        'outdent',
                        '|',
                        'imageUpload',
                        'blockQuote',
                        'insertTable',
                        'mediaEmbed',
                        'undo',
                        'redo'
                    ]
                },
                language: 'en',
                image: {
                    toolbar: [
                        'imageTextAlternative',
                        'imageStyle:full',
                        'imageStyle:side'
                    ]
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells'
                    ]
                },
                licenseKey: '',

            } )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something gone wrong!' );
                console.error( 'Please, report the following error in the https://github.com/ckeditor/ckeditor5 with the build id and the error stack trace:' );
                console.warn( 'Build id: ia47t6hg9pi0-8o65j7c6blw0' );
                console.error( error );
            } );

    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();
var KTCAnswer = function () {
    // Private functions
    var demos = function () {
        ClassicEditor
            .create( document.querySelector( '.answerEditor' ), {

                toolbar: {
                    items: [
                        'heading',
                        '|',
                        'bold',
                        'italic',
                        'link',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'indent',
                        'outdent',
                        '|',
                        'imageUpload',
                        'blockQuote',
                        'insertTable',
                        'mediaEmbed',
                        'undo',
                        'redo'
                    ]
                },
                language: 'en',
                image: {
                    toolbar: [
                        'imageTextAlternative',
                        'imageStyle:full',
                        'imageStyle:side'
                    ]
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells'
                    ]
                },
                licenseKey: '',

            } )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( error => {
                console.error( 'Oops, something gone wrong!' );
                console.error( 'Please, report the following error in the https://github.com/ckeditor/ckeditor5 with the build id and the error stack trace:' );
                console.warn( 'Build id: ia47t6hg9pi0-8o65j7c6blw0' );
                console.error( error );
            } );

    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();
// Initialization
jQuery(document).ready(function() {
    KTCkeditor.init();
    KTCAnswer.init();
});
