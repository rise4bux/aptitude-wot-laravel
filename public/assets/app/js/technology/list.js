$(document).ready(function () {

    $('#nameError').hide();
    $('#name').on('keyup',function () {
        if($(this).val() != '') {
            $('#nameError').hide();
        } else {
            $('#nameError').show();
        }
    });

    // change the technology status (Active/Inactive)
    $(document).on('click', '.eligibalClass', function() {
        var id = $(this).attr('id');
        var status;
        if($(this).hasClass('checked')) {
            $(this).removeClass('checked');
            $(this).addClass('unchecked');
            status = '0';
        } else {
            $(this).removeClass('unchecked');
            $(this).addClass('checked');
            status = '1';
        }
        $.ajax({
            url:'/admin/technology/status',
            type: 'POST',
            data:{ id:id, status :status },
            success: function (response) {

            },
        });
    });

    // submit technology form
    $(document).on('click', '#addButton', function () {
        var form = $("#create_form")[0];
        var data = new FormData(form);
        $.ajax({
            url: 'technology/store',
            data: data,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                window.location.reload();
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors.errors, function (key, value) {
                    if(key == 'name') {
                        $('#nameError').show();
                        $('#nameError').html(value);
                    }
                });

            }
        });
     });

    // show create technology popup modal (clear value and hide error)
    $(document).on('click','#addCollegeButton',function () {
        clear();
        $('#nameError').hide();
        $('.save').attr('id','addButton').attr('onclick','').html('Add');
    });
    $(document).on('click','#cancel',function(){
        $('#nameError').hide();
        clear();
    });

});

// clear technology form data value
function clear() {
    $('#name').val('');
    $('#status').val($('#status option:first').val());
    $('#exampleModalLongTitle').html('Add New Technology')
}

// edit technology data
function editData(id) {
    $.ajax({
        url: 'technology/edit/'+id,
        dataType: 'json',
        type: 'get',
        success: function (response) {
            $('#exampleModalLongTitle').html('Update Technology');
            $('#name').val(response.data.name);
            $('#city').val(response.data.city);
            $('#status').val(response.data.status);
            $('#university_id').val(response.data.university_id);
            $('.save').attr('id', 'updateData').attr('onclick', `updateData(` + response.data.id + `)`).html('Update');
        }
    })
}

// delete technology data
function deleteData(id) {
    $.ajax({
        url: "technology/delete",
        dataType: "json",
        type: "post",
        data: {"id": id},
        success: function (res) {
            if(res['status'] == 'deleted') {
                window.location.reload();
            }
        }
    })
}

// click on delete button show confirmation popup modal
$(document).on('click', '.delete-button-action', function (e) {
    $('#myModal').modal();
    $('#delete_btn').attr('onclick',`deleteData(`+$(this).data('id')+`)`);
});

//update technology data
function updateData(id) {
    var form = $('#create_form')[0];
    var data = new FormData(form);
    $.ajax({
        url:'technology/update/'+id,
        dataType:'json',
        type:'post',
        data:data,
        contentType: false,
        cache: false,
        processData: false,
        success:function () {
            window.location.reload();
        },
        error: function (xhr) {
            var errors = xhr.responseJSON;
            $.each(errors.errors, function (key, value) {
                if(key == 'name') {
                    $('#nameError').show();
                    $('#nameError').html(value);
                }
            });

        }

    })
};
