$(document).ready(function () {
    user.getData();
    $("#exam_date").datepicker().datepicker("setDate", new Date());;
    $("#m_select2_3").select2({placeholder:"Select College"});

    $(document).on('click', '.markDetail', function (e) {

        $.ajax({
            url: AjaxUrl + '/admin/exams/participants/users-marks',
            type: 'POST',
            data:{ exam_id:examID,user_id:$(this).data('id')},
            success: function (response) {
                /*console.log(response);*/
                $(".categoryResult").html(response);
                $('#examModal').modal();
            },
        });
    });
    $(document).on('click', '.exportButton', function (e) {
        $("#filterForm").submit();


    });
    $(document).on('click', '#m_reset', function (e) {
        /* $("#filterForm").resetForm();
         var college_id = $(".collegeSelect").val();
         var exam_date = $("#exam_date").val();
         //user.assignTable.setDataSrc("college_id",college_id).draw();

         if(college_id !='') {
             user.assignTable.destroy();
             user.assignTable = user.bindDatatable(user.tableid,college_id,exam_date);
         }
         else {
             user.assignTable.destroy();
             user.assignTable = user.bindDatatable(user.tableid);
         }*/
    });

});

user = {
    id: -1,
    assignTable: {},
    tableid: "examsDatatable",

    getData: function () {
        user.assignTable = user.bindDatatable(user.tableid);
    },



    bindDatatable: function (datatableID) {


        var gridObj = $('#' + datatableID).DataTable({

            dom: "<'card-header d-flex flex-row'<'data-table-title-responsive'><'search-paper pmd-textfield ml-auto'f>>" +
                    "<'row'<'col-sm-12 datatableCustom'tr>>" +
                "<'card-footer' <'pmd-datatable-pagination clearfix' l i p>>",
            processing: true,
            serverSide: true,
            ajax: {
                "type": "POST",
                "url": AjaxUrl + '/admin/exams/participants/examusers-ajax/'+examID,
                "dataType": "JSON",
                "data":  function ( d ) {
                    d.college_id = $(".collegeSelect").val();
                    d.exam_date = $('#exam_date').val();
                    // d.custom = $('#myInput').val();
                    // etc
                }
                , function(res) {
                    // map your server's response to the DataTables format and pass it to
                    // DataTables' callback
                    callback({
                        data: res.data
                    });
                }
            },

            columns: [{
                data: "id"
            }, {
                data: "first_name"
            }, {
                data: "university"
            }, {
                data: "college"
            }, {
                data: "enrollment_no"
            }, {
                data: "email"
            }, {
                data: "total_questions"
            }, {
                data: "total_correct_answers"
            }, {
                data: "percentage"
            }, {
                data: "exam_date"
            }, {
                data: "status"
            }, {
                data: "Action"
            }],
            columnDefs: [{

                "orderable": false,
                "targets": 0,
                "render": function (full, data, type, row) {
                    console.log(type);
                    return row.row + 1;
                }

            },{
                orderable: false,
                targets: 1,
                "render": function (full, data, type, row) {
                    return type.users.first_name+' '+type.users.last_name;
                }
            },
                {
                    orderable: false,
                    targets: 2,
                    "render": function (full, data, type, row) {
                        return type.users.university.name;
                    }
                },{
                    orderable: false,
                    targets:3,
                    "render": function (full, data, type, row) {
                        return type.users.college.name;
                    }
                },
                {
                    orderable: false,
                    targets:4,
                    "render": function (full, data, type, row) {

                        return type.users.enrollment_no;
                    }
                },{
                    orderable: false,
                    targets:5,
                    "render": function (full, data, type, row) {

                        return type.users.email;
                    }
                },{
                    orderable: false,
                    targets:6,
                    "render": function (full, data, type, row) {

                        return type.total_questions;
                    }
                },{
                    orderable: false,
                    targets:7,
                    "render": function (full, data, type, row) {

                        return type.total_correct_answers;

                    }
                },{
                    orderable: false,
                    targets:8,
                    "render": function (full, data, type, row) {

                        if(type.percentage != '' && type.percentage != null) {
                            return type.percentage+'%';
                        }
                        else {
                            return '';
                        }


                    }
                },{
                    orderable: false,
                    targets:9,
                    "render": function (full, data, type, row) {

                       return type.exam_date;



                    }
                },{
                    orderable: false,
                    targets:10,
                    "render": function (full, data, type, row) {

                        if(type.status==='Completed'){
                            return `<span style="width: 128px;"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-success">`+ type.status + `</span></span>`
                        }
                        else{
                            return `<span style="width: 128px;"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-danger">`+ type.status + `</span></span>`
                        }
                    }
                },
                {
                    orderable: false,
                    targets: 11,
                    'render': function (data, type, full, meta) {
                        var buttonHTML = '';
                        buttonHTML += `<a href="javascript:void(0)"><button  data-id="`+full.user_id+`" class="btn btn-sm btn-clean btn-icon btn-icon-md markDetail" title='Marks Detail'><i class="la la-file-text"></i></button></a>`;
                        return buttonHTML;
                    }
                }

            ],
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                var oSettings = this.fnSettings();
                if(oSettings) {
                    //$("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);
                }

                return nRow;
            },
            infoCallback: function(settings, start, end, max, total, pre){

                return (!isNaN(total))
                    ? "Showing " + start + " to " + end + " of " + total + " entries"
                    + ((total !== max) ? " (filtered from " + max + " total entries)" : "")
                    : "Showing " + start + " to " + (start + this.api().data().length - 1) + " entries";
            },
            //For checkall..
            // select: {
            //     style: 'multi',
            //     selector: 'td:first-child'
            // },

            dom:
                "<'card-header d-flex flex-row'<'data-table-title-responsive'><'search-paper pmd-textfield ml-auto'f>>" +
                "<'row'<'col-sm-12 datatableCustom'tr>>" +
                "<'card-footer' <'pmd-datatable-pagination clearfix' l i p>>",

        });

       /* gridObj.on('draw', function () {

            console.log("in12");
            $("#" + datatableID + "_previous").find('.page-link').html('<i class="la la-angle-left" style="font-size: 0.9rem !important;"></i>');
            $("#" + datatableID + "_next").find('.page-link').html('<i class="la la-angle-right" style="font-size: 0.9rem !important;"></i>');
        });*/
        // Apply the filter


       /* $('#' + datatableID + ' tbody').unbind("click");
        $(".custom-select-action").html('<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">delete</i></button><button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">more_vert</i></button>');


        if (gridObj.data().any()) {
            $('#' + datatableID + "_filter").hide();
            $('#' + datatableID + "_info").hide();
            $('#' + datatableID + "_paginate").hide();

            $('#selectAction').hide();
        } else {
            $('#' + datatableID + "_filter").show();
            $('#' + datatableID + "_info").show();
            $('#' + datatableID + "_paginate").show();

            $('#selectAction').show();
        }

        $("div.dataTables_filter input").keyup(function (e) {
            if ($(this).val() != '') {
                $('.btnClearDataTableFilter').removeClass('hidden');
            } else {
                $('.btnClearDataTableFilter').addClass('hidden');
            }
        });
*/

        return gridObj;
    }

};
