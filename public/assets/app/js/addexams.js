
$(document).on('click', '#addPhaseButton', function () {
    console.log(12);
    var clone = $('#phaseGroup').clone();
    console.log(clone);
    $(clone).attr('id', count);
    $(clone).find('.category').attr('name', 'phases[' + count + '][category]');
    $(clone).find('.difficulty').attr('name', 'phases[' + count + '][difficulty]');
    $(clone).find('.count').attr('name', 'phases[' + count + '][count]').val('');
    $(clone).show();
    var button = $('#hiddenButton').clone();
    $(button).attr('id', '');
    $(button).show();
    $(clone).append(button);
    $('#phaseContainer').append(clone);
    count++;
});

$(document).on('click','.toggle-password', function() {
    var id = $(this).siblings('input').attr('id');
    if($('#'+id).attr('type') == 'password') {
        $('#'+id).attr('type','text');
    } else {
        $('#'+id).attr('type','password');
    }
});

$(document).on('click', '.phaseRemove', function () {
    var id = $(this).parent().parent().attr('id');
    $('#deletePhaseBtn').attr('name',id);
    $('#deletePhase').modal({
        backdrop: 'static',
        keyboard: true
    }).on('click', '#deletePhaseBtn', function(e) {
        var divId = $(this).attr('name');
        $('#phaseContainer').find('#'+divId).remove();
        $("#deletePhase").modal('hide');
    });
    // $(this).parent().parent().remove();
});

$(document).on('change', '.questionCategory', function () {
    console.log($(this).val());
    var select = $(this).parent().parent().find('.difficultySelect');
    $.ajax({
        url: AjaxUrl+ "/admin/exams/get-category-difficulty",
        /*dataType: "json",*/
        type: "post",
        data: {"catid": $(this).val()},
        success: function (res) {
            if(res) {
                select.empty().append(res);
            }
        }
    })
});
ClassicEditor
    .create( document.querySelector( '.instructionEditor' ), {

        toolbar: {
            items: [
                'heading',
                '|',
                'bold',
                'italic',
                'link',
                'bulletedList',
                'numberedList',
                '|',
                'indent',
                'outdent',
                '|',
                'imageUpload',
                'blockQuote',
                'insertTable',
                'mediaEmbed',
                'undo',
                'redo'
            ]
        },
        language: 'en',
        image: {
            toolbar: [
                'imageTextAlternative',
                'imageStyle:full',
                'imageStyle:side'
            ]
        },
        table: {
            contentToolbar: [
                'tableColumn',
                'tableRow',
                'mergeTableCells'
            ]
        },
        licenseKey: '',

    } )
    .then( editor => {
        window.editor = editor;
    } )
    .catch( error => {
        console.error( 'Oops, something gone wrong!' );
        console.error( 'Please, report the following error in the https://github.com/ckeditor/ckeditor5 with the build id and the error stack trace:' );
        console.warn( 'Build id: ia47t6hg9pi0-8o65j7c6blw0' );
        console.error( error );
    } );
/*
ClassicEditor
    .create( document.querySelector( '.instructionEditor' ) ,{
        toolbar: {
            items: [
                'heading',
                '|',
                'alignment',                                                 // <--- ADDED
                'bold',
                'italic',
                'link',
                'bulletedList',
                'numberedList',
                'imageUpload',
                'blockQuote',
                'undo',
                'redo'
            ]
        },
        image: {
            toolbar: [
                'imageStyle:full',
                'imageStyle:side',
                '|',
                'imageTextAlternative'
            ]
        },
        plugins: [ "Base64UploadAdapter"],
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
            ]
        }
    })
    .then( editor => {
        console.log( editor );
    } )
    .catch( error => {
        console.error( error );
    } );
*/




