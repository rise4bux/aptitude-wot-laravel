$(document).ready(function () {
    // change category status (Active/Inactive)
    $(document).on('click', '.eligibalClass', function() {
        var id = $(this).attr('id');
        var status;
        if($(this).hasClass('checked')) {
            $(this).removeClass('checked');
            $(this).addClass('unchecked');
            status = 0;
        } else {
            $(this).removeClass('unchecked');
            $(this).addClass('checked');
            status = 1;
        }
        $.ajax({
            url:'/admin/category/status',
            type: 'POST',
            data:{ id:id, status :status },
            success: function (response) {

            },
        });
    });
    // show/hide error
    $('#name').on('keyup',function () {
    if($('#name').val() != '') {
        $('#nameError').hide();
    } else {
        $('#nameError').show();
    }
});
    $('#description').on('keyup', function() {
    if($('#description').val() != '') {
        $('#desError').hide();
    } else {
        $('#desError').show();
    }
});

    // add new category
    $(document).on('click', '#addCategory', function () {
        var form = $("#category_form")[0];
        var data = new FormData(form);
        $.ajax({
            url: 'category/create',
            data: data,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                window.location.reload();
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors.errors, function (key, value) {
                    if(key == 'name') {
                        $('#nameError').show();
                        $('#nameError').html(value);
                    }
                    if(key == 'description') {
                        $('#desError').show();
                        $('#desError').html(value);
                    }
                });
            }
        });
    });

    // clear field value and hide error message
    $(document).on('click','#addCategoryButton',function () {
        clear();
        $('#nameError').hide();
        $('#desError').hide();
        $('.save').attr('id','addCategory').attr('onclick','').html('Add');
    });
    $(document).on('click','#cancel',function(){
        clear();
        $('#nameError').hide();
        $('#desError').hide();
    });

});

// clear form field value
function clear() {
    $('#name').val('');
    $('#description').val('');
    $('#status').val($('#status option:first').val())
    $('#exampleModalLongTitle').html('Add New Category')
    $('#namError').hide();
}

// update category data
function updateCategory(id) {
    var form = $("#category_form")[0];
    var data = new FormData(form);
    $.ajax({
        url:'category/updateCategory/'+id,
        dataType:'json',
        type:'post',
        data:data,
        contentType: false,
        cache: false,
        processData: false,
        success:function () {
            window.location.reload();
        },
        error: function (xhr) {
            var errors = xhr.responseJSON;
            $.each(errors.errors, function (key, value) {
                if(key == 'name') {
                    $('#nameError').show();
                    $('#nameError').html(value);
                }
                if(key == 'description') {
                    $('#desError').show();
                    $('#desError').html(value);
                }
            });
        }
    })
}

// edit category data
function editCategory(id) {
    $.ajax({
        url: 'category/editCategory/' + id,
        dataType: 'json',
        type: 'get',
        success: function (response) {
            $('#exampleModalLongTitle').html('Update Category')
            $('#name').val(response.data.name);
            $('#description').val(response.data.description);
            $('#status').val(response.data.status);
            $('.save').attr('id', 'updatCategory').attr('onclick', `updateCategory(` + response.data.id + `)`).html('Update');
        }
    })
}

// delete category data
function deleteCategory(id) {
    $.ajax({
        url: 'category/deleteCategory',
        dataType: 'json',
        type: 'post',
        data: {'id': id},
        success: function (res) {
            window.location.reload();
        }
    })
}

// click on delete button show confirmation modal
$(document).on('click', '.delete-button-action', function (e) {
    $('#myModal').modal();
    $('#delete_btn').attr('onclick',`deleteCategory(`+$(this).data('id')+`)`);
});
