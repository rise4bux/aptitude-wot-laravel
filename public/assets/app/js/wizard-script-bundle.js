"use strict";

var KTWizard = function (t, e) {
    var a = this,
        n = KTUtil.get(t);
    KTUtil.get("body");
    if (n) {
        var o = {
            startStep: 1,
            clickableSteps: false
        },
            i = {
                construct: function (t) {
                    return KTUtil.data(n).has("wizard") ? a = KTUtil.data(n).get("wizard") : (i.init(t), i.build(), KTUtil.data(n).set("wizard", a)), a
                },
                init: function (t) {
                    a.element = n, a.events = [], a.options = KTUtil.deepExtend({}, o, t), a.steps = KTUtil.findAll(n, '[data-ktwizard-type="step"]'), a.btnSubmit = KTUtil.find(n, '[data-ktwizard-type="action-submit"]'), a.btnNext = KTUtil.find(n, '[data-ktwizard-type="action-next"]'), a.btnPrev = KTUtil.find(n, '[data-ktwizard-type="action-prev"]'), a.btnLast = KTUtil.find(n, '[data-ktwizard-type="action-last"]'), a.btnFirst = KTUtil.find(n, '[data-ktwizard-type="action-first"]'), a.events = [], a.currentStep = 1, a.stopped = !1, a.totalSteps = a.steps.length, a.options.startStep > 1 && i.goTo(a.options.startStep), i.updateUI()
                },
                build: function () {
                    KTUtil.addEvent(a.btnNext, "click", function (t) {
                        t.preventDefault(), i.goTo(i.getNextStep(), !0)
                    }), KTUtil.addEvent(a.btnPrev, "click", function (t) {
                        t.preventDefault(), i.goTo(i.getPrevStep(), !0)
                    }), KTUtil.addEvent(a.btnFirst, "click", function (t) {
                        t.preventDefault(), i.goTo(1, !0)
                    }), KTUtil.addEvent(a.btnLast, "click", function (t) {
                        t.preventDefault(), i.goTo(a.totalSteps, !0)
                    }), !0 === a.options.clickableSteps && KTUtil.on(n, '[data-ktwizard-type="step"]', "click", function () {
                        var t = Array.prototype.indexOf.call(a.steps, this) + 1;
                        t !== a.currentStep && i.goTo(t, !0)
                    })
                },
                goTo: function (t, e) {
                    if (!(t === a.currentStep || t > a.totalSteps || t < 0)) {
                        var n;
                        if (t = t ? parseInt(t) : i.getNextStep(), !0 === e && (n = t > a.currentStep ? i.eventTrigger("beforeNext") : i.eventTrigger("beforePrev")), !0 !== a.stopped) return !1 !== n && (!0 === e && i.eventTrigger("beforeChange"), a.currentStep = t, i.updateUI(), !0 === e && i.eventTrigger("change")), !0 === e ? t > a.startStep ? i.eventTrigger("afterNext") : i.eventTrigger("afterPrev") : a.stopped = !0, a;
                        a.stopped = !1
                    }
                },
                stop: function () {
                    a.stopped = !0
                },
                start: function () {
                    a.stopped = !1
                },
                isLastStep: function () {
                    return a.currentStep === a.totalSteps
                },
                isFirstStep: function () {
                    return 1 === a.currentStep
                },
                isBetweenStep: function () {
                    return !1 === i.isLastStep() && !1 === i.isFirstStep()
                },
                updateUI: function () {
                    var t = "",
                        e = a.currentStep - 1;
                    t = i.isLastStep() ? "last" : i.isFirstStep() ? "first" : "between", KTUtil.attr(a.element, "data-ktwizard-state", t);
                    var n = KTUtil.findAll(a.element, '[data-ktwizard-type="step"]');
                    if (n && n.length > 0)
                        for (var o = 0, l = n.length; o < l; o++) o == e ? KTUtil.attr(n[o], "data-ktwizard-state", "current") : o < e ? KTUtil.attr(n[o], "data-ktwizard-state", "done") : KTUtil.attr(n[o], "data-ktwizard-state", "pending");
                    var r = KTUtil.findAll(a.element, '[data-ktwizard-type="step-info"]');
                    if (r && r.length > 0)
                        for (o = 0, l = r.length; o < l; o++) o == e ? KTUtil.attr(r[o], "data-ktwizard-state", "current") : KTUtil.removeAttr(r[o], "data-ktwizard-state");
                    var s = KTUtil.findAll(a.element, '[data-ktwizard-type="step-content"]');
                    if (s && s.length > 0)
                        for (o = 0, l = s.length; o < l; o++) o == e ? KTUtil.attr(s[o], "data-ktwizard-state", "current") : KTUtil.removeAttr(s[o], "data-ktwizard-state")
                },
                getNextStep: function () {
                    return a.totalSteps >= a.currentStep + 1 ? a.currentStep + 1 : a.totalSteps
                },
                getPrevStep: function () {
                    return a.currentStep - 1 >= 1 ? a.currentStep - 1 : 1
                },
                eventTrigger: function (t, e) {
                    for (var n = 0; n < a.events.length; n++) {
                        var o = a.events[n];
                        if (o.name == t) {
                            if (1 != o.one) return o.handler.call(this, a);
                            if (0 == o.fired) return a.events[n].fired = !0, o.handler.call(this, a)
                        }
                    }
                },
                addEvent: function (t, e, n) {
                    return a.events.push({
                        name: t,
                        handler: e,
                        one: n,
                        fired: !1
                    }), a
                }
            };
        return a.setDefaults = function (t) {
            o = t
        }, a.goNext = function (t) {
            return i.goTo(i.getNextStep(), t)
        }, a.goPrev = function (t) {
            return i.goTo(i.getPrevStep(), t)
        }, a.goLast = function (t) {
            return i.goTo(a.totalSteps, t)
        }, a.goFirst = function (t) {
            return i.goTo(1, t)
        }, a.goTo = function (t, e) {
            return i.goTo(t, e)
        }, a.stop = function () {
            return i.stop()
        }, a.start = function () {
            return i.start()
        }, a.getStep = function () {
            return a.currentStep
        }, a.isLastStep = function () {
            return i.isLastStep()
        }, a.isFirstStep = function () {
            return i.isFirstStep()
        }, a.on = function (t, e) {
            return i.addEvent(t, e)
        }, a.one = function (t, e) {
            return i.addEvent(t, e, !0)
        }, i.construct.apply(a, [e]), a
    }
};
"undefined" != typeof module && void 0 !== module.exports && (module.exports = KTWizard)


