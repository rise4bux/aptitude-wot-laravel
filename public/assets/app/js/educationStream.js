
$(document).ready(function () {
    // change education stream status (Active/Inactive)
    $(document).on('click', '.eligibalClass', function() {
        var id = $(this).attr('id');
        var status;
        if($(this).hasClass('checked')) {
            $(this).removeClass('checked');
            $(this).addClass('unchecked');
            status = 0;
        } else {
            $(this).removeClass('unchecked');
            $(this).addClass('checked');
            status = 1;
        }
        $.ajax({
            url:'/admin/educationstream/status',
            type: 'POST',
            data:{ id:id, status :status },
            success: function (response) {

            },
        });
    });

    $('#name').on('keyup',function () {
        if($('#name').val() != '') {
            $('#streamError').hide();
        } else {
            $('#streamError').show();
        }
    });

    // create education stream
    $(document).on('click', '#addButton', function () {
        var form = $('#create_form')[0];
        var data = new FormData(form);
        $.ajax({
            url: 'educationstream/save',
            data: data,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                if(response['status'] == 'success') {
                    window.location.reload();
                }
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors.errors, function (key, value) {
                    if(key == 'name') {
                        $('#streamError').show();
                        $('#streamError').html(value);
                    }
                });
            }
        });
    });
    // show/hide error
    $(document).on('click','#addStreamButton',function () {
        clear();
        $('#streamError').hide();
        $('.save').attr('id','addButton').attr('onclick','').html('Add');
    });
    $(document).on('click','#cancel',function(){
        clear();
        $('#streamError').hide();
    });
});

    // clear form value
    function clear() {
        $('#name').val('');
        $('#status').val($('#status option:first').val());
        $('#exampleModalLongTitle').html('Add New Education stream')
    }

    // edit education stream data
    function editData(id) {
        clear();
        $.ajax({
            url: 'educationstream/edit/' + id,
            dataType: 'json',
            type: 'get',
            success: function (response) {
                $('#exampleModalLongTitle').html('Update Education stream');
                $('#name').val(response.data.name);
                status_value = response.data.status;
                $(`#status option[value='${status_value}']`).prop('selected', true);
                $('.save').attr('id', 'updateData').attr('onclick', `updateData(` + response.data.id + `)`).html('Update');
            }
        })
    }

    // delete education stream data
    function deleteData(id) {
        $.ajax({
            url: 'educationstream/delete',
            dataType: 'json',
            type: 'post',
            data: {'id': id},
            success: function (res) {
                window.location.reload();
            }
        })
    }

    // show delete confirmation popup modal
    $(document).on('click', '.delete-button-action', function (e) {
        $('#myModal').modal();
        $('#delete_btn').attr('onclick',`deleteData(`+$(this).data('id')+`)`);
    });

    // update education stream data
    function updateData(id) {
        var form = $('#create_form')[0];
        var data = new FormData(form);
        $.ajax({
            url:'educationstream/update/'+id,
            dataType:'json',
            type:'post',
            data:data,
            contentType: false,
            cache: false,
            processData: false,
            success:function () {
                window.location.reload();
            },
            error: function (xhr) {
                var errors = xhr.responseJSON;
                $.each(errors.errors, function (key, value) {
                    if(key == 'name') {
                        $('#streamError').show();
                        $('#streamError').html(value);
                    }
                });
            }
        })
    }


