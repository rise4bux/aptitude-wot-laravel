<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamPhase extends Model
{
    protected $guarded = [];

    public function exams()
    {
        return $this->belongsTo('App\Exam');
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }
}
