<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    protected  $guarded=[];
    use SoftDeletes;
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $model->link = createSlug($model->name);
        });
        static::updating(function ($model) {
            $model->link = createSlug($model->name,$model->id);
        });

    }
    public function getRouteKeyName()
    {
        return 'link';
    }

    public function phases()
    {
        return $this->hasMany('App\ExamPhase');
    }

    public function examUsers(){
        return $this->hasMany('App\ExamUser');
    }

}
