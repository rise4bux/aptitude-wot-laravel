<?php

namespace App\Imports;

use App\Category;
use App\Question;
use App\QuestionOption;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ExamImport implements ToCollection,WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    private $rows = 0;
    public function __construct()
    {

    }

    public function collection(Collection $rows)
    {
        if(!empty($rows)) {

            foreach ($rows as $row)
            {
                if(isset($row['category_id']) && !empty($row['category_id'])) {
                    $checkCategory = Category::find($row['category_id']);

                    if(!empty($checkCategory)) {

                        $insertQuestion = [];
                        $insertQuestion['category_id'] = $checkCategory->id;
                        $insertQuestion['title'] = $row['question'];
                        $insertQuestion['difficulty'] = ucwords($row['moderator_level']);
                        $insertQuestion['status'] = $row['status'];

                        $question = Question::create($insertQuestion);

                        if($question) {

                            ++$this->rows;
                            $correct_1 = ($row['answer'] == 1) ? 1 : 0;
                            $correct_2 = ($row['answer'] == 2) ? 1 : 0;
                            $correct_3 = ($row['answer'] == 3) ? 1 : 0;
                            $correct_4 = ($row['answer'] == 4) ? 1 : 0;
                            $option1 = $this->insertAnswer($question->id,$row['option1'],$correct_1);
                            $option2 = $this->insertAnswer($question->id,$row['option2'],$correct_2);
                            $option3 = $this->insertAnswer($question->id,$row['option3'],$correct_3);
                            $option4 = $this->insertAnswer($question->id,$row['option4'],$correct_4);


                        }

                    }
                }
                else {
                    return '';
                }

            }
        }
        else {
            return '';
        }
    }
    public function getRowCount(): int
    {
        return $this->rows;
    }
    public  function insertAnswer($question_id,$answer,$is_correct) {
        //Add Answers
        $insertAnswer['question_id'] = $question_id;
        $insertAnswer['content'] = $answer;
        $insertAnswer['is_correct'] = $is_correct;
        $insertAnswer['status'] = 1;
        $question_option = QuestionOption::create($insertAnswer);
        return $question_option;
    }
}
