<?php

namespace App\Repositories\university;

interface UniversityContract
{

    public function getAll();

    public function find($id);

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
