<?php

namespace App\Repositories\college;
use App\Models\College;
use App\Repositories\college\CollegeContract;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class CollegeRepository implements CollegeContract
{

    protected $model;

    public function __construct(College $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->with('university')->orderBy('id', 'desc')->latest()->get();
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function store($input)
    {
        $role = $this->model->create($input);
        return $role;
    }

    public function update($id, $input)
    {
        $role = $this->model->findOrFail($id);
        return $role->update($input);
    }

    public function delete($id)
    {
        $role = $this->model->findOrFail($id);
        $role->destroy($id);
    }

}
