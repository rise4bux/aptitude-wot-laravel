<?php

namespace App\Repositories\technology;

interface TechnologyContract
{

    public function getAll();

    public function find($id);

    public function store($input);

    public function update($id,$input);

    public function delete($id);

}
