<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class College extends Model
{
    use SoftDeletes;

    protected  $guarded=[];

    protected $fillable = ['name','city','university_id','status'];

    public function university() {
        return $this->belongsTo('App\Models\University','university_id');
    }
}
