<?php

namespace App\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

trait FailedValidationTrait
{
    public function failedValidation(Validator $validator)
    {
        $response = [];
        foreach ($validator->errors()->all() as $key => $value) {
            $response[] = $value;
        }
        $finalRes = [
            'status'  => 'error',
            'message' => $response,
            'code'    => 422
        ];
        throw new HttpResponseException(response()->json($finalRes, 422));
    }
}
