<?php

namespace App\Providers;

use App\Repositories\technology\TechnologyContract;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\university\UniversityContract::class,
            \App\Repositories\university\UniversityRepository::class
        );
        $this->app->bind(
            \App\Repositories\college\CollegeContract::class,
            \App\Repositories\college\CollegeRepository::class
        );
        $this->app->bind(
            \App\Repositories\technology\TechnologyContract::class,
            \App\Repositories\technology\TechnologyRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

    }
}
