<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected  $guarded=[];

    public function questions()
    {
        return $this->hasMany('App\Question','category_id')->where('status',1);
    }

    public function exams(){
        return $this->hasMany('App\ExamPhase');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($category) { // before delete() method call this
            $category->questions()->delete();
            // do the rest of the cleanup...
        });
    }
}
