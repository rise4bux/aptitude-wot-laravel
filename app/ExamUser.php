<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamUser extends Model
{
   protected $guarded=[];

    protected $casts = [
        'started_at' => 'datetime:H:i:s',
        'completed_at' => 'datetime:H:i:s',
    ];

   public function exam(){
       return $this->belongsTo('App\Exam');
   }

   public function questionAnswer(){
       return $this->hasMany('App\QuestionAnswer');
   }
   public function users(){
       return $this->belongsTo('App\User','user_id');
   }

   public function kpiData(){
       return $this->belongsTo(ExamUserKpi::class,'exam_user_id');
   }

   public function practical(){
       return $this->belongsTo(Practical::class,'practical_id');
   }
}
