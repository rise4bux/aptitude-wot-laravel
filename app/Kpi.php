<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\OpeningMasters;

class Kpi extends Model
{
    
    protected $fillable = [
    	'opening_id',
    ];


    protected $guarded = [];

    public function kpiValues()
    {
        return $this->hasMany(KpiValue::class, 'kpi_id');
    }

    public function opening() {
    	return $this->belongsTo(OpeningMasters::class, 'opening_id');
    }
}
