<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OpeningMasters extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'status','examTime','difficulty','easy', 'medium','hard',
    ];
}
