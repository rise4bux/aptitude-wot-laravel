<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExamUserKpi extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $casts = [
        'kpi_data' => 'array'
    ];

    public function setKpiDataAttribute($value)
    {
        $kpi_datas = [];
        foreach ($value as $array_item) {
            if (!is_null($array_item['name'])) {
                $kpi_datas[] = $array_item;
            }
        }
        $this->attributes['kpi_data'] = json_encode($kpi_datas);
    }
}
