<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Practical extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'exam_id',
        'user_id',
        'paper_type',
        'chosen_language',
        'started_at',
        'completed_at',
        'code_1',
        'result_1',
        'code_2',
        'result_2',
        'code_3',
        'result_3',
        'status',
        'comment_1',
        'comment_2',
        'comment_3',

        'result_4',
        'result_5',
        'result_6',
        'result_7',
        'comment_4',
        'comment_5',
        'comment_6',
        'comment_7',
        'video_status',
    ];

    protected $appends = [
        'code1_full_url',
        'code2_full_url',
        'code3_full_url',
        'result1_full_url',
        'result2_full_url',
        'result3_full_url',
        'result4_full_url',
        'result5_full_url',
        'result6_full_url',
        'result7_full_url',
    ];

    protected $casts = [
        'started_at' => 'datetime:H:i:s',
        'completed_at' => 'datetime:H:i:s',
    ];

    public function getCode1FullUrlAttribute()
    {
        return asset(Storage::url('practicals/'.$this->code_1));
    }

    public function getCode2FullUrlAttribute()
    {
        return asset(Storage::url('practicals/'.$this->code_2));
    }

    public function getCode3FullUrlAttribute()
    {
        return asset(Storage::url('practicals/'.$this->code_3));
    }

    public function getResult1FullUrlAttribute()
    {
        return asset(Storage::url('practicals/'.$this->result_1));
    }

    public function getResult2FullUrlAttribute()
    {
        return asset(Storage::url('practicals/'.$this->result_2));
    }

    public function getResult3FullUrlAttribute()
    {
        return asset(Storage::url('practicals/'.$this->result_3));
    }

    public function getResult4FullUrlAttribute()
    {
        return asset(Storage::url('practicals/'.$this->result_4));
    }

    public function getResult5FullUrlAttribute()
    {
        return asset(Storage::url('practicals/'.$this->result_5));
    }

    public function getResult6FullUrlAttribute()
    {
        return asset(Storage::url('practicals/'.$this->result_6));
    }

    public function getResult7FullUrlAttribute()
    {
        return asset(Storage::url('practicals/'.$this->result_7));
    }
}
