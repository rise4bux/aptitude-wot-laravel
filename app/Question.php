<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
//   protected  $guarded=[];

    protected $fillable = ['category_id', 'title', 'difficulty', 'status'];

    use SoftDeletes;

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function options(){
        return $this->hasMany('App\QuestionOption');
	}

}
