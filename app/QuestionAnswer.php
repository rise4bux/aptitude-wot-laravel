<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    protected $guarded=[];

    public function examUser(){
        return $this->belongsTo('App\ExamUser');
    }
    public function questions() {
        return $this->belongsTo('App\Question', 'question');
    }
}
