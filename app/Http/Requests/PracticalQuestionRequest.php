<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PracticalQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'answer' => 'required',
            'opaning' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please enter Defination ',
            'answer.required' => 'Please enter Predifind Answer ',
            'opaning.required' => 'Please select opening ',
        ];
    }
}
