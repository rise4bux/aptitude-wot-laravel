<?php

namespace App\Http\Requests;

use App\PracticalQuestion;
use Illuminate\Foundation\Http\FormRequest;

class OpeningRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $easy = PracticalQuestion::where('opening_id',$this->opening_id)->where('status',1)->where('difficulty','Easy')->count();
        $medium = PracticalQuestion::where('opening_id',$this->opening_id)->where('status',1)->where('difficulty','Medium')->count();
        $hard = PracticalQuestion::where('opening_id',$this->opening_id)->where('status',1)->where('difficulty','Hard')->count();

        if($easy >= $this->easy) {
            if($medium >= $this->medium) {
                if($hard >= $this->hard) {
                    return [
                        'name' => 'required',
                        'examTime' => 'required|numeric',
                    ];
                } else {
                    return [
                        'name' => 'required',
                        'examTime' => 'required|numeric',
                        'hard' => 'required|numeric|min:500',
                    ];
                }
            } else {
                return [
                    'name' => 'required',
                    'examTime' => 'required|numeric',
                    'medium' => 'required|numeric|min:500',
                ];
            }
        } else {
            return [
                'name' => 'required',
                'examTime' => 'required|numeric',
                'easy' => 'required|numeric|min:500',
            ];
        }
    }

    public function messages() {
        return [
            'name.required' => 'Please Select opening option ',
            'examTime.required' => 'Please enter valid practical exam time ',
            'easy.min' => 'Please enter valid number ',
            'medium.min' => 'Please enter valid number ',
            'hard.min' => 'Please enter valid number ',
        ];

    }
}
