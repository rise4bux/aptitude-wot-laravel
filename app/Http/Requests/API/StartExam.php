<?php

namespace App\Http\Requests\API;

use App\Traits\FailedValidationTrait;
use Illuminate\Foundation\Http\FormRequest;

class StartExam extends FormRequest
{
    use FailedValidationTrait;

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'paper_type' => 'required',
            'user_id' => 'required',
            'chosen_language' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'paper_type.required' => 'Paper type is required.!',
            'user_id.required' => 'User id is required.!',
            'chosen_language.required' => 'language is required.!',
        ];
    }
}
