<?php

namespace App\Http\Requests\API;

use App\Traits\FailedValidationTrait;
use Illuminate\Foundation\Http\FormRequest;

class CompleteExam extends FormRequest
{
    use FailedValidationTrait;

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'practical_id' => 'required',
            'code_1'       => 'required',
            'code_2'       => 'required',
            'code_3'       => 'required',
            'result_1'     => 'required',
            'result_2'     => 'required',
            'result_3'     => 'required',
        ];
    }

    public function messages()
    {
        return [
            'practical_id.required' => 'Practical id is required.!',
            'code_1.required'       => 'Code 1 is required.!',
//            'code_1.mimetypes'          => 'Code 1, supported extension is not supported.!',
            'code_2.required'       => 'Code 2 is required.!',
//            'code_2.mimetypes'          => 'Code 2, supported extension is not supported.!',
            'code_3.required'       => 'Code 3 is required.!',
//            'code_3.mimetypes'          => 'Code 4, supported extension is not supported.!',
            'result_1.required'     => 'Result 1 is required.!',
//            'result_1.mimetypes'        => 'Result 1, supported extension is not supported.!',
            'result_2.required'     => 'Result 2 is required.!',
//            'result_2.mimetypes'        => 'Result 2, supported extension is not supported.!',
            'result_3.required'     => 'Result 3 is required.!',
//            'result_3.mimetypes'        => 'Result 3, supported extension is not supported.!',
        ];
    }
}
