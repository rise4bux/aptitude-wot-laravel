<?php

namespace App\Http\Requests\API;

use App\Traits\FailedValidationTrait;
use Illuminate\Foundation\Http\FormRequest;

class VerifyEligibility extends FormRequest
{
    use FailedValidationTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'enrollment_number' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'enrollment_number.required' => 'Enrollment number is required.!',
        ];
    }
}
