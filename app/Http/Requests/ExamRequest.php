<?php

namespace App\Http\Requests;

use App\Question;
use Illuminate\Foundation\Http\FormRequest;

class ExamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        return [
            'name' => 'required',
            'description' => 'required',
            'instruction' => 'required',
            'status' => 'required',
            'exam_password' => 'required|min:8|max:15',
            'time' => 'required|numeric|min:1|not_in:0',
            'phases'=>'required|array|min:1'
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'Exam title is required',
            'description.required' => 'Exam description is required',
            'instruction.required' => 'Instructions are required',
            'status.required' => 'Please select exam status',
            'time.required' => 'Please add time duration',
            'time.min'=>'Time must be valid number'
        ];
    }
}
