<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       $options= $this->options;

        $main_validate = [
            'title'=>'required',
            'category'=>'required',
            'difficulty'=>'required',
            'status'=>'required',
            'options.*.option' => 'required',
            ];

        $correct=0;

       foreach ($options as $key=>$option) {
           $collection = new Collection([$option]);
           if ($collection->contains("is_correct", '1')) {
               $correct = $correct + 1;
           }

           //$main_validate['options['.$key.'][option]'] = 'required';

           //$validator[$key] = ;
       }
           if($correct<1){
                   $main_validate['options[is_correct]']= 'required';
                   return $main_validate;
           }
           else{
               return $main_validate;
           }
    }


    public function messages()
    {
        return [
          'title.required'=>'Question title is required',
          'category.required'=>'Category  is required',
          'difficulty.required'=>"Question's difficulty level is required" ,
          'status.required' =>'Status is required',
          'options[is_correct].required'=>'Atleast One answer must be seleted as correct',
          'options.*.option.required'=>'Answer Content is required',
        ];
    }
}
