<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KpiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value.*.name' => 'required',
            'value.*.weightage' => 'required',
            'openings' => 'required',
        ];
    }

    public function messages() {
        return [
            'value.required' => 'Please enter the name ',
            'value.required' => 'Please enter the weightage ',
            'openings.required' => 'Please select opening ',
        ];
    }
}
