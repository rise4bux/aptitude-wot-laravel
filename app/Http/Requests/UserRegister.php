<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'f_name'=>'required',
            'l_name'=>'required',
            'email' => ['required',  'email', 'max:255', 'unique:users'],
            'mobile_no'=>['required', 'numeric','digits:10'],
            'enrollment'=>['required', 'alpha_num', 'min:6',Rule::unique('users', 'enrollment_no')],
            'college' => 'required',
            'password' => ['required','min:8','max:30','regex:/^(?=(.*[a-z]){2})(?=(.*[A-Z]){2})(?=(.*\d){2})(?=(.*(_|[^\w]){2})).+$/','confirmed'],
            'openings' => 'required',
            'address' => 'required',
            'future_studies' => 'required',
            'university' => 'required',
            'year' => 'required',
            'interest' => 'required',
            'stream' => 'required',
            'cvFile' => 'mimes:pdf,doc,docx|max:5120',
        ];

    }
    public function messages()
    {
        return [
            'f_name.required'=>'Please enter your First name ',
            'l_name.required'=>'Please enter your last name ',
            'email.required'=>'Please enter your email ID ',
            'mobile_no.required'=>'Please enter your mobile number ',
            'mobile_no.numeric'=>'Only numeric value allowed',
            'enrollment.required'=>'Please enter your enrollment number ',
            'enrollment.unique'=>'Enrollment number already exists',
            'password.min'=>'Password must be 6 character long',
            'college.required' => 'Please select your college ',
            'openings.required' => 'Opening field is required ',
            'address.required' => 'Please enter your address ',
            'future_studies.required' => 'Please select Yes or No ',
            'university.required' => 'Please select your university ',
            'year.required' => 'Please select your passing year ',
            'interest.required' => 'Please write your interest here ',
            'stream.required' => 'Eduction stream is required ',
            'cvFile.max' => 'Maximum file size is 5MB ',
            'cvFile.mimes' => 'Please select valid file ',
            'password.required' => 'Please enter password ',
        ];
    }
}
