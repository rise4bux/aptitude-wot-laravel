<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\User;

class CandidateRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = User::where('enrollment_no',$this->enrollment)->first();
        $rules = [
            'f_name'=>'required',
            'l_name'=>'required',
            'university_id'=>'required',
            'college_id'=>'required',
            'openings'=>'required',
            'year'=>'required',
            'streams'=>'required',
            'other_interest'=>'required',
            'future_studies'=>'required',
            'email' => 'required|email',
            'mobile_no'=> 'required|numeric|digits:10',
            'enrollment'=> 'required|alpha_num|min:6',
            'address' => 'required',
        ];
        if($data == null) {
            $rules['cvFile'] = 'required|mimes:pdf,doc,docx|max:5120';
            return $rules;
        }
        else if(!$data->cv) {
            $rules['cvFile'] = 'required|mimes:pdf,doc,docx|max:5120';
            return $rules;
        }
        else {
            $rules['cvFile'] = 'mimes:pdf,doc,docx|max:5120';
            return $rules;
        }
    }
    public function messages()
    {
        return [
            'f_name.required'=>'First name is required',
            'l_name.required'=>'Last name is required',
            'university_id.required'=>'Please Select University ',
            'college_id.required'=>'Please Select College',
            'other_interest.required'=>'Other Interest is required',
            'future_studies.required'=>'Select Future Option',
            'mobile_no.required'=>'Contact number is required',
            'mobile_no.numeric'=>'Only numeric value allowed',
            'mobile_no.digits' => 'Please enter valid mobile number ',
            'enrollment.required'=>'Enrollment number is required ',
            'streams.required' => 'Please select Education stream ',
            'openings.required' => 'Please select Field ',
            'year.required' => 'Please select passing year ',
            'address.required' => 'Please enter your address ',
            'cvFile.required' => 'Please select file ',
            'cvFile.mimes' => 'Only .pdf, .doc and .docx file supported ',
            'cvFile.max' => 'Please upload file smaller than 5MB ',
            'email.email' => 'Please enter valid email address ',
            'email.required' => 'Please enter email address ',
            ];
    }
}
