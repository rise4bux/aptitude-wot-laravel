<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserupdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {
        return [
            'f_name'=>'required',
            'l_name'=>'required',
            'email'         => [
                'nullable',
                'email',
                Rule::unique('users', 'email')->ignore($this->id)
            ],
            'mobile_no'=>['required', 'numeric','digits:10'],
            'enrollment'=>['required', 'alpha_num', 'min:6',
                Rule::unique('users', 'enrollment_no')->ignore($this->id)],
            'password' => ['nullable','min:8','max:30','regex:/^(?=(.*[a-z]){2})(?=(.*[A-Z]){2})(?=(.*\d){2})(?=(.*(_|[^\w]){2})).+$/','confirmed'],
            'college' => 'required',
            'openings' => 'required',
            'address' => 'required',
            'future_studies' => 'required',
            'university' => 'required',
            'year' => 'required',
            'interest' => 'required',
            'stream' => 'required',
            'cvFile' => 'mimes:pdf,doc,docx|max:5120',

            ];

    }
    public function messages()
    {
        return [
            'f_name.required'=>'First name is required',
            'l_name.required'=>'Last name is required',
            'email.required'=>'Email is required',
            'mobile_no.required'=>'Contact number is required',
            'mobile_no.numeric'=>'Only numeric value allowed',
            'enrollment.required'=>'Enrollment number is required',
            'enrollment.unique'=>'Enrollment number already exists',
            'password.min'=>'Password must be 6 character long',

        ];
    }


}
