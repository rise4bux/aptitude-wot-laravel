<?php

namespace App\Http\Requests;

use App\Question;
use Illuminate\Foundation\Http\FormRequest;

class CollegeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        return [
            'name' => 'required',
            'city' => 'required',
            'university_id' => 'required',
            ];
    }


    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'city.required' => 'City is required',
            'university_id.required' => 'University is required',

        ];
    }
}
