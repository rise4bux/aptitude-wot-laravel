<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        if($this->password != '') {
            if($this->password == $this->c_password) {
                return [
                    'fname' => 'required',
                    'lname' => 'required',
                    'password' => ['min:8','max:30'],
                ];
            } else {
                return [
                    'fname' => 'required',
                    'lname' => 'required',
                    'password' => 'confirmed',
                ];
            }
            
        } else {
            return [
                'fname' => 'required',
                'lname' => 'required',
            ];
        }
    }

    public function messages() {
        return [
            'fname.required' => 'Please enter the first name ',
            'lname.required' => 'Please enter the last name ',
            'password.confirmed' => 'Please enter same password ',
        ];
    }
}
