<?php

namespace App\Http\Controllers;

use App\Category;
use App\Exam;

use App\ExamPhase;
use App\ExamUser;
use App\Http\Requests\CandidateRegister;
use App\Models\College;
use App\Models\Technology;
use App\Models\University;
use App\Question;
use App\QuestionAnswer;
use App\QuestionOption;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PharIo\Manifest\Email;
use App\EducationStreamMasters;
use App\OpeningMasters;

use Session;

class CandidateController extends Controller
{
    // show register view
    public function registerView($slug)
    {
        $exam = Exam::where('link', $slug)->where('status',1)->first();
        if(empty($exam)) {
            // return false;
            return redirect()->route('welcome');
        }
        $universities = University::where('status','1')->get();
        $technologies = Technology::where('status','1')->get();
        $streams = EducationStreamMasters::where('status','1')->get();
        $openings = OpeningMasters::where('status','1')->get();

        return view('frontend.register', compact('slug','universities','technologies','streams','openings'));
    }

    // check user (it is available for this exam or not)
    public function checkUser(Request $request) {
        $data= $request->only('envNo','exam');
        if(!empty($data['envNo']) && !empty($data['exam'])) {
            $user = User::where("enrollment_no",$data['envNo'])->first();

            if(!empty($user)) {
                $exam = Exam::where('link', $data['exam'])->first();
                $checkExistingExamUser =  ExamUser::where('exam_id',$exam->id)->where('status','Completed')->where('user_id',$user->id)->first();
                if(!empty($checkExistingExamUser)) {
                    return response()->json(['status' => 'error','message'=>'Your Exam is Completed'], 404);
                }
                else {
                    return response()->json(['status' => 'success','user'=>$user], 200);
                }

            }
        }
    }

    // return college data
    public function getColleges(Request $request) {
        $data= $request->only('uniId');
        if( !empty($data['uniId']) ) {
            $colleges = College::where('university_id',$data['uniId'])->where('status','1')->get();
            return response()->json(['status' => 'success','college'=>$colleges], 200);
        }
    }

    // register user
    public function register(CandidateRegister $request)
    {
        $data = $request->all();
        $data['role'] = 'user';
        $slug = $data['slug'];
        $user = User::where('enrollment_no', '=',  $data['enrollment'])->first();
        if(!empty($user)) {
            $user->find($user->id)->update([
                'first_name' => $data['f_name'],
                'last_name' => $data['l_name'],
                'mobile_no' => $data['mobile_no'],
                'email' => $data['email'],
                'university_id' => $data['university_id'],
                'college_id' => $data['college_id'],
                'future_studies' => $data['future_studies'],
                'openings' => $data['openings'],
                'year' => $data['year'],
                'education_stream' => $data['streams'],
                'other_interest' => $data['other_interest'],
                'address' => $data['address'],
            ]);
            if($request->hasFile('cvFile')) {
                $file = $request->file('cvFile');
                $new_name = rand().'.'.$file->getClientOriginalExtension();
                $file->move(public_path('cvFiles'), $new_name);
                $user->find($user->id)->update([
                    'cv' => $new_name,
                ]);
            }


            /*Update */
            $checkExisting = User::where('email', '=', $data['email'])->where('id','!=',$user->id)->first();

//            if(!empty($checkExisting)) {
//                session()->flash('error','Email already Exists');
//                return redirect()->back()->withInput();
//            }

            if(!empty($data['password'])) {
                $user->find($user->id)->update(['password' => bcrypt($data['password'])]);
            }
        }
        else {
            $file = $request->file('cvFile');
            $new_name = rand().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('cvFiles'), $new_name);
            $user = User::create([
                 'first_name' => $data['f_name'],
                 'last_name' => $data['l_name'],
                 'mobile_no' => $data['mobile_no'],
                 'enrollment_no' => $data['enrollment'],
                 'email' => $data['email'],
                 'university_id' => $data['university_id'],
                 'college_id' => $data['college_id'],
                 'future_studies' => $data['future_studies'],
                 'openings' => $data['openings'],
                 'year' => $data['year'],
                 'education_stream' => $data['streams'],
                 'other_interest' => $data['other_interest'],
                 'address' => $data['address'],
                 'cv' => $new_name,

            ]);
            $user->attachRole($data['role']);

//            $user = User::where('email', '=', $data['email'])->first();

//               /* if(!empty($data['password'])) {
//                    $user->find($user->id)->update(['password' => bcrypt($data['password'])]);
//                }*/
//                $user->attachRole($data['role']);
//            }
//            else {
//                session()->flash('error','Email already Exists');
//                return redirect()->back()->withInput();
//            }
        }
        $instruction = Exam::where('link', $slug)->select('instruction')->first();
        $id = $user->id;

        Auth::loginUsingId($id);
        session(['exam' =>$slug]);

        return redirect()->route('candidate.instruction');
        //return view('frontend.instruction', compact('slug', 'instruction', 'id'));

    }

    // show instruction
    public function instruction() {

        if(auth()->id()==null){
            return redirect()->route('welcome');
        }
        $slug = Session::get('exam');
        Session::forget('exam');
        if(empty($slug)) {
            return redirect()->route('welcome');
        }
        $exam = Exam::where('link', $slug)->first();

        if(empty($exam)) {
            return redirect()->route('welcome');
        }
        $instruction = $exam->instruction;
        $id = auth()->id();
        $examuser = ExamUser::where('exam_id',$exam->id)->where('user_id',$id)->first();

        $category_ids = ExamPhase::where('exam_id',$exam->id)->get()->pluck('category_id')->toArray();

        //$category_ids = array_column($categories,'category_id');
        $category_names = Category::whereIn('id',$category_ids)->get()->pluck('name')->toArray();

        $instruction_text = $instruction;
        $newInstrcution = str_replace('{{time}}',$exam->time,$instruction_text);
        $newInstrcution = str_replace('{{category_count}}',count($category_names),$newInstrcution);
        $newInstrcution = str_replace('{{category_name}}',implode(', ',$category_names),$newInstrcution);
        $instruction = $newInstrcution;

        return view('frontend.instruction', compact('slug', 'instruction', 'id','examuser'));

    }

    // start exam
    public function start($slug, $id)
    {

        try {
            $exam = Exam::with('phases')->where('link', $slug)->first();
            $examuser = ExamUser::where('exam_id',$exam->id)->where('user_id',$id)->first();


            if(empty($examuser)) {
                $total_question = 0;
                foreach ($exam->phases as $phase) {
                    $total_question = $total_question + $phase->question_count;
                    $questions = Question::where(['category_id' => $phase->category_id, 'difficulty' => $phase->difficulty, 'status' => 1])->get();
                    if($questions->count() > 0) {
                        $questions = $questions->random($phase->question_count);

                        foreach ($questions as $question) {
                            QuestionAnswer::create([
                                'exam_users_id' => $id,
                                'exam_id' => $exam->id,
                                'question' => $question->id,
                            ]);
                        }
                    }

                }
                ExamUser::create([
                    'exam_id' => $exam->id,
                    'user_id' => $id,
                    'started_at' => Carbon::now(),
                    'completed_at' => Carbon::now()->addMinute($exam->time),
                    'status' => 'OnGoing',
                    'remaining_time' => $exam->time,
                    'total_questions' => $total_question,
                ]);
            }

            return redirect()->route('candidate.start', ['slug' => $slug]);

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    // return question randomly
    public function getQuestions($slug)
    {
        $exam=Exam::where('link',$slug)->first();
        $exam_id = $exam->id;
        $questionAnswer = QuestionAnswer::where('exam_users_id', auth()->id())->where('exam_id',$exam_id)->get();

        $question = $questionAnswer->pluck('question');
        $categoryWiseQuestion = Question::with('category', 'options')->whereIn('id', $question)->get();

        $allQuestion = collect($categoryWiseQuestion)->groupBy('category_id');

        $finalData = [];
        foreach ($allQuestion as $signalQuestion) {
            $finalData[createSlug($signalQuestion[0]->category->name)] = $signalQuestion;
        }
        $selectedoption=collect($questionAnswer)->where('answer','!=',null)->pluck('answer');
//        dd($selectedoption);

        $options = QuestionOption::whereIn('question_id', $question)->get()->groupBy('question_id');

        $alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
        $count = ExamUser::where(['exam_id' => $exam_id, 'user_id' => auth()->id()])->value('remaining_time');

        return view('frontend.main-page', compact('finalData', 'options', 'alphabet', 'questionAnswer', 'count', 'exam_id','selectedoption','slug'));

    }

    // save question answer
    public function saveAnswer(Request $request)
    {
        try {
            $answer = $request->answer_id;
            if ($answer == 0) {
                $answer = null;
            }
            $remainingTime = $request->remaining;
            $exam_id = $request->exam;
            QuestionAnswer::where('question', $request->question_id)->where('exam_id',$exam_id)->where('exam_users_id',auth()->id())->update(['answer' => $answer]);
            ExamUser::where(['exam_id' => $exam_id, 'user_id' => auth()->id()])->update(['remaining_time' => $remainingTime]);

            $correctAnswer = 0;
            $incorrectAnswers = 0;
            $filledAnswers = QuestionAnswer::where('exam_users_id', auth()->id())->where('exam_id',$exam_id)->get();
            foreach ($filledAnswers as $answer) {
                $correct = QuestionOption::where(['question_id' => $answer->question, 'is_correct' => 1])->value('id');
                if ($correct == $answer->answer) {
                    $correctAnswer = $correctAnswer + 1;
                }
                elseif ($correct !== $answer->answer && $answer->answer !== null) {
                    $incorrectAnswers = $incorrectAnswers + 1;

                }
            }
            $attempted = $filledAnswers->where('answer', '!=', null)->count();
            $notAttempted = $filledAnswers->where('answer', '=', null)->count();

            $examUser = ExamUser::where(['exam_id' => $exam_id, 'user_id' => auth()->id()])->first();
            $totalQuestions = $examUser->total_questions;
            $percentage = ($correctAnswer / $totalQuestions) * 100;
            $examU = $examUser->update([
                'percentage' => $percentage,
                'total_attempted' => $attempted,
                'total_not_attempted' => $notAttempted,
                'total_incorrect' => $incorrectAnswers,
                'total_correct_answers' => $correctAnswer
            ]);

            return response()->json(['status' => 'success'], 200);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    // save time
    public function saveTimer(Request $request)
    {
        try {
            $exam = $request->exam;
            if ($exam == '') {
                return response()->json(['status' => 'error'], 404);
                //$answer = null;
            }
            $remainingTime = $request->remaining;
            $exam_id = $exam;
           // QuestionAnswer::where('question', $request->question_id)->where('exam_id',$exam_id)->where('exam_users_id',auth()->id())->update(['answer' => $answer]);
            ExamUser::where(['exam_id' => $exam_id, 'user_id' => auth()->id()])->update(['remaining_time' => $remainingTime]);
            if($request->closed) {
             $this->clearSession();
            }
            return response()->json(['status' => 'success'], 200);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    // clear session
    public function clearSession() {
        Auth::logout();
        Session::flush();
        Log::info('Session Clear');

    }

    // submit exam and save data
    public function submit($exam_id, $user_id, $slug)
    {
        if (auth()->id() == null) {
            return redirect()->route('welcome');
        }
        $correctAnswer = 0;
        $incorrectAnswers = 0;
        $filledAnswers = QuestionAnswer::where('exam_users_id', $user_id)->where('exam_id',$exam_id)->get();
        foreach ($filledAnswers as $answer) {
            $correct = QuestionOption::where(['question_id' => $answer->question, 'is_correct' => 1])->value('id');
            if ($correct == $answer->answer) {
                $correctAnswer = $correctAnswer + 1;
            }
            elseif ($correct !== $answer->answer && $answer->answer !== null) {
                $incorrectAnswers = $incorrectAnswers + 1;

            }
        }
        $attempted = $filledAnswers->where('answer', '!=', null)->count();
        $notAttempted = $filledAnswers->where('answer', '=', null)->count();
        $examUser = ExamUser::where(['exam_id' => $exam_id, 'user_id' => $user_id])->first();
        $totalQuestions = $examUser->total_questions;
        $percentage = ($correctAnswer / $totalQuestions) * 100;
        $examU = $examUser->update([
            'completed_at' => Carbon::now(),
            'percentage' => $percentage,
            'status' => 'Completed',
            'total_attempted' => $attempted,
            'total_not_attempted' => $notAttempted,
            'total_incorrect' => $incorrectAnswers,
            'total_correct_answers' => $correctAnswer]);
        if($examU) {
            $this->clearSession();
        }
        session(['exam_status' =>1]);
        return response()->json(['status' => 'success'], 200);
        //return view('frontend.thankyou');
    }

    public function submitAjax(Request $request)
    {
        try {
            if (auth()->id() == null) {
                return response()->json(['status' => 'error','message'=>'No User Found'], 404);
            }

            $user_id = auth()->id();
            $exam_id = $request->exam;
            $remaining = $request->remaining;
            $correctAnswer = 0;
            $incorrectAnswers = 0;
            $filledAnswers = QuestionAnswer::where('exam_users_id', $user_id)->where('exam_id',$exam_id)->get();
            foreach ($filledAnswers as $answer) {
                $correct = QuestionOption::where(['question_id' => $answer->question, 'is_correct' => 1])->value('id');
                if ($correct == $answer->answer) {
                    $correctAnswer = $correctAnswer + 1;
                }
                elseif ($correct !== $answer->answer && $answer->answer !== null) {
                    $incorrectAnswers = $incorrectAnswers + 1;

                }
            }
            $attempted = $filledAnswers->where('answer', '!=', null)->count();
            $notAttempted = $filledAnswers->where('answer', '=', null)->count();
            $examUser = ExamUser::where(['exam_id' => $exam_id, 'user_id' => $user_id])->first();
            $totalQuestions = $examUser->total_questions;
            $percentage = ($correctAnswer / $totalQuestions) * 100;
            $examU = $examUser->update([
                'completed_at' => Carbon::now(),
                'percentage' => $percentage,
                'status' => 'Completed',
                'total_attempted' => $attempted,
                'total_not_attempted' => $notAttempted,
                'total_incorrect' => $incorrectAnswers,
                'remaining_time'=>$remaining,
                'total_correct_answers' => $correctAnswer]);
            if($examU) {
                $this->clearSession();
            }
            return response()->json(['status' => 'success'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'error' => $exception->getMessage()], 400);
        }


        //return view('frontend.thankyou');
    }

    // show login view
    public function loginView($slug){

        $exam = Exam::where('link', $slug)->where('status',1)->first();

        if(empty($exam)) {
            session()->flash('error','Your Exam not found');
            return redirect()->route('welcome');
        }
            return view('frontend.login',compact('slug'));
    }

    // login user
    public function login(Request $request){

        $password=$request->password;
       $enrollment=$request->enrollment;

       $slug=$request->slug;
       $user= User::where('enrollment_no', '=', $enrollment)->first();

       if(empty($user)) {
           session()->flash('error','Enrollment Number not found');
           return redirect()->back()->withInput();
       }
        if ($user->enrollment_no == $enrollment) {

           /* echo $password.'<br>';
            echo env('EXAM_RESUME_PASSWORD');*/
            $exam = Exam::where('link', $slug)->where('status',1)->first();

            if(empty($exam)) {
                session()->flash('error','Your Exam not Found');
                return redirect()->back()->withInput();
            }
            /*if($password == env('EXAM_RESUME_PASSWORD')){*/
            if($password == $exam->exam_password){
                    $instruction=$exam->instruction;
                    $id = $user->id;
                    $auth= ExamUser::where(['user_id'=>$id,'exam_id'=>$exam->id])->first();
                    if(!empty($auth)) {
                        if($auth->status!=='OnGoing'){
                            session()->flash('error','Your exam has been Completed');
                            return redirect()->back()->withInput();
                        }
                    }
                    else {
                        session()->flash('error','Not register for this Exam');
                        return redirect()->back()->withInput();
                    }
                    $examuser = ExamUser::where('exam_id',$exam->id)->where('user_id',$id)->first();
                    Auth::loginUsingId($id);
                    return view('frontend.instruction', compact('slug', 'instruction', 'id','examuser'));

                }
                else{
                    session()->flash('error','Please check your password');
                    return redirect()->back()->withInput();
                }
            }
        else{
            session()->flash('error','Please enter correct enrollment number');
            return redirect()->back()->withInput();
        }

    }

    // show thank you page
    public function finish(){
        return view('frontend.thankyou');
    }
}
