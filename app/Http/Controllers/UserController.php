<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRegister;
use App\Http\Requests\UserupdateRequest;
use App\Models\University;
use App\EducationStreamMasters;
use App\OpeningMasters;
use App\User;
use App\Models\College;
use Illuminate\Http\Request;
use App\Http\Requests\AdminUpdateRequest;
use DataTables;
use Response;

class UserController extends Controller
{
    // create user
    public function registerUser(UserRegister $request)
    {
        $data = $request->all();
        {
//            $data['role']='user';
            $user=User::create([
                'first_name' => $data['f_name'],
                'last_name' => $data['l_name'],
                'mobile_no'=>$data['mobile_no'],
                'enrollment_no'=>$data['enrollment'],
                'email' => $data['email'],
                'college_id' => $data['college'],
                'address' => $data['address'],
                'university_id' => $data['university'],
                'openings' => $data['openings'],
                'year' => $data['year'],
                'future_studies' => $data['future_studies'],
                'other_interest' => $data['interest'],
                'education_stream' => $data['stream'],
            ]);

            if($data['password']!=null){
                $user->update([
                    'password'=>bcrypt($data['password']),
                ]);
            }
            if($request->hasFile('cvFile')) {
                $file = $request->file('cvFile');
                $new_name = rand().'.'.$file->getClientOriginalExtension();
                $file->move(public_path('cvFiles'), $new_name);
                $user->find($user->id)->update([
                    'cv' => $new_name,
                ]);
            }
            $user->attachRole($data['role']);
            session()->flash('success','User Created Successfully');
            return response()->json( ['status'=>'success','message'=>'User Created Successfully','data'=>$user, 'code'=>200],200
            );
        }
    }

    // show user with its detail
    public function index(Request $request) {
        $universities = University::all();
        $openings = OpeningMasters::all();
        $streams = EducationStreamMasters::all();

        if($request->ajax()) {
            $users =  User::with('college')->where('id', '!=', auth()->user()->id)->with('roles','college')->latest()->get();
            return Datatables::of($users)
                ->addIndexColumn()
                ->addColumn('college', function($users){
                    return $users->college->name;
                })
                ->addColumn('roll', function($users){
                    if($users->hasRole('admin')) {
                        return 'Admin';
                    } else {
                        return 'User';
                    }
                })
                ->addColumn('action', function($users) {
                    $button = '<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon" title="Edit details" onclick="editUser('.$users["id"].')" data-toggle="modal" data-target="#exampleModalLongInner"><i class="la la-edit" style="color:black;font-size: 23px;"></i></a>';
                    $button .= '<a href="javascript:void(0)" data-id="'.$users["id"].'" class="delete-button-action btn btn-sm btn-clean btn-icon delete" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                    if($users['cv'] != '') {
                        $button .= '<a href="users/download/'.$users["cv"].'" class="btn btn-sm btn-clean btn-icon" title="Download CV"><i class="fa fa-download" style="color: green"></i></a>';
                    }
                    return $button;
                })
                ->addColumn('status', function($users){
                    if($users['status'] == "0") {
                        return '<span class="switch switch-info"><label><input type="checkbox" name="select" id="'.$users["id"].'" class="eligibalClass unchecked"><span></span></label></span>';
                    } else {
                        return '<span class="switch switch-info"><label><input type="checkbox" checked name="select" id="'.$users["id"].'" class="eligibalClass checked"><span></span></label></span>';
                    }
                })
                ->rawColumns(['action','status'])
                ->make(true);
        }
        return view('Users_new',compact( 'openings','universities', 'streams'));
    }

    public function getUsers(){
        $users = User::where('id', '!=', auth()->user()->id)->with('roles','college')->get();
        return response()->json(['status'=>'success','message'=>'User data fetched','data'=>$users, 'code'=>200],200
        );
    }

    // return college data
    public function ajaxCollege($id) {
        $data = College::where('university_id',$id)->get();
        return response()->json(['status'=>'success','message'=>'User data fetched','data'=>$data, 'code'=>200],200);
    }

    // edit user data
    public  function editUser($id){
          $user=  User::where('id',$id)->with('roles')->first();
          return response()->json(['data'=>$user,'status'=>'success']);
    }

    // update user data
    public function updateUser(UserupdateRequest $request ,$id){
        $data=$request->all();
        $user=User::findOrFail($id);
        if($user->hasRole('admin')) {
            if($data['role'] != 'admin') {
                $user->detachRole('admin');
                $user->attachRole('user');
            }
        } else {
            if($data['role'] != 'user') {
                $user->detachRole('user');
                $user->attachRole('admin');
            }
        }
         $user->update([
                'first_name' => $data['f_name'],
                'last_name' => $data['l_name'],
                'mobile_no'=>$data['mobile_no'],
                'enrollment_no'=>$data['enrollment'],
                'college_id' => $data['college'],
                'openings' => $data['openings'],
                 'address' => $data['address'],
                 'university_id' => $data['university'],
                 'year' => $data['year'],
                 'future_studies' => $data['future_studies'],
                 'other_interest' => $data['interest'],
                 'education_stream' => $data['stream'],
                 'college.required' => 'Please select your college ',
                 'openings.required' => 'Opening field is required ',
                 'address.required' => 'Please enter your address ',
                 'future_studies.required' => 'Please select Yes or No ',
                 'university.required' => 'Please select your university ',
                 'year.required' => 'Please select your passing year ',
                 'interest.required' => 'Please write your interest here ',
                 'stream.required' => 'Eduction stream is required ',
                 'cvFile.max' => 'Maximum file size is 5MB ',
                 'cvFile.mimes' => 'Please select valid file ',
            ]);
            if($data['password']!=null){
                $user->update([
                    'password'=>bcrypt($data['password']),
                ]);
            }
        if($request->hasFile('cvFile')) {
            $file = $request->file('cvFile');
            $new_name = rand().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('cvFiles'), $new_name);
            $user->findOrFail($id)->update([
                'cv' => $new_name,
            ]);
        }

        session()->flash('success','User Updated Successfully');
        return response()->json( ['status'=>'success','message'=>'User Updated Successfully','data'=>$user, 'code'=>200],200
        );
    }

    // delete user data
    public function deleteUser(Request $request){
        $id=$request->id;
        User::where('id',$id)->delete();
        session()->flash('success','user deleted');
        return response()->json(['status'=>'deleted']);
    }

    // edit admin profile
    public function editProfile($id) {
        $data = User::find($id);
        return view('update_profile',compact('data','id'));
    }

    // update admin profile
    public function updateProfile($id,AdminUpdateRequest $request) {
        if($request->password != '') {
            User::find($id)->update(['first_name'=>$request->fname,'last_name'=>$request->lname, 'password' =>bcrypt($request->password)]);
        } else {
            User::find($id)->update(['first_name'=>$request->fname,'last_name'=>$request->lname]);
        }
        session()->flash('success','Admin profile updated Successfully');
        return redirect()->route('exams.index');
    }

    // download cv files
    public function downloadFile($file) {
        $downloadFile = public_path()."/cvFiles/".$file;
        $headers = array('Content-Type: application/pdf',);
        return Response::download($downloadFile, $file,$headers);
    }

}
