<?php

namespace App\Http\Controllers;
use App\Http\Requests\CollegeRequest;
use App\Models\University;
use App\Repositories\college\CollegeContract;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use App\Models\College;
use DataTables;


class CollegeController extends Controller
{
    use SoftDeletes;

    protected $repository;

    public function __construct(CollegeContract $repository)
    {
        $this->repository = $repository;
    }

    // show all college data in index page
    public function index(Request $request) {
        if($request->ajax()) {
            $colleges = College::with('university')->latest()->get();
            return Datatables::of($colleges)
                ->addIndexColumn()
                ->addColumn('university', function($colleges) {
                    return $colleges->university->name;
                })
                ->addColumn('action', function($colleges) {
                    $button = '<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon" onclick= "editData('.$colleges["id"].')" data-toggle="modal" data-target="#exampleModalLongInner" title="Edit details"><i class="la la-edit" style="color:black;font-size: 23px"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="javascript:void(0)" data-id="'.$colleges["id"].'" class="delete-button-action btn btn-sm btn-clean btn-icon delete" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                    return $button;
                })
                ->addColumn('status', function($colleges){
                    if($colleges['status'] == "0") {
                        return '<span class="switch switch-info"><label><input type="checkbox" name="select" id="'.$colleges["id"].'" class="eligibalClass unchecked"><span></span></label></span>';
                    } else {
                        return '<span class="switch switch-info"><label><input type="checkbox" checked name="select" id="'.$colleges["id"].'" class="eligibalClass checked"><span></span></label></span>';
                    }
                })
                ->rawColumns(['action','status'])
                ->make(true);
        }
        $universities = University::latest()->get();
        return view('college.index',compact('universities'));
    }

    // college list
    public function collegeLists(){
       $colleges =$this->repository->getAll();
       return response()->json(['status'=>'success','data'=>$colleges,'code'=>200],200);
   }

    // save college data
    public function store(CollegeRequest $request)
    {
        try{
            $data = $request->all();
            $college = $this->repository->store($data);
            session()->flash('success','College created Successfully');
            return response()->json(['status'=>'success'],200);
        }
        catch (\Exception $e) {
            Log::error('College management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // edit college data
    public  function edit($id){
        $college=  $this->repository->find($id);
        return response()->json(['data'=>$college,'status'=>'success']);
    }

    // update college data
    public function update(CollegeRequest $request ,$id){
        try{
            $data=$request->except('_token');
            $college=$this->repository->update($id,$data);
            session()->flash('success','College updated Successfully');
            return response()->json( ['status'=>'success','message'=>'College updated Successfully','data'=>$college, 'code'=>200],200);
        }
        catch (\Exception $e) {
            Log::error('College management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }

    }

    // delete college data
    public function delete(Request $request){
        $id=$request->id;
        try {
            $this->repository->delete($id);
            User::where('college_id',$id)->delete();
            session()->flash('success','College deleted Successfully');
            return response()->json(['status'=>'deleted']);
        } catch (\Exception $e) {
            Log::error('College management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // change college status
    public function statusChange(Request $request) {
        College::find($request->id)->update(['status'=>$request->status]);
        return response()->json(['status' => 'success'], 200);
    }

    // return college data data with university
    public function ajaxData(Request $request) {
        $colleges = College::with('university')->latest()->get();
        return response()->json(['status' => 'success',"draw"=>1, 'data' => $colleges], 200);
    }
}

