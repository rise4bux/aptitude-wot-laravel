<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\PracticalQuestionRequest;
use App\Models\College;
use App\OpeningMasters;
use App\PracticalQuestion;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use function Sodium\compare;
use DataTables;

class PracticalQuestionController extends Controller
{
    // show all practical question in index page
    public function index(Request $request)
    {
        Session::forget('opening');
        Session::forget('practicalDifficulty');
        $categories = OpeningMasters::where('status',1)->get();
        if($request->ajax()) {
            $status = '';
            $difficulty = '';
            $opening = '';
            if (isset($request->status)) {
                if ($request->status == 2) {
                    $status = "0";
                } else {
                    $status = "1";
                }
            }
            if (isset($request->difficulty)) {
                $difficulty = $request->difficulty;
            }
            // category means openings
            if (isset($request->category)) {
                $opening = $request->category;
            }
            $questions = PracticalQuestion::with('opening')->where(function ($query) use ($difficulty, $status, $opening) {
                if (isset($difficulty) && $difficulty != null) {
                    $query->where('difficulty', $difficulty);
                }
                if (isset($status) && $status != null) {
                    $query->where('status', $status);
                }
                if (isset($opening) && $opening != null && $opening != 'Select Opening') {
                    $query->where('opening_id', $opening);
                }
            })->latest()->get();
            return $this->ajaxData($questions);
        }
        return view('practical_question.index',compact('categories'));
    }

    // return data to index method
    public function ajaxData($questions) {
        return Datatables::of($questions)
            ->addIndexColumn()
            ->addColumn('defination', function($questions) {
                return strip_tags($questions->defination);
            })
            ->addColumn('opening', function($questions){
                return $questions->opening->name;
            })
            ->addColumn('action', function($questions) {
                $button = '<a href="practical-questions/edit-question/'.$questions["id"].'" class="btn btn-sm btn-clean btn-icon" title="Edit details"><i class="la la-edit" style="color:black;font-size: 23px"></i></a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<a href="javascript:void(0)" data-id="'.$questions["id"].'" class="delete-button-action btn btn-sm btn-clean btn-icon delete" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                return $button;
            })
            ->addColumn('status', function($questions){
                if($questions['status'] == "0") {
                    return '<span class="switch switch-info"><label><input type="checkbox" name="select" id="'.$questions["id"].'" class="eligibalClass unchecked"><span></span></label></span>';
                } else {
                    return '<span class="switch switch-info"><label><input type="checkbox" checked name="select" id="'.$questions["id"].'" class="eligibalClass checked"><span></span></label></span>';
                }
            })
            ->rawColumns(['action','status'])
            ->make(true);
    }

    // show create practical question form
    public function addQuestion()
    {
        $openings = OpeningMasters::latest()->get();
        return view('practical_question.add',compact('openings'));
    }

    // save new practical question data
    public function save(PracticalQuestionRequest $request)
    {
        try {
            PracticalQuestion::create([
                'defination' => $request->title,
                'predefined_answer' => $request->answer,
                'opening_id' => $request->opaning,
                'difficulty' => $request->difficulty,
                'status' => $request->status,
            ]);
            Session::forget('opening');
            Session::forget('practicalDifficulty');
            session()->flash('success','Practical question created Successfully');
            if(isset($_POST["save"])) {
                return redirect(route('practical.questions.index'));
            }
              if(isset($_POST["saveNew"])) {
                  session()->put('opening' ,$request->opaning);
                  session()->put('practicalDifficulty' ,$request->difficulty);
                  return redirect(route('practical.questions.add'));
              }
        } catch(\Exception $e){
            session()->flash('error','something went wrong');
            DB::rollBack();
            dd($e->getMessage());
        }
    }

    // edit practical question
    public function edit($id)
    {
        try {
            $question = PracticalQuestion::findOrFail($id);
            Session::forget('opening');
            Session::forget('practicalDifficulty');
            $openings = OpeningMasters::latest()->get();
            return view('practical_question.add', compact('openings','question'));
        } catch (\Exception $e) {
            session()->flash('error','This practical question is not available.');
            Log::error('Practical Question error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }

    }

    // update practical question data
    public function update(PracticalQuestionRequest $request, $id)
    {
        try {
            PracticalQuestion::find($id)->update([
                'defination' => $request->title,
                'predefined_answer' => $request->answer,
                'opening_id' => $request->opaning,
                'difficulty' => $request->difficulty,
                'status' => $request->status,
            ]);
            session()->flash('success','Practical question updated Successfully');
            return redirect(route('practical.questions.index'));

        } catch(\Exception $e){
            session()->flash('error','something went wrong');
            DB::rollBack();
            dd($e->getMessage());
        }
    }

    // delete practical question data
    public function delete(Request $request) {
        try {
            PracticalQuestion::findOrFail($request->id)->delete();
            session()->flash('success','Practical question deleted Successfully');
            return redirect(route('practical.questions.index'));
        }  catch (\Exception $e) {
            session()->flash('error','This practical question is not available.');
            Log::error('Practical Question error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // change practical question status
    public function statusChange(Request $request) {
        PracticalQuestion::find($request->id)->update(['status'=>$request->status]);
        return response()->json(['status' => 'success'], 200);
    }
}
