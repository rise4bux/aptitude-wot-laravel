<?php

namespace App\Http\Controllers;

use App\Kpi;
use App\OpeningMasters;
use Illuminate\Http\Request;
use App\Http\Requests\KpiRequest;
use App\KpiValue;
use DataTables;
use Illuminate\Support\Facades\Session;


class KpiController extends Controller
{
    // show all kpi in listing page
    public function index(Request $request)
    {
        Session::forget('KPIValue');
        if($request->ajax()) {
            $kpis = Kpi::with('opening')->latest()->get();
            return Datatables::of($kpis)
                ->addIndexColumn()
                ->addColumn('openings', function($kpis) {
                    return $kpis->opening->name;
                })
                ->addColumn('action', function($kpis) {
                    $button = '<a href="/admin/kpi/'.$kpis["id"].'/edit" class="btn btn-sm btn-clean btn-icon" title="Edit details"><i class="la la-edit" style="color:black;font-size: 23px"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="javascript:void(0)" data-id="'.$kpis["id"].'" class="btn btn-sm btn-clean btn-icon delete delete-button-action" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('kpi.list_kpi');
    }

    // open create kpi form
    public function add()
    {
        $openings = OpeningMasters::where('status', '1')->get();
        return view('kpi.add_kpi', compact('openings'));
    }

    // edit kpi data data and open form
    public function edit($id)
    {
        Session::forget('KPIValue');
        try {
            $openings = OpeningMasters::where('status', '1')->get();
            $kpi = Kpi::where('id',$id)->firstOrFail();
            $kpiValue = KpiValue::where('kpi_id',$kpi->id)->get();
            return view('kpi.add_kpi', compact('openings','kpiValue','kpi','id'));
        }  catch (\Exception $e) {
            Log::error('KPI management error:' . $e->getMessage());
            return response()->json(['status' => 'failure', 'message' => 'Something went wrong'], 404);
        }
    }

    // save kpi data
    public function save(KpiRequest $request)
    {
        Session::forget('KPIValue');
        try {
            $kpi = Kpi::create(['opening_id'=>$request->openings]);
            $values = $request->value;
            $kpi->kpiValues()->createMany($values);
            session()->flash('success','KPI value created Successfully');
            if(isset($_POST['save'])) {
                return redirect()->route('kpi.index');
            }
            if(isset($_POST['saveNew'])) {
                session()->put('KPIValue', $request->openings);
                return redirect(route('kpi.add'));
            }
        } catch (\Exception $e) {
            Log::error('KPI management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // update KPI value data
    public function update($id,KpiRequest $request)
    {
        try {
            $kpi = Kpi::with('kpiValues')->where('id',$id)->firstOrFail();
            $kpi->update(['opening_id'=>$request->openings]);
            $kpi->kpiValues()->delete();
            $values = $request->value;
            $kpi->kpiValues()->createMany($values);
            session()->flash('success','KPI value updated Successfully');
            return redirect()->route('kpi.index');
        } catch (\Exception $e) {
            Log::error('KPI management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // delete kpi data
    public function delete($id, Request $request)
    {
        $kpi = Kpi::where('id',$id)->firstOrFail();
        $kpi->kpiValues()->delete();
        $kpi->delete();
        session()->flash('success','KPI value deleted Successfully');
        return redirect()->route('kpi.index');
    }
}
