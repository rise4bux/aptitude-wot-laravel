<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function getInstruction($category='',$time='') {

        return '<ol><li>Total duration of examination is {{time}} minutes.</li>
                <li>The Countdown Clock will start as soon as you click on "Start Exam" button down under this page. The timer will appear in the top right corner. When timer reaches zero your examination will be over and will submit the answers you saved.</li>
                <li>Each question is classified as "Answered" , "Not Answered" , and "Not Visited". Status of each question can be found at the bottom of the right hand side bar.</li><li>Answered: The one you have answered and saved</li><li>Not Answered: The one you have not answered yet but saved.</li>
                <li>Not Visited: The one You haven\'t yet visited. Hence, not saved also.</li>
                <li>The Online Test is divided onto {{category_count}} sections, viz. {{category_name}}.  Try to attempt all the sections as no negative marking is implied.</li>
                 <li>Before moving to next Question, please make sure you hit the "Save &amp; Next" Button. 
                 The Answers which have been marked but not saved will not be considered.</li>
                 <li>You can navigate to different Sections from the "Sections" Tab provided at the Top Menu Bar.</li>
                 <li>You can navigate to any question of a particular section from the "Question Tab" provided in the right sided pane.</li>
                 <li>Strict actions will be taken against the candidate caught doing "Web-Search" through different Web-Browser or by any other means including phone, tablet, etc. will result into termination of the candidate from the selection process.</li>
                 <li>Any query during the examination will be handled by the supervisors. Internal Discussion is STRICTLY prohibited.</li>
                 </ol>';
    }
}

