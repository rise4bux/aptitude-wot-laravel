<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Question;
use DataTables;

class CategoryController extends Controller
{
    // show all category data with total niber of question
    public function index(Request $request){
        if($request->ajax()) {
            $categories = Category::latest()->get();
            return Datatables::of($categories)
                ->addIndexColumn()
                ->addColumn('total', function($categories) {
                        $questions = Question::where('category_id', $categories->id)->where('status', '1')->get();
                        $easy = 0; $medium = 0; $hard = 0;
                        foreach($questions as $question) {
                            if($question->difficulty == 'Easy') {
                                $easy = $easy + 1;
                            } else if($question->difficulty == 'Medium') {
                                $medium = $medium + 1;
                            } else if($question->difficulty == 'Hard') {
                                $hard = $hard + 1;
                            }
                        }
                        $data = $easy + $medium + $hard;
                        $data .= " (E-".$easy.", M-".$medium.", H-".$hard.")";
                    return $data;
                })
                ->addColumn('action', function($categories) {
                    $button = '<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon" title="Edit details" onclick= "editCategory('.$categories["id"].')" data-toggle="modal" data-target="#exampleModalLongInner"><i class="la la-edit" style="color:black;font-size: 23px"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="javascript:void(0)" data-id="'.$categories["id"].'" class="delete-button-action btn btn-sm btn-clean btn-icon delete" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                    return $button;                })
                ->addColumn('status', function($categories){
                    if($categories['status'] == "0") {
                        return '<span class="switch switch-info"><label><input type="checkbox" name="select" id="'.$categories["id"].'" class="eligibalClass unchecked"><span></span></label></span>';
                    } else {
                        return '<span class="switch switch-info"><label><input type="checkbox" checked name="select" id="'.$categories["id"].'" class="eligibalClass checked"><span></span></label></span>';
                    }
                })
                ->rawColumns(['action','status'])
                ->make(true);
        }
        return view('Categories');
    }

    // category list
    public function categoryList(){
        $category=Category::all();
        return response()->json(['status'=>'success','data'=>$category,'code'=>200],200);
    }

    // create new category
    public function create(CategoryRequest $request)
    {
        $data = $request->all();
        {
            $category=Category::create($data);
            session()->flash('success','Category created Successfully');
            return response()->json(['status'=>'success'],200);
        }
    }

    // edit category data
    public  function editCategory($id){
        $category=  Category::where('id',$id)->first();
        return response()->json(['data'=>$category,'status'=>'success']);
    }

    // update category data
    public function updateCategory(CategoryRequest $request ,$id){
        $data=$request->except('_token');
        $category =  Category::where('id',$id)->update($data);
        session()->flash('success','Category updated Successfully');
        return response()->json( ['status'=>'success','message'=>'Category updated Successfully','data'=>$category, 'code'=>200],200
        );
    }

    // delete category data
    public function deleteCategory(Request $request){
        $id=$request->id;
        Category::find($id)->delete();
        session()->flash('success','Category deleted Successfully');
        return response()->json(['status'=>'deleted']);
    }

    // return total active questions number for this category
    public function countQuestion($id) {
        $questions = Question::where('category_id',$id)->where('status','1')->get();
        $count = count($questions);
        return $count;
    }

    // change the category status
    public function statusChange(Request $request) {
      Category::find($request->id)->update(['status'=>$request->status]);
      return response()->json(['status' => 'success'], 200);
    }

}
