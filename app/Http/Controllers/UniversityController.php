<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\UniversityRequest;
use App\Models\College;
use App\Repositories\university\UniversityContract;
use Illuminate\Http\Request;
use App\Models\University;
use DataTables;

class UniversityController extends Controller
{
    protected $repository;

    public function __construct(UniversityContract $repository)
    {
        $this->repository = $repository;
    }

    // show all university in index page
    public function index(Request $request){
        if($request->ajax()) {
            $universities = University::latest()->get();
            return Datatables::of($universities)
                ->addIndexColumn()
                ->addColumn('action', function($universities) {
                    $button = '<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon" title="Edit details" onclick= "editUniversity('.$universities["id"].')" data-toggle="modal" data-target="#exampleModalLongInner"><i class="la la-edit" style="color:black;font-size: 23px"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="javascript:void(0)" data-id="'.$universities["id"].'" class="delete-button-action btn btn-sm btn-clean btn-icon delete" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                    return $button;
                })
                ->addColumn('status', function($universities){
                    if($universities['status'] == '0') {
                        return '<span class="switch switch-info "><label><input type="checkbox" name="select" id="'.$universities["id"].'" class="eligibalClass unchecked"><span></span></label></span>';
                    } else {
                        return '<span class="switch switch-info"><label><input type="checkbox" checked name="select" id="'.$universities["id"].'" class="eligibalClass checked"><span></span></label></span>';
                    }
                })
                ->rawColumns(['action','status'])
                ->make(true);
        }
        return view('university.index');
    }

    // university list
    public function uniLists(){
       $universities =$this->repository->getAll();
       return response()->json(['status'=>'success','data'=>$universities,'code'=>200],200);
    }

    // save university data
    public function store(UniversityRequest $request)
    {
        try{
            $data = $request->all();
            $university=$this->repository->store($data);
            session()->flash('success','University created Successfully');
            return response()->json(['status'=>'success'],200);
        }
        catch (\Exception $e) {
            Log::error("University management error:" . $e->getMessage());
            return response()->json(["status"=>"failure","message"=>"Something went wrong"],404);
        }
    }

    // edit university data
    public  function edit($id){
        $category=  $this->repository->find($id);
        return response()->json(['data'=>$category,'status'=>'success']);
    }

    // update university data
    public function update(UniversityRequest $request ,$id){
        try{
            $data=$request->except('_token');
            $university=$this->repository->update($id,$data);
            session()->flash('success','University updated Successfully');
            return response()->json( ['status'=>'success','message'=>'University updated Successfully','data'=>$university, 'code'=>200],200);
        }
        catch (\Exception $e) {
            Log::error('University management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // delete university data
    public function delete(Request $request){
        $id=$request->id;
        try {
            $this->repository->delete($id);
            College::where('university_id',$id)->delete();
            session()->flash('success','University deleted Successfully');
            return response()->json(['status'=>'deleted']);
        } catch (\Exception $e) {
            Log::error('University management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // change the university status
    public function statusChange(Request $request) {
        University::find($request->id)->update(['status'=>$request->status]);
        return response()->json(['status' => 'success'], 200);
    }

    // return university data(ajax)
    public function ajaxData(Request $request) {
        $universities = University::latest()->get();
        return response()->json(['status' => 'success','draw'=>1, 'data' => $universities], 200);
    }

}
