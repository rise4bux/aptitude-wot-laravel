<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SystemSetting;

class SystemSettingController extends Controller
{
    // edit system setting
    public function editSystemSettings() {
    	 try {
            $settingsData = SystemSetting::all();
            $setting = [];
            foreach ($settingsData as $s) {
                $setting[$s->key] = $s->value;
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
        return view('settings', compact('setting'));
    }

    // update system settings
    public function updateSystemSettings(Request $request) {
    	try {
            $data = $request->all();
            unset($data['_token']);
            foreach ($data as $key => $datum) {
                $setting = SystemSetting::all();
                $exist = $setting->where('key', $key)->first();
                if ($exist) {
                    $exist->update(['value' => $datum]);
                } else {
                    SystemSetting::create(['key' => $key, 'value' => $datum]);
                }
            }
            session()->flash('success', 'Setting updated successfully.!');
            return redirect()->back();
        } catch (\Exception $e) {
            session()->flash('error', 'Settings not updated! Try again later.');
            return redirect()->back()->withInput();
        }
    }
}
