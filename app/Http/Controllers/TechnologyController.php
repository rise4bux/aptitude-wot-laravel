<?php

namespace App\Http\Controllers;
use App\Http\Requests\TechnologyRequest;
use App\Models\University;
use App\Repositories\technology\TechnologyContract;
use Illuminate\Http\Request;
use App\Models\Technology;
use DataTables;

class TechnologyController extends Controller
{
    protected $repository;

    public function __construct(TechnologyContract $repository)
    {
        $this->repository = $repository;
    }

    // show all technology data in index page
    public function index(Request $request){
        if($request->ajax()) {
            $technologies = Technology::latest()->get();
            return Datatables::of($technologies)
                ->addIndexColumn()
                ->addColumn('action', function($technologies) {
                    $button = '<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon" title="Edit details" onclick= "editData('.$technologies["id"].')" data-toggle="modal" data-target="#exampleModalLongInner"><i class="la la-edit" style="color:black;font-size: 23px"></i></a>';
                    $button .= "&nbsp;&nbsp;";
                    $button .= '<a href="javascript:void(0)" data-id="'.$technologies["id"].'" class="delete-button-action btn btn-sm btn-clean btn-icon delete" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                    return $button;
                })
                ->addColumn('status', function($technologies){
                    if($technologies['status'] == '0') {
                        return '<span class="switch switch-info"><label><input type="checkbox" name="select" id="'.$technologies["id"].'" class="eligibalClass unchecked"><span></span></label></span>';
                    } else {
                        return '<span class="switch switch-info "><label><input type="checkbox" checked name="select" id="'.$technologies["id"].'" class="eligibalClass checked"><span></span></label></span>';
                    }
                })
                ->rawColumns(['action','status'])
                ->make(true);
        }
        return view('technology.index');
    }

    // return technology list
    public function technologyLists(){
       $technologies =$this->repository->getAll();
       return response()->json(['status'=>'success','data'=>$technologies,'code'=>200],200);
   }

    // save new technology data
    public function store(TechnologyRequest $request)
    {
        try{
            $data = $request->all();
            $technology=$this->repository->store($data);
            session()->flash('success','Technology created Successfully');
            return response()->json(['status'=>'success'],200);
        }
        catch (\Exception $e) {
            Log::error('Technology management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // edit technology data
    public  function edit($id){
        $technology=  $this->repository->find($id);
        return response()->json(['data'=>$technology,'status'=>'success']);
    }

    // update technology data
    public function update(TechnologyRequest $request ,$id){
        try{
            $data=$request->except('_token');
            $technology=$this->repository->update($id,$data);
            session()->flash('success','Technology updated Successfully');
            return response()->json( ['status'=>'success','message'=>'Technology updated Successfully','data'=>$technology, 'code'=>200],200);
        }
        catch (\Exception $e) {
            Log::error('Technology management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // delete technology data
    public function delete(Request $request){
        $id=$request->id;
        try {
            $this->repository->delete($id);
            session()->flash('success','Technology deleted Successfully');
            return response()->json(['status'=>'deleted']);
        } catch (\Exception $e) {
            Log::error('Technology management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // change technology status
    public function statusChange(Request $request) {
        Technology::find($request->id)->update(['status'=>$request->status]);
        return response()->json(['status' => 'success'], 200);
    }

}
