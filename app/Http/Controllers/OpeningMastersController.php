<?php

namespace App\Http\Controllers;

use App\OpeningMasters;
use App\PracticalQuestion;
use Illuminate\Http\Request;
use App\Http\Requests\OpeningRequest;
use DataTables;

class OpeningMastersController extends Controller
{
    // show all opening data in index view
    public function index(Request $request){
        if($request->ajax()) {
            $openings = OpeningMasters::latest()->get();
            return Datatables::of($openings)
                ->addIndexColumn()
                ->addColumn('action', function($openings) {
                    $button = '<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon" title="Edit details" onclick= "editData('.$openings["id"].')" data-toggle="modal" data-target="#exampleModalLongInner"><i class="la la-edit" style="color:black;font-size: 23px"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="javascript:void(0)" data-id="'.$openings["id"].'" class="delete-button-action btn btn-sm btn-clean btn-icon delete" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                    return $button;
                })
                ->addColumn('status', function($openings){
                    if($openings['status'] == '0') {
                        return '<span class="switch switch-info"><label><input type="checkbox" name="select" id="'.$openings["id"].'" class="eligibalClass unchecked"><span></span></label></span>';
                    } else {
                        return '<span class="switch switch-info"><label><input type="checkbox" checked name="select" id="'.$openings["id"].'" class="eligibalClass checked"><span></span></label></span>';
                    }
                })
                ->rawColumns(['action','status'])
                ->make(true);
        }
        return view('openings');
    }

    // save opening data
    public function save(OpeningRequest $request) {
      OpeningMasters::create($request->all());
      session()->flash('success','Opening created Successfully');
      return response()->json(['status'=>'success'],200);
    }

    // edit opening data
    public function edit($id) {
      $openings=  OpeningMasters::find($id);
      return response()->json(['data'=>$openings,'status'=>'success']);
    }

    // update opening data
    public function update(OpeningRequest $request ,$id){
        try{
            $data = $request->except('_token');
            $opening = OpeningMasters::find($id)->update($request->all());
            session()->flash('success','Opening updated Successfully');
            return response()->json( ['status'=>'success','message'=>'Opening updated Successfully','data'=>$opening, 'code'=>200],200);
        }
        catch (\Exception $e) {
            Log::error('Opening management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // delete opening data
    public function delete(Request $request){
        $id=$request->id;
        try {
            OpeningMasters::find($id)->delete();
            session()->flash('success','Opening deleted Successfully');
            return response()->json(['status'=>'deleted']);
        } catch (\Exception $e) {
            Log::error('Opening management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // change opening status
    public function statusChange(Request $request) {
      OpeningMasters::find($request->id)->update(['status'=>$request->status]);
      return response()->json(['status' => 'success'], 200);
    }

    // Find total opening questions
    public function totalQuestion($id) {
        $easy = 0; $medium = 0; $hard = 0;
        $practicalQuestions = PracticalQuestion::where('opening_id',$id)->where('status',1)->groupBy('difficulty')->get();
        foreach($practicalQuestions as $data) {
            if($data->difficulty == 'Easy') {
                $easy = $easy + 1;
            } else if($data->difficulty == 'Medium') {
                $medium = $medium + 1;
            } else if($data->difficulty == 'Hard') {
                $hard = $hard + 1;
            }
        }
        $questions['total'] = $easy + $medium + $hard;
        $questions['easy'] = $easy;
        $questions['medium'] = $medium;
        $questions['hard'] = $hard;
        $questions['id'] = $id;
        return response()->json(['data'=>$questions,'status'=>'success']);
    }
}
