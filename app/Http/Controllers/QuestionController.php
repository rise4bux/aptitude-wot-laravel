<?php

namespace App\Http\Controllers;

use App\Category;
use App\EducationStreamMasters;
use App\ExamUser;
use App\Http\Requests\QuestionRequest;
use App\Imports\ExamImport;
use App\Models\College;
use App\OpeningMasters;
use App\PracticalQuestion;
use App\Question;
use App\QuestionOption;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;

class QuestionController extends Controller
{
    protected  $question;

    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    // show all question in index page
    public function index(Request $request){
         $categories = Category::where('status',1)->get();
         if($request->ajax()) {
             $status = '';
             $difficulty = '';
             $category = '';
             if (isset($request->status)) {
                 if ($request->status == 2) {
                     $status = "0";
                 } else {
                     $status = "1";
                 }
             }
             if (isset($request->difficulty)) {
                 $difficulty = $request->difficulty;
             }
             if (isset($request->category)) {
                 $category = $request->category;
             }
             $questions = Question::with('category')->where(function ($query) use ($difficulty, $status, $category) {
                 if (isset($difficulty) && $difficulty != null) {
                     $query->where('difficulty', $difficulty);
                 }
                 if (isset($status) && $status != null) {
                     $query->where('status', $status);
                 }
                 if (isset($category) && $category != null && $category != 'Select Category') {
                     $query->where('category_id', $category);
                 }
             })->latest()->get();
             return $this->ajaxData($questions);
         }
         return view('questions_new', compact('categories'));
     }

    // return question list
    public function questionList(){
         $questions= Question::with('category')->get();
          return response()->json(['status'=>'success', 'data'=>$questions],200);
      }

    // return questions ajax data from ihe index method
    public function ajaxData($questions){
        return Datatables::of($questions)
            ->addIndexColumn()
            ->addColumn('title', function ($questions) {
                return strip_tags($questions->title);
            })
            ->addColumn('language', function($questions) {
                return $questions->category->name;
            })
            ->addColumn('action', function($questions) {
                $button = '<a href="questions/edit-question/'.$questions["id"].'" class="btn btn-sm btn-clean btn-icon" title="Edit details"><i class="la la-edit" style="color:black;font-size: 23px"></i></a>';
                $button .= '&nbsp;&nbsp;';
                $button .= '<a href="javascript:void(0)" data-id="'.$questions["id"].'" class="delete-button-action btn btn-sm btn-clean btn-icon delete" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                return $button;

            })
            ->addColumn('status', function($questions){
                if($questions['status'] == "0") {
                    return '<span class="switch switch-info"><label><input type="checkbox" name="select" id="'.$questions["id"].'" class="eligibalClass unchecked"><span></span></label></span>';
                } else {
                    return '<span class="switch switch-info"><label><input type="checkbox" checked name="select" id="'.$questions["id"].'" class="eligibalClass checked"><span></span></label></span>';
                }
            })
            ->rawColumns(['action','status'])
            ->make(true);
    }

    // show create question form
    public function addQuestion() {
            $category=Category::where('status','1')->get();
            $question=null;
            return view('addquestion_new',compact('category','question'));
      }

    // save new question data
    public function save(QuestionRequest $request){
        Session::forget('category');
        Session::forget('difficulty');
          $data=$request->all();
          DB::beginTransaction();
          try {
              $question = Question::create([
                  'title' => $data['title'],
                  'category_id' => $data['category'],
                  'difficulty' => $data['difficulty'],
                  'status' => $data['status'],
              ]);
              foreach ($data['options'] as $option) {
                  $collection = new Collection([$option]);
                  if($option['option'] !='') {
                      if ($collection->contains("is_correct", '1')) {
                          QuestionOption::create([
                              'question_id' => $question->id,
                              'content' => $option['option'],
                              'status' => '1',
                              'is_correct' => '1'
                          ]);
                      } else {
                          QuestionOption::create([
                              'question_id' => $question->id,
                              'content' => $option['option'],
                              'status' => '1',
                              'is_correct' => '0'
                          ]);
                      }
                  }
                  else {
                      continue;
                  }
              }
              DB::commit();
              session()->flash('success','Question created Successfully');
              if(isset($_POST["save"])) {
                  return redirect(route('questions.index'));
              }
              if(isset($_POST["saveNew"])) {
                  session()->put('category' ,$request->category);
                  session()->put('difficulty' ,$request->difficulty);
                  return redirect(route('questions.add'));
              }

          } catch(\Exception $e){
              session()->flash('error','something went wrong');
               DB::rollBack();
               dd($e->getMessage());
          }
     }

    // edit question data
    public function edit($id){
        try {
            $question=  Question::where('id',$id)->with('options')->firstOrFail();
            Session::forget('category');
            Session::forget('difficulty');
            $category=Category::where('status','1')->get();
            $count=QuestionOption::where('question_id',$id)->get()->count();
            return view('addquestion_new',compact('question','category','count'));
        }  catch (\Exception $e) {
            Log::error('Question management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // update question data
    public function update(QuestionRequest $request,$id){
      $data=$request->all();
      DB::beginTransaction();
      try {

          Question::where('id', $id)->update([
              'title' => $data['title'],
              'category_id' => $data['category'],
              'difficulty' => $data['difficulty'],
              'status' => $data['status'],
          ]);
          QuestionOption::where('question_id',$id)->delete();
          foreach ($data['options'] as $option) {
              $collection = new Collection([$option]);
              if($option['option'] !='') {
                  if ($collection->contains("is_correct", '1')) {
                      QuestionOption::create([
                          'question_id' => $id,
                          'content' => $option['option'],
                          'status' => '1',
                          'is_correct' => '1'
                      ]);
                  } else {
                      QuestionOption::create([
                          'question_id' => $id,
                          'content' => $option['option'],
                          'status' => '1',
                          'is_correct' => '0'
                      ]);
                  }
              }
              else {
              }
          }
          DB::commit();
          session()->flash('success','Question updated Successfully');
          return redirect(route('questions.index'));
      } catch(\Exception $e){
          DB::rollBack();
          session()->flash('error','something went wrong');

      }
    }

    // delete question data
    public function delete($id){
        //TODO : update needed
        $ques= Question::findOrFail($id);
        $ques->options()->delete();
        $ques->delete();
        session()->flash('success','Question deleted Successfully');
        return redirect()->route('questions.index');
    }

    public function import() {
        return view('question.import');
    }

    public function import_data(Request $request) {
        try{
            $data = $request->file('import_file');
            $import = new ExamImport();
            Excel::import($import, $request->file('import_file'));
            if($import->getRowCount() > 0 ){
                return redirect()->route('questions.index')->with('success', $import->getRowCount().' Question imported successfully');
            }
            else {
                return redirect()->route('questions.index')->with('failure',' Not Question added , Please check your data');
            }

        }
        catch (\Exception $e) {

            Log::error('Exam import error:' . $e->getMessage() . 'on Line ' . $e->getLine() . '-->' . $e->getFile());
            return redirect()->route('questions')->with('failure','Failed to import data , please check your data and try again');
        }
    }

    // change the question status
    public function statusChange(Request $request) {
      Question::find($request->id)->update(['status'=>$request->status]);
      return response()->json(['status' => 'success'], 200);
    }

}
