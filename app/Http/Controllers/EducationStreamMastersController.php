<?php

namespace App\Http\Controllers;

use App\EducationStreamMasters;
use Illuminate\Http\Request;
use App\Http\Requests\EducationStreamRequest;
use DataTables;

class EducationStreamMastersController extends Controller
{
    // show all data in index page
    public function index(Request $request){
        if($request->ajax()) {
            $streams = EducationStreamMasters::latest()->get();
            return Datatables::of($streams)
                ->addIndexColumn()
                ->addColumn('action', function($streams) {
                    $button = '<a href="javascript:void(0)" class="btn btn-sm btn-clean btn-icon" title="Edit details" onclick= "editData('.$streams["id"].')" data-toggle="modal" data-target="#exampleModalLongInner"><i class="la la-edit" style="color:black;font-size: 23px"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="javascript:void(0)" data-id="'.$streams["id"].'" class="delete-button-action btn btn-sm btn-clean btn-icon delete" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                    return $button;
                })
                ->addColumn('status', function($streams){
                    if($streams['status'] == '0') {
                        return '<span class="switch switch-info"><label><input type="checkbox" name="select" id="'.$streams["id"].'" class="eligibalClass unchecked"><span></span></label></span>';
                    } else {
                        return '<span class="switch switch-info"><label><input type="checkbox" checked name="select" id="'.$streams["id"].'" class="eligibalClass checked"><span></span></label></span>';
                    }
                })
                ->rawColumns(['action','status'])
                ->make(true);
        }
        return view('educationStream');
    }

    // save education stream data
    public function save(EducationStreamRequest $request) {
      EducationStreamMasters::create($request->all());
      session()->flash('success','Education stream created Successfully');
      return response()->json(['status'=>'success'],200);
    }

    // edit education stream data
    public function edit($id) {
      $stream=  EducationStreamMasters::find($id);
      return response()->json(['data'=>$stream,'status'=>'success']);
    }

    // update education stream data
    public function update(EducationStreamRequest $request ,$id){
        try{
            $data = $request->except('_token');
            $opening = EducationStreamMasters::find($id)->update($request->all());
            session()->flash('success','Education stream updated Successfully');
            return response()->json( ['status'=>'success','message'=>'Education stream updated Successfully','data'=>$opening, 'code'=>200],200);
        }
        catch (\Exception $e) {
            Log::error('Education stream management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // Delete education stream data
    public function delete(Request $request){
        $id=$request->id;
        try {
            EducationStreamMasters::find($id)->delete();
            session()->flash('success','Education stream deleted Successfully');
            return response()->json(['status'=>'deleted']);
        } catch (\Exception $e) {
            Log::error('Education stream management error:' . $e->getMessage());
            return response()->json(['status'=>'failure','message'=>'Something went wrong'],404);
        }
    }

    // change the education stream status
    public function statusChange(Request $request) {
      EducationStreamMasters::find($request->id)->update(['status'=>$request->status]);
      return response()->json(['status' => 'success'], 200);
    }
}
