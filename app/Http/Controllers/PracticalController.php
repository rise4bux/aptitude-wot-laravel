<?php

namespace App\Http\Controllers;

use App\Exam;
use App\ExamUser;
use App\Http\Requests\API\CompleteExam;
use App\Http\Requests\API\StartExam;
use App\Http\Requests\API\VerifyEligibility;
use App\Practical;
use App\SystemSetting;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PracticalController extends Controller
{

    public function practicalDetails($practicalId)
    {
        try {
            $practical = Practical::select('id', 'paper_type', 'chosen_language', 'exam_id', 'status', 'started_at', 'completed_at', 'code_1',
                'result_1', 'code_2', 'result_2', 'code_3', 'result_3', 'result_4', 'result_5', 'result_6', 'result_7')
                ->findOrFail($practicalId);
            $settingsData = SystemSetting::all();
            $setting = [];
            foreach ($settingsData as $s) {
                $setting[$s->key] = $s->value;
            }
            return response()->json([
                'status'  => 'success',
                'data'    => $practical,
                'system_setting' => $setting,
                'message' => 'Practical fetch successfully.!'
            ], 200);
        }
        catch (\Exception $exception) {
            return response()->json([
                'status'  => 'error',
                'message' => 'Something went wrong.!'
            ], 404);
        }
    }

    public function verifyEligibility(VerifyEligibility $request)
    {
        try {
            $settingsData = SystemSetting::all();
            $setting = [];
            foreach ($settingsData as $s) {
                $setting[$s->key] = $s->value;
            }
            $enrollmentNumber = $request->enrollment_number;
            $user = User::select('id', 'first_name', 'last_name', 'email', 'mobile_no', 'enrollment_no')
                ->where('enrollment_no', $enrollmentNumber)
                ->first();

            if (empty($user)) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'User not found.!'
                ], 400);
            }

            $latestExamOfUser = ExamUser::where('user_id', $user->id)->latest()->first();
            if (empty($latestExamOfUser)) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'You haven\'t attend any exams yet.!'
                ], 400);
            }

            if ($latestExamOfUser->eligibal == 0) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'you\'re not eligible for practical test11.!',
                    'check' => $latestExamOfUser->eligibal
                ], 400);
            }

            $practical = null;
            if (!empty($latestExamOfUser->practical_id)) {
                $practical = Practical::where('id', $latestExamOfUser->practical_id)->first();
                if ($practical->status == 'completed') {
                    return response()->json([
                        'status'  => 'error',
                        'message' => 'You have already given practical exam.!'
                    ], 400);
                }
            }

            $latestExam = Exam::where('id', $latestExamOfUser->exam_id)->first();
            if (empty($latestExam)) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'Exam not found, may be it was deleted. contact HR.!'
                ], 400);
            }
//            $cutOff = $latestExam->cut_off_percentage;
//            if ($latestExamOfUser->percentage < $cutOff) {
//                return response()->json([
//                    'status'  => 'error',
//                    'message' => 'You are not eligible for practical exam.!'
//                ], 400);
//            }
            $finalData = [
                'status'  => 'success',
                'data'    => $user,
                'system_setting' => $setting,
                'message' => 'User fetch successfully.!'
            ];
            if ($practical != null) {
                $finalData['practical'] = $practical;
            }
            return response()->json($finalData, 200);
        }
        catch (\Exception $e) {
            return response()->json([
                'status'  => 'error',
                'message' => 'Something went wrong.!'
            ], 404);
        }
    }

    public function startExam(StartExam $request)
    {
        try {
            $latestExamOfUser = ExamUser::where('user_id', $request->user_id)->latest()->first();
            if (empty($latestExamOfUser)) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'You haven\'t attend any exams yet.!'
                ], 400);
            }

            if ($latestExamOfUser->eligibal == 0) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'you\'re not eligible for practical test.!'
                ], 400);
            }

            if (!empty($latestExamOfUser->practical_id)) {
                $practical = Practical::where('id', $latestExamOfUser->practical_id)->first();
                if ($practical->status == 'completed') {
                    return response()->json([
                        'status'  => 'error',
                        'message' => 'You have already given practical exam.!'
                    ], 400);
                } else if ($practical->status == 'on_going') {
                    $finalData = [
                        'status'  => 'success',
                        'data'    => $practical,
                        'message' => 'Exam started successfully.!'
                    ];
                    if (!empty($practical)) {
                        $finalData['practical'] = $practical;
                    }
                    return response()->json([
                        'status'  => 'success',
                        'data'    => $practical,
                        'message' => 'Exam Resume successfully.!'
                    ], 200);
                }
            }

            $latestExam = Exam::where('id', $latestExamOfUser->exam_id)->first();
            if (empty($latestExam)) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'Exam not found, may be it was deleted. contact HR.!'
                ], 400);
            }
            $payload = [
                'exam_id'         => $latestExam->id,
                'user_id'         => $request->user_id,
                'paper_type'      => $request->paper_type,
                'chosen_language' => $request->chosen_language,
                'status'          => 'on_going',
            ];

            $practical = Practical::create($payload);
            $practical = Practical::select('id', 'paper_type', 'chosen_language', 'exam_id', 'status', 'started_at')
                ->findOrFail($practical->id);
            $latestExamOfUser->update([
                'practical_id' => $practical->id
            ]);
            return response()->json([
                'status'  => 'success',
                'data'    => $practical,
                'message' => 'Exam started successfully.!'
            ], 200);
        }
        catch (\Exception $exception) {
            return response()->json([
                'status'  => 'error',
                'message' => 'Something went wrong.!'
            ], 404);
        }
    }

    public function completeExam(CompleteExam $request)
    {
        try {

            $practical = Practical::findOrFail($request->practical_id);

            $latestExamOfUser = ExamUser::where('user_id', $practical->user_id)->latest()->first();
            if (empty($latestExamOfUser)) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'You haven\'t attend any exams yet.!'
                ], 400);
            }

            if ($latestExamOfUser->eligibal == 0) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'you\'re not eligible for practical test.!'
                ], 400);
            }


            // code 1 is file or URL
            if($request->hasFile('code_1')) {
                $uploadedFileCode1 = $request->file('code_1');
                $code1filename = time() . '_user_' . $request->user_id . '_code1_' . $uploadedFileCode1->getClientOriginalName();
                Storage::disk('public')->putFileAs('practicals', $uploadedFileCode1, $code1filename);
            } else {
                $code1filename = $request->code_1;
            }

            // code 2
            $uploadedFileCode2 = $request->file('code_2');
            $code2filename = time() . '_user_' . $request->user_id . '_code2_' . $uploadedFileCode2->getClientOriginalName();
            Storage::disk('public')->putFileAs('practicals', $uploadedFileCode2, $code2filename);
            // code 3
            $uploadedFileCode3 = $request->file('code_3');
            $code3filename = time() . '_user_' . $request->user_id . '_code3_' . $uploadedFileCode3->getClientOriginalName();
            Storage::disk('public')->putFileAs('practicals', $uploadedFileCode3, $code3filename);
            // result 1
            $uploadedFileResult1 = $request->file('result_1');
            $result1filename = time() . '_user_' . $request->user_id . '_result1_' . $uploadedFileResult1->getClientOriginalName();
            Storage::disk('public')->putFileAs('practicals', $uploadedFileResult1, $result1filename);
            // result 2
            $uploadedFileResult2 = $request->file('result_2');
            $result2filename = time() . '_user_' . $request->user_id . '_result2_' . $uploadedFileResult2->getClientOriginalName();
            Storage::disk('public')->putFileAs('practicals', $uploadedFileResult2, $result2filename);
            // result 3
            $uploadedFileResult3 = $request->file('result_3');
            $result3filename = time() . '_user_' . $request->user_id . '_result3_' . $uploadedFileResult3->getClientOriginalName();
            Storage::disk('public')->putFileAs('practicals', $uploadedFileResult3, $result3filename);


            // result 4 - optional which will be used only for BA/BDE
            if($request->hasFile('result_4')) {
                $uploadedFileResult4 = $request->file('result_4');
                $result4filename = time() . '_user_' . $request->user_id . '_result4_' . $uploadedFileResult4->getClientOriginalName();
                Storage::disk('public')->putFileAs('practicals', $uploadedFileResult4, $result4filename);
            } else {
                $result4filename = null;
            }

            // result 5 - optional which will be used only for BA/BDE
            if($request->hasFile('result_5')) {
                $uploadedFileResult5 = $request->file('result_5');
                $result5filename = time() . '_user_' . $request->user_id . '_result5_' . $uploadedFileResult5->getClientOriginalName();
                Storage::disk('public')->putFileAs('practicals', $uploadedFileResult5, $result5filename);
            } else {
                $result5filename = null;
            }

            // result 6 - optional which will be used only for BA/BDE
            if ($request->hasFile('result_6')) {
                $uploadedFileResult6 = $request->file('result_6');
                $result6filename = time() . '_user_' . $request->user_id . '_result6_' . $uploadedFileResult6->getClientOriginalName();
                Storage::disk('public')->putFileAs('practicals', $uploadedFileResult6, $result6filename);
            }
            else {
                $result6filename = null;
            }

            // result 7 - optional which will be used only for BA/BDE
            if ($request->hasFile('result_7')) {
                $uploadedFileResult7 = $request->file('result_7');
                $result7filename = time() . '_user_' . $request->user_id . '_result7_' . $uploadedFileResult7->getClientOriginalName();
                Storage::disk('public')->putFileAs('practicals', $uploadedFileResult7, $result7filename);
            }
            else {
                $result7filename = null;
            }


            $updatePayload = [
                'code_1'       => $code1filename,
                'code_2'       => $code2filename,
                'code_3'       => $code3filename,
                'result_1'     => $result1filename,
                'result_2'     => $result2filename,
                'result_3'     => $result3filename,
                'result_4'     => $result4filename,
                'result_5'     => $result5filename,
                'result_6'     => $result6filename,
                'result_7'     => $result7filename,
                'status'       => 'completed',
                'completed_at' => Carbon::now(),
            ];
            $practical->update($updatePayload);

            $practical = Practical::select('id', 'paper_type', 'chosen_language', 'exam_id', 'status', 'started_at', 'completed_at', 'code_1', 'code_2', 'code_3',
                'result_1', 'result_2', 'result_3', 'result_4', 'result_5', 'result_6', 'result_7')
                ->findOrFail($practical->id);

//            $latestExamOfUser->update([
//                'practical_id' => $practical->id
//            ]);
            return response()->json([
                'status'  => 'success',
                'data'    => $practical,
                'message' => 'Exam completed successfully.!'
            ], 200);
        }
        catch (\Exception $exception) {
            return response()->json([
                'status'  => 'error',
                'message' => 'Practical not found.!'
            ], 404);
        }
    }

    public function videoProcessing(Request $request)
    {
        $input = $request->only('status', 'enrollment_number' );

        $enrollmentNumber = $input['enrollment_number'];
        $user = User::select('id', 'first_name', 'last_name', 'email', 'mobile_no', 'enrollment_no')
            ->where('enrollment_no', $enrollmentNumber)
            ->first();

        if (empty($user)) {
            return response()->json([
                'status'  => 'error',
                'message' => 'User not found.!'
            ], 400);
        }

        $latestExamOfUser = ExamUser::where('user_id', $user->id)->latest()->first();
        if (empty($latestExamOfUser)) {
            return response()->json([
                'status'  => 'error',
                'message' => 'You haven\'t attend any exams yet.!'
            ], 400);
        }

        if ($latestExamOfUser->practical_id == null) {
            return response()->json([
                'status'  => 'error',
                'message' => 'You haven\'t attend practical exams yet.!'
            ], 400);
        }

        if (isset($input['status']) && $input['status'] != null) {
            try {
                $practical = Practical::findOrFail($latestExamOfUser->practical_id);
                $practical->update([
                    'video_status' => $input['status']
                ]);
                return response()->json([
                    'status'  => 'success',
                    'message' => 'Video processing status updated successfully.!'
                ], 200);
            } catch (\Exception $e) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'Practical not found.!'
                ], 404);
            }
        } else {
            return response()->json([
                'status'  => 'error',
                'message' => 'Please provide status.!'
            ], 404);
        }
    }

    public function getAllEnrollmentOfUnProcessedVideo(Request $request, $id)
    {
        if (empty($id)) {
            return response()->json([
                'status'  => 'error',
                'message' => 'Please provide Exam id.!'
            ], 404);
        }
        try {
            $exam = Exam::select('name')->where('id', $id)->get()->pluck('name')->first();
            $examUsers = ExamUser::whereHas('practical', function ($query){
                $query->where('status','completed')->where('video_status',0);
            })->where('exam_id', $id)->where('eligibal',1)->pluck('user_id');
            $enrollment_number = User::select('enrollment_no')->whereIn('id',$examUsers)->get()->pluck('enrollment_no');
            return response()->json([
                'status'  => 'success',
                'exam_name' => $exam,
                'data' => $enrollment_number,
                'message' => 'Enrollement is fetched successfully.!'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
               'status'  => 'error',
               'message' => 'Something went wrong.!'
           ], 404);
        }
    }
}
