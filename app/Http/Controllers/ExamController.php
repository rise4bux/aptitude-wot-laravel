<?php

namespace App\Http\Controllers;

use App\Category;
use App\Exam;
use App\ExamPhase;
use App\ExamUser;
use App\ExamUserKpi;
use App\Exports\ExamUserExport;
use App\Http\Requests\ExamRequest;
use App\Models\College;
use App\Practical;
use App\Question;
use App\QuestionAnswer;
use App\QuestionOption;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\EducationStreamMasters;
use App\OpeningMasters;
use DataTables;

class ExamController extends Controller
{
    // show all exam in index page
    public function index(Request $request)
    {
    //        $exam = Exam::with('phases')->latest()->get();
    //        return view('exam_new',compact('exam'));
        if($request->ajax()) {
            $exams = Exam::with('phases')->latest()->get();
            return Datatables::of($exams)
                ->addIndexColumn()
                ->addColumn('cut_off_percentage', function($exams) {
                    return $exams->cut_off_percentage."%";
                })
                ->addColumn('question', function($exams) {
                    $count=0;
                    for($i=0;$i< count($exams->phases); $i++){
                        $count=$count+$exams->phases[$i]->question_count;
                    }
                    return $count;
                })
                ->addColumn('link', function($exams) {

                    $buttonHTML = '<button class="btn btn-sm btn-success Copylink" title= '.$exams->link.' >Copy</button> ';
                    /*buttonHTML +=  `&nbsp;<button class="btn btn-sm btn-primary start-exam" title= `+"start"+full.link+`>Start exam</button>`*/
                    $buttonHTML .= ' <button class="btn btn-sm btn-info LoginLink" title= '.$exams->link.'>Login</button>';

                    return $buttonHTML;
                })
                ->addColumn('action', function($exams) {
                    $button = '<a href="exams/edit-exam/'.$exams["id"].'" class="btn btn-sm btn-clean btn-icon" title="Edit details"><i class="la la-edit" style="color:black;font-size: 23px;"></i></a>';
                    $button .= '<a href="exams/participants/'.$exams["id"].'"><button class="btn btn-sm btn-clean btn-icon btn-icon-md" title="participated users" ><i class="fa fa-users" style="color:#6993FF"></i></button></a>';
                    $button .= '<a href="javascript:void(0)" data-id="'.$exams["id"].'" class="delete-button-action btn btn-sm btn-clean btn-icon delete" title="Delete" data-toggle="modal" data-target="#myModal"><i class="la la-trash" style="color:red;font-size: 23px"></i></a>';
                    return $button;
                })
                ->addColumn('status', function($exams){
                    if($exams['status'] == "0") {
                        return '<span class="switch switch-info"><label><input type="checkbox" name="select" id="'.$exams["id"].'" class="eligibalClass unchecked"><span></span></label></span>';
                    } else {
                        return '<span class="switch switch-info"><label><input type="checkbox" checked name="select" id="'.$exams["id"].'" class="eligibalClass checked"><span></span></label></span>';
                    }
                })
                ->rawColumns(['action','status','link'])
                ->make(true);
        }
        return view('exam_new');
    }

    // return examlist
    public function examList()
    {

        $exam = Exam::with('phases')->get();
        return response()->json(['status' => 'success', 'data' => $exam], 200);
    }

    // show create exam form
    public function create()
    {
        $exam = null;
        $counts = $this->statsData();

        $category = Category::with('questions')->where('status', '1')->get();
        $categories = $category->filter(function ($item){
            if($item->questions->count() > 0 ) {
                $item->questions_count = $item->questions->count();
                $questions = collect($item->questions)->groupBy('difficulty');
                foreach ($questions as $qkey=>$question) {
                    $item[$qkey] = $question->count();
                }

            }
            return $item;
        });

        $difficulty = ['Easy','Medium','Hard'];
        $instruction_text = $this->getInstruction();
        return view('addexam_new', compact('exam', 'categories', 'counts','difficulty','instruction_text'));
    }

    function array_merge_numeric_values()
    {
        $arrays = func_get_args();
        $merged = array();
        foreach ($arrays[0] as $k => $array)
        {
            $merged[$array['category'].'_'.$array['difficulty']] = $array;
            $merged[$array['category'].'_'.$array['difficulty']]['count'] = 0;
            foreach ($array as $key => $value)
            {
                if ( $key != 'count')
                {
                    continue;
                }
                if (!isset($merged[$array['category'].'_'.$array['difficulty']][$key]))
                {
                    $merged[$array['category'].'_'.$array['difficulty']][$key] = $value;
                }
                else
                {
                    $merged[$array['category'].'_'.$array['difficulty']][$key] += $value;
                }
            }
        }
        return $merged;
    }

    // save new exam data
    public function save(ExamRequest $request)
    {

        $data = $request->all();

     $temp= $this->array_merge_numeric_values($data['phases']);

        foreach ($temp as $t){
            if($t['count']==null){
                return redirect()->back()->withErrors('Question count is required')->withInput();
            }
            $questionCountInDB= Question::where(['category_id'=>$t['category'],'difficulty'=>$t['difficulty'],'status'=>1])->count();

            if($questionCountInDB<$t['count']){
               $cat_name= Category::where('id',$t['category'])->pluck('name')->first();

                session()->flash('error',$questionCountInDB.' Questions are available for '.$cat_name.' '.$t['difficulty'].' level');
                return redirect()->back()->withInput();
            }

        }
        DB::beginTransaction();
        try {

            $exam = Exam::create([
                'name' => $data['name'],
                'description' => $data['description'],
                'instruction' => $data['instruction'],
                'time' => $data['time'],
                'status' => $data['status'],
                'exam_password' => $data['exam_password'],
                'cut_off_percentage' => $data['percentage'],
            ]);
            $category_names = [];
            foreach ($data['phases'] as $key=>$phase) {
                $samePhase = ExamPhase::where('exam_id',$exam->id)->where('category_id',$phase['category'])->where('difficulty',$phase['difficulty'])->first();

                if(!empty($samePhase)) {
                    $samePhase->question_count = intval($samePhase->question_count + $phase['count']);
                    $samePhase->update();
                }
                else {
                    ExamPhase::create([
                        'exam_id' => $exam->id,
                        'category_id' => $phase['category'],
                        'difficulty' => $phase['difficulty'],
                        'question_count' => $phase['count']
                    ]);

                }
            }
           /* $category_ids = array_column($data['phases'],'category');
            $category_names = Category::whereIn('id',$category_ids)->get()->pluck('name')->toArray();

            $instruction_text = $data['instruction'];
            $newInstrcution = str_replace('{{time}}',$data['time'],$instruction_text);
            $newInstrcution = str_replace('{{category_count}}',count($category_names),$newInstrcution);
            $newInstrcution = str_replace('{{category_name}}',implode(', ',$category_names),$newInstrcution);

            Exam::find($exam->id)->update(['instruction'=>$newInstrcution]);*/

            DB::commit();
            session()->flash('success', 'Exam Created Successfully');
            return redirect(route('exams.index'));
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'something went wrong');
        }
    }

    // show edit exam form
    public function edit($id)
    {
        $exam = Exam::where('id', $id)->with('phases')->first();
        $category = Category::with('questions')->where('status', '1')->get();
        $categories = $category->filter(function ($item){
            if($item->questions->count() > 0 ) {
                $item->questions_count = $item->questions->count();
                $questions = collect($item->questions)->groupBy('difficulty');
                foreach ($questions as $qkey=>$question) {
                    $item[$qkey] = $question->count();
                }

            }
            return $item;
        });
        $difficulty = ['Easy','Medium','Hard'];
        $count = ExamPhase::where('exam_id', $id)->get()->count();
        $counts = $this->statsData();
        return view('addexam_new', compact('exam', 'categories', 'count', 'counts','difficulty'));
    }

    // update exam
    public function update(ExamRequest $request, $id)
    {
        $data = $request->all();
        $temp= $this->array_merge_numeric_values($data['phases']);
        foreach ($temp as $t){
            if($t['count']==null){
                return redirect()->back()->withErrors('Question count is required')->withInput();
            }
            $questionCountInDB= Question::where(['category_id'=>$t['category'],'difficulty'=>$t['difficulty'],'status'=>1])->count();
            if($questionCountInDB<$t['count']){
                $cat_name= Category::where('id',$t['category'])->pluck('name')->first();

                session()->flash('error',$questionCountInDB.' Questions are available for '.$cat_name.' '.$t['difficulty'].' level');
                return redirect()->back()->withInput();
            }

        }

        DB::beginTransaction();
        try {
            $exam = Exam::find($id)->update([
                'name' => $data['name'],
                'description' => $data['description'],
                'instruction' => $data['instruction'],
                'time' => $data['time'],
                'status' => $data['status'],
                'exam_password' => $data['exam_password'],
                'cut_off_percentage' => $data['percentage'],
            ]);

            ExamPhase::where('exam_id', $id)->delete();

            foreach ($data['phases'] as $phase) {
                $samePhase = ExamPhase::where('exam_id',$id)->where('category_id',$phase['category'])->where('difficulty',$phase['difficulty'])->first();

                if(!empty($samePhase)) {
                    $samePhase->question_count = intval($samePhase->question_count + $phase['count']);
                    $samePhase->update();
                }
                else {
                    ExamPhase::create([
                        'exam_id' => $id,
                        'category_id' => $phase['category'],
                        'difficulty' => $phase['difficulty'],
                        'question_count' => $phase['count']
                    ]);
                }

            }
            DB::commit();
            session()->flash('success', 'Exam updated Successfully');
            return redirect(route('exams.index'));
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'something went wrong');
        }
    }

    // delete exam data
    public function delete($id)
    {
        $exam = Exam::findOrFail($id);
        $exam->phases()->delete();
        $exam->delete();
        session()->flash('success', 'Exam deleted Successfully');
        return redirect(route('exams.index'));
    }

    public function statsData()
    {
        $allCatGrouped = Category::with('questions')->where('status',1)->get();
        $stats = [];
        foreach ($allCatGrouped as $catName => $catItem) {
            $allCatGroupedByDifficulty = $catItem->questions->groupBy('difficulty');

            $default = [
                'Easy' => [
                    'active' => 0,
                    'inActive' => 0,
                ],
                'Medium' => [
                    'active' => 0,
                    'inActive' => 0,
                ],
                'Hard' => [
                    'active' => 0,
                    'inActive' => 0,
                ]

            ];
            foreach ($allCatGroupedByDifficulty as $Dkey => $DItem) {
                $stats[$catItem->name] = $default;
            }

            foreach ($allCatGroupedByDifficulty as $Dkey => $DItem) {
                $stats[$catItem->name][$Dkey] = [
                    'active' => $DItem->where('status', 1)->count(),
                    'inActive' => $DItem->where('status', 0)->count()
                ];
            }
        }
        return $stats;
    }

    public function getParticipants($id){
      /* $users= ExamUser::where('exam_id',$id)->pluck('user_id')->toArray();

       $examusers= User::with('exams')->whereIn('id',$users)->get();*/
        $examusers = ExamUser::with('users')->where('exam_id',$id)->get();
        $colleges = College::where('status','1')->get();
        $exam_id = $id;
        $streams = EducationStreamMasters::where('status','1')->get();
        $openings = OpeningMasters::where('status','1')->get();
        return view('examusers_new',compact('exam_id','colleges','examusers','streams','openings'));
        //return view('examusers_old',compact('exam_id','colleges'));
    }

    public function getExamUsersId($colleg_id='',$searchKey='') {

        if(!empty($colleg_id)) {
            $usersID = User::
            whereIn('college_id', $colleg_id)
                ->where(function($query) use ($searchKey) {
                    $query->where('first_name', 'LIKE', "%$searchKey%");
                    $query->orWhere(function ($query) use ($searchKey) {
                        $query->where('last_name', 'LIKE', "%$searchKey%");
                    });
                    $query->orWhere(function ($query) use ($searchKey) {
                        $query->where('address', 'LIKE', "%$searchKey%");
                    });
                    $query->orWhere(function ($query) use ($searchKey) {
                        $query->where('email', 'LIKE', "%$searchKey%");
                    });
                    $query->orWhere(function ($query) use ($searchKey) {
                        $query->where('enrollment_no', 'LIKE', "%$searchKey%");
                    });
                    $query->orWhere(function ($query) use ($searchKey) {
                        $query->where('mobile_no', 'LIKE', "%$searchKey%");
                    });
                })
                ->pluck('id')->toArray();
        }
        else {
            $usersID = User::
                    where(function($query) use ($searchKey) {
                    $query->where('enrollment_no', 'LIKE', "%$searchKey%");
                    $query->orWhere(function ($query) use ($searchKey) {
                        $query->where('last_name', 'LIKE', "%$searchKey%");
                    });
                    $query->orWhere(function ($query) use ($searchKey) {
                        $query->where('email', 'LIKE', "%$searchKey%");
                    });
                    $query->orWhere(function ($query) use ($searchKey) {
                        $query->where('first_name', 'LIKE', "%$searchKey%");
                    });
                })
                ->pluck('id')->toArray();
        }
        return $usersID;
    }

    public  function  getExamUsersListsByStatus($exam_id,$usersID='',$exam_date='',$status='',$marks='', $eligibleForSecondRound = '') {

        if(!empty($exam_id)) {

            $examusers = ExamUser::with(['kpiData','users.university','users.college', 'users.opening', 'users.applystream'])
                        ->where('exam_id',$exam_id);
            if(!empty($usersID)) {
                $examusers->orWhereIn('user_id', $usersID);
            }
            if(!empty($exam_date)) {
                $examusers->whereDate('started_at', '=', $exam_date);
            }
            if(!empty($status)) {
                $examusers->orWhere('status', $status);
            }
            if(!empty($marks)) {
                $examusers->orWhere('total_correct_answers','>=', $marks);
            }

            return $examusers->latest()->get();
        }
        else {
            return '';
        }
    }

    public function examuserslist($id,Request $request){

    //        'exam_date'
    //        filter
        $input = $request->only('openings','college_id','marks','stream','eligible_filter','query','exam_status', 'practical_status',
        'firstlastname', 'enrollmentno', 'mobileno', 'email', 'video_status');

        $exam_status = '';
        $practical_status = '';
        $college_id=[];
        $openings=[];
        $stream = [];
        $marks='';
        $searchKey='';
        $eligible_filter='';
        $video_status='';

        $enrollmentno = '';
        $firstlastname = '';
        $mobileno = '';
        $email = '';

//        if(!empty($input['query']['generalSearch'])) {
//            if( isset($input['query']['generalSearch']) && !empty($input['query']['generalSearch']) ) {
//                $searchKey = $input['query']['generalSearch'];
//            }
//        }

        // general search by field
        if( !empty($input['firstlastname']) ) {
            $firstlastname = $input['firstlastname'];
        }
        if( !empty($input['enrollmentno']) ) {
            $enrollmentno = $input['enrollmentno'];
        }
        if( !empty($input['mobileno']) ) {
            $mobileno = $input['mobileno'];
        }
        if( !empty($input['email']) ) {
            $email = $input['email'];
        }


        if( !empty($input['college_id']) ) {
            $college_id = $input['college_id'];
        }
        if(!empty($input['openings'])) {
            $openings = $input['openings'];
        }
        if(!empty($input['stream'])) {
            $stream = $input['stream'];
        }
        if(!empty($input['exam_status'])) {
            $exam_status = $input['exam_status'];
        }
        if(isset($input['marks']) && $input['marks'] != null) {
            $marks = $input['marks'];
        }
        if(isset($input['practical_status']) && $input['practical_status'] != null) {
            $practical_status = $input['practical_status'];
        }
        if(isset($input['eligible_filter']) && $input['eligible_filter'] != null) {
            $eligible_filter = $input['eligible_filter'];
        }
        if(isset($input['video_status']) && $input['video_status'] != null) {
            $video_status = $input['video_status'];
        }

        $usersID = User::where(function($query) use ($searchKey, $openings,$college_id, $stream, $firstlastname, $enrollmentno, $mobileno, $email) {

            if (!empty($openings)) {
                $query->whereIn('openings', $openings);
            }
            if (!empty($college_id)) {
                $query->whereIn('college_id', $college_id);
            }
            if (!empty($stream)) {
                $query->whereIn('education_stream', $stream);
            }

            if (isset($firstlastname) && $firstlastname != null) {
                $searchValues = preg_split('/\s+/', $firstlastname, -1, PREG_SPLIT_NO_EMPTY);
                $query->where(function ($rr) use ($searchValues) {
                    foreach ($searchValues as $key => $value) {
                        $rr->where('first_name', 'LIKE', "%$value%")
                           ->orWhere('last_name', 'LIKE', "%$value%");
                    }
                });
            }

            if (isset($enrollmentno) && $enrollmentno != null) {
                $query->where('enrollment_no', $enrollmentno);
            }

            if (isset($mobileno) && $mobileno != null) {
                $query->where('mobile_no', $mobileno);
            }

            if (isset($email) && $email != null) {
                $query->where('email', 'LIKE', "%$email%");
            }

            if (!empty($searchKey)) {
//                $searchStringToArray = explode(' ', $searchKey);
//                $query->where('first_name', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]')
//                        ->where('last_name', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]')
//                        ->where('address', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]')
//                        ->where('email', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]')
//                        ->where('enrollment_no', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]')
//                        ->where('mobile_no', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]');

//                $query->where(function ($subQuery) use ($searchKey, $searchStringToArray) {
//                $query->orWhere('first_name', 'LIKE', "%$firstlastname%")
//                    ->orWhere('last_name', 'LIKE', "%$firstlastname%")
                    // ->orWhere('address', 'LIKE', "%$filterWord%")
//                    ->orWhere('email', 'LIKE', "%$filterWord%")
//                    ->orWhere('enrollment_no', 'LIKE', "%$enrollmentno%")
//                    ->orWhere('mobile_no', 'LIKE', "%$filterWord%");
//                    foreach ($searchStringToArray as $word) {
//                        $filterWord = str_replace(' ', '', $word);
//
//                    }
//                });

            }

        })
        ->pluck('id')->toArray();
//        dd($);

        $examusers = ExamUser::with([
            'kpiData', 'users.university',
            'users.college', 'users.opening',
            'practical',
            'users.applystream']
        )->where('exam_id', $id);

//        if (isset($practical_status) && $practical_status != null) {
//            $examusers->whereHas('practical', function ($hasQuery) use ($practical_status) {
//                $hasQuery->where('status', $practical_status);
//            });
//        }

        if (isset($practical_status) && $practical_status != null && $practical_status != 'notStarted') {
            $examusers->whereHas('practical', function ($hasQuery) use ($practical_status) {
                $hasQuery->where('status', $practical_status);
            });
        }
        if (isset($practical_status) && $practical_status != null && $practical_status == 'notStarted') {
            $examusers->where('practical_id', null);
        }

        if (isset($exam_status) && $exam_status != null) {
            $examusers->where('status', $exam_status);
        }
        if (isset($eligible_filter) && $eligible_filter != null) {
            $examusers->where('eligibal', $eligible_filter);
        }

        if (isset($video_status) && $video_status != null) {
            $examusers->whereHas('practical', function ($p) use ($video_status) {
                $p->where('video_status', $video_status);
            });
        }

        if (isset($marks) && $marks != null) {
            $examusers->where('total_correct_answers', '>=', $marks);
        }

//        if (!empty($usersID)) {
//            $examusers->whereIn('user_id', $usersID);
//        }
        $examusers->whereIn('user_id', $usersID);


        $examUsersData =  $examusers->latest()->get();

       $filteredResponse = $examUsersData->filter(function ($item) {

           $item->first_name = $item->users->first_name.' '.$item->users->last_name;
           $item->university = $item->users->university->name;
           $item->college = $item->users->college->name;
           $item->enrollment_no = $item->users->enrollment_no;
           $item->exam_date = date("d/m/Y",strtotime($item->started_at));
           $item->percentage = ($item->percentage != '') ? number_format($item->percentage,2)  : '';
           return $item;
       });

        return response()->json(['status' => 'success',"draw"=>1, 'data' => $filteredResponse, 'recordsFiltered' => $filteredResponse->count(),'recordsTotal'=>$filteredResponse->count()], 200);

    }

    public function examUsersMarks(Request $request){

        $input = $request->only('exam_id','user_id');


        $filledAnswers = QuestionAnswer::where('exam_users_id', $input['user_id'])->where('exam_id',$input['exam_id'])->get();
        $correctAnswer =[];
        foreach ($filledAnswers as $answer) {
            $correct = QuestionOption::where(['question_id' => $answer->question, 'is_correct' => 1])->first();
            if ($correct->id == $answer->answer) {
                $correctAnswer[] = $answer;
            }

        }
       $questionIds = collect($correctAnswer)->pluck('question')->toArray();

        $exam_phases_data = ExamPhase::with('category')->where('exam_id',$input['exam_id'])->groupBy('category_id')->get();

        $questionAnswer = QuestionAnswer::where('exam_users_id', $input['user_id'])->where('exam_id',$input['exam_id'])->get();
        $question = $questionAnswer->pluck('question');

        if(!empty($exam_phases_data)) {
            $exam_phases = $exam_phases_data->filter(function ($item) use($questionIds,$question) {

                 $item->total_correct = Question::where('category_id',$item->category_id)->whereIn("id",$questionIds)->count();
                 $item->total_question = Question::where('category_id',$item->category_id)->whereIn("id",$question)->count();
                return $item;
             });
        }

        return view('examusers_marks',compact('exam_phases'));
    }

    public function getCategoryCount($exam_id,$user_id){

        $filledAnswers = QuestionAnswer::where('exam_users_id', $user_id)->where('exam_id',$exam_id)->get();
        $correctAnswer =[];
        foreach ($filledAnswers as $answer) {
            $correct = QuestionOption::where(['question_id' => $answer->question, 'is_correct' => 1])->first();
            if ($correct->id == $answer->answer) {
                $correctAnswer[] = $answer;
            }

        }
        $questionIds = collect($correctAnswer)->pluck('question')->toArray();

        $exam_phases_data = ExamPhase::with('category')->where('exam_id',$exam_id)->groupBy('category_id')->get();

        $questionAnswer = QuestionAnswer::where('exam_users_id',$user_id)->where('exam_id',$exam_id)->get();
        $question = $questionAnswer->pluck('question');

        if(!empty($exam_phases_data)) {
            $exam_phases = $exam_phases_data->filter(function ($item) use($questionIds,$question) {
                $item->cat_name = Category::where("id",$item->category_id)->value('name');
                $item->total_correct = Question::where('category_id',$item->category_id)->whereIn("id",$questionIds)->count();
                $item->total_question = Question::where('category_id',$item->category_id)->whereIn("id",$question)->count();
                return $item;
            });
        }
        return $exam_phases;

    }

    public function exportStudent($id,Request $request) {

        $input = $request->only('openings','college_id','marks','stream','eligible_filter','query','exam_status', 'practical_status',
        'firstlastname', 'enrollmentno', 'mobileno', 'email', 'video_status');

        $exam_status = '';
        $practical_status = '';
        $college_id=[];
        $openings=[];
        $stream = [];
        $marks='';
        $searchKey='';
        $eligible_filter='';
        $video_status='';

        $enrollmentno = '';
        $firstlastname = '';
        $mobileno = '';
        $email = '';

//        if(!empty($input['query']['generalSearch'])) {
//            if( isset($input['query']['generalSearch']) && !empty($input['query']['generalSearch']) ) {
//                $searchKey = $input['query']['generalSearch'];
//            }
//        }

        // general search by field
        if( !empty($input['firstlastname']) ) {
            $firstlastname = $input['firstlastname'];
        }
        if( !empty($input['enrollmentno']) ) {
            $enrollmentno = $input['enrollmentno'];
        }
        if( !empty($input['mobileno']) ) {
            $mobileno = $input['mobileno'];
        }
        if( !empty($input['email']) ) {
            $email = $input['email'];
        }


        if( !empty($input['college_id']) ) {
            $college_id = $input['college_id'];
        }
        if(!empty($input['openings'])) {
            $openings = $input['openings'];
        }
        if(!empty($input['stream'])) {
            $stream = $input['stream'];
        }
        if(!empty($input['exam_status'])) {
            $exam_status = $input['exam_status'];
        }
        if(isset($input['marks']) && $input['marks'] != null) {
            $marks = $input['marks'];
        }
        if(isset($input['practical_status']) && $input['practical_status'] != null) {
            $practical_status = $input['practical_status'];
        }
        if(isset($input['eligible_filter']) && $input['eligible_filter'] != null) {
            $eligible_filter = $input['eligible_filter'];
        }
        if(isset($input['video_status']) && $input['video_status'] != null) {
            $video_status = $input['video_status'];
        }

        $usersID = User::where(function($query) use ($searchKey, $openings,$college_id, $stream, $firstlastname, $enrollmentno, $mobileno, $email) {

            if (!empty($openings)) {
                $query->whereIn('openings', $openings);
            }
            if (!empty($college_id)) {
                $query->whereIn('college_id', $college_id);
            }
            if (!empty($stream)) {
                $query->whereIn('education_stream', $stream);
            }

            if (isset($firstlastname) && $firstlastname != null) {
               $searchValues = preg_split('/\s+/', $firstlastname, -1, PREG_SPLIT_NO_EMPTY);
               $query->where(function ($rr) use ($searchValues) {
                   foreach ($searchValues as $key => $value) {
                       $rr->where('first_name', 'LIKE', "%$value%")
                          ->orWhere('last_name', 'LIKE', "%$value%");
                   }
               });
           }

            if (isset($enrollmentno) && $enrollmentno != null) {
                $query->where('enrollment_no', $enrollmentno);
            }

            if (isset($mobileno) && $mobileno != null) {
                $query->where('mobile_no', $mobileno);
            }

            if (isset($email) && $email != null) {
                $query->where('email', 'LIKE', "%$email%");
            }

            if (!empty($searchKey)) {
//                $searchStringToArray = explode(' ', $searchKey);
//                $query->where('first_name', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]')
//                        ->where('last_name', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]')
//                        ->where('address', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]')
//                        ->where('email', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]')
//                        ->where('enrollment_no', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]')
//                        ->where('mobile_no', 'RLIKE', '[[:<:]]'.$searchKey.'[[:>:]]');

//                $query->where(function ($subQuery) use ($searchKey, $searchStringToArray) {
//                $query->orWhere('first_name', 'LIKE', "%$firstlastname%")
//                    ->orWhere('last_name', 'LIKE', "%$firstlastname%")
                    // ->orWhere('address', 'LIKE', "%$filterWord%")
//                    ->orWhere('email', 'LIKE', "%$filterWord%")
//                    ->orWhere('enrollment_no', 'LIKE', "%$enrollmentno%")
//                    ->orWhere('mobile_no', 'LIKE', "%$filterWord%");
//                    foreach ($searchStringToArray as $word) {
//                        $filterWord = str_replace(' ', '', $word);
//
//                    }
//                });

            }

        })
        ->pluck('id')->toArray();

        $examusers = ExamUser::with([
            'kpiData', 'users.university',
            'users.college', 'users.opening',
            'practical',
            'users.applystream']
        )->where('exam_id', $id);

        if (isset($practical_status) && $practical_status != null && $practical_status != 'notStarted') {
            $examusers->whereHas('practical', function ($hasQuery) use ($practical_status) {
                $hasQuery->where('status', $practical_status);
            });
        }
        if (isset($practical_status) && $practical_status != null && $practical_status == 'notStarted') {
            $examusers->where('practical_id', null);
        }

        if (isset($exam_status) && $exam_status != null) {
            $examusers->where('status', $exam_status);
        }
        if (isset($eligible_filter) && $eligible_filter != null) {
            $examusers->where('eligibal', $eligible_filter);
        }

        if (isset($video_status) && $video_status != null) {
            $examusers->whereHas('practical', function ($p) use ($video_status) {
                $p->where('video_status', $video_status);
            });
        }

        if (isset($marks) && $marks != null) {
            $examusers->where('total_correct_answers', '>=', $marks);
        }

//        if (!empty($usersID)) {
//            $examusers->whereIn('user_id', $usersID);
//        }
        $examusers->whereIn('user_id', $usersID);


        $examUsersData =  $examusers->latest()->get();


        //$examusers = ExamUser::with('users','exam')->where('exam_id',$id)->get();
        $examUsersLists =[];
        if(!empty($examUsersData)) {
            $examUsersLists = $examUsersData->filter(function ($item) {
                $item->categories = $this->getCategoryCount($item->exam_id,$item->user_id);
                $item->percentage = (!empty($item->percentage))? $item->percentage.'%' :'';
                return $item;
            });
        }

        return Excel::download(new ExamUserExport($examUsersLists), 'examuser.xlsx');
    }

    public function getCategoryDiff(Request $request) {
        $data = $request->all();
        if(!empty($data['catid'])) {
            $category = Category::with('questions')->where('id',$data['catid'])->where('status', '1')->get();

            $categories = $category->filter(function ($item){
                if($item->questions->count() > 0 ) {
                    $item->questions_count = $item->questions->count();
                    $questions = collect($item->questions)->groupBy('difficulty');
                    foreach ($questions as $qkey=>$question) {
                        $item[$qkey] = $question->count();
                    }

                }
                return $item;
            });
            $categories = $categories[0];
            $difficulty = ['Easy','Medium','Hard'];
            return view('category_difficulty',compact('difficulty','categories'));

        }
    }

    public function eligibalValue(Request $request) {
        $id = $request->id;
        ExamUser::find($id)->update(['eligibal'=>$request->status]);
        return $id;
    }

    public function userDetail(Request $request) {
        $input = $request->only('exam_id','user_id');
        $user = User::find($input['user_id']);
        return view('user_detail',compact('user'));
    }

    public function examTime(Request $request) {
        $input = $request->only('exam_id','user_id');
        $user = ExamUser::with('practical')->where('user_id',$input['user_id'])->first();
        return view('exam_time',compact('user'));
    }

    // this is for exam user answer sheet
    public function questionDetail(Request $request) {
        $input = $request->only('exam_id','user_id','category_id');
        $questions = QuestionAnswer::with('questions')->where('exam_users_id', $input['user_id'])->where('exam_id', $input['exam_id'])->get();
        $j = 0;
        if(session()->has('category_id') || $input['category_id'] == '0') {
            $id = session()->get('category_id');
            session()->forget('category_id');
        } else {
            $id = $input['category_id'];
        }
        foreach($questions as $question) {
            if($question->questions->category_id == $id) {
                $currentQuestion = strip_tags($question->questions->title);
                $data['question'] = $currentQuestion;
                $options = QuestionOption::where('question_id',$question->questions->id)->get();
                if($question->answer != null) {
                    $answer = QuestionOption::where('id',$question->answer)->pluck('content');
                    $answer = strip_tags($answer[0]);
                    $data['answerId'] = $question->answer;
                    $data['answer'] = $answer;
                } else {
                    $data['answerId'] = 0;
                    $data['answer'] = '-';
                }
                $optionArray = [];
                foreach($options as $option) {
                    if($option->is_correct == '1') {
                        $optionArray[0] = $option->id;
                    }
                }
                foreach($options as $option) {
                    $optionArray[$option->id] = strip_tags($option->content);
                }
                $data['option'] = $optionArray;

                $finalData[$j] = $data;
                $j++;
            }

        }
        return view('answer_sheet',compact('finalData'));
    }
    // this is for exam user answer sheet
    public function categoryDetail(Request $request) {
        $input = $request->only('exam_id');
        $categories = ExamPhase::with('category')->where('exam_id', $input['exam_id'])->groupBy('category_id')->get();
        session()->put('category_id',$categories[0]->category->id);
        return view('exam_category', compact('categories'));
    }

    // return practical data
    public function getPracticalData(Request $request)
    {
        $input = $request->only('exam_user_id');
        $exam_user_id = $input['exam_user_id'];
        $examUser = ExamUser::where('id', $exam_user_id)->first();
        $openings = User::where('id',$examUser->user_id)->first();
        $practical = Practical::where('id', $examUser->practical_id)->first();
        return view('practical_detail',compact('practical', 'examUser','openings'));
    }

    public function UpdatePracticalComment(Request $request)
    {
        $input = $request->only('practical_id');
        $practical_id = $input['practical_id'];
        $practical = Practical::where('id', $practical_id)->first();
        $practical->update([
            'comment_1' => $request->comment_1,
            'comment_2' => $request->comment_2,
            'comment_3' => $request->comment_3,
            'comment_4' => $request->comment_4,
            'comment_5' => $request->comment_5,
            'comment_6' => $request->comment_6,
            'comment_7' => $request->comment_7,
        ]);
    }

    public function UpdatePracticalStatus(Request $request)
    {
        $input = $request->only('practical_id', 'value');
        $practical_id = $input['practical_id'];
        $practical = Practical::where('id', $practical_id)->first();
        $practical->update([
            'status' => $input['value'] == 'true' ? 'completed' : 'on_going'
        ]);
    }

    public function getNotesData(Request $request)
    {
        $input = $request->only('exam_user_id');
        $exam_user_id = $input['exam_user_id'];
        $examUser = ExamUser::where('id', $exam_user_id)->first();
        return response()->json(['data' => [
                'pi_notes' => $examUser->pi_notes,
                'hr_notes' => $examUser->hr_notes
        ]], 200);
    }

    public function updateNotesData(Request $request)
    {
        $input = $request->only('exam_user_id', 'piNotes','hrNotes');
        $exam_user_id = $input['exam_user_id'];
        $examUser = ExamUser::where('id', $exam_user_id)->first();
        $examUser->update([
            'pi_notes' => $input['piNotes'],
           'hr_notes' => $input['hrNotes']
        ]);
    }

    public function getKpiData(Request $request) {
        $input = $request->only('exam_user_id');
        $exam_user_id = $input['exam_user_id'];
        $examUser = ExamUser::where('id', $exam_user_id)->first();


        $kpiUserData = ExamUserKpi::where('exam_user_id', $exam_user_id)->first();
        if (empty($kpiUserData)) {
            $kpiData = \App\Kpi::where('opening_id', $examUser->users->openings)->first();
            $kpiUserData = \App\KpiValue::select('name', 'weightage')->where('kpi_id', $kpiData->id)->get();
            $kpiUserData = $kpiUserData->toArray();
            $kpiUserData = collect($kpiUserData)->map(function ($item) {
                $item['user_score'] = 10;
                $item['notes'] = '';
                return $item;
            })->toArray();
        } else {
            $kpiUserData = $kpiUserData->kpi_data;
        }
//        $data = ExamUserKpi::create([
//            'exam_user_id' => 44,
//            'kpi_data' => $kpiUserData
//        ]);
        return view('kpi_detail',compact('kpiUserData', 'exam_user_id'));
    }

    public function updateKpiData(Request $request)
    {
        $input = $request->only('exam_user_id');
        $exam_user_id = $input['exam_user_id'];
        $kpiUserData = ExamUserKpi::where('exam_user_id', $exam_user_id)->first();
        if (!empty($kpiUserData)) {
            $kpiUserData->update([
                'kpi_data' => $request->kpiData['kpiData']
            ]);
        } else {
            ExamUserKpi::create([
                'exam_user_id' => $exam_user_id,
                'kpi_data'     => $request->kpiData['kpiData']
            ]);
        }

        $kpiUserData = ExamUserKpi::where('exam_user_id', $exam_user_id)->first();
        $kpiUserData = $kpiUserData->kpi_data;
        return view('kpi_detail',compact('kpiUserData', 'exam_user_id'));
    }

    // change the practical status
    public function statusChange(Request $request) {
        $data = Exam::find($request->id)->update(['status'=>$request->status]);
        return response()->json(['status' => 'success'], 200);
    }



}
