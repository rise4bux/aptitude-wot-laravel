<?php

namespace App\Http\Middleware;

use App\Exam;
use App\ExamUser;
use Closure;
use Illuminate\Support\Facades\Session;   

class StartAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $slug = $request->slug;
        //dd(auth()->id());
        if(Session::get('exam_status')) {
            Session::forget('exam_status');
            return redirect(route('exam.finish'));
        }
        if (auth()->id() == null) {
            return redirect(route('candidate.register'));
        } else {
            $exam = Exam::where('link', $slug)->where('status',1)->first();
            if(empty($exam)) {
                // return false;
                return redirect()->route('welcome');
            }
            $status = ExamUser::where(['exam_id' => $exam->id, 'user_id' => auth()->id()])->value('status');
            if ($status == "OnGoing") {
              return $next($request);
            } elseif ($status == 'Completed') {
              return redirect(route('exam.finish'));
            } else {
                return $next($request);
            }
        }
    }
}
