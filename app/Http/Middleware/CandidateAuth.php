<?php

namespace App\Http\Middleware;

use App\Exam;
use App\ExamUser;
use Closure;
use Session;
class CandidateAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $slug=$request->slug;

        if(auth()->id()==null){
            return $next($request);
        }
        else{
            $exam=Exam::where('link',$slug)->where('status',1)->first();

            if(empty($exam)) {

                return redirect()->route('welcome');
            }
            else {
                $status=ExamUser::where(['exam_id'=>$exam->id,'user_id'=>auth()->id()])->value('status');
                if($status) {
                    if($status=="OnGoing"){
                        session(['exam' =>$slug]);
                        return  redirect()->route('candidate.instruction');
                    }
                    elseif($status=='Completed') {
                        return redirect(route('exam.finish'));
                    }
                    else{
                        return $next($request);
                    }
                }
                else {
                    return redirect()->route('welcome');
                }

            }

        }
    }
}
