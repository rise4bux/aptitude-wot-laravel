<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PracticalQuestion extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'defination', 'status', 'opening_id', 'difficulty', 'predefined_answer',
    ];

    public function opening() {
        return $this->belongsTo(OpeningMasters::class,'opening_id');
    }
}
