<?php

namespace App\Exports;

use App\ExamUser;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ExamUserExport implements  FromView,WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    /*public function collection()
    {
        return ExamUser::all();
    }*/
    public function view() : View
    {

        return view('exam_export_users', [
            'data' => $this->data
        ]);
    }
    /*public function headings(): array
    {
        return [
            '#',
            'User',
            'University',
            'College',
            'Enrollment No',
            'Email',
            'Total Questions',
            'Correct Answer',
            'Percentage',
            'Status',
            'Future Study',
            'Primary Interest',
            'Secondary Interest',
            'Other Interest',
        ];
    }
    public function map($euser): array
    {
        return [
            $euser->id,
            $euser->users->first_name,
            $euser->users->university->name,
            $euser->users->college->name,
            $euser->users->enrollment_no,
            $euser->users->email,
            $euser->total_questions,
            $euser->total_correct_answers,
            $euser->total_correct_answers,
            $euser->percentage,
            $euser->status,
            $euser->users->future_studies,
            $euser->users->primary_interest,
            $euser->users->secondary_interest,
            $euser->users->other_interest,

        ];
    }
    public function query()
    {
        //return Invoice::query()->whereYear('created_at', $this->year);
        //return (new InvoicesExport(2018))->download('invoices.xlsx');

    }*/
    public function columnFormats(): array
    {

        return [
//            'G' => '0%',

        ];
    }


}
