<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;
    use LaratrustUserTrait;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'remember_token',
    ];

    public function exams(){
        return $this->hasOne('App\ExamUser','user_id');
    }
    public function college(){
        return $this->belongsTo('App\Models\College','college_id');
    }
    public function university(){
        return $this->belongsTo('App\Models\University','university_id');
    }
    public function p_interest(){
        return $this->belongsTo('App\Models\Technology','primary_interest');
    }
    public function s_interest(){
        return $this->belongsTo('App\Models\Technology','secondary_interest');
    }
    public function applystream(){
        return $this->belongsTo('App\EducationStreamMasters','education_stream');
    }

    public function opening(){
        return $this->belongsTo(OpeningMasters::class,'openings');
    }

    public function educationstream() {
        return $this->belongsTo(EducationStreamMasters::class, 'education_stream');
    }
    /*public static function boot() {
        parent::boot();

        static::deleting(function($user) { // before delete() method call this
            $user->exams()->delete();
            // do the rest of the cleanup...
        });
    }*/
}
