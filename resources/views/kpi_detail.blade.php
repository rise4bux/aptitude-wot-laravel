<button class="btn btn-primary" id="updateKpiData" exam-user-id="{{$exam_user_id}}" style="display: none">Update</button>
<form id="kpiDataForm">
<table class="table table-responsive  table-striped">
    <thead>
        <tr>
            <td style="width: 200px">Name</td>
            <td style="width: 100px">User Score <br> <small>(Out of 10)</small></td>
            <td align="right" style="width: 80px">Weightage</td>
            <td align="right" style="width: 80px">Calculated Score</td>
            <td  style="width: 400px" align="center">Notes</td>
        </tr>
    </thead>
    <tbody>
        @foreach($kpiUserData as $key => $k)
        <tr class="kpiRow">
            <td>{{$k['name']}}</td>
            <input type="hidden" name="kpiData[{{$key}}][name]" value="{{$k['name']}}">
            <td style="width: 100px"><input type="text" step="0.1" min="0" max="10" name="kpiData[{{$key}}][user_score]"
                                            oninput="(!validity.rangeOverflow||(value=10)) && (!validity.rangeUnderflow||(value=1)) &&(!validity.stepMismatch||(value=parseInt(this.value)));"
                                            value="{{$k['user_score']}}" class="form-control userScoreInput" ></td>
            <td align="right" style="width: 80px">{{$k['weightage']}}</td>
            <td align="right" style="width: 80px">{{round((float)$k['user_score'] * ((float)$k['weightage'] / 100), 2) }}</td>
            <td><input type="text" name="kpiData[{{$key}}][notes]" value="{{isset($k['notes']) ?
            $k['notes'] : ''}}" class="form-control userScoreInput" ></td>
            <input type="hidden" name="kpiData[{{$key}}][weightage]" value="{{$k['weightage']}}">
        </tr>
        @endforeach
        @php
            $averageScore = 0;
            foreach($kpiUserData as $key => $k) {
                $averageScore = $averageScore + round((float)$k['user_score'] * ((float)$k['weightage'] / 100), 2);
            }
        @endphp
        <tr class="kpiRow">
            <td></td>
            <td></td>
            <td  align="right" style="width: 150px"><b>Average Score</b></td>
            <td align="right"><b>{{$averageScore}}</b></td>
        </tr>
    </tbody>
</table>
</form>
