@extends('layouts.app')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">

        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <a href="{{route('home')}}">
                                <h3 class="m-subheader__title m-subheader__title--separator">
                                    Dashboard
                                </h3>
                            </a>
                            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                <li class="m-nav__item ">
                                    <a href="{{route('exams.index')}}" class="m-nav__link">
												<span class="m-nav__link-text">
										Exam
												</span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
												<span class="m-nav__link-text">
										>
												</span>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{route('exams.add')}}" class="m-nav__link">
												<span class="m-nav__link-text">
											{{!empty($exam) ? 'Edit' : 'Add'}} Exam

												</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                     id="kt_content" style="padding-top:0">
                    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                        <div class="kt-portlet kt-portlet--mobile">
                            <div class="kt-portlet__head kt-portlet__head--lg">
                                <div class="kt-portlet__head-label">
                                                    <span class="kt-portlet__head-icon">
                                                        <i class="la la-check-square"></i>
                                                    </span>
                                    <h3 class="kt-portlet__head-title">
                                        {{!empty($exam) ? 'Edit' : 'Add'}} Exam
                                    </h3>

                                </div>

                                <div class="kt-portlet__head-label">
                                    <button type="button" class="btn btn-sm btn-dark" id="statistics"
                                            data-toggle="modal" data-target="#kt_modal_KTDatatable_remote">
                                        Questions/Categories Statistics
                                    </button>
                                </div>
                            </div>
                            <div class="kt-portlet--tabs row" style="justify-content: center">

                                <div class="card-block p-4 col-sm-6">
                                    @if(count($categories) <= 0)
                                        <div class="alert alert-warning" role="alert">
                                            You have to add category first! <a href="{{route('category.list')}}" class="m-link" >&nbsp; Click here to add.</a>
                                        </div>
                                    @endif

                                    <form class="kt-form row" id="addform"
                                          action="{{!empty($exam) ? route('exams.update',$exam->id) : route('exams.save')}}"
                                          method="post">
                                        @csrf
                                        <div class="row"
                                             style="background-color: #f1f1f1; padding: 10px;border-radius: 10px; border-bottom:1px solid #ebedf3; ">
                                            <div class="col-sm-12 form-group">
                                                <label class="form-check-label" for="title">Title</label>
                                                <input name="name" class="form-control" type="text"
                                                       value="{{!empty($exam) ? $exam->name : old('name')}}">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-sm-12 form-group">
                                                <label class="form-check-label" for="description">Description</label>
                                                <input name="description" class="form-control" type="text"
                                                       id="description"
                                                       value="{{!empty($exam) ? $exam->description : old('description')}}">
                                                @error('description')
                                                <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-sm-12 form-group">
                                                <label class="form-check-label" for="description">Instruction</label>
                                               {{-- <input id="instruction" @if($exam!==null) value="{{$exam->instruction}}"
                                                       @else value="{{old('instruction')}}" @endif type="hidden"
                                                       name="instruction">
                                                <trix-editor input="instruction"
                                                             style="background-color: #fff !important;"></trix-editor>--}}
                                                <textarea name="instruction" class="instructionEditor" rows="10">{!! (isset($exam)) ? $exam->instruction :$instruction_text !!}</textarea>
                                                @error('instruction')
                                                <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-check-label" for="time">Time</label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control " id="time" name="time"
                                                           value="{{!empty($exam) ? $exam->time : old('time')}}" min="1">
                                                    <span class="input-group-btn btn btn-default" style="color:black">Minutes</span>
                                                </div>
                                                @error('time')
                                                <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label class="form-check-label" for="status">Status</label>
                                                <select name="status" class="form-control" id="status">
                                                    <option value=1 @if($exam!==null && $exam->status==1) selected
                                                            @endif @if(old('status')==1) selected @endif>Active
                                                    </option>
                                                    <option value=0
                                                            @if($exam!==null && $exam->status==0) selected @endif >
                                                        Inactive
                                                    </option>
                                                </select>
                                                @error('status')
                                                <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="kt-portlet__head kt-portlet__head--lg"
                                                 style="padding: 0 !important; border: none ;    border-top: 1px solid #ebedf3;">
                                                <div class="kt-portlet__head-label">
                                                    <label class="form-check-label" for="status" style="font-weight: 400; font-size: 16px">Phases</label>
                                                </div>
                                                <div class="kt-portlet__head-toolbar">
                                                    <div class="kt-portlet__head-wrapper">
                                                        <div class="">
                                                            <button type="button" class="btn btn-sm btn-dark"
                                                                    id="addPhaseButton">
                                                                Add phase
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @error('phases')
                                            <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                            @enderror

                                        </div>
                                        <div id="phaseContainer" class="col-sm-12">
                                            @if($exam!==null)
                                                @foreach($exam->phases as $phase)
                                                    <div class="form-group row"
                                                         style="margin-right: 0; margin-left: 0; background-color: #f1f1f1; padding: 10px;border-radius: 10px;">
                                                        <div class="col-sm-4">
                                                            <label class="form-check-label">Category</label>
                                                            <select name="phases[{{$loop->iteration-1}}][category]"
                                                                    class="form-control category questionCategory">
                                                                @foreach($categories as $cat)
                                                                    @if($cat->id==$phase->category_id) @php $selected_cat = $cat; @endphp @endif
                                                                    <option value="{{$cat->id}}"
                                                                            @if($cat->id==$phase->category_id) selected @endif>{{$cat->name}}
                                                                            @if($cat->questions)
                                                                            {{ ' ('.$cat->questions->count().')' }}
                                                                            @endif
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label class="form-check-label">Difficulty</label>
                                                            <select name="phases[{{$loop->iteration-1}}][difficulty]"
                                                                    class="form-control difficulty difficultySelect">
                                                                @foreach($difficulty as $diff)
                                                                    <option class="{{$phase->difficulty}}" value="{{$diff}}" @if($phase->difficulty == $diff) selected @endif>{{$diff}}
                                                                        @if($selected_cat[$diff] )
                                                                            {{ '('.$selected_cat[$diff].')' }}
                                                                        @endif
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label class="form-check-label">Question count</label>
                                                            <input name="phases[{{$loop->iteration-1}}][count]"
                                                                   type="number" value="{{$phase->question_count}}"
                                                                   class="form-control count" min="1">
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <button type="button"
                                                                    class="btn btn-sm btn-clean btn-icon btn-icon-md active phaseRemove"><i class="la la-trash"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                @endforeach

                                            @else
                                                <div class="row form-group"
                                                     style="margin-right: 0; margin-left: 0; background-color: #f1f1f1; padding: 10px;border-radius: 10px;">
                                                    <div class="col-sm-4">
                                                        <label class="form-check-label">Category</label>
                                                        <select class="form-control category questionCategory" name="phases[0][category]">
                                                            @foreach($categories as $ckey=>$cat)
                                                                @if($ckey == 0) @php $selected_cat = $cat; @endphp @endif
                                                                <option value="{{$cat->id}}">{{$cat->name}}
                                                                    @if($cat->questions)
                                                                        {{ ' ('.$cat->questions->count().')' }}
                                                                    @endif</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label class="form-check-label">Difficulty</label>
                                                        <select class="form-control difficulty difficultySelect" name="phases[0][difficulty]">
                                                            @foreach($difficulty as $diff)
                                                                <option value="{{$diff}}">{{$diff}}
                                                                @if($selected_cat[$diff] )
                                                                    {{ '('.$selected_cat[$diff].')' }}
                                                                @endif
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label class="form-check-label" >Question count</label>
                                                        <input type="number" class="form-control count" name="phases[0][count]" min="1">
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-sm-12 form-group">
                                            <div class="">
                                                <label class="form-check-label">Exam Password</label>
                                                <input type="password" name="exam_password" class="form-control" value="{!! (isset($exam))? $exam->exam_password : '' !!}" id="password">
                                                <span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-password" style="position: relative; top: -28px;left: 430px;"></span>
                                            </div>
                                        </div>

                                        @if(count($categories) > 0)
                                           <div class="row form-group">
                                            <div class="col-sm-12 clearfix">
                                                <button type="submit" class="btn btn-primary">{{!empty($exam) ? 'Update' : 'Save'}}</button>&nbsp;&nbsp;&nbsp;
                                                <a href="{{route('exams.index')}}" class="btn btn-secondary float-right"> Cancel</a>
                                            </div>
                                        </div>

                                        @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-1" id="hiddenButton" style="display:none">
        <button  type="button"
                class="btn btn-sm btn-clean btn-icon btn-icon-md active phaseRemove"><i class="la la-trash"></i>
        </button>
    </div>

    <div id="kt_modal_KTDatatable_remote" class="modal fade" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="min-height: 590px;">
                <div class="modal-header">
                    <h5 class="modal-title">
                        Statistics
                        <small>Question availability</small>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!--begin: Search Form -->
                    <table class="table table-responsive table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>
                                Category
                            </th>
                            <th>Easy</th>
                            <th>Medium</th>
                            <th>Hard</th>
                            <th>Inactive</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($counts as $stkey=>$cat)
                            <tr>
                                <td>{{$stkey}}</td>
                                <td>{{$counts[$stkey]['Easy']['active']}}</td>
                                <td>{{$counts[$stkey]['Medium']['active']}}</td>
                                <td>{{$counts[$stkey]['Hard']['active']}}</td>
                                <td>{{$counts[$stkey]['Easy']['inActive']+$counts[$stkey]['Medium']['inActive']+$counts[$stkey]['Hard']['inActive']}}</td>
                            </tr>
                        </tbody>
                        @endforeach

                    </table>
                    <!--end: Search Form -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-clean btn-bold btn-upper btn-font-sm" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row form-group" id="phaseGroup"
         style="margin-right: 0; margin-left: 0; background-color: #f1f1f1; padding: 10px;border-radius: 10px;display: none">
        <div class="col-sm-4">
            <label class="form-check-label">Category</label>
            <select class="form-control category questionCategory" name="phases[0][category]">
                @foreach($categories as $ckey=>$cat)
                    <option value="{{$cat->id}}">{{$cat->name}}
                        @if($ckey == 0) @php $selected_cat = $cat; @endphp @endif
                        @if($cat->questions)
                            {{ ' ('.$cat->questions->count().')' }}
                        @endif</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-4">
            <label class="form-check-label">Difficulty</label>
            <select class="form-control difficulty difficultySelect" name="phases[0][difficulty]">
                @foreach($difficulty as $diff)
                    <option value="{{$diff}}">{{$diff}}
                        @if($selected_cat[$diff] )
                            {{ '('.$selected_cat[$diff].')' }}
                        @endif
                    </option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-3">
            <label class="form-check-label" >Question count</label>
            <input type="number" class="form-control count" name="phases[0][count]" min="1">
        </div>
    </div>


@endsection
@section('scripts')
    <script src="{{ asset('assets/app/js/ckeditor5/build/ckeditor.js') }}"></script>

    <script>
        var AjaxUrl = '{{URL::to('/')}}';
        var question = `@php echo !empty($exam) ? $exam : null @endphp`;
        if (question !== null) {
            var count = `@php echo !empty($count) ? $count : 1 @endphp`;
        }
    </script>

    <script src="{{asset('assets/app/js/addexams.js')}}"></script>
@endsection
