<table class="table table-responsive  table-striped">
    <thead>
    <tr>
        <th>
            Category
        </th>
        <th>Total Questions</th>
        <th>Correct Answers</th>

    </tr>
    </thead>
    <tbody>

    @foreach($exam_phases as $stkey=>$cat)
        <tr>
            <td>{!! (isset($cat->category)) ? $cat->category->name : '' !!} </td>
            <td>{{ $cat->total_question }}</td>
            <td>{{ $cat->total_correct }}</td>
        </tr>

    @endforeach
    </tbody>
</table>