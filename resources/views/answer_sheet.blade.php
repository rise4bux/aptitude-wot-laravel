
<table class="table table-responsive table-striped text-left" style="padding-left: 10px">
    <tbody>
    <?php foreach($finalData as $questionData) { ?>
        <tr>
            <td style="width: 45%;">{!! htmlspecialchars_decode($questionData['question'])!!}</td>
            <td style="width: 37%;">
                <ul>
                    <?php foreach($questionData['option'] as $key=>$option) { ?>
                                <?php if($key == 0) { $number = $option; } else { if($number == $key) { ?>
                        <li style="color: green">
                            {!! htmlspecialchars_decode($option)!!}</li>
                    <?php } else { ?>
                        <li>{!! htmlspecialchars_decode($option)!!}</li>
<?php }  ?>
                                <?php } ?>
                        <?php } ?>
                </ul>
            </td>
                <?php if($questionData['answerId'] == $number) {?>
                <td style="width: 20%;color: green">{!! htmlspecialchars_decode($questionData['answer'])!!}
                <?php } else { ?>
                <td style="width: 20%;color: red">{!! htmlspecialchars_decode($questionData['answer'])!!}
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
