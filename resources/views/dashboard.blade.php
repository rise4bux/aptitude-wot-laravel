@extends('layouts.app')
<!-- END: Header -->
<!-- BEGIN: Left Aside -->
@section('content')

    <!-- END: Left Aside -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">
        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title m-subheader__title--separator">
                                Dashboard
                            </h3>
                            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                <li class="m-nav__item m-nav__item--home">
                                    <a href="#" class="m-nav__link m-nav__link--icon">
                                        <i class="m-nav__link-icon la la-home"></i>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="" class="m-nav__link">
												<span class="m-nav__link-text">
													Dashboard
												</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row no-padding forMac forlaptop">
                    <div class="col-xl-12 order-lg-2 order-xl-1 ">
                        <!--begin:: Widgets/Facts and Figures-->
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-widget17">
                                <div class="kt-widget17__stats forMobileFull">
                                    <div class="kt-widget17__items" style=" display: flex;">

                                        <div class="kt-widget17__item"
                                             style="cursor: default;-webkit-box-flex: 1;background-color: white;box-shadow: rgba(69, 65, 78, 0.06) 0px 1px 15px 1px;cursor: pointer;padding: 2rem;flex: 1 1 0%;overflow: hidden;margin: 0.3rem;transition: all 0.3s ease 0s;">
                                            <a href="{{route('user.list')}}">
                                            <span class="kt-widget17__icon">
																<i class="m-menu__link-icon la la-users"
                                                                   style="font-size: 2.6rem;"></i>
															</span>
                                            <span class="kt-widget17__subtitle" style=" display: block; margin-top: 0.75rem; font-size: 1.2rem; font-weight: 500; color: rgb(0, 0, 0);">Users
															</span>
                                        </a>
                                        </div>

                                        <div class="kt-widget17__item"
                                             style="cursor: default;-webkit-box-flex: 1;background-color: white;box-shadow: rgba(69, 65, 78, 0.06) 0px 1px 15px 1px;cursor: pointer;padding: 2rem;flex: 1 1 0%;overflow: hidden;margin: 0.3rem;transition: all 0.3s ease 0s;">
                                            <a href="{{route('exams.index')}}">
															<span class="kt-widget17__icon">
																<i class="m-menu__link-icon la 	la-check-square"
                                                                   style="font-size: 2.6rem;"></i>
																 </span>
                                            <span class="kt-widget17__subtitle" style=" display: block; margin-top: 0.75rem; font-size: 1.2rem; font-weight: 500; color: rgb(0, 0, 0);">
																Exams	</span>
                                            </a>
                                        </div>

                                        <div class="kt-widget17__item"
                                             style="cursor: default;-webkit-box-flex: 1;background-color: white;box-shadow: rgba(69, 65, 78, 0.06) 0px 1px 15px 1px;cursor: pointer;padding: 2rem;flex: 1 1 0%;overflow: hidden;margin: 0.3rem;transition: all 0.3s ease 0s;">
                                            <a href="{{route('category.list')}}">
															<span class="kt-widget17__icon">
																<i class="m-menu__link-icon la la-th-list"
                                                                   style="font-size: 2.6rem;"></i>
																 </span>
                                            <span class="kt-widget17__subtitle" style=" display: block; margin-top: 0.75rem; font-size: 1.2rem; font-weight: 500; color: rgb(0, 0, 0);">
															Categories</span>
                                            </a>
                                        </div>
                                        <div class="kt-widget17__item"
                                             style="cursor: default;-webkit-box-flex: 1;background-color: white;box-shadow: rgba(69, 65, 78, 0.06) 0px 1px 15px 1px;cursor: pointer;padding: 2rem;flex: 1 1 0%;overflow: hidden;margin: 0.3rem;transition: all 0.3s ease 0s;">
                                            <a href="{{route('questions.index')}}">
															<span class="kt-widget17__icon">
																<i class="m-menu__link-icon la la-question"
                                                                   style="font-size: 2.6rem;"></i>
																 </span>
                                            <span class="kt-widget17__subtitle" style=" display: block; margin-top: 0.75rem; font-size: 1.2rem; font-weight: 500; color: rgb(0, 0, 0);">
																Questions
															</span>
                                            </a>
                                        </div>
                                        <div class="kt-widget17__item"
                                             style="cursor: default;-webkit-box-flex: 1;background-color: white;box-shadow: rgba(69, 65, 78, 0.06) 0px 1px 15px 1px;cursor: pointer;padding: 2rem;flex: 1 1 0%;overflow: hidden;margin: 0.3rem;transition: all 0.3s ease 0s;">
                                            <a href="{{route('university.index')}}">
															<span class="kt-widget17__icon">
																<i class="m-menu__link-icon la la-university"
                                                                   style="font-size: 2.6rem;"></i>
																 </span>
                                            <span class="kt-widget17__subtitle" style=" display: block; margin-top: 0.75rem; font-size: 1.2rem; font-weight: 500; color: rgb(0, 0, 0);">
																Universities
															</span>
                                            </a>
                                        </div>
                                        <div class="kt-widget17__item"
                                             style="cursor: default;-webkit-box-flex: 1;background-color: white;box-shadow: rgba(69, 65, 78, 0.06) 0px 1px 15px 1px;cursor: pointer;padding: 2rem;flex: 1 1 0%;overflow: hidden;margin: 0.3rem;transition: all 0.3s ease 0s;">
                                            <a href="{{route('college.index')}}">
															<span class="kt-widget17__icon">
																<i class="m-menu__link-icon la la-institution"
                                                                   style="font-size: 2.6rem;"></i>
																 </span>
                                            <span class="kt-widget17__subtitle" style=" display: block; margin-top: 0.75rem; font-size: 1.2rem; font-weight: 500; color: rgb(0, 0, 0);">
																Colleges
															</span>
                                            </a>
                                        </div>
                                        <div class="kt-widget17__item"
                                             style="cursor: default;-webkit-box-flex: 1;background-color: white;box-shadow: rgba(69, 65, 78, 0.06) 0px 1px 15px 1px;cursor: pointer;padding: 2rem;flex: 1 1 0%;overflow: hidden;margin: 0.3rem;transition: all 0.3s ease 0s;">
                                            <a href="{{route('technology.index')}}">
															<span class="kt-widget17__icon">
																<i class="m-menu__link-icon la-code la"
                                                                   style="font-size: 2.6rem;"></i>
																 </span>
                                            <span class="kt-widget17__subtitle" style=" display: block; margin-top: 0.75rem; font-size: 1.2rem; font-weight: 500; color: rgb(0, 0, 0);">
																Technologies
															</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end:: Widgets/Facts and Figures-->
                    </div>


                </div>

            </div>
        </div>
    </div>


@endsection

