<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        Aptitude test
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->





    <!--end::Web font -->
    <!--begin::Base Styles -->
    <!--begin::Page Vendors -->

    <!--end::Page Vendors -->
    <link href="{{asset('assets/app/css/dataTables.css')}}">
    <link href={{asset('assets/vendors/base/vendors.bundle.css')}} rel="stylesheet" type="text/css"/>
    <link href={{asset("assets/demo/demo9/base/style.bundle.css")}} rel="stylesheet" type="text/css" />
    <link href={{asset("assets/app/css/dashboard.css")}} rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href={{asset("assets/app/media/img/logos/weblogo.png")}} />
    <link rel="stylesheet" href="{{asset('assets/app/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('assets/app/css/style.bundle.css')}}" >
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
    <link href="{{asset('assets/app/toastr/toastr.min.css')}}" rel="stylesheet">
</head>
<!-- end::Head -->
<!-- end::Body -->
<body  class="m--skin- m-page--loading-enabled m-page--loading m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default"  >
<!-- begin::Page loader -->
<div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
				<span>
					Please wait...
				</span>
        <span>
					<div class="m-loader m-loader--brand"></div>
				</span>
    </div>
</div>
<!-- end::Page Loader -->
<!-- begin:: Page -->

<!-- BEGIN: Header -->
<div>
    <header id="m_header" class="m-grid__item    m-header "  m-minimize="minimize" m-minimize-mobile="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200" >
        <div class="m-container m-container--fluid m-container--full-height">

            <div class="m-stack m-stack--ver m-stack--desktop  m-header__wrapper">
                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand m-brand--mobile">
                    <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                            <a href="{{route('home')}}" class="m-brand__logo-wrapper">
                                <img alt="" src={{asset("assets/app/media/img/logos/weblogo.png")}}/>
                            </a>
                        </div>

                        <div class="m-stack__item m-stack__item--middle m-brand__tools">
                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <a href="javascript:;" id="m_aside_left_toggle_mobile" class="m-brand__icon m-brand__toggler m-brand__toggler--left">
                                <span></span>
                            </a>
                            <!-- END -->
                            <!-- BEGIN: Responsive Header Menu Toggler -->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler">
                                <span></span>
                            </a>
                            <!-- END -->
                            <!-- BEGIN: Topbar Toggler -->
                            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon">
                                <i class="flaticon-more"></i>
                            </a>
                            <!-- BEGIN: Topbar Toggler -->
                        </div>
                    </div>
                </div>
                <!-- END: Brand -->

                <div class="m-stack__item m-stack__item--middle m-stack__item--center">
                    <!-- BEGIN: Brand -->
                    <a href="#" class="m-brand m-brand--desktop">
                        <img alt="" src="{{asset('assets/app/media/img/logos/weboccult.png')}}"/>
                    </a>
                    <!-- END: Brand -->
                </div>

            </div>

        </div>

    </header>
</div>

    <!-- END: Left Aside -->
    <!-- begin::Body -->
<div class="m-content">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">
        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->

                <div class="card col-sm-12 mt-4" style="align-self: center ; text-align: center">
                    <h2 class="mt-2">Your exam has been recorded.</h2>
                    <p>Please wait for the result to be announced in next 30 minutes.</p>
                    <p>Good Luck!</p>
                </div>
            </div>
        </div>
    </div>
</div>
@extends('partials.footer')
</body>
</html>