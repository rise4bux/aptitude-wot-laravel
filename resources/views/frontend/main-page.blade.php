<!-- saved from url=(0049)http://apti.test4you.in/exam/showQuestionsAngular -->
<html ng-app="myApp" class="ng-scope">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body { letter-spacing: 1px; background: -webkit-linear-gradient(to top, #EF473A, #CB2D3E); background: linear-gradient(to top, #EF473A, #CB2D3E);  }

        .main section { position: relative; }

        .answerSection label { margin-bottom: 0 !important; }

        h2 { font-size: 36px; }

        .m-brand.m-brand--mobile { display: none; }

        .m-stack.m-stack--desktop.m-stack--ver > .m-stack__item.m-stack__item--center { text-align: center; }

        .m-stack.m-stack--desktop.m-stack--ver > .m-stack__item.m-stack__item--middle { vertical-align: middle; }

        .mt-2, .my-2 { margin-top: 0.5rem !important; }

        h2, .h2 { font-size: 2rem; }

        .m-stack.m-stack--desktop.m-stack--ver > .m-stack__item.m-stack__item--middle { vertical-align: middle; }

        .sectionTab .active{ border-bottom: 1px solid #dc1c46; }

        .clearfix:before, .clearfix:after, .container:before, .container:after, .row:before, .row:after, .nav:before, .nav:after { display: table; content: " "; }

        .clearfix:after, .container:after, .row:after, .nav:after { clear: both; }

        .header { text-align: center; padding: 10px 0 8px; border-bottom: 2px solid #dc1c46; }

        .header .row { margin: 0; }

        .form form, .instructions_page_box { margin: 80px 0; border: 1px solid #bbbbbb; padding: 25px !important; box-shadow: 0px 0px 7px 1px rgba(0, 0, 0, 0.27); }

        .form form { margin: 94px 0; }

        .form form h2, .instructions_page_box h2 { background: #dc1c46; color: #fff; text-align: center; padding: 10px 0; font-size: 36px; margin: 0; }

        .form form input[type=submit], .form form input[type=button], .form form button { padding: 8px; }

        .form form .row { margin-bottom: 0; }

        footer { height: 40px; }

        .footer-copyright { border-top: 1px solid; background: #fff; position: relative; box-shadow: 0 -2px 10px #00000017; padding: 10px 0; text-align: center; font-size: 12px; color: #e43c2a; letter-spacing: 1px; height: 100%;}

        .instructions_page_box ol { margin: 20px 35px; padding: 0; }

        .instructions_page_box ol li { font-size: 16px; color: #757575; padding: 4px 0; }

        .instructions_page_box ol li strong { font-weight: 700; color: #4e4c4c; }

        .instructions_page_box h6 { font-size: 20px; color: #757575; margin-top: 15px; }

        .instructions_page_box h6 span { font-weight: 600; color: #4e4c4c; }

        ul.time-name { background: #d2323d; margin: 0; padding: 10px 5px !important; width: 100%; display: block; font-size: 16px; border-radius: 10px; }
        li.user-name { font-size: 18px; font-weight:600 }

        .time-name-outer { padding: 15px 15px 0; }
        .time-name { display: flex; float: none ; flex-direction: column; }
        .time-name li { color: #fff; display: block; padding: 5px 10px;  width: 100%; text-align: left; }
        .sidebar h6 { color: #757575; font-size: 16px; }
        .dashboard_logo { background-color: #fff; padding: 20px; border-radius: 0 0 10px 10px; text-align: center;}
        li.exam-time { display: flex; justify-content: space-between;  }
        .user-name { color: #fff; text-align: left;  }
        .exam-time .section_view { padding-right: 0; font-weight: 600;}
        .exam-time span:last-child:after { content: none; }
        .questions-palette, .legends { padding: 0 30px !important; }
        .questions-palette ul { margin: 0; padding: 10px 0 0 20px; font-size: 0; }
        .questions-palette ul li { display: inline-block; }
        .questions-palette ul li a { margin: 0; padding: 0; display: inline-block; height: 30px; width: 30px; border-radius: 5px; background: #dc1c46; text-align: center; line-height: 30px; font-size: 14px; margin: 4px; font-weight: 400; color: #fff; }
        .modal-dialog { width:auto; }
        .questions-palette ul li.ans a { background: #52cc85; color: #fff; }
        .questions-palette ul li.not_ans a { background: #dc1c46; color: #fff; }
        .legends .ans, .legends .not_ans, .legends .not-saved { float: left; margin: 5px 0 5px 25px; letter-spacing: 1px; }
        .test-dashboard_inner {background-color: rgb(255 255 255 / 0.9);border-radius: 10px; overflow: hidden; margin-bottom: 30px}
        .legends_outer { padding-top: 5px; }
        .legends_outer > div { float: none !important; margin-left: 4px !important; }
        .ans span, .not_ans span, .not-saved span { height: 20px; width: 20px; display: inline-block; border-radius: 50px; vertical-align: bottom; margin-right: 4px; }
        .ans span { background: #52cc85; }
        .not_ans span { background: #0056b3 }
        .not-saved span { background: #dc1c46; }
        .current { background: #0056b3 !important; }
        .right-section { margin: 20px 0 30px; border-radius: 10px; padding-bottom: 40px !important; background-color: #fff;}
        .right-section h5 { font-size: 18px; text-align: center; padding: 15px; border-bottom: 1px solid #d2323d; margin: 0; font-weight: 700; color: #4e4c4c; }
        .right-section .question { width: 100%; margin: 30px auto; padding-bottom: 15px; font-size: 18px; color: #333; border-bottom: 1px solid #d7363c }
        .right-section fieldset { width: 100%; margin: 10px auto; padding: 0; border-radius: 5px; background: #efefef; border: 0 }
        .right-section fieldset p { display: inline-block; margin: 0%; }
        .logo img { max-height: 50px; }
        .question_page > .row { min-height: calc( 100vh - 40px );  margin-bottom: 0; }
        .submit_box { text-align: center; background-color: #fff; box-shadow: 0px -10px 6px 0px rgb(0 0 0 / 0.03); margin-bottom: 0 !important; }
        .answerSection { margin: 0 auto }
        .answerSection label { color: #333 }
        .sveAndNext.btn, .submitTest.btn, .modal-footer .btn { background: linear-gradient(to bottom, #EF473A, #CB2D3E) !important;  border-radius: 4px; padding: 8px 38px; height: auto; line-height: inherit; text-transform: capitalize;}
        .sveAndNext.btn:hover, .submitTest.btn:hover, .modal-footer .btn:hover { transform: scale(1.03); -moz-transform: scale(1.03); -webkit-transform: scale(1.03)}
        .right-section .btn { margin: 15px auto 0 }
        .answerButtonSection { margin: 0 auto; text-align: center; }
        .right-section input[type="radio"] + label:before, .right-section input[type="radio"] + label:after { content: normal; }
        .question_page  .right-section input[type="radio"] + label { position: relative; padding: 10px; display: block; padding-left: 55px; line-height: 24px; height: auto; }
        .right-section input[type="radio"] + label * { font-size: 1rem !important; color: inherit !important; padding: 0 !important; margin: 0 !important; }
        .right-section input[type="radio"]:checked + label * { color: #fff !important }
        .right-section input[type="radio"] + label i.count { font-style: normal; border: 1px solid #333; width: 24px; height: 24px; text-align: center; position: absolute; left: 20px; top: 50%; line-height: 22px; transform: translateY(-50%) }
        .right-section input[type="radio"]:checked + label { background: #dc393c; color: #fff; border-radius: 4px; }
        .right-section .checked + label { background: #dc1c46; color: #fff; border-radius: 4px; }
        .right-section input[type="radio"]:checked + label i.count { border-color: #fff; color: #fff; }
        .tabParent { background-color: #fff; overflow: hidden; }
        .tabs::-webkit-scrollbar { width: 5px; height:5px; }
        .tabs::-webkit-scrollbar-thumb { background-color: silver; outline: 1px solid slategrey; }
        .tabParent { background-color: #fff; overflow: hidden; border-radius: 0 0 10px 10px; }
        .tabs { flex-wrap: nowrap !important; height: unset !important; height: 90px !important;  background-color: #fff !important; border-radius: 0 0 10px 10px;}
        .tabs .tab { height: 100% !important; text-align: center; line-height: 45px; height: 45px; padding: 0; margin: 0; text-transform: uppercase; }
        .tabs .tab a { height: 100% !important; line-height: 1.4 !important; display: flex !important; justify-content: center; align-items: center; border-bottom: 5px solid #fff; }
        .tabs .tab a:hover, .tabs .tab a.active { cursor: pointer; background-color: #fff !important;     color: #cc2e3d !important; border-bottom: 5px solid #cc2e3d; border-radius: 5px; }
        .tabs .tab a.single_tab:hover, .tabs .tab a.active.single_tab{ border-color: #fff }
        .modal-open .modal { width: 100% !important; max-height: 100vh !important; height: 100% !important; display: flex !important; justify-content: center; align-items: center}
        .modal-dialog { max-width: 50% !important; width: 100% !important;}
        .modal.show .modal-dialog { -webkit-transform: none; transform: none; width: 50%; height: 265px; margin: 0; }
        .modal-body p { margin-bottom: 0  }
        .modal .modal-content { padding: 20px !important; border: none !important; border-radius: 10px;      }
        .modal .modal-header { align-items: center }
        .modal .modal-footer { height: unset !important; min-height: 56px; padding: 15px 0 0 !important; background-color : transparent !important}
        .modal .modal-header .close { position: absolute; font-size: 22px !important; line-height: 32px; padding: 0px !important; margin: 0 !important; top: 7px; right: 7px; background: #e23d3b; color: #fff;
    height: 30px; width: 30px; border-radius: 7px; font-weight: 400; opacity: 1; text-shadow: none !important;}
        .modal .modal-header .close:hover, .modal .modal-header .close:focus {  opacity: 1 !important; opacity: 1 !important; color: #e23d3b; background: #fff; }
        .modal .modal-footer > .btn + .btn { margin-left: 10px !important; }
        .modal-footer .btn { float: none !important; margin: 0 !important }
        .thank_you_page_outer { width: 100%; min-height: 100vh; padding: 0 0 20px !important; display: flex; flex-direction: column; justify-content: center; align-items: center; background: -webkit-linear-gradient(to top, #EF473A, #CB2D3E); background: linear-gradient(to top, #EF473A, #CB2D3E);   }
        .thank_you_page_outer .dashboard_logo { width: 70%; border-radius: 10px; }
        .thank_you_page_outer .text_message { margin-top: 10px;  font-size: 24px; color: #333; text-align: center; font-weight: 700;}
        .thank_you_page_outer .sub_message { text-align: center; font-weight: 500; color: #333; }
        .thank_you_box { width: 70%; margin: 0 auto; background-color: #fff; border-radius: 10px; padding: 20px 20px 25px; }
        .thank_you_image { width: 25%; margin: 0 auto; margin-top: 20px;  }
        .thank_you_image img { width: 100%; height: auto }

        .modal-open { overflow-y: auto !important; padding: 0 !important }

        @media (max-width: 2881px) and (min-width: 2401px){
            .dashboard_logo {
                height: 150px;
                padding: 25px;
                margin-bottom: 40px !important;
            }
            .logo img {
                min-height: 100px;
            }
            .tabs {
                height: 150px !important;
            }
            .tabs .tab {
                height: 100% !important;
            }
            .tabs .tab a {
                height: unset !important;
                font-size: 26px !important;
                font-weight: 600 !important;
            }
            .time-name-outer {
                padding: 30px 30px 0;
            }
            ul.time-name {
                padding: 20px 10px !important;
                font-size: 24px;
            }
            li.user-name {
                font-size: 26px;
            }
            .questions-palette, .legends {
                padding: 0 35px !important;
            }
            .sidebar h6 {
                 font-size: 26px;
            }
            .questions-palette ul li a {
                height: 50px;
                width: 50px;
                line-height: 50px;
                font-size: 22px;
            }
            .legends_outer {
                display: flex;
                justify-content: flex-start;
                align-items: center;
                font-size: 22px;
            }
            .legends_outer > div + div {
                padding-left: 15px
            }
            .ans span, .not_ans span, .not-saved span {
                height: 40px;
                width: 40px;
                vertical-align: middle;
            }
            .row.submit_box > div:first-child {
                padding: 30px !important;
            }
            .submit_box p {
                font-size: 22px;
            }
            .submit_box small {
                font-size: 18px;
            }
            .sveAndNext.btn, .submitTest.btn, .modal-footer .btn {
                padding: 15px 45px;
                font-size: 22px;
            }
            .right-section h5 {
                font-size: 40px;
            }
            .question p {
                font-size: 32px
            }
            .question pre {
                font-size: 30px;
            }
            .right-section {
                margin: 40px 0 40px;
            }
            .right-section fieldset {
                margin: 15px auto;
            }
            .question_page .right-section input[type="radio"] + label {

                padding: 25px;
                padding-left: 75px;
            }
            .right-section input[type="radio"] + label * { font-size: 28px !important; }
            .right-section input[type="radio"] + label i.count {
                width: 40px;
                height: 40px;
                line-height: 37px;
            }
            .modal-title {
                font-size: 40px;
            }
            .modal .modal-content {
                padding: 40px !important;
            }
            .modal-body p {
                font-size: 26px;
            }
            .modal .modal-footer {
                padding: 25px 0 0 !important;
            }
            .modal .modal-header .close {
                font-size: 36px !important;
                line-height: 50px;
                height: 50px;
                width: 50px;
            }
            .question_page > .row {
                min-height: calc( 100vh - 80px );
            }
            footer {
                height: 80px;
            }
            .footer-copyright {
                font-size: 26px
            }
            .thank_you_page_outer .text_message { font-size: 30px }
            .thank_you_page_outer .sub_message { font-size: 26px;}
        }
        @media (max-width: 2400px) and (min-width: 1921px){
            .dashboard_logo {
                height: 150px;
                padding: 25px;
                margin-bottom: 40px !important;
            }
            .logo img {
                min-height: 100px;
            }
            .tabs {
                height: 150px !important;
            }
            .tabs .tab {
                height: 100% !important;
            }
            .tabs .tab a {
                height: unset !important;
                font-size: 26px !important;
                font-weight: 600 !important;
            }
            .time-name-outer {
                padding: 30px 30px 0;
            }
            ul.time-name {
                padding: 20px 10px !important;
                font-size: 24px;
            }
            li.user-name {
                font-size: 26px;
            }
            .questions-palette, .legends {
                padding: 0 35px !important;
            }
            .sidebar h6 {
                 font-size: 26px;
            }
            .questions-palette ul li a {
                height: 50px;
                width: 50px;
                line-height: 50px;
                font-size: 22px;
            }
            .legends_outer {
                display: flex;
                justify-content: flex-start;
                align-items: center;
                font-size: 22px;
            }
            .legends_outer > div + div {
                padding-left: 15px
            }
            .ans span, .not_ans span, .not-saved span {
                height: 40px;
                width: 40px;
                vertical-align: middle;
            }
            .row.submit_box > div:first-child {
                padding: 30px !important;
            }
            .submit_box p {
                font-size: 22px;
            }
            .submit_box small {
                font-size: 18px;
            }
            .sveAndNext.btn, .submitTest.btn, .modal-footer .btn {
                padding: 15px 45px;
                font-size: 22px;
            }
            .right-section h5 {
                font-size: 40px;
            }
            .question p {
                font-size: 32px
            }
            .question pre {
                font-size: 30px;
            }
            .right-section {
                margin: 40px 0 40px;
            }
            .right-section fieldset {
                margin: 15px auto;
            }
            .question_page .right-section input[type="radio"] + label {

                padding: 25px;
                padding-left: 75px;
            }
            .right-section input[type="radio"] + label * { font-size: 28px !important; }
            .right-section input[type="radio"] + label i.count {
                width: 40px;
                height: 40px;
                line-height: 37px;
            }
            .modal-title {
                font-size: 40px;
            }
            .modal .modal-content {
                padding: 35px !important;
            }
            .modal-body p {
                font-size: 26px;
            }
            .modal .modal-footer {
                padding: 20px 0 0 !important;
            }
            .modal .modal-header .close {
                font-size: 30px !important;
                line-height: 40px;
                height: 40px;
                width: 40px;
            }
            .question_page > .row {
                min-height: calc( 100vh - 60px );
            }
            footer {
                height: 60px;
            }
            .footer-copyright {
                font-size: 24px
            }
            .thank_you_page_outer .text_message { font-size: 24px }
            .thank_you_page_outer .sub_message { font-size: 20px;}
        }
        @media (max-width: 1920px){}
/* Desktop Screen */
        @media only screen and  (max-width: 1024px) {
            .form form, .instructions_page_box { margin: 50px 0 35px; }
            .form form h2, .instructions_page_box h2 { font-size: 30px; }
            .questions-palette, .legends { padding: 0 20px !important; }
            .questions-palette ul { padding: 10px 0 0 0px; }
            .legends .ans, .legends .not_ans, .legends .not-saved { margin-left: 0; }
            .sidebar .btn { margin: 0 0px 20px !important; }
            .right-section .btn { margin-left: 50px; }
            .sidebar { padding-bottom: 75px; }
            .tabs .tab a { font-size: 14px; padding: 0 10px; }
            .time-name li { font-size: 12px; padding: 5px}

        }
/* Desktop Screen */
        @media only screen and  (max-width: 1023px) {
            .form form h2, .instructions_page_box h2 { font-size: 26px; }
            .instructions_page_box h6 { margin-left: 10px; font-size: 18px; }
            .tabs .tab a { padding: 0px 5px; }
            .tabs .tab { height: auto; line-height: 20px; vertical-align: bottom; }
        }
        /* Tablet Screen */
        @media only screen and (max-width: 991px){
            ul.time-name li {  float: none; width: 100%; text-align: center; font-size: 14px; flex-wrap: wrap; justify-content:center }
            li.user-name { font-size: 16px; }
            .time-name .exam-time .section_view { padding: 0 5px}
            .tabs .tab a { font-size: 12px !important }
            .logo img { max-height: unset; width: 100%; height: auto; }
            .thank_you_page_outer .logo a { display: block; width: fit-content; margin: 0 auto; }
            /* .tabs .tab { width: 50% !important; flex-grow: unset; float: none; flex-basis: unset; } */
        }
/* Mobile Screen */
        @media only screen and  (max-width: 767px) {
            ul.time-name li { justify-content:space-between }
            .logo { max-width: 200px !important; width: 100% !important; margin: 0 auto; }
            .logo a { display: block; width: inherit; margin: 0 auto; }
            .form form, .instructions_page_box { margin: 40px 0 25px; padding: 15px 10px !important; }
            .form form .row { margin: 0; }
            .form form h2, .instructions_page_box h2 { margin: 0; font-size: 24px; }
            .instructions_page_box h6 { margin-left: 10px; font-size: 16px; }
            .instructions_page_box ol li { padding: 2px 0; font-size: 14px; }
            .legends .ans, .legends .not_ans, .legends .not-saved { margin-left: 8px; }
            .right-section { margin: 30px 0; padding-bottom: 20px !important; }
            .right-section .btn { margin-left: 15px; }
            .time-name li { padding: 2px 5px; }
            .right-section p { font-size: 16px; line-height: 20px; }
            .sidebar { padding-bottom: 4px; }
            .right-section h5 { font-size: 18px; padding: 10px 0; }
            .tabs {
                height: unset !important;
                flex-wrap: wrap !important;
            }
            .tabs .tab { width: 100% !important; height: 30px !important; flex-basis: auto;
    float: none; }
            .tabs .tab a { border-bottom: 0; }
            .tabs .tab a:hover, .tabs .tab a.active {     border-bottom: 0; background-color: #4e4c4c !important; color: #fff !important; border-radius: 0;}
            .tabs .tab asingle_tab:hover, .tabs .tab a.active.single_tab{ background-color: #fff !important; color: #333 !important; }
            .modal.show .modal-dialog { max-width: 80% !important }
            .modal-footer { justify-content: center !important; }
            .thank_you_page_outer .logo { width: 200px !important; margin: 0 auto; text-align: center; }

            .thank_you_page_outer .dashboard_logo, .thank_you_box { width: 90% }
            .thank_you_page_outer .text_message { font-size: 16px }
            .thank_you_page_outer .sub_message { font-size: 14px;}
            .thank_you_image { width: 30% }
            .tabs .tab a { font-weight: 500 !important; font-size: 14px !important }
        }
        /* Small Screen */
        @media only screen and  (max-width: 480px){
            ul.time-name li { justify-content:center }
            .modal .modal-footer { flex-direction: column !important;  }
            .modal .modal-footer > .btn + .btn { margin-left: 0 !important; margin-top: 10px !important }
            .thank_you_image { width: 50% }
            .modal-body { padding: 10px !important }
        }
        .btn:hover { color:#fff !important; }
    </style>
    <link href="{{asset('assets/app/toastr/toastr.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link href="{{asset('assets/app/css/materialize.css')}}" rel="stylesheet">
    <script src="{{asset('assets/app/js/materialize.js')}}"></script>
    <script src="{{asset('assets/app/js/bootstrap.bundle.js')}}"></script>

</head>

<body style="-moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;"
      unselectable="on" onselectstart="return false" onmousedown="return false;" ondragstart="return false;"
      ondrop="return false;" oncontextmenu="return false">

<div class="main">
    <section>
        <div class="question_page">
            <div class="row">
                <div class="col-md-4 test-dashboard">
                    <div class="sidebar">
                        <div class="row dashboard_logo">
                            <div class="col s12 logo">
                                <a href="javascript:void(0);"><img src="{{asset('assets/app/Exam_files/logo.png')}}"></a>
                            </div>
                        </div>
                        <div class="test-dashboard_inner">
                        <div class="row time-name-outer">
                            <ul class="time-name clearfix col s12">
                                <li class="user-name">{{auth()->user()->first_name}} {{auth()->user()->last_name}}</li>
                                <li class="exam-time">
                                    <span>Remaining Time: </span>
                                    <div class=" section_view rem-time" id="timer">00:00:00</div>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col s12 questions-palette">
                                <h6>Questions Palette:</h6>

                                @foreach($finalData as $key=>$value)
                                    <div id="{{$key}}" class="tab-pane {{$key.'-'.$key}}">
                                        <ul>
                                            @foreach($value as $val)
                                                <li ng-class="ids.flag" on-last-repeat="" class="questionBox">
                                                    <a onclick="showQuestion({{$val->id}})" id="palette{{$val->id}}"
                                                       class="nv getQuestionHtml ng-binding"
                                                       href="javascript:void(0)" @foreach($questionAnswer as $que) @if($que->question==$val->id && $que->answer != null) style="background-color:#52cc85;" @endif  @endforeach>{{$loop->iteration}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 legends">
                                <h6>Legends</h6>
                                <div class="legends_outer">
                                    <div class="ans"><span></span> Answered</div>
                                    <div class="not_ans"><span></span> Current Position</div>
                                    <div class="not-saved"><span></span> Not Answered</div>
                                </div>
                            </div>
                        </div>
                        <div class="row submit_box">
                            <div class="col-sm-12 p-4">
                                <p>After finishing exam click on submit button below!</p>
                                <small>Note: Do not click until you are finished!
                                </small>
                            </div>
                            <div class="col s12" style="text-align-last: center; margin: 0px 20px 20px;">

                                    <button class="submitTest btn" type="button"  >
                                      Submit
                                    </button>

                            </div>
                        </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-8 ">
                    <div class="tabParent">
                    <ul class="tabs nav nav-tabs tab-container">
                        @if(count($finalData) == 1)
                            @foreach($finalData as $key=>$value)
                                <li class="tab col {{$key}}">
                                    <a class="sectionTab single_tab" data-sectionid="{{$value[0]->category_id}}" data-toggle="tab"
                                    href="{{'#'.$key}}" title="{{$key}}">{{$key}}</a>
                                </li>
                            @endforeach
                        @else
                            @foreach($finalData as $key=>$value)
                                <li class="tab col {{$key}}">
                                    <a class="sectionTab " data-sectionid="{{$value[0]->category_id}}" data-toggle="tab"
                                    href="{{'#'.$key}}" title="{{$key}}">{{$key}}</a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                    </div>

                    @foreach($finalData as $key=>$value)
                        <div id="{{$key}}" class="tab-pane {{$key}}">
                            @foreach($value as $kv=>$val)

                                <div id="questionContainer{{$val->id}}" class="right-section" style="display: none">
                                    <h5>
                                        <div class="col-md-12 insturction">
                                            <div class="qtype ng-binding">Question
                                                No:{{$loop->iteration}}
                                            </div>
                                        </div>
                                    </h5>
                                    <div class="col-md-10 question ng-isolate-scope">
                                        <p>{!! htmlspecialchars_decode($val->title)!!}</p></div>
                                    <div class="col-md-10  answerSection">
                                    @foreach($options[$val->id] as $okey=>$option)
                                        <fieldset class="filed optionBox">
                                            <input type="radio" id="id{{$option->id}}"  name="answerID{{$val->id}}"
                                                   value="{{$option->id}}" onclick="answer=$(this).val()"
                                                   class="ng-pristine ng-valid answerID{{$val->id}}"/>
                                            <label for="id{{$option->id}}">
                                                <i class="count">{{$alphabet[$loop->iteration-1]}}</i>
                                                <span class="ng-isolate-scope">
                                                    <p class="ng-scope" >{!!$option->content!!}</p>
                                                </span>
                                            </label>
                                        </fieldset>
                                    @endforeach
                                    </div>
                                    <div class="col-md-10 answerButtonSection">
                                            <button class="sveAndNext btn {{ $kv }} xx {{$value === end($finalData) && count($value) == ($kv+1) ? "lastSaveNext" : ""}}"
                                                    onclick="saveNnext({{$val->id}}, {{$value === end($finalData) && count($value) == ($kv+1)}})">
                                                @if($value === end($finalData) && count($value) == ($kv+1))
                                                    Save
                                            @else
                                                    Save &amp; Next
                                            @endif
                                            </button>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="footer-copyright">
            <div class="container">
                Copyright © WebOccult Technologies Pvt. Ltd. 2020
            </div>
        </div>
    </footer>
    <div class="thankYouContent" style="display: none;">

        <!-- end::Page Loader -->
        <!-- begin:: Page -->

        <!-- END: Left Aside -->
        <!-- begin::Body -->
        <div class="thank_you_page_outer m-content">
            <!-- BEGIN: Main Logo -->
            <div class="row dashboard_logo">
                <div class="col s12 logo">
                    <a href="javascript:void(0);"><img src="../../../public/assets/app/Exam_files/logo.png"></a>
                </div>
            </div>
            <!-- BEGIN: Thank you content -->
            <div class="thank_you_box m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
                <div class="text_message">
                <p>Your exam has been recorded.<p>
                </div>
                <div class="sub_message">
                    <p>Please wait for the result to be announced in next 30 minutes.</p>
                    <p>Good Luck!</p>
                </div>
                <div class="thank_you_image">
                    <img src="../../../public/assets/app/Exam_files/thank_you_4.png" alt="" srcset="">
                </div>

            </div>
        </div>
    </div>
</div>
<div id="myModal" data-backdrop="static" class="modal fade in">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header" >
                <h4 class="modal-title">Submit Exam?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body"  >
                <p>Do you really want to submit Exam?</p>
            </div>
            <div class="modal-footer" style="padding-top: 15px">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                <a href="javascript:void(0);" class="btn btn-danger" onclick="submitExam()">Submit</a>
            </div>
        </div>
    </div>
</div>
<script>
    var days;
    var hours;
    var minutes;
    var seconds;
    var interval;
    var count;
    var current;
    var answer = 0;

    document.addEventListener('contextmenu', function(e) {
      e.preventDefault();
    });

    $('document').ready(function () {

    $(document).on('click','.submitTest',function () {
        $('#myModal').modal();
    });
     var m= '@json($selectedoption)';
        m=JSON.parse(m);

        for(var i=0; i<m.length;i++){
            $('#id'+m[i]).addClass(' checked');
            $('#id'+m[i]).attr('checked', true);
        }
        $('.filed').one('click',function() {
            $(this).siblings().find('.checked').removeClass('checked')
        })
        $('.getQuestionHtml').first().addClass(' current');
        $('.right-section').first().show();

        $('.sectionTab').click(function () {
            $('.right-section').hide();
            var slug = $(this).html();
            console.log(slug);
            var len=  $('.'+slug).find('.selected').length;
            if(len==0){
                $('.' + slug).find('.right-section').first().show();
            }
            $('.' + slug).find('.selected').show();
            var selected = $('.' + slug + '-' + slug).find('.current').length;
            if (selected == 0) {
                $('.' + slug + '-' + slug).find('.getQuestionHtml').first().addClass(' current');
            }
            $(document).find('.lastSaveNext').text('Save');
        });
    });

    $(document).on('click', '.questionBox, .optionBox', function () {
        $(document).find('.lastSaveNext').text('Save');
    });

    function showQuestion(id) {
        $('.right-section').hide();
        $('#palette' + id).parent().parent().parent().find('.getQuestionHtml').removeClass('current');
        $('#palette' + id).addClass('current');
        $('#questionContainer'+id).parent().find('.right-section').removeClass('selected');
        $('#questionContainer' + id).addClass('selected').show();
    }

    function saveNnext(question_id, isLast = false) {
        var timer = $('#timer').html();
        if(timer) {
            var a = timer.split(':'); // split it at the colons
            console.log(a,'save and next');
            // minutes are worth 60 seconds. Hours are worth 60 minutes.
            var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
            var remainingTime = seconds / 60;
            var exam_id = '@json($exam_id)';
            console.log(question_id);
            answerID = $('#questionContainer'+question_id).find('.answerSection').find('input:radio.answerID'+question_id+':checked').val();
            console.log(answerID);

            if( answerID != 0 && answerID != undefined) {
                $.ajax({
                    url: '{{env('APP_URL')}}/exam/save-next',
                    type: 'post',
                    dataType: 'json',
                    data: {question_id: question_id, answer_id: answerID, remaining: remainingTime, exam: exam_id},
                    success: function (res) {
                        var pallet_id = $('#palette' + question_id).parent().next().find('a').attr('id');
                        if (answerID !== 0) {
                            $('#palette' + question_id).css('background-color', '#52cc85');
                            $('#' + pallet_id).addClass(' current');
                        }

                        var buttonText = $('#questionContainer'+question_id).next().length;
                        if (buttonText ==0) {
                            /* answer = 0;*/
                            var nexttab= $('#questionContainer'+question_id).parent().next().length;
                            if(nexttab==1){
                                var nextvar=$('#questionContainer'+question_id).parent().next().attr('id');
                                $('.sectionTab').removeClass(' active');
                                $('.'+nextvar).find('.sectionTab').addClass('active').trigger('click');
                            }else{
                                return false;
                            }
                        } else {
                            $('.right-section').hide();
                            $('#questionContainer'+question_id).parent().find('.right-section').removeClass('selected');
                            $('#questionContainer' + question_id).next().addClass(' selected').show();
                            $('#' + pallet_id).parent().parent().parent().find('.getQuestionHtml').removeClass('current')
                            $('#' + pallet_id).addClass(' current');
                        }
                        /*answer = 0;*/
                    }
                });
            }
            else {
                var pallet_id = $('#palette' + question_id).parent().next().find('a').attr('id');
                var buttonText = $('#questionContainer'+question_id).next().length;
                if (buttonText ==0) {
                    /* answer = 0;*/
                    var nexttab= $('#questionContainer'+question_id).parent().next().length;
                    if(nexttab==1){
                        var nextvar=$('#questionContainer'+question_id).parent().next().attr('id');
                        $('.sectionTab').removeClass(' active');
                        $('.'+nextvar).find('.sectionTab').addClass('active').trigger('click');
                    }else{
                        return false;
                    }
                } else {
                    $('.right-section').hide();
                    $('#questionContainer' + question_id).next().addClass(' selected').show();
                    $('#' + pallet_id).parent().parent().parent().find('.getQuestionHtml').removeClass('current')
                    $('#' + pallet_id).addClass(' current');
                }
            }
        }

        $(document).on('click','.sveAndNext', function () {
            if($(this).hasClass('lastSaveNext')) {
                var thisButton = $(this)
                thisButton.text('Saving...')
                setTimeout(function () {
                    thisButton.text('Saved.!')
                }, 1000);
            } else {
                $(document).find('.lastSaveNext').text('Save');
            }
        })


    }
    callSubmitAjax = false;
    function newtimer() {

        hours = parseInt(count / (60 * 60) % 24, 10); // calculation for hours
        minutes = parseInt(count / 60, 10);           // calculation for minutes
        seconds = parseInt(count % 60, 10);           // calculation for seconds

        /* display digits in clock manner */

        hours = hours < 10 ? "0" + hours : hours;
        if(minutes >= 60 ) {
            minutes = minutes - 60 * hours;
        }
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        if (count-- <= 0) {    //if statement for reset when time over.
            clearInterval(interval);

           // $( ".submitTest" ).trigger( "click" );

                if(callSubmitAjax) {

                }
                else {
                    callSubmitAjax = true;
                    $.ajax({
                        url: '{{env('APP_URL')}}/exam/save-submit',
                        type: 'post',
                        dataType: 'json',
                        data: {remaining: 0, exam: '{{$exam_id}}'},
                        success: function (res) {
                            if(res) {
                                //callSubmitAjax = false;
                                $("body").html($(".thankYouContent").html());
                                //                    alert('Your Exam is Submitted');
                                $("body").addClass('m--skin- m-page--loading-enabled m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default')

                            }
                            $('#timer').html('Timer is unset');

                        }
                        ,error :function () {
                            //callSubmitAjax = false;
                        }
                    });

                }

        } else {
            $("#timer").text(hours + " : " + minutes + " : " + seconds);
        }
    }

    $(window).on('load', function () {
        document.onkeydown = function (e) {
            return false;
        }
        count = '@php echo $count @endphp';
        count = '@json($count)';
        count = count * 60;
        setInterval(newtimer, 1000);

        setInterval(saveFormData, 60*1000);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    function submitExam() {
        console.log("in submit");
        window.onbeforeunload = true;
        var timer = $('#timer').html();
            if(timer) {
                var a = timer.split(':'); // split it at the colons

                // minutes are worth 60 seconds. Hours are worth 60 minutes.
                var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
                var remainingTime = seconds / 60;
            }
            else {
                remainingTime = 0;
            }
        callSubmitAjax = true;
        $.ajax({
            url: '{{env('APP_URL')}}/exam/save-submit',
            type: 'post',
            dataType: 'json',
            data: {remaining: remainingTime, exam: '{{$exam_id}}'},
            success: function (res) {
                if(res) {
                    //callSubmitAjax = false;
                    $("body").html($(".thankYouContent").html());

                    $("body").addClass('m--skin- m-page--loading-enabled m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default')

                }
                //$('#timer').html('Timer is unset');

            }
            ,error :function () {
                //callSubmitAjax = false;
            }
        });
    }

     $(window).on("beforeunload", function() {
         if(callSubmitAjax) {
             console.log('In Onload')
            // window.onbeforeunload = true;
             window.location.href = '{{env('APP_URL')}}/finished/thankyou';
         }
         else {
             window.onbeforeunload = true;
             saveFormData(true);
             return 'Data not saved will be lost!'
         }
        // clearSession();

     });
    function saveFormData(closed) {

        var timer = $('#timer').html();
        if(timer) {
            var a = timer.split(':'); // split it at the colons
            var exam_id = '@json($exam_id)';
            // minutes are worth 60 seconds. Hours are worth 60 minutes.
            var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
            var remainingTime = seconds / 60;

            $.ajax({
                url: '{{env('APP_URL')}}/exam/save-timer',
                type: 'post',
                dataType: 'json',
                data: {remaining: remainingTime, exam: exam_id,closed:closed},
                success: function (res) {

                }
            });
        }
    }
    function clearSession() {
        console.log('clear session');
        $.ajax({
            url: '{{env('APP_URL')}}/exam/clear-session',
            type: 'post',
            dataType: 'json',
            data: {closed:true},
            success: function (res) {

            }
        });
    }
</script>
</body>
</html>
