<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />

    <title>Aptitue test | Register</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <!--end::Fonts -->


    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{asset('assets/app/Login/css/login-page.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{asset('assets/app/Login/plugins/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/app/Login/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <!--end::Layout Skins -->

    <link rel="shortcut icon" href="{{asset("assets/app/media/img/logos/weblogo.png")}}" />
    <link href="{{asset('assets/app/toastr/toastr.min.css')}}" rel="stylesheet">
    <!-- This style for student registration form -->
    <style>
        body { background: -webkit-linear-gradient(to top, #EF473A, #CB2D3E); background: linear-gradient(to top, #EF473A, #CB2D3E);  }
        .error {
            color: #e74c3c;
        }
        #enrollment-error {font-size: 12px;margin: 0px;padding: 0px;}
        #f_name-error {font-size: 12px;margin: 0px;padding: 0px;}
        #l_name-error {font-size: 12px;margin: 0px;padding: 0px;}
        #email-error {font-size: 12px;margin: 0px;padding: 0px;}
        #address-error {font-size: 12px;margin: 0px;padding: 0px;}
        #mobile_no-error {font-size: 12px;margin: 0px;padding: 0px;}
        #openings-error {font-size: 12px;margin: 0px;padding: 0px;}
        #future_studies-error {font-size: 12px;margin: 0px;padding: 0px;}
        #university_id-error {font-size: 12px;margin: 0px;padding: 0px;}
        #college_id-error {font-size: 12px;margin: 0px;padding: 0px;}
        #streams-error {font-size: 12px;margin: 0px;padding: 0px;}
        #year-error {font-size: 12px;margin: 0px;padding: 0px;}
        #other_interest-error {font-size: 12px;margin: 0px;padding: 0px;}
        #cvFile-error {font-size: 12px;margin: 0px;padding: 0px;}



        .student_registration_form { position: relative; }
        .student_registration_form .kt-login__wrapper{ padding: 40px 0 !important; margin: 0 auto !important; width: 70% !important; }
        .student_registration_form .kt-login__logo { background: #fff; padding: 20px; width: 100%; border-radius: 10px; }

        .student_registration_form .sign_up_form_box { padding: 30px; background-color: #fff; border-radius: 10px; }
        .student_registration_form .kt-login__logo { margin: 0 auto 40px !important  }
        .student_registration_form .kt-login__title, .student_registration_form .kt-login__desc { color: #000 !important; }

        .student_registration_form .kt-login--signup { width: 100% !important }

        .student_registration_form .form-group { position: relative; margin-bottom: 35px; color: #000 !important }

        .student_registration_form .form-control { background: transparent !important; }
        .student_registration_form label:first-child { position: absolute; top: 7px; left: 0; padding: 5px 30px 0px 12px !important; z-index: 3; transition: all 150ms ease-out; -moz-transition: all 150ms ease-out; -webkit-transition: all 150ms ease-out; width: 100%; text-overflow: ellipsis; overflow: hidden; white-space: nowrap; }

        .student_registration_form  .focused label:first-child { transform: translate(0, -29px); font-size: 12px;     text-overflow: unset; overflow: unset; white-space: inherit; }

        .arrow_btn { position: absolute; right: 10px; top: 14px; width: 13px; }

        .arrow_btn svg { width: 100%; height: 100%; }

        .input_underline { display: block; position: absolute; left: 0; top: 44px; height: 2px; transform: translateX(-50%); -moz-transform: translateX(-50%); -webkit-transform: translateX(-50%); left: 50%; width: 0; background: rgb(0 0 0 / 40%); z-index: 5; transition: all ease-in-out .2s; -moz-transition: all ease-in-out .2s; -webkit-transition: all ease-in-out .2s; }

        .focused .input_underline { width: calc(100% - 20px); }

        .student_registration_form input,
        .student_registration_form select { color: #000 !important; border-radius: 0 !important; border: none !important; border-bottom: 1px solid #0000004a !important; padding: 5px 12px !important; position: relative; z-index: 4; }
        .student_registration_form select { -moz-appearance: none; -webkit-appearance: none; -moz-appearance: none; text-indent: 1px;  }
        .student_registration_form select option { color: #000 !important }

        .student_registration_form select option[value=""] { display: none; }
        .student_registration_form .kt-login__actions {  margin-top: 15px !important; }
        .student_registration_form .btn-brand { background-color: #333 !important; color: #fff !important; border: none; border-radius: 45px !important;}
        .student_registration_form .btn-brand:hover { background-color: #e23232 !important}


        /* Responsive */
        @media (max-width: 2880px){

        }
        @media (max-width: 2400px){

        }
        @media (max-width: 1920px){

        }
        @media (max-width: 991px) {
            .student_registration_form .kt-login__wrapper{ width: 90% !important; padding-right: 15px !important; padding-left: 15px !important }
        }
        @media (max-width: 767px) {
            .student_registration_form .kt-login__wrapper{ width: 100% !important;  }
            .student_registration_form label { font-size: 13px !important }
            .student_registration_form .sign_up_form_box { padding: 20px }

        }
        @media (max-width: 580px){
            .student_registration_form .focused label {
                font-size: 10px !important;
                transform: translate(0, -33px);
            }
        }
    </style>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-37564768-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-37564768-1');
    </script>
</head>
<!-- end::Head -->

<!-- begin::Body -->

<body style="/*background-image: url({{asset('assets/app/media/img/bg/bg-2.jpg')}}); background-position: center top; background-size: 100% 350px;*/" class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">
<div id="loaderdiv" style="position: fixed; width: 100%; transition: all 1s; opacity: 1; height: 100%;z-index: 99999; display: none">
    <img src="{{asset('js/loader121.gif')}}" alt="" style="position: fixed; left: 44%; top: 40%; width: 200px; height: 200px;">
    <p style="top: 63%; color: black; left: 45%; position: relative; z-index: 9999; font-size: 19px; font-weight: 600;">Please Wait...</p>
    </div>
    <!-- begin::Page loader -->

    <!-- end::Page Loader -->
    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root kt-page student_registration_form">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="/*background-image: url({{asset('assets/app/media/img/bg/bg-2.jpg')}});*/">
                <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper ">
                    <div class="kt-login__container kt-login--signup">
                        <div class="kt-login__logo">
                            <a href="{{ route('welcome') }}" class="m-brand m-brand--desktop">
                                <img src="{{asset('assets/app/media/img/logos/weboccult.png')}}">
                            </a>
                        </div>
                        <div class="sign_up_form_box">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">Sign Up</h3>
                                <div class="kt-login__desc">Enter your details to create your account:</div>
                            </div>

                            <form class="kt-form" role="form" id="registerForm" method="POST" action="{{route('candidate.register')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="form-group m-form__group col-md-6">
                                        <label for="enrollmentAdd">Enrollment Number</label>
                                        <input class="form-control m-input enrollmentAdd" type="text" placeholder=" "
                                               name="enrollment" value="{{old('enrollment')}}" autocomplete="off" id="enrollment">
                                        <span class="input_underline"></span>
                                        @error('enrollment')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                        <div class="loadingState" style="display: none;">
                                            <img src="{{ asset('assets/app/media/img/DualRing-1s-32px.gif') }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group col-md-6">
                                        <label for="email">Email</label>
                                        <input class="form-control m-input" type="text" placeholder="" name="email" autocomplete="off" value="{{old('email')}}" id="email">
                                        <span class="input_underline"></span>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-form__group col-md-6">
                                        <label for="first_name">First Name</label>
                                        <input class="form-control m-input" type="text" placeholder="" name="f_name" value="{{old('f_name')}}" id="f_name">
                                        <span class="input_underline"></span>
                                        @error('f_name')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>
                                    <input name="slug" type="hidden" value="{{$slug}}">
                                    <div class="form-group m-form__group col-md-6">
                                        <label for="last_name">Last Name</label>
                                        <input class="form-control m-input" type="text" id="l_name" placeholder="" name="l_name" value="{{old('l_name')}}">
                                        <span class="input_underline"></span>
                                        @error('l_name')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-form__group col-lg-12 address">
                                        <label for="last_name">Address</label>
                                        <input class="form-control m-input" type="text" id="address" placeholder="" name="address" value="{{old('address')}}">
                                        <span class="input_underline"></span>
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-form__group col-lg-6">
                                        <label for="contact_number">Contact Number</label>
                                        <input class="form-control m-input" type="text" placeholder="" id="mobile_no" name="mobile_no" value="{{old('mobile_no')}}">
                                        <span class="input_underline"></span>
                                        @error('mobile_no')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror

                                    </div>
                                    <div class="form-group m-form__group col-lg-6 otherIntrest">
                                        <label for="openings">Applying For</label>
                                        <div class="arrow_btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="199.404px" height="199.404px" viewBox="0 0 199.404 199.404" style="enable-background:new 0 0 199.404 199.404;" xml:space="preserve">
                                                <g>
                                                	<polygon points="199.404,63.993 171.12,35.709 99.702,107.127 28.284,35.709 0,63.993 99.702,163.695  "/>
                                                </g>
                                            </svg>
                                        </div>
                                        <select name="openings" class="form-control m-input future_studies" id="openings">
                                            <option value=""></option>
                                            @foreach($openings as $opening)
                                                <option value="{{ $opening->id }}" @if(old('openings')==$opening->id) selected @endif>{{ $opening->name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="input_underline"></span>
                                        @error('openings')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-form__group col-lg-12 futherStudies">
                                            <label for="future_studies">Planning for future studies or going abroad?</label>
                                            <div class="arrow_btn">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="199.404px" height="199.404px" viewBox="0 0 199.404 199.404" style="enable-background:new 0 0 199.404 199.404;" xml:space="preserve">
                                                    <g>
                                                    	<polygon points="199.404,63.993 171.12,35.709 99.702,107.127 28.284,35.709 0,63.993 99.702,163.695  "/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <select name="future_studies" class="form-control m-input future_studies" id="future_studies">
                                                <option value=""></option>
                                                <option value="Yes" @if(old('future_studies')=='Yes' ) selected @endif>Yes</option>
                                                <option value="No" @if(old('future_studies')=='No' ) selected @endif>No</option>
                                            </select>
                                            <span class="input_underline"></span>
                                            @error('future_studies')
                                            <span class="invalid-feedback" role="alert">
                                                <span>{{ $message }}</span>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="form-group m-form__group col-md-6">
                                        <label for="university_id">Select University</label>
                                        <div class="arrow_btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="199.404px" height="199.404px" viewBox="0 0 199.404 199.404" style="enable-background:new 0 0 199.404 199.404;" xml:space="preserve">
                                                <g>
                                                	<polygon points="199.404,63.993 171.12,35.709 99.702,107.127 28.284,35.709 0,63.993 99.702,163.695  "/>
                                                </g>
                                            </svg>
                                        </div>
                                        <select name="university_id" class="form-control m-input m-select2-general uniSelect" id="university_id">
                                            <option value=""></option>
                                            @if(!empty($universities))
                                            @foreach($universities as $uni)
                                            <option value="{{ $uni->id }}" @if(old('university_id')==$uni->id) selected @endif>{{ $uni->name }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span class="input_underline"></span>
                                        @error('university_id')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group m-form__group col-md-6">
                                        <label for="college_id">Select College</label>
                                        <div class="arrow_btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="199.404px" height="199.404px" viewBox="0 0 199.404 199.404" style="enable-background:new 0 0 199.404 199.404;" xml:space="preserve">
                                                <g>
                                                	<polygon points="199.404,63.993 171.12,35.709 99.702,107.127 28.284,35.709 0,63.993 99.702,163.695  "/>
                                                </g>
                                            </svg>
                                        </div>
                                        <select name="college_id" class="form-control m-input m-select2-general collegeSelect" id="college_id">
                                            <option value=""></option>
                                        </select>
                                        <span class="input_underline"></span>
                                        @error('college_id')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group m-form__group col-lg-6 futherStudies">
                                        <label for="streams">Education Stream</label>
                                        <div class="arrow_btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="199.404px" height="199.404px" viewBox="0 0 199.404 199.404" style="enable-background:new 0 0 199.404 199.404;" xml:space="preserve">
                                                <g>
                                                	<polygon points="199.404,63.993 171.12,35.709 99.702,107.127 28.284,35.709 0,63.993 99.702,163.695  "/>
                                                </g>
                                            </svg>
                                        </div>
                                        <select name="streams" class="form-control m-input future_studies" id="streams">
                                            <option value=""></option>
                                            @foreach($streams as $stream)
                                                <option value="{{ $stream->id }}" @if(old('streams')==$stream->id) selected @endif>{{ $stream->name }}</option>
                                           @endforeach
                                        </select>
                                        <span class="input_underline"></span>
                                        @error('streams')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group m-form__group col-lg-6 futherStudies">
                                        <label for="passing_year">Passing Year</label>
                                        <div class="arrow_btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="199.404px" height="199.404px" viewBox="0 0 199.404 199.404" style="enable-background:new 0 0 199.404 199.404;" xml:space="preserve">
                                                <g>
                                                	<polygon points="199.404,63.993 171.12,35.709 99.702,107.127 28.284,35.709 0,63.993 99.702,163.695  "/>
                                                </g>
                                            </svg>
                                        </div>
                                        <select name="year" class="form-control m-input future_studies" id="year">
                                            <option value=""></option>
                                            <option value="2021" @if(old('year')==2021) selected @endif>2021</option>
                                            <option value="2020" @if(old('year')==2020) selected @endif>2020</option>
                                            <option value="2019" @if(old('year')==2019) selected @endif>2019</option>
                                            <option value="2018" @if(old('year')==2018) selected @endif>2018</option>
                                            <option value="2017" @if(old('year')==2017) selected @endif>2017</option>
                                            <option value="2016" @if(old('year')==2016) selected @endif>2016</option>
                                            <option value="2015" @if(old('year')==2015) selected @endif>2015</option>
                                            <option value="2014" @if(old('year')==2014) selected @endif>2014</option>
                                            <option value="2013" @if(old('year')==2013) selected @endif>2013</option>
                                            <option value="2012" @if(old('year')==2012) selected @endif>2012</option>
                                        </select>
                                        <span class="input_underline"></span>
                                        @error('year')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group m-form__group col-lg-12 otherIntrest">
                                        <label for="other_interest">Interested technologies like... PHP, Python, ML, AngularJS, NodeJS, IONIC etc.</label>
                                        <input type="text" class="form-control other_interest m-input" name="other_interest" value="{{ old('other_interest') }}" placeholder="" id="other_interest">
                                        <span class="input_underline"></span>
                                        @error('other_interest')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group m-form__group col-lg-12 otherIntrest">
                                        <input type="file" class="form-control other_interest m-input" name="cvFile" value="{{ old('cvFile') }}" placeholder="" id="cvFile">
                                        Upload CV here [Only <strong>.pdf,.doc,.docx</strong> format files are allowed and file size must not exceed <strong>5MB</strong>]<span class="input_underline"></span><span><strong id="optional" style="display: none"> ( Optional )</strong></span><br>
                                        @error('cvFile')
                                        <span class="invalid-feedback" role="alert">
                                            <span>{{ $message }}</span>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="kt-login__actions">
                                    <input type="submit" id="submitForm" class="btn btn-brand btn-pill kt-login__btn-primary" value="Sign Up">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Page -->


    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#366cf3",
                    "light": "#ffffff",
                    "dark": "#282a3c",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>
    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{asset('assets/app/Login/plugins/plugins.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/app/Login/js/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->
    <script src="{{asset('assets/app/toastr/toastr.min.js')}}"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>

    {{--
<script src="{{asset('assets/app/Login/js/select2.js')}}" type="text/javascript"></script>
    --}}
<script>
    var AjaxUrl = '{{URL::to('/')}}';
    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    @if(session()->has('error'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ session()->get('error') }}", "Error");
    @php
        session()->forget('error');
    @endphp
            @endif
            @if(session()->has('info'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ session()->get('info') }}", "Info");
    @php
        session()->forget('info');
    @endphp
    @endif

    $('document').ready(function () {
        $('#enrollment').on('keyup', function() {
            var value = $(this).val();
            if(value == '') {
                $(this).siblings('.invalid-feedback').show();
            } else {
                 $(this).siblings('.invalid-feedback').hide();
            }

        });
        $('#email').on('keyup', function() {
            var email = $(this).val();
            console.log(email);
            var emailExp = new RegExp(/^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i);
            if(emailExp.test(email)) {
                $(this).siblings('.invalid-feedback').hide();
            } else {
                $(this).siblings('.invalid-feedback').show();
            }
        });
        $('#cvFile').change(function(e){
            var fileName = e.target.files[0].name;
            var temp = fileName.split('.');
            var extension = temp[1];
            if(extension == 'pdf' || extension == 'PDF') {
                $(this).siblings('.invalid-feedback').hide();
            } else {
                $(this).siblings('.invalid-feedback').show();
            }

        });
        $('#f_name').on('keyup', function() {
            var value = $(this).val();
            if(value == '') {
                $(this).siblings('.invalid-feedback').show();
            } else {
                 $(this).siblings('.invalid-feedback').hide();
            }
        });
        $('#l_name').on('keyup', function() {
            var value = $(this).val();
            if(value == '') {
                $(this).siblings('.invalid-feedback').show();
            } else {
                 $(this).siblings('.invalid-feedback').hide();
            }
        });
        $('#address').on('keyup', function() {
            var value = $(this).val();
            if(value == '') {
                $(this).siblings('.invalid-feedback').show();
            } else {
                 $(this).siblings('.invalid-feedback').hide();
            }
        });
        $('#mobile_no').on('keyup', function() {
            var value = $(this).val();
            if(value == '') {
                $(this).siblings('.invalid-feedback').show();
            } else if (value.length != 10) {
                $(this).siblings('.invalid-feedback').show();
            } else {
                $(this).siblings('.invalid-feedback').hide();
            }
        });
        $('#studies').on('change', function(){
            $(this).siblings('.invalid-feedback').hide();
        });
        $('#university_id').on('change', function(){
            $(this).siblings('.invalid-feedback').hide();
            $('#collegeId').siblings('.invalid-feedback').hide();
        });
        $('#college_id').on('change', function() {
            $(this).siblings('.invalid-feedback').hide();
        });
        $('#openings').on('change', function() {
            $(this).siblings('.invalid-feedback').hide();
        });
        $('#streams').on('change', function() {
            $(this).siblings('.invalid-feedback').hide();
        });
        $('#year').on('change', function() {
            $(this).siblings('.invalid-feedback').hide();
        });
        $('#other_interest').on('keyup', function() {
            var value = $(this).val();
            if(value == '') {
                $(this).siblings('.invalid-feedback').show();
            } else {
                $(this).siblings('.invalid-feedback').hide();
            }

        });
        if($(".uniSelect").val() !='') {

            var uniId = $(".uniSelect").val();
            $.ajax({
                url: AjaxUrl +'/exam/getColleges',
                data : {"uniId" : uniId,"exam":'{{$slug}}'},
                type: 'post',
                success: function (response) {
                    if(response != '') {
                        if(response.college.length > 0) {
                            $(".collegeSelect").empty().append('<option value=""></option>');
                            $.each(response.college,function(key, value)
                            {
                                $(".collegeSelect").append('<option value=' + value.id + '>' + value.name + '</option>');
                                $(".collegeSelect").parents('.form-group').addClass('focused');
                                $(".collegeSelect").addClass('filled');
                            });
                            $(".collegeSelect").val("{{ old('college_id') }}");
                        }
                        else {
                            $(".collegeSelect").empty().append('<option value=""></option>');
                        }
                    }
                    else {
                        $(".collegeSelect").empty().append('<option value=""></option>');
                    }
                },
            });
        }
        var checkUniAjax = false;
        $(".uniSelect").change(function() {
            collegeCheck='false';
            $('.collegeSelect').find('option').remove()
            var uniId = $(this).val();
            // if(checkUserAjax) {
                // console.log('already called');
            // }
            // else {
                checkUniAjax = true;
                $.ajax({
                    url: AjaxUrl + '/exam/getColleges',
                    data: {"uniId": uniId, "exam": '{{$slug}}'},
                    type: 'post',
                    success: function (response) {
                        if (response != '') {
                            if (response.college.length > 0) {
                                $(".collegeSelect").empty().append('<option value=""></option>');
                                $.each(response.college, function (key, value) {
                                    $(".collegeSelect").append('<option value=' + value.id + '>' + value.name + '</option>');
                                    $(".collegeSelect").parents('.form-group').addClass('focused');
                                    $(".collegeSelect").addClass('filled');
                                });
                            }
                            else {
                                $(".collegeSelect").empty().append('<option value=""></option>');
                            }

                        }
                        else {
                            $(".collegeSelect").empty().append('<option value=""></option>');
                        }
                        checkUniAjax = false;
                        $(".collegeSelect").removeClass('filled');
                        $(".collegeSelect").parents('.form-group').removeClass('focused');
                    },
                });
            // }

        });
        var checkUserAjax = false;
        $(".enrollmentAdd").change(function(){

                var envNo = $(this).val();

                if($.trim(envNo) != '' && envNo.length != 0) {


                    $(".loadingState").show();
                    if(checkUserAjax) {
                        console.log('already called');
                    }
                    else {
                        checkUserAjax = true;
                        $.ajax({
                            url: AjaxUrl +'/exam/checkUser',
                            data : {"envNo" : envNo,"exam":'{{$slug}}'},
                            type: 'post',
                            success: function (response) {
                                $(".loadingState").hide();
                                if(response != '') {
                                    console.log(response);
                                    if(response.user.cv) {
                                        $('#optional').show();
                                    }
                                    $(".enrollmentAdd").prop('readonly',true);
                                    $('input[name=f_name]').val(response.user.first_name);
                                    $('#f_name').parents('.form-group').addClass('focused');
                                    $('#f_name').addClass('filled');
                                    $('input[name=l_name]').val(response.user.last_name);
                                    $('#l_name').parents('.form-group').addClass('focused');
                                    $('#l_name').addClass('filled');
                                    $('input[name=email]').val(response.user.email);
                                    $('#email').parents('.form-group').addClass('focused');
                                    $('#email').addClass('filled');
                                    $('input[name=mobile_no]').val(response.user.mobile_no);
                                    $('#mobile_no').parents('.form-group').addClass('focused');
                                    $('#mobile_no').addClass('filled');
                                    $('input[name=address]').val(response.user.address);
                                    $('#address').parents('.form-group').addClass('focused');
                                    $('#address').addClass('filled');
                                    $('input[name=other_interest]').val(response.user.other_interest);
                                    $('#other_interest').parents('.form-group').addClass('focused');
                                    $('#other_interest').addClass('filled');
                                    var collegeId = response.user.college_id;
                                    if(response.user.university_id > 0 ) {
                                        $(`#university_id option[value='${response.user.university_id}']`).prop('selected', true);
                                        $('#university_id').parents('.form-group').addClass('focused');
                                        $('#university_id').addClass('filled');
                                        $.ajax({
                                            url: AjaxUrl +'/exam/getColleges',
                                            data : {"uniId" : response.user.university_id,"exam":'{{$slug}}'},
                                            type: 'post',
                                            success: function (res) {
                                                console.log(res);
                                                if(res != '') {
                                                    if(res.college.length > 0) {
                                                        $.each(res.college,function(key, value)
                                                        {
                                                            if(collegeId == value.id) {
                                                                $(".collegeSelect").append('<option value=' + value.id + ' selected >' + value.name + '</option>');
                                                                $(".collegeSelect").parents('.form-group').addClass('focused');
                                                                $(".collegeSelect").addClass('filled');

                                                            } else {
                                                                $(".collegeSelect").append('<option value=' + value.id + '>' + value.name + '</option>');
                                                            }
                                                        });
                                                        if(response.user.college_id > 0 ) {
                                                            $(".collegeSelect").val(response.user.college_id);
                                                        }
                                                    }
                                                    else {
                                                        $(".collegeSelect").empty().append('<option value=""></option>');
                                                    }
                                                }
                                                else {
                                                    $(".collegeSelect").empty().append('<option value=""></option>')
                                                }
                                                if(response.user.openings) {
                                                    $(`#openings option[value='${response.user.openings}']`).prop('selected', true);
                                                    $('#openings').parents('.form-group').addClass('focused');
                                                    $('#openings').addClass('filled');
                                                }
                                                if(response.user.education_stream) {
                                                    $(`#streams option[value='${response.user.education_stream}']`).prop('selected', true);
                                                    $('#streams').parents('.form-group').addClass('focused');
                                                    $('#streams').addClass('filled');
                                                }
                                                if(response.user.year) {
                                                    $('#year option[value="'+response.user.year+'"]').prop('selected', true);
                                                    $('#year').parents('.form-group').addClass('focused');
                                                    $('#year').addClass('filled');
                                                }
                                            },
                                        });
                                    }
                                    if( response.user.future_studies != '' ) {
                                        $(".future_studies").val(response.user.future_studies);
                                        $('#future_studies').parents('.form-group').addClass('focused');
                                        $('#future_studies').addClass('filled');
                                    }
                                    $("#email").siblings('.invalid-feedback').hide();
                                    $("#f_name").siblings('.invalid-feedback').hide();
                                    $("#l_name").siblings('.invalid-feedback').hide();
                                    $("#mobile_no").siblings('.invalid-feedback').hide();
                                    $("#future_studies").siblings('.invalid-feedback').hide();
                                    $("#university_id").siblings('.invalid-feedback').hide();
                                    $("#college_id").siblings('.invalid-feedback').hide();
                                    $("#openings").siblings('.invalid-feedback').hide();
                                    $("#year").siblings('.invalid-feedback').hide();
                                    $("#streams").siblings('.invalid-feedback').hide();
                                    $("#other_interest").siblings('.invalid-feedback').hide();
                                    $("#address").siblings('.invalid-feedback').hide();
                                    $("#cvFile").siblings('.invalid-feedback').hide();
                                }
                                checkUserAjax = false;
                            },
                            error: function (xhr) {
                                $(".loadingState").hide();
                                $(".enrollmentAdd").val('');
                                $('input[name=f_name]').val('');
                                $('input[name=l_name]').val('');
                                $('input[name=email]').val('');
                                $('input[name=mobile_no]').val('');
                                $(".enrollmentAdd").prop('disabled',false);
                                var errors = xhr.responseJSON;
                                console.log(errors.message);

                                toastr.options = {
                                    "progressBar": true,
                                };
                                toastr.error(errors.message , "Error");
                                checkUserAjax = false;
                            }
                        });
                    }

                }
        });
        $('#submitForm').on('click', function() {
            $('#loaderdiv').show();
            setTimeout(function(){
                var $form = $("#registerForm"),
                $successMsg = $(".alert");
                $.validator.addMethod('alphabets', function(value, element) {
                    return this.optional(element) || value == value.match(/^[a-zA-Z0-9\s]*$/);
                });
                $.validator.addMethod("letters", function(value, element) {
                    return this.optional(element) || value == value.match(/^[a-zA-Z\s]*$/);
                });
                $form.validate({

                    rules: {
                        enrollment: {
                            required: true,
                            minlength: 6,
                            alphabets: true,
                        },
                        f_name: {
                            required: true,
                        },
                        l_name: {
                            required: true,
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        address: {
                            required: true,
                        },
                        streams: {
                            required: true,
                        },
                        mobile_no: {
                            required: true,
                            minlength: 10,
                            maxlength: 10,
                        },
                        future_studies: {
                            required: true,
                        },
                        university_id: {
                            required: true,
                        },
                        college_id : {
                            required: true,
                        },
                        openings : {
                            required: true,
                        },
                        year: {
                            required: true,
                        },
                        other_interest: {
                            required: true,
                        }
                    },
                    messages: {
                        enrollment: 'Enrollment number is required ',
                        f_name: "First name is required",
                        l_name: "Last name is required",
                        email: "Email ID is required",
                        streams: "Please select education stream ",
                        address: 'Please enter your address ',
                        mobile_no: 'Please enter valid mobile number ',
                        future_studies: 'Please select one option ',
                        university_id: 'Please select university ',
                        college_id: 'Please select your college ',
                        openings: 'Please select your opening ',
                        year: 'Please select your passing year ',
                        other_interest: 'Please write your interest ',

                    },
                    invalidHandler: function(){
                        //the function is called when anything was invalid in the form
                        $('#loaderdiv').hide();
                    },
                    submitHandler: function() {
                        $("#registerForm").submit();
                    }

                });
            }, 10000);


        });
    });
</script>
    <!--begin::Page Scripts(used by this page) -->
    <!-- Script For Design animation -->
    <script>
        $(document).ready(function() {
            $(document).on('focus keypress', 'input, select', function() {
                $(this).parents('.form-group').addClass('focused');
            });

            $('input, select').each(function() {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.form-group').removeClass('focused');
                } else {
                    $(this).parents('.form-group').addClass('focused');
                    $(this).addClass('filled');
                }
            });

            $(document).on('blur submit', 'input, select', function() {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.form-group').removeClass('focused');
                } else {
                    $(this).parents('.form-group').addClass('focused');
                    $(this).addClass('filled');
                }
            });


        });
    </script>
    <!-- Script For Design animation -->
    <!--end::Page Scripts -->
</body>
<!-- end::Body -->

</html>
