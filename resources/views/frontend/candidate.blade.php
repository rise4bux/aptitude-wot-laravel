@extends('layouts.front')
<!-- END: Header -->
<!-- BEGIN: Left Aside -->
@section('content')

    <!-- END: Left Aside -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">
        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->

                <div class="card col-sm-12 mt-4" style="align-self: center ; text-align: center">
                    <h3 class="mt-2">SERVICES</h3>
                    <div class="mt-4" style="text-align: left">
                        <h4>WEB APPLICATION DEVELOPMENT</h4>
                        <p>Web Applications tend to make our lives easier and fast. We take a very cautionary approach
                            while engineering a Web Application keeping in mind "Customer satisfaction" in the center.
                            Based on the complexities and deliverable we apply different development strategies to make
                            sure we develop a finishing web app. We provide our valuable services in many industries
                            that are shifting from a local business to an online business. We just do not develop! But
                            we maintain and we grow.</p>
                    </div>
                    <div class="mt-4" style="text-align: left">
                        <h4> GRAPHIC & WEB DESIGN</h4>
                        <p>A greatly designed website is certainly the most important requisite for a business owner,
                            who enters into the online business world and wants to offer 100% satisfaction to his
                            customers. Our inclusive approach to web design goes beyond the traditional "Desktop view"
                            and encompasses how the site looks, how it functions, and how different users interact with
                            the Website features giving them a sense of 'one of it's own' and 'trust'.</p>
                    </div>
                    <div class="mt-4" style="text-align: left">
                        <h4> MOBILE APPLICATION DEVELOPMENT</h4>
                        <p>In this generation, we need to satisfy customer’s needs by creating, communicating,
                            delivering and exchanging offering that has value for customers and clients. Keeping this in
                            mind, we Design and Develop Mobile applications that is not only a Great Value for Money,
                            but also has a special place in the society which binds people from different cores
                            together.</p>
                    </div>
                    <div class="mt-4" style="text-align: left">
                        <h4> ARTIFICIAL INTELLIGENCE</h4>
                        <p>With advancing technology, we are into the age where raw data is so much and the Desire of a
                            system that does computing intelligently, Artificial Intelligence has a very bright future
                            in all the major sectors like Financial Services, Government, Health Care, Military,
                            Transportation and Oil & Gas. On the other hand, Machine Learning being a part of AI, it's
                            necessity is vital factor as advanced systems constantly needs an algorithm that self
                            evolves based on patterns and gives output wisely. We relentlessly research and work on
                            solutions that makes the life easier</p>
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection

