
<!DOCTYPE html>

<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>

    <title>Aptitude test | Login Page</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">        <!--end::Fonts -->


    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{asset('assets/app/Login/css/login-page.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{asset('assets/app/Login/plugins/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/app/Login/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />


    <link rel="shortcut icon" href="{{asset("assets/app/media/img/logos/weblogo.png")}}" />
    <link href="{{asset('assets/app/toastr/toastr.min.css')}}" rel="stylesheet">
    <!-- This style for student registration form -->
    <style>
        html { height: 100%; }
        body { background: -webkit-linear-gradient(to top, #EF473A, #CB2D3E); background: linear-gradient(to top, #EF473A, #CB2D3E);  }
        .student_login_form { position: relative; }
        .student_login_form .kt-login__wrapper { padding: 40px 0 !important; margin: 0 auto !important; width: 70% !important; }
        .student_login_form .kt-login__container { width: 100% !important;  }
        .student_login_form .kt-login__logo { background: #fff; padding: 20px; width: 100%; border-radius: 10px; margin: 0 auto 40px !important; }

        .student_login_form .login-form_box { padding: 30px; background-color: #fff; border-radius: 10px; }

        .student_login_form .kt-login__title, .student_login_form .kt-login__desc { color: #000 !important; }

        .student_login_form .kt-login--signup { width: 800px !important }

        .student_login_form .input-group { position: relative; margin-bottom: 35px !important; color: #000 !important }

        .student_login_form .form-control { margin-top: 0 !important; background: transparent !important;}

        .student_login_form label { font-size: 15px; position: absolute; top: 7px; left: 0; padding: 5px 12px 0 0  !important;  z-index: 3; transition: all 150ms ease-out; -moz-transition: all 150ms ease-out; -webkit-transition: all 150ms ease-out; text-overflow: ellipsis; white-space: nowrap; overflow: hidden; width: 100%; }

        .student_login_form .focused label { transform: translate(0, -29px); font-size: 12px }

        .input_underline { display: block; position: absolute; left: 0; top: 44px; height: 2px; transform: translateX(-50%); -moz-transform: translateX(-50%); -webkit-transform: translateX(-50%); left: 50%; width: 0; background: rgb(0 0 0 / 40%); z-index: 5; transition: all ease-in-out .2s; -moz-transition: all ease-in-out .2s; -webkit-transition: all ease-in-out .2s; }

        .focused .input_underline { width: 100%; }

        .student_login_form input  { color: #000 !important; border-radius: 0 !important; border: none !important; border-bottom: 1px solid #0000004a !important; padding: 5px 12px !important; position: relative !important; z-index: 4; }

        .student_login_form .btn  { background-color: #333 !important; color: #fff !important; border: none !important; border-radius: 25px !important; padding: 0 40px !important; height: 45px !important;}

        .student_login_form .btn:hover { background-color: #e23033 !important}
        .student_login_form  .kt-login__actions {  margin-top: 15px !important; }
        /* Responsive */
        @media (max-width: 2881px) and (min-width: 2401px) {     
            
            .student_login_form .focused label {
                font-size: 24px !important;
                transform: translate(0, -48px) !important;
            }

            .student_login_form .input-group {
                margin-bottom: 45px !important
            }  

            .student_login_form .input_underline {
                top: 76px;
                height: 4px;
            }

            .login-form_box {
                padding: 45px !important
            }  
  
            .kt-form .input-group label {
                font-size: 26px;
                top: 14px
            }  

            .student_registration_form .input-group {
                margin-bottom: 65px !important
            } 

            .kt-login.kt-login--v4 .kt-login__wrapper .kt-login__container .kt-form .input-group .invalid-feedback {
                font-size: 20px;
                padding-left: 0 !important;
            }  
 
        }

        @media (max-width: 2400px) and (min-width: 1921px) {  

            .student_login_form .focused label {
                font-size: 24px !important;
                transform: translate(0, -48px) !important;
            }
        
            .student_login_form .input-group {
                margin-bottom: 40px !important
            }  
        
            .student_login_form .input_underline {
                top: 66px;
                height: 4px;
            }
        
            .login-form_box {
                padding: 45px !important
            }  

            .kt-form .input-group label {
                font-size: 26px;
                top: 14px
            }  

            .student_registration_form .input-group {
                margin-bottom: 45px !important
            } 

            .kt-login.kt-login--v4 .kt-login__wrapper .kt-login__container .kt-form .input-group .invalid-feedback {
                font-size: 20px;
                padding-left: 0 !important;
            } 

        }
        @media (max-width: 991px){
            .student_login_form .kt-login__wrapper { width: 90% !important; padding-right: 15px !important; padding-left: 15px !important;  }
        }
        @media (max-width: 767px){
            .student_login_form .kt-login__wrapper { width: 100% !important; }
            .student_login_form label { font-size: 13px !important }
            .student_login_form .sign_up_form_box { padding: 20px }
        }
    </style>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-37564768-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-37564768-1');
    </script>    </head>
<!-- end::Head -->

<!-- begin::Body -->
<body  style=""  class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >

<div class="kt-grid kt-grid--ver kt-grid--root kt-page student_login_form">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{--{{asset('/assets/app/media/img/bg/bg-2.jpg')}}--}});">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="{{ route('welcome') }}">
                            <img src="{{asset('assets/app/media/img/logos/weboccult.png')}}" >
                        </a>
                    </div>
                    <div class="kt-login__signin login-form_box ">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">Sign In for Candidates</h3>
                        </div>
                        <form class="kt-form" action="{{route('candidate.save')}}" method="post">
                            @csrf
                            <div class="input-group">
                                <label for="enroll_number">Enrollment Number</label>
                                <input class="form-control" type="text" placeholder="" id="enroll_number" name="enrollment" autocomplete="off">
                                <span class="input_underline"></span>
                                @error('enrollment')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group">
                                <label for="password_input">Password</label>
                                <input class="form-control" type="password" id="password_input" placeholder="" name="password">
                                <span class="input_underline"></span>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <input type="hidden" name="slug" value="{{$slug}}">
                            <div class="kt-login__actions">
                                <button type="submit" class="btn btn-brand btn-pill kt-login__btn-primary">Sign In</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Page -->


<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {"colors":{"state":{"brand":"#366cf3","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};
</script>
<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{asset('assets/app/Login/plugins/plugins.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/app/Login/js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/app/toastr/toastr.min.js')}}"></script>
<!--end::Global Theme Bundle -->
<script>
    @if(session()->has('error'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ session()->get('error') }}", "Error");
    @php
        session()->forget('error');
    @endphp
    @endif
    @if(session()->has('info'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ session()->get('info') }}", "Info");
    @php
        session()->forget('info');
    @endphp
    @endif
</script>

<!--begin::Page Scripts(used by this page) -->
<!-- Script For Design animation -->
<script>
        $(document).ready(function() {
            $(document).on('focus keypress', 'input', function() {
                $(this).parents('.input-group').addClass('focused');
            });

            $('input').each(function() {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.input-group').removeClass('focused');
                } else {
                    $(this).parents('.input-group').addClass('focused');
                    $(this).addClass('filled');
                }
            });

            $(document).on('blur submit', 'input', function() {
                var inputValue = $(this).val();
                if (inputValue == "") {
                    $(this).removeClass('filled');
                    $(this).parents('.input-group').removeClass('focused');
                } else {
                    $(this).parents('.input-group').addClass('focused');
                    $(this).addClass('filled');
                }
            });
        });
    </script>
    <!-- Script For Design animation -->
<!--end::Page Scripts -->
</body>
<!-- end::Body -->
</html>
