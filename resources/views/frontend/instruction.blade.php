@extends('layouts.front')

@section('css')
<style>
    .m-header--fixed .m-body.instruction_section_outer { padding-top: 40px !important; padding-bottom: 40px}
    .instruction_section_outer .kt-login__logo { margin: 0 auto 40px !important; text-align: center; background: #fff; padding: 20px; width: 100%; border-radius: 10px; }
    .instruction_section_outer{ min-height: 100vh; position: relative; background: -webkit-linear-gradient(to top, #EF473A, #CB2D3E); background: linear-gradient(to top, #EF473A, #CB2D3E);   }
    .instruction_section_outer .w_70 { width: 70%; margin: 0 auto; }
    .instruction_section_outer .instructions_page_box{ position: relative;  }
    .instruction_section_outer .instruction_title { color: #000; text-align: center; font-size: 34px; border-bottom: 1px solid; padding-bottom: 15px; }
    .instruction_section_outer .instructions_page_box h6 span { color: #000 }
    .instruction_section_outer .instructions_page_box h6 { margin-top: 25px }
    .instruction_section_outer span.arrow_icon { padding-left: 10px; display:inline-block; }
    .instruction_section_outer .btn { font-size: 14px; height: 40px; background-color: #333 !important; outline: none; border: none !important; border-radius: 25px !important; padding: 0 40px !important; color: #fff !important;}
    .instruction_section_outer .btn:hover { background-color: #e12d36 !important; }
    .all_the_best_img { width: 170px; margin: 0 auto; padding: 15px 0; }
    .all_the_best_img img { width: 100%; height: auto; } 
    /* Responsive */
    @media (max-width: 2881px) and (min-width: 2401px) {
        .kt-login__logo img { min-height: 100px }
        .instructions_page_box { padding: 45px !important }
        .instruction_section_outer .instruction_title { font-size: 45px }
        .instructions_page_box h6 { font-size: 30px !important }
        .instructions_page_box ol li { font-size: 26px !important; line-height: 1.8 }
        .all_the_best_img { width: 400px }
        .btn_outer { padding-top: 40px !important }
        .instruction_section_outer .btn { font-size: 26px; border-radius: 45px !important; padding: 0 95px !important; height: 90px !important; }
    }
    @media (max-width: 2400px) and (min-width: 1921px) {
        .kt-login__logo img { min-height: 80px }
        .instructions_page_box { padding: 45px !important }
        .instruction_section_outer .instruction_title { font-size: 2.8rem }
        .instructions_page_box h6 { font-size: 26px !important }
        .instructions_page_box ol li { font-size: 24px !important; line-height: 1.6 }
        .all_the_best_img { width: 400px }
        .btn_outer { padding-top: 40px !important }
        .instruction_section_outer .btn { font-size: 26px; border-radius: 45px !important; padding: 0 60px !important; height: 70px !important; }
    }
    @media (max-width: 1920px) {}
    @media (max-width: 991px){
        .instruction_section_outer .w_70 { width: 90%; padding-right: 15px ; padding-left: 15px; }
    }
    @media (max-width: 767px){
        .instruction_section_outer .w_70 { width: 100%; }
        .instruction_section_outer .instructions_page_box { padding: 20px !important  }
        .instruction_section_outer .instruction_title { font-size: 25px }
        .instructions_page_box ol { margin: 15px 10px 15px 20px !important;  }
    } 
    /* Key frame */
    @keyframes swaping {
        0%, 20%, 50%, 80%, 100% {
            transform: translateX(0px);
        }
        40% {
            transform: translateX(15px);
        }
        60% {
            transform: translateX(5px);
        }
    }
</style>
<!-- <script src={{asset("assets/vendors/base/vendors.bundle.js")}} type="text/javascript"></script> -->
@endsection

@section('content')

@section('content') 
    <!-- END: Left Aside -->
    <!-- begin::Body -->
    <div class="instruction_section_outer m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
          >
        <div class="w_70 m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop   m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Header Logo -->
                
                    <div class="kt-login__logo">
                        <a href="{{ route('welcome') }}">
                            <img src="{{asset('assets/app/media/img/logos/weboccult.png')}}" >
                        </a>
                    </div>
                
                <!-- END: Header Logo -->
                <!-- BEGIN: Subheader -->
                <div class="">
                    <div class="">
                        <div class="">
                            <div class="instructions_page_box" style=" padding: 30px;  background: #fff; border-radius: 10px">

                                <h2 class="instruction_title" >Instructions</h2>
                                <h6>Welcome, <span>{{auth()->user()->first_name}} {{auth()->user()->last_name}}</span></h6>
                                {!! html_entity_decode($instruction)!!}
                                <div class="all_the_best_img">
                                    <img src="{{asset('assets\demo\demo9\media\img\all_the_best.png')}}" alt="ALL THE BEST" srcset="">
                                </div>
                                <div class="btn_outer" style="width: 100%; margin: 0 auto; text-align: center; padding-top: 20px">
                                    <button class="btn startButton" style=" color: #ffffff" onclick="start();" >
                                      @if(!empty($examuser))
                                          <span>Resume Exam<span class="arrow_icon">⟶</span></span>
                                      @else
                                          <span>Start Exam<span class="arrow_icon">⟶</span></span>
                                      @endif

                                    </button>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script type="text/javascript">

        function start()
        {
            console.log("in start");
           var slug= '@json($slug)';
           var id='@json($id)';
           $('.startButton').remove();
           var h = $(window).height(); 
            var w = $(window).width();
           window.open('{{env("APP_URL")}}/exam/questions/{{$slug}}/{{$id}}', "Ratting", "width="+w+",height="+h+",directories=no,channelmode=yes,titlebar=no,toolbar=no,status=0,scrollbars=no,location=no,menubar=no,navigationbar=no,resizable=no"); 
            $(this).hide('slow');                                                         
            clickme();
            return false;
        }
        function clickme()
        {
            window.close()
        }
    </script>


@endsection
