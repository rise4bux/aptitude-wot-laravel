@extends('layouts.app')
<!-- END: Header -->
<!-- BEGIN: Left Aside -->
@section('content')   

    <!-- END: Left Aside -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">

        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <a href="{{route('home')}}">
                                <h3 class="m-subheader__title m-subheader__title--separator">
                                    Dashboard
                                </h3>
                            </a>
                            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                <li class="m-nav__item">
                                    <a href="{{route('exams.index')}}" class="m-nav__link">
												<span class="m-nav__link-text">
									Exams
												</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="m-content">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <i class="la la-users kt-font-brand"></i>
                                        Exam Users
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <a href="javascript:void(0);" data-id="{{ $exam_id }}" class=" exportButton btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" >
												<span>
													<i class="la la-file-excel-o"></i>
													<span>  Export</span>
												</span>
                                </a>
                               {{-- <a href="javascript:void(0);" class="exportButton" data-id="{{ $exam_id }}">
                                    <button type="button" class="btn btn-primary" id="">
                                        Export
                                    </button>
                                </a>--}}
                               {{-- <a href="{{route('exams.add')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-book"></i>
													<span> Create exam</span>
												</span>
                                </a>--}}
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-form m-form--label-align-right m--margin-bottom-30">
                            <form class="m-form m-form--fit m--margin-bottom-20" id="filterForm" action="{{ route('exams.exportstudent',$exam_id) }}" method="post">
                                @csrf
                                <input type="hidden" name="filter" value="" id="filterData"/>
                                <div class="row m--margin-bottom-20">
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label class="">College</label>
                                            <select class="form-control m-input collegeSelect m-select2" id="m_select2_3" name="college_id[]" multiple>
                                                <option value="">Select College</option>
                                                @if(!empty($colleges))
                                                    @foreach($colleges as $college)
                                                        <option value="{{ $college->id }}">{{ $college->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                    </div>
                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                        <label class="">Exam Date</label>
                                        <input type="text" name="exam_date" id="exam_date" readonly="readonly" class="m-input form-control" value="{{ date('m/d/Y') }}">
                                    </div>
                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                        <label class="">Status</label>
                                       <select class="form-control" name="exam_status" id="exam_status">
                                           <option value="">Select Status</option>
                                           <option value="Completed">Completed</option>
                                           <option value="OnGoing">OnGoing</option>
                                       </select>
                                    </div>
                                    <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                        <label class="">Marks</label>
                                       <input type="number" class="form-control" id="marks" name="marks" min="1" placeholder="Eg. 7 meaning >= 7">
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile m--margin-top-25">
                                        <button  type="button" class="btn btn-brand m-btn m-btn--icon" id="m_search">
                                                    <span>
                                                        <i class="la la-search"></i>
                                                        <span>Search</span>
                                                    </span>
                                        </button>
                                        <button type="button" class="btn btn-secondary m-btn m-btn--icon" id="m_reset" >
                                                    <span>
                                                        <i class="la la-close"></i>
                                                        <span>Reset</span>
                                                    </span>
                                        </button>
                                    </div>
                                </div>
                                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                    <div class="row ">
                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch" name="generalSearch">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
															<span><i class="la la-search"></i></span>
														</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                            </div>
                            <div class="m_datatable" id="ajax_data"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="examModal" class="modal fade in">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px">
                    <h4 class="modal-title">Marks Detail</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body categoryResult text-center" style="padding: 20px;text-align: center">

                </div>
                <div class="modal-footer" style="padding: 15px">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var examID = '{{$exam_id}}';
        var AjaxUrl = '{{URL::to('/')}}';
      /*  $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });*/
        var DatatableRemoteAjaxDemo = {

            init: function () {

                $("#exam_date").datepicker().datepicker("setDate", new Date());
                $("#m_select2_3").select2({placeholder:"Select College"});

                var t;
                atable = $(".m_datatable").mDatatable({
                    data: {
                        saveState :false,
                        type: "remote",
                        source: {
                            read: {
                                type: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: AjaxUrl + '/admin/exams/participants/examusers-ajax/'+examID,
                               /* params:{'exam_date':$("#exam_date").val()},*/
                                map: function (t) {
                                    console.log(t);
                                    var e = t;
                                    return void 0 !== t.data && (e = t.data), e
                                }
                            }
                        },
                        pageSize: 10,
                        serverPaging: !0,
                        serverFiltering: !0,
                        serverSorting: false,

                    },

                    layout: {
                        scroll: false,
                        footer: false
                    },
                    sortable: !0,
                    pagination: !0,
                    toolbar: {
                        items: {
                            pagination: {
                                pageSizeSelect: [10, 20, 30, 50, 100]
                            }
                        }
                    },
                    search: {
                        input: $("#generalSearch"),'key':'search'
                    },
                    columns: [{
                        field: "id",
                        title: "#",
                        sortable: !1,
                        width: 10,
                        selector: !1,
                        textAlign: "center"
                    }, {
                        field: "first_name",
                        title: "User",
                        sortable: true,
                        width: 60,
                        template: function (t) {
                            return t.users.first_name+' '+t.users.last_name;
                        }
                    }, {
                        field: "university",
                        title: "University",
                      /*  attr: {
                            nowrap: "nowrap"
                        },*/
                        width: 100,
                        template: function (t) {
                            return t.users.university.name;
                        }
                    }, {
                        field: "college",
                        title: "College",
                        width: 100,
                        template: function (t) {
                            return t.users.college.name;
                        }
                    }, {
                        field: "enrollment_no",
                        title: "Enrollment No",
                        width: 120,
                        template: function (t) {
                            return t.users.enrollment_no;
                        }
                    }, {
                        field: "email",
                        title: "Email",
                        width: 120,
                        template: function (t) {
                            return t.users.email;
                        }
                    }, {
                        field: "total_questions",
                        title: "Total Questions",
                        width: 80,
                        template: function (t) {
                            return t.total_questions;
                        }

                    }, {
                        field: "total_correct_answers",
                        title: "Correct Answers",
                        width: 80,
                        template: function (t) {
                            return t.total_correct_answers;
                        }

                    }, {
                        field: "percentage",
                        title: "Percentage",
                        width: 80,
                        template: function (t) {
                            if(t.percentage != '' && t.percentage != null) {
                                return t.percentage+'%';
                            }
                            else {
                                return '';
                            }
                        }

                    }, {
                        field: "exam_date",
                        width: 80,
                        title: "Exam Date",
                    }, {
                        field: "status",
                        title: "Status",
                        width: 100,
                        template: function (t) {
                            if(t.status==='Completed'){
                                return `<span style="width: 128px;"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-success">`+ t.status + `</span></span>`
                            }
                            else{
                                return `<span style="width: 128px;"><span class="btn btn-bold btn-sm btn-font-sm  btn-label-danger">`+ t.status + `</span></span>`
                            }
                        }

                    }, {
                        field: "Actions",
                        width: 50,
                        title: "Actions",
                        sortable: !1,
                        overflow: "visible",
                        template: function (t, e, a) {
                            var buttonHTML = '';
                            buttonHTML += `<a href="javascript:void(0)"><button  data-id="`+t.user_id+`" class="btn btn-sm btn-clean btn-icon btn-icon-md markDetail" title='Marks Detail'><i class="la la-file-text"></i></button></a>`;
                            return buttonHTML;
                        }
                    }]
                }), $("#m_form_status").on("change", function () {
                    atable.search($(this).val(), "Status")
                }), $("#m_form_type").on("change", function () {
                    atable.search($(this).val(), "Type")
                }), $("#m_form_status, #m_form_type").selectpicker(),

                $(document).on('click','#m_search',function (e) {


                    atable.setDataSourceParam('college_id',$(".collegeSelect").val());
                    atable.setDataSourceParam('exam_date',$('#exam_date').val());
                    atable.setDataSourceParam('exam_status',$('#exam_status').val());
                    atable.setDataSourceParam('marks',$('#marks').val());
                    atable.setDataSourceParam('filter','1');
                    $("#filterData").val(1);
                    //$('.m_datatable').mDatatable('reload');
                   // atable.search($(".collegeSelect").val(), 'college_id');
                    atable.reload();

                });

            }
        };
        jQuery(document).ready(function () {
            DatatableRemoteAjaxDemo.init();
            $(document).on('click', '#m_reset', function (e) {
                $("#filterForm").resetForm();
                $('#exam_date').val('');
                //$('#m_select2_3').val(null);
                $("#m_select2_3").select2("val", "");
                $("#m_select2_3").val([]).trigger('change');
                atable.setDataSourceParam('college_id','');
                atable.setDataSourceParam('exam_date','');
                atable.setDataSourceParam('exam_status','');
                atable.setDataSourceParam('marks','');
                atable.setDataSourceParam('filter','1');
                $("#filterData").val(1);
                atable.reload();
            });
        });
        $(document).on('click', '.markDetail', function (e) {

            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/users-marks',
                type: 'POST',
                data:{ exam_id:examID,user_id:$(this).data('id')},
                success: function (response) {
                    /*console.log(response);*/
                    $(".categoryResult").html(response);
                    $('#examModal').modal();
                },
            });
        });
        $(document).on('click', '.exportButton', function (e) {
            $("#filterForm").submit();
        });


    </script>
 {{--   <script src="{{asset('assets/app/js/examusers.js')}}"></script>--}}
    {{--<script src="{{asset('assets/app/js/exams.js')}}"></script>--}}
@endsection