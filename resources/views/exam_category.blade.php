<div class="tabParent">
    <ul class="nav tab-container">
            @foreach($categories as $key=>$value)
                <li class="tab col" id="{{$key}}" style="margin: 0 auto">
                    @if($key == 0)
                        <button class="btn btn-secondary activeClass tabClass" id="{{$value->category->id}}" style="padding: 10px;width: 100%">{{$value->category->name}}</button>
                    @else
                        <button class="btn btn-secondary tabClass" id="{{$value->category->id}}" style="padding: 10px;width: 100%">{{$value->category->name}}</button>
                    @endif
                </li>
        @endforeach
    </ul>
</div>
