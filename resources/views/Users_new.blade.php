@extends('layouts.app_demo')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <style>
        .dataTables_info { float: right }
        .dataTables_paginate, .dataTables_info, .dataTables_length { display: inline-block}
        .questionClass { display: none}
        .dataTables_length { float: right}
        .dataTables_wrapper .dataTables_paginate .pagination .page-item.active > .page-link {background-color: #3699FF;color: #ffffff;}
        .dataTables_wrapper .dataTables_paginate .pagination .page-item:hover:not(.disabled) > .page-link {background-color: #3699FF;color: #ffffff;}
        .dataTables_processing {margin-top: -60px !important;background: #ffffff !important;color:  cornflowerblue !important;border-radius: 5px !important;font-size: 16px !important;opacity : 1 !important;text-decoration: none;padding-left: 10px;margin: -60px 45% 0px 45%;}
        .dataTables_wrapper .dataTable th.sorting_asc, .dataTables_wrapper .dataTable td.sorting_asc {
            color: cornflowerblue !important;
        }
        #DataTables_Table_0_length {width: 144px;padding-right: 0px;margin-right: 10px;}
        .custom-select.custom-select-sm.form-control.form-control-sm {width: 57px;}
        #DataTables_Table_0_info {padding-top: 6px;}
        .custom-select:focus {border-color: cornflowerblue;outline: 0;}
    </style>
@endsection

@section('content')
    <div class="content flex-column-fluid" id="kt_content">

        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Users</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="javascript:void(0)" class="btn btn-info font-weight-bolder" data-toggle="modal" id="addCategoryButton">
                        <i class='fas fa-users' style='font-size:18px'></i>Create User</a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin::Search Form-->
                <!--end::Search Form-->
                <!--begin: Datatable-->
                <div class="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" style="">
                    <table class="table datatable-table usersDatatable">
                        <thead>
                        <tr>

                            <th>NO</th>
                            <th>FIRST NAME</th>
                            <th>LAST NAME</th>
                            <th>EMAIL</th>
                            <th>CONTACT NUMBER</th>
                            <th>ENROLLMENT NO</th>
                            <th>COLLEGE</th>
                            <th>ROLL</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
    </div>
@endsection

@section('popForm')
    <div class="modal fade in" id="exampleModalLongInner" data-keyboard="true" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New User</h5>
                    <button type="button" class="close" id="closeModal" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form class="kt-form"  data-scroll="true" method="POST" id="user_form" action="JavaScript:void(0)">
                        @csrf
                        <div class="row col-sm-12">
                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>Enrollment No.</strong></label>
                                <input class="form-control" type="text" placeholder="Enrollment number" name="enrollment" value="{{ old('enrollment') }}" id="enrollment">
                                <span id="enrollmentError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>Email</strong></label>
                                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}" id="email">
                                <span id="emailError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                        </div>
                        <div class="row col-sm-12">
                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>First name</strong></label>
                                <input class="form-control" type="text" placeholder="First name" name="f_name" value="{{ old('f_name') }}" id="f_name">
                                <span id="f_nameError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>

                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>Last name</strong></label>
                                <input class="form-control" type="text" placeholder="Last name" name="l_name" value="{{ old('l_name') }}" id="l_name" >
                                <span id="l_nameError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-check-label"><strong>Address</strong></label>
                                <input class="form-control" type="text" placeholder="Enter your address" name="address" value="{{ old('address') }}" id="address" style="width: 96.5%">
                                <span id="addressError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                        </div>
                        <div class="row col-sm-12">
                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>Mobile No.</strong></label>
                                <input class="form-control" type="text" placeholder="Contact number" name="mobile_no" id="mobile_no">
                                <span id="mobile_noError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>Applied For</strong></label>
                                <select class="form-control" name="openings" id="openings">
                                    <option value="">Select</option>
                                    @if(!empty($openings))
                                        @foreach($openings as $opening)
                                            <option value="{{ $opening->id }}">{{ $opening->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span id="openingsError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                            {{--                            <div class="form-group col-sm-6">--}}
                            {{--                                <label class="form-check-label">Role</label>--}}
                            {{--                                <select class="form-control" id="roles" name="role">--}}
                            {{--                                    <option value="admin" id="admin">Admin</option>--}}
                            {{--                                    <option value="user" id="user">User</option>--}}
                            {{--                                </select>--}}
                            {{--                            </div>--}}
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-check-label"><strong>Planning for future studies or going abroad ?</strong></label>
                                <select name="future_studies" class="form-control m-input future_studies" id="future_studies" style="width: 96.5%">
                                    <option value="">Select</option>
                                    <option value="Yes" @if(old('future_studies')=='Yes' ) selected @endif>Yes</option>
                                    <option value="No" @if(old('future_studies')=='No' ) selected @endif>No</option>
                                </select>
                                <span id="future_studiesError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                        </div>
                        <div class="row col-sm-12">
                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>University</strong></label>
                                <select class="form-control" name="university" id="university">
                                    <option value="">Select</option>
                                    @if(!empty($universities))
                                        @foreach($universities as $university)
                                            <option value="{{ $university->id }}">{{ $university->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span id="universityError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>College</strong></label>
                                <select class="form-control" name="college" id="college">
                                    <option value="">Select</option>
                                </select>
                                <span id="collegeError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                        </div>
                        <div class="row col-sm-12">
                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>Education Stream</strong></label>
                                <select class="form-control" name="stream" id="stream">
                                    <option value="">Select</option>
                                    @if(!empty($streams))
                                        @foreach($streams as $stream)
                                            <option value="{{ $stream->id }}">{{ $stream->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span id="streamError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>Passing Year</strong></label>
                                <select name="year" class="form-control" id="year">
                                    <option value="">Select</option>
                                    <option value="2021" @if(old('year')==2021) selected @endif>2021</option>
                                    <option value="2020" @if(old('year')==2020) selected @endif>2020</option>
                                    <option value="2019" @if(old('year')==2019) selected @endif>2019</option>
                                    <option value="2018" @if(old('year')==2018) selected @endif>2018</option>
                                    <option value="2017" @if(old('year')==2017) selected @endif>2017</option>
                                    <option value="2016" @if(old('year')==2016) selected @endif>2016</option>
                                    <option value="2015" @if(old('year')==2015) selected @endif>2015</option>
                                    <option value="2014" @if(old('year')==2014) selected @endif>2014</option>
                                    <option value="2013" @if(old('year')==2013) selected @endif>2013</option>
                                    <option value="2012" @if(old('year')==2012) selected @endif>2012</option>
                                </select>
                                <span id="yearError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-check-label"><strong>Interested technologies like... PHP, Python, ML, AngularJS, NodeJS, IONIC etc.</strong></label>
                                <input class="form-control" type="text" placeholder="Enter your interest" name="interest" value="{{ old('interest') }}" id="interest" style="width: 96.5%">
                                <span id="interestError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                        </div>
                        <div class="row col-sm-12">
                            <div class="form-group col-sm-6">
                                <label class="form-check-label"><strong>CV File</strong></label>
                                <input class="form-control" type="file" placeholder="" name="cvFile" id="cvFile" style="margin-bottom: 5px">
                                Upload CV here [Only <strong>.pdf,.doc,.docx</strong> format files are allowed and file size must not exceed <strong>5MB</strong>]<span class="input_underline"></span><br>
                                <span id="cvFileError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="form-check-label">Role</label>
                                <select class="form-control" id="roles" name="role">
                                    <option value="user" id="user">User</option>
                                    <option value="admin" id="admin">Admin</option>
                                </select>
                            </div>
                        </div>
                        <div class="row col-sm-12">
                            <div class="form-group col-sm-6" >
                                <label class="form-check-label">Password</label>
                                <input class="form-control" type="password" placeholder="Password"
                                       name="password" id="password">
                                <span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-password" style="position: relative; top: -28px;left: 316px;"></span>
                                <span id="passwordError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                            <div class="form-group col-sm-6" >
                                <label class="form-check-label">Confirm Password</label>
                                <input class="form-control" type="password" id="rpassword" placeholder="Confirm Password"
                                       name="password_confirmation">
                                <span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-password" style="position: relative; top: -28px;left: 316px;"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info save" id="adduser">Add</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" data-keyboard="true" data-backdrop="static" class="modal fade in">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px">
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="padding: 20px">
                    <p>Do you really want to delete this record?</p>
                </div>
                <div class="modal-footer" style="padding: 15px">
                    <a href="javascript:void(0);" id="delete_btn" class="btn btn-danger">Delete</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function () {
            var table = $('.usersDatatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('user.list') }}",
                "columnDefs": [
                    { "width": "8%", "targets": 0 },
                    { "width": "13%", "targets": 8 }
                ],
                dom: '<"top"f>rt<"bottom"pil><"clear">',
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'email', name: 'email'},
                    {data: 'mobile_no', name: 'mobile_no'},
                    {data: 'enrollment_no', name: 'enrollment_no'},
                    {data: 'college', name:'college'},
                    {data: 'roll' , name: 'roll'},
                    {data: 'action', name: 'action'},
                ]
            });
        });
    </script>
    <script src="{{asset('assets/app/js/users.js')}}"></script>
@endsection
