@extends("layouts.app_demo")

@section("css")
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <!-- css -->
    <style>
        .dataTables_info { float: right }
        .dataTables_paginate, .dataTables_info, .dataTables_length { display: inline-block}
        .questionClass { display: none}
        .dataTables_length { float: right}
        .dataTables_wrapper .dataTables_paginate .pagination .page-item.active > .page-link {background-color: #3699FF;color: #ffffff;}
        .dataTables_wrapper .dataTables_paginate .pagination .page-item:hover:not(.disabled) > .page-link {background-color: #3699FF;color: #ffffff;}
        .dataTables_processing {margin-top: -60px !important;background: #ffffff !important;color:  cornflowerblue !important;border-radius: 5px !important;font-size: 16px !important;opacity : 1 !important;text-decoration: none;padding-left: 10px;margin: -60px 45% 0px 45%;}
        .dataTables_wrapper .dataTable th.sorting_asc, .dataTables_wrapper .dataTable td.sorting_asc {color: cornflowerblue !important;}
        #DataTables_Table_0_length {width: 144px;padding-right: 0px;margin-right: 10px;}
        .custom-select.custom-select-sm.form-control.form-control-sm {width: 57px;}
        #DataTables_Table_0_info {padding-top: 6px;}
        .custom-select:focus {border-color: cornflowerblue;outline: 0;}
    </style>
@endsection

@section("content")
    <div class="content flex-column-fluid" id="kt_content">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Universities</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="javascript:void(0)" class="btn btn-info font-weight-bolder"   data-toggle="modal" id="addButton" data-target="#exampleModalLongInner">
                        <i class="la la-university kt-font-brand"></i>Create University</a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <div class="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" style="">
                    <table class="table datatable-table universityTable">
                        <thead>
                        <tr>
                            <th>NO</th>
                            <th>NAME</th>
                            <th>STATUS</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!--end: Datatable-->
            </div>
        </div>
    </div>
@endsection

@section("popForm")
    <div class="modal fade" data-keyboard="true" id="exampleModalLongInner" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="exampleModalLongTitle" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New University</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

                <div class="modal-body">
                    <form class="kt-form"  data-scroll="true" method="POST" id="create_form" action="JavaScript:void(0)" >
                        @csrf
                        <label for="name"><strong>Name</strong></label>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="University name" name="name" value="{{ old('name') }}" id="name">
                            <span id="nameError" style="display: none; font-size: 10px;color: red"></span>
                        </div>

                        <label for="status"><strong>Status</strong></label>
                        <div class="form-group">
                            <select class="form-control" name="status" id="status">
                                <option class="form-control" value="1" selected>Active</option>
                                <option class="form-control" value="0">Inactive</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info save" id="addUni">Add</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" data-keyboard="true" data-backdrop="static" class="modal fade in" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px">
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="padding: 20px">
                    <p>Do you really want to delete this record?</p>
                </div>
                <div class="modal-footer" style="padding: 15px">
                    <a href="javascript:void(0);" id="delete_btn" class="btn btn-danger">Delete</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <!-- Server side pagination -->
    <script type="text/javascript">
        $(function () {
            var table = $('.universityTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route("university.index") }}",
                "columnDefs": [
                    { "width": "8%", "targets": 0 },
                    { "width": "13%", "targets": 2 },
                    { "width": "13%", "targets": 3 }
                ],
                dom: '<"top"f>rt<"bottom"pil><"clear">',
                columns: [
                    {data: "DT_RowIndex", name: "DT_RowIndex"},
                    {data: "name", name: "name"},
                    {data: "status", name: "status"},
                    {data: "action", name: "action"},
                ]
            });
        });
    </script>
    <!-- All crud operation in this js -->
    <script src="{{asset('assets/app/js/university/list.js')}}"></script>
@endsection
