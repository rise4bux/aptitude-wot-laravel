@extends('layouts.app')
<!-- END: Header -->
<!-- BEGIN: Left Aside -->
@section('content')

    <!-- END: Left Aside -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">

        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <a href="{{route('home')}}">
                                <h3 class="m-subheader__title m-subheader__title--separator">
                                    Dashboard
                                </h3>
                            </a>
                            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                <li class="m-nav__item">
                                    <a href="{{route('exams.index')}}" class="m-nav__link">
												<span class="m-nav__link-text">
									Exams
												</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="m-content">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <span class="kt-portlet__head-icon">
                                                         <i class="la la-check-square kt-font-brand"></i>
                                        </span>Exams</h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <a href="{{route('exams.add')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-check-square"></i>
													<span> Create Exam</span>
												</span>
                                </a>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-form m-form--label-align-right m--margin-bottom-30">
                                <div class="row ">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch" name="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
															<span><i class="la la-search"></i></span>
														</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="examsDatatable" id="ajax_data"></div>
                            {{--<table class="table" id="examsDatatable" colspan='0' cellspacing='0'>
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Title
                                    </th>
                                    <th>
                                        Description
                                    </th>
                                    <th>
                                        Question count
                                    </th>
                                    <th>
                                        Total time
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Link
                                    </th>
                                    <th>
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.delete')

@endsection
@section('scripts')
    <script src="{{asset('assets/app/js/exams.js')}}"></script>
    <script>
        $(document).on('click','.Copylink',function () {
            var link=$(this).attr('title');
            var link="{{env('APP_URL')}}/exam/"+link;
            var tempInput = document.createElement("input");
            tempInput.style = "position: absolute; left: -1000px; top: -1000px";
            tempInput.value = link;
            console.log(tempInput.value);
            document.body.appendChild(tempInput);
            tempInput.select();
            document.execCommand("copy");
            document.body.removeChild(tempInput);
            toastr.success("Exam Link Copied", "Success");
        });
        $(document).on('click','.LoginLink',function () {
            var link=$(this).attr('title');
            var link="{{env('APP_URL')}}/exam/login/"+link;
            var tempInput = document.createElement("input");
            tempInput.style = "position: absolute; left: -1000px; top: -1000px";
            tempInput.value = link;
            console.log(tempInput.value);
            document.body.appendChild(tempInput);
            tempInput.select();
            document.execCommand("copy");
            document.body.removeChild(tempInput);
            toastr.success("Exam Login Link Copied", "Success");
        });
        $(document).on('click','.start-exam',function () {
            var link=$(this).attr('title');
           link= link.split('start').join("");
            console.log(link);
            var url="{{env('APP_URL')}}/exam/"+link;
           window.open(url, "_private");
            // chrome.windows.create({ incognito: true });
            // browser.windows.create({ incognito: true }).then(onSuccess, onError);
        });
        var exam = {!! $exam !!};
    </script>
@endsection





