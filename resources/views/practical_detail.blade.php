@if(isset($practical))
        <div class="modal-header" style="padding: 15px">
           <h4 class="modal-title">Programs
           </h4>
            <h4 class="modal-title float-right">
                Practical Status
                <span class="switch switch-sm switch-icon" style="padding-left: 10px;vertical-align: middle;">
                   <label style="margin: 0px">
                       <input type="checkbox" data-practical-id="{{$practical->id}}" {{$practical->status == 'completed' ? 'checked' : 'unchecked'}}
                       name="select" class="practicalToggle"><span></span>
                   </label>
               </span>
            </h4>

       </div>
       <div class="modal-body text-center" style="padding: 20px;text-align: center">
           <table class="table table-responsive  table-striped">
               @if($openings->openings == 5 || $openings->openings == 10 || $openings->openings == 12)
                   <thead>
                   <tr>
                       <th style="width:200px">Practical Zip</th>
                       <th>Comments</th>
                   </tr>
                   </thead>
                   <tbody>
                        <tr>
                            <td><a href="{{$practical->code_1}}" target="_blank">Assets</a></td>
                            <td><input type="text" name="comment_1" data-practical-id="{{$practical->id}}"
                                  value="{{$practical->comment_1}}"
                                  class="form-control
                   practicalInput" ></td>
                        </tr>
                        <tr>
                            <td><b>Paper Type</b></td>
                            <td>{{strtoupper($practical->paper_type)}}</td>
                            <td></td>
                        </tr>
                   </tbody>



               @elseif($openings->openings == 6 || $openings->openings == 7 || $openings->openings == 8 || $openings->openings == 11)
                   <thead>
                   <tr>
                       <th style="width:200px">Answers</th>
                       <th>Comments</th>
                   </tr>
                   </thead>
                   <tbody>
                   <tr>
                       <td><a href="{{$practical->result1_full_url}}" target="_blank">Answer 1</a></td>
                       <td><input type="text" name="comment_1" data-practical-id="{{$practical->id}}"
                                  value="{{$practical->comment_1}}"
                                  class="form-control
                   practicalInput" ></td>
                   </tr>
                   <tr>
                       <td><a href="{{$practical->result2_full_url}}" target="_blank">Answer 2</a></td>
                       <td><input type="text" name="comment_2" data-practical-id="{{$practical->id}}"
                                  value="{{$practical->comment_2}}"
                                  class="form-control
                   practicalInput" ></td>
                   </tr>
                   <tr>
                       <td><a href="{{$practical->result3_full_url}}" target="_blank">Answer 3</a></td>
                       <td><input type="text" name="comment_3" data-practical-id="{{$practical->id}}"
                                  value="{{$practical->comment_3}}"
                                  class="form-control
                   practicalInput" ></td>
                   </tr>
                   <tr>
                       <td><a href="{{$practical->result4_full_url}}" target="_blank">Answer 4</a></td>
                       <td><input type="text" name="comment_4" data-practical-id="{{$practical->id}}"
                                  value="{{$practical->comment_4}}"
                                  class="form-control
                   practicalInput" ></td>
                   </tr>
                   <tr>
                       <td><a href="{{$practical->result5_full_url}}" target="_blank">Answer 5</a></td>
                       <td><input type="text" name="comment_5" data-practical-id="{{$practical->id}}"
                                  value="{{$practical->comment_5}}"
                                  class="form-control
                   practicalInput" ></td>
                   </tr>
                   <tr>
                       <td><a href="{{$practical->result6_full_url}}" target="_blank">Answer 6</a></td>
                       <td><input type="text" name="comment_6" data-practical-id="{{$practical->id}}"
                                  value="{{$practical->comment_6}}"
                                  class="form-control
                   practicalInput" ></td>
                   </tr><tr>
                       <td><a href="{{$practical->result7_full_url}}" target="_blank">Answer 7</a></td>
                       <td><input type="text" name="comment_7" data-practical-id="{{$practical->id}}"
                                  value="{{$practical->comment_7}}"
                                  class="form-control
                   practicalInput" ></td>
                   </tr>
                   <tr>
                       <td><b>Paper Type</b></td>
                       <td>{{strtoupper($practical->paper_type)}}</td>
                       <td></td>
                   </tr>

                   </tbody>
               @else
                   <thead>
                   <tr>
                       <th style="width:200px">Programs</th>
                       <th style="width:200px">Output</th>
                       <th>Comments</th>
                   </tr>
                   </thead>
                   <tbody>
                   <tr>
                       <td><a href="{{$practical->code1_full_url}}" target="_blank">Code 1</a></td>
                       <td><a href="{{$practical->result1_full_url}}" target="_blank">Output 1</a></td>
                       <td><input type="text" name="comment_1" data-practical-id="{{$practical->id}}"
                                  value="{{$practical->comment_1}}"
                                  class="form-control
                   practicalInput" ></td>
                   </tr>
                   <tr>
                       <td><a href="{{$practical->code2_full_url}}" target="_blank">Code 2</a></td>
                       <td><a href="{{$practical->result2_full_url}}" target="_blank">Output 2</a></td>
                       <td><input type="text" name="comment_2" value="{{$practical->comment_2}}"
                                  data-practical-id="{{$practical->id}}"
                                  class="form-control practicalInput" ></td>
                   </tr>
                   <tr>
                       <td><a href="{{$practical->code3_full_url}}" target="_blank">Code 3</a></td>
                       <td><a href="{{$practical->result3_full_url}}" target="_blank">Output 3</a></td>
                       <td><input type="text" name="comment_3" value="{{$practical->comment_3}}"
                                  data-practical-id="{{$practical->id}}"
                                  class="form-control practicalInput" ></td>
                   </tr>
                   <tr>
                       <td><b>Paper Type</b></td>
                       <td>{{strtoupper($practical->paper_type)}}</td>
                       <td></td>
                   </tr>
                   </tbody>
               @endif
           </table>
       </div>
@endif
