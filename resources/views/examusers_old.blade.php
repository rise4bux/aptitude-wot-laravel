@extends('layouts.app')
<!-- END: Header -->
<!-- BEGIN: Left Aside -->
@section('content')

    <!-- END: Left Aside -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">

        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <a href="{{route('home')}}">
                                <h3 class="m-subheader__title m-subheader__title--separator">
                                    Dashboard
                                </h3>
                            </a>
                            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                <li class="m-nav__item">
                                    <a href="{{route('exams.index')}}" class="m-nav__link">
												<span class="m-nav__link-text">
									Exams
												</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="m-content">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Exam Users
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <a href="javascript:void(0);" class="exportButton" data-id="{{ $exam_id }}">
                                    <button type="button" class="btn btn-primary" id="">
                                        Export
                                    </button>
                                </a>
                               {{-- <a href="{{route('exams.add')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-book"></i>
													<span> Create exam</span>
												</span>
                                </a>--}}
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <form class="m-form m-form--fit m--margin-bottom-20">
                                <div class="row m--margin-bottom-20">
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>RecordID:</label>
                                        <input type="text" class="form-control m-input" placeholder="E.g: 4590" data-col-index="0">
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>OrderID:</label>
                                        <input type="text" class="form-control m-input" placeholder="E.g: 37000-300" data-col-index="1">
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>Country:</label>
                                        <select class="form-control m-input" data-col-index="2">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>Agent:</label>
                                        <input type="text" class="form-control m-input" placeholder="Agent ID or name" data-col-index="4">
                                    </div>
                                </div>
                                <div class="row m--margin-bottom-20">
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>Ship Date:</label>
                                        <div class="input-daterange input-group" id="m_datepicker">
                                            <input type="text" class="form-control m-input" name="start" placeholder="From" data-col-index="5" />
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                            </div>
                                            <input type="text" class="form-control m-input" name="end" placeholder="To" data-col-index="5" />
                                        </div>
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>Status:</label>
                                        <select class="form-control m-input" data-col-index="6">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                        <label>Type:</label>
                                        <select class="form-control m-input" data-col-index="7">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="m-separator m-separator--md m-separator--dashed"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-brand m-btn m-btn--icon" id="m_search">
												<span>
													<i class="la la-search"></i>
													<span>Search</span>
												</span>
                                        </button>
                                        &nbsp;&nbsp;
                                        <button class="btn btn-secondary m-btn m-btn--icon" id="m_reset">
												<span>
													<i class="la la-close"></i>
													<span>Reset</span>
												</span>
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                                <thead>
                                <tr>
                                    <th>Record ID</th>
                                    <th>Order ID</th>
                                    <th>Country</th>
                                    <th>Ship City</th>
                                    <th>Company Agent</th>
                                    <th>Ship Date</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Record ID</th>
                                    <th>Order ID</th>
                                    <th>Country</th>
                                    <th>Ship City</th>
                                    <th>Company Agent</th>
                                    <th>Ship Date</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="examModal" class="modal fade in">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px">
                    <h4 class="modal-title">Marks Detail</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body categoryResult text-center" style="padding: 20px;text-align: center">

                </div>
                <div class="modal-footer" style="padding: 15px">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var examID = '{{$exam_id}}';
        var AjaxUrl = '{{URL::to('/')}}';
       /* $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });*/
    </script>
    <script src="{{asset('assets/app/js/advanced-search.js')}}"></script>
    {{--<script src="{{asset('assets/app/js/exams.js')}}"></script>--}}
@endsection