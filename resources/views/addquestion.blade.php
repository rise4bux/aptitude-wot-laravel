@extends('layouts.app')
@section('content')
    <script src="{{ asset('assets/app/js/ckeditor5/build/ckeditor.js') }}"></script>
    <style>
        .alert.alert-danger {
            background: #f66e84;
            border: 1px solid #f66e84;
            color: #ffffff;
        }       
    </style>
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">

        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <a href="{{route('home')}}">
                                <h3 class="m-subheader__title m-subheader__title--separator">
                                    Dashboard
                                </h3>
                            </a>
                            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                <li class="m-nav__item ">
                                    <a href="{{route('questions.index')}}" class="m-nav__link">
												<span class="m-nav__link-text">
													Questions
												</span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
												<span class="m-nav__link-text">
										>
												</span>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{route('questions.add')}}" class="m-nav__link">
												<span class="m-nav__link-text">
											{{!empty($question) ? 'Edit' : 'Add'}} Question

												</span>
                                    </a>   
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                     id="kt_content" style="padding-top:0">
                    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                        <div class="kt-portlet kt-portlet--mobile">
                            <div class="kt-portlet__head kt-portlet__head--lg">
                                <div class="kt-portlet__head-label">
                                                    <span class="kt-portlet__head-icon">
                                                        <i class="la la-question kt-font-brand"></i>
                                                    </span>
                                    <h3 class="kt-portlet__head-title">
                                        {{!empty($question) ? 'Edit' : 'Add'}} Question
                                    </h3>
                                </div>

                            </div>
                            <div class="kt-portlet--tabs row" style="justify-content: center">
                                <div class="card-block p-4 col-sm-6">
                                    @if($errors)
                                        @foreach ($errors->all() as $error)
                                            <li class="alert alert-danger">{{ $error }}</li>
                                        @endforeach
                                    @endif
                                    <form id="form" class="kt-form row" action="{{!empty($question) ? route('questions.update',$question->id) : route('questions.save')}}"  method="post">
                                        @csrf
                                        <div class="col-sm-12 form-group">
                                            <label class="form-check-label" for="title">Title</label>
                                            <input id="title" @if($question!==null) value="{{$question->title}}" @endif type="hidden"
                                                   name="title">
                                            <textarea name="title" class=" form-control titleeditor"  rows="3">{!!($question!==null) ? $question->title : old('title')!!}</textarea>
                                            {{--<trix-editor input="title"></trix-editor>--}}
                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                            <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                        <div class="form-group col-sm-4">
                                            <label class="form-check-label" for="category">Category</label>
                                            <select class="form-control" id="category" name="category">
                                                @foreach($category as $cat)
                                                    <option class="form-control" value="{{$cat->id}}" @if($question!==null) @if($cat->id==$question->category_id) selected @endif @endif>{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('category')
                                            <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label class="form-check-label" for="difficulty">Difficulty</label>
                                            <select class="form-control" id="difficulty" name="difficulty">
                                                <option class="form-control" value="Easy" @if($question!==null && $question->difficulty=="Easy") selected @endif>Easy</option>
                                                <option class="form-control" value="Medium" @if($question!==null && $question->difficulty=="Medium") selected @endif>Medium</option>
                                                <option class="form-control" value="Hard" @if($question!==null && $question->difficulty=="Hard") selected @endif>Hard</option>
                                            </select>
                                            @error('difficulty')
                                            <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label for="status" class="form-check-label">Select Status</label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="1" @if($question!==null && $question->status=='1') selected @endif>Active</option>
                                                <option value="0" @if($question!==null &&  $question->status=='0') selected @endif>Inactive</option>
                                            </select>
                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                        <div class="col-sm-12">
                                            @error('options')
                                            <span class="invalid-feedback" style="display: block !important" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                                            @enderror
                                            <div class="kt-portlet__head kt-portlet__head--lg"
                                                 style="padding: 0 !important; border: none ;    border-top: 1px solid #ebedf3;">
                                                <div class="kt-portlet__head-label">
                                                    <!-- <h3 class="kt-portlet__head-title">
                                                        Options
                                                    </h3> -->
                                                    <label for="status" class="form-check-label" style="font-size: 16px">Options</label>
                                                </div>
                                                <div class="kt-portlet__head-toolbar">
                                                    <div class="kt-portlet__head-wrapper">
                                                        <div class="">
                                                            <button type="button" class="btn btn-sm btn-primary"
                                                                    id="addOptionButton">
                                                                Add Options
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="option-container" class="w-100">
                                       @if($question !==null)
                                           @foreach($question->options as $option)
                                                    <div class="form-group">
                                                        <label class="form-check-label" for="title">{{$loop->iteration.'.'}}</label>
                                                      {{--  <input id="option{{$loop->iteration-1}}" value="{{$option->content}}" type="hidden"
                                                               name="options[{{$loop->iteration-1}}][option]">--}}
                                                       {{-- <textarea name="option{{$loop->iteration-1}}" class="form-control"  rows="3" id="option{{$loop->iteration}}">{{$option->content}}</textarea>
                                                        <script type="text/javascript">
                                                            CKEDITOR.replace( 'options{{$loop->iteration}}' );
                                                             CKEDITOR.add
                                                         </script>--}}
                                                        {{-- <trix-editor input="option{{$loop->iteration-1}}"></trix-editor>--}}
                                                        <textarea name="options[{{$loop->iteration-1}}][option]" class="kt-ckeditor-{{ $loop->iteration-1 }}">{{$option->content}}</textarea>
                                                        <script>
                                                            ClassicEditor
                                                                .create( document.querySelector( '.kt-ckeditor-{{ $loop->iteration-1 }}' ), {

                                                                    toolbar: {
                                                                        items: [
                                                                            'heading',
                                                                            '|',
                                                                            'bold',
                                                                            'italic',
                                                                            'link',
                                                                            'bulletedList',
                                                                            'numberedList',
                                                                            '|',
                                                                            'indent',
                                                                            'outdent',
                                                                            '|',
                                                                            'imageUpload',
                                                                            'blockQuote',
                                                                            'insertTable',
                                                                            'mediaEmbed',
                                                                            'undo',
                                                                            'redo'
                                                                        ]
                                                                    },
                                                                    language: 'en',
                                                                    image: {
                                                                        toolbar: [
                                                                            'imageTextAlternative',
                                                                            'imageStyle:full',
                                                                            'imageStyle:side'
                                                                        ]
                                                                    },
                                                                    table: {
                                                                        contentToolbar: [
                                                                            'tableColumn',
                                                                            'tableRow',
                                                                            'mergeTableCells'
                                                                        ]
                                                                    },
                                                                    licenseKey: '',

                                                                } )
                                                                .then( editor => {
                                                                    window.editor = editor;
                                                                } )
                                                                .catch( error => {
                                                                    console.error( 'Oops, something gone wrong!' );
                                                                    console.error( 'Please, report the following error in the https://github.com/ckeditor/ckeditor5 with the build id and the error stack trace:' );
                                                                    console.warn( 'Build id: ia47t6hg9pi0-8o65j7c6blw0' );
                                                                    console.error( error );
                                                                } );

                                                        </script>
                                                        <div class="clearfix m-1">
                                                            <label class="mt-checkbox">
                                                                <input type="radio" class="radioSelect"
                                                                       name="options[{{$loop->iteration-1}}][is_correct]" @if($option->is_correct=='1') value="1" checked @else value="0" @endif>
                                                                <span class="text-muted">correct</span>
                                                            </label>
                                                            <button style="float: right" type="button"
                                                                    class="btn btn-sm btn-clean btn-icon btn-icon-md optionRemove"><i class="la la-trash"></i>
                                                            </button>
                                                        </div>
                                                    </div>
@endforeach
                                           @else
                                                <div  class="form-group">
                                                    <label class="form-check-label" for="title">1.</label>
                                               {{--     <input id="option" value="" type="hidden"
                                                           name="options[0][option]">
                                                    --}}{{--<textarea name="options[0][option]" class="form-control" rows="3"></textarea>--}}{{--
                                                    <trix-editor input="option"></trix-editor>--}}
                                                    <textarea name="options[0][option]" class="kt-ckeditor-0" id="options1"></textarea>
                                                    <script>
                                                        ClassicEditor
                                                            .create( document.querySelector( '.kt-ckeditor-0' ), {

                                                                toolbar: {
                                                                    items: [
                                                                        'heading',
                                                                        '|',
                                                                        'bold',
                                                                        'italic',
                                                                        'link',
                                                                        'bulletedList',
                                                                        'numberedList',
                                                                        '|',
                                                                        'indent',
                                                                        'outdent',
                                                                        '|',
                                                                        'imageUpload',
                                                                        'blockQuote',
                                                                        'insertTable',
                                                                        'mediaEmbed',
                                                                        'undo',
                                                                        'redo'
                                                                    ]
                                                                },
                                                                language: 'en',
                                                                image: {
                                                                    toolbar: [
                                                                        'imageTextAlternative',
                                                                        'imageStyle:full',
                                                                        'imageStyle:side'
                                                                    ]
                                                                },
                                                                table: {
                                                                    contentToolbar: [
                                                                        'tableColumn',
                                                                        'tableRow',
                                                                        'mergeTableCells'
                                                                    ]
                                                                },
                                                                licenseKey: '',

                                                            } )
                                                            .then( editor => {
                                                                window.editor = editor;
                                                            } )
                                                            .catch( error => {
                                                                console.error( 'Oops, something gone wrong!' );
                                                                console.error( 'Please, report the following error in the https://github.com/ckeditor/ckeditor5 with the build id and the error stack trace:' );
                                                                console.warn( 'Build id: ia47t6hg9pi0-8o65j7c6blw0' );
                                                                console.error( error );
                                                            } );

                                                    </script>
                                                    <div class="clearfix m-1">
                                                        <label class="mt-checkbox">
                                                            <input type="radio" class="radioSelect"
                                                                   name="options[0][is_correct]" value="0">
                                                            <span class="text-muted">Correct</span>
                                                        </label>
                                                    </div>
                                                </div>
                                          @endif
                                        </div>
                                        <div class="form-group"><input type="submit" value="{{!empty($question) ?
                                        'Update' : 'Save'}}" class="btn btn-info"> &nbsp;
                                            <a href="{{route('questions.index')}}" class="btn btn-secondary">Cancel</a>
                                            </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="optionGroup" class="form-group" style="display: none">
        <label class="form-check-label" for="title" id="phaseCount">1.</label>
     {{--   <input id="option" value="" type="hidden"
               name="options[0][option]">
        --}}{{--<textarea name="options[0][option]" class="form-control" rows="3"></textarea>--}}{{--
        <trix-editor input="options[0][option]"></trix-editor>--}}
        <textarea name="options[0][option]" class="kt-ckeditor-new" id="options0"></textarea>
        <div class="clearfix m-1">
            <label class="mt-checkbox">
                <input type="radio" class="radioSelect"
                       name="options[0][is_correct]" value="0">
                <span class="text-muted">correct</span>
            </label>
            <button style="float: right" type="button"
                    class="btn btn-sm btn-clean btn-icon btn-icon-md optionRemove"><i class="la la-trash"></i>
            </button>
        </div>
    </div>
@endsection
@section('scripts')
    <!--begin::Page Vendors(used by this page)-->

    <!--end::Page Vendors-->
    <!--begin::Page Scripts(used by this page)-->
   {{-- <script src="assets/js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.3"></script>--}}
    <script>
        var question=`@php echo !empty($question) ? $question : null @endphp`;
        if (question !== null){
            var count =`@php echo !empty($count) ? $count : 1 @endphp`;
        }
    </script>
 {{--   <script src="{{ asset('assets/app/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/app/js/ckeditor/samples/js/sample.js') }}"></script>--}}
{{--
    <link rel="stylesheet" href="{{ asset('assets/app/js/ckeditor/samples/css/samples.css') }}">
--}}

    <script src="{{asset('assets/app/js/jquery.validate.js')}}"></script>

    <script>
        $(document).ready(function () {

            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });

            /*$('#form').validate({
                rules: {
                    title: "required",
                    category: "required",
                    difficulty: "required",
                    status: "required",
                },
                messages: {},
                submitHandler: function (form) {
                    //form.submit();
                }, errorPlacement: function (error, element) {
                    console.log(element);
                    element.parent().append(error);
                }
            });*/

        });
    </script>
    <script src="{{asset('assets/app/js/addQuestions.js')}}"></script>

@endsection
