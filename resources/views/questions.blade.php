@extends('layouts.app')
<!-- END: Header -->
<!-- BEGIN: Left Aside -->
@section('content')

    <!-- END: Left Aside -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">

        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <a href="{{route('home')}}">
                                <h3 class="m-subheader__title m-subheader__title--separator">
                                    Dashboard
                                </h3>
                            </a>
                            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                <li class="m-nav__item">
                                    <a href="{{route('questions.index')}}" class="m-nav__link">
												<span class="m-nav__link-text">
													Questions
												</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="m-content">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <span class="kt-portlet__head-icon">
                                                         <i class="la la-question kt-font-brand"></i>
                                        </span>Questions</h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <a href="{{route('questions.add')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill"   id="addQuestionButton">
												<span>
													<i class="la la-question"></i>
													<span> Create Question</span>
												</span>
                                </a>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-form m-form--label-align-right m--margin-bottom-30">
                                <div class="row ">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch" name="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
															<span><i class="la la-search"></i></span>
														</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="questionsDatatable" id="ajax_data"></div>
                            {{--<table class="table" id="questionsDatatable" colspan='0' cellspacing='0'>
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Title
                                    </th>
                                    <th>
                                        Language
                                    </th>
                                    <th>
                                        Difficulty
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.delete')
@endsection
@section('scripts')
    <script> var questions ={!! $questions !!} </script>
    <script src="{{asset('assets/app/js/questions.js')}}"></script>
@endsection





