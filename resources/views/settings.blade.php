@extends("layouts.app_demo")

@section("css")
<style>
    .radio.radio-info > span {
        background-color: #ECF0F3;
        border: 1px solid cornflowerblue;
    }
</style>
@endsection

@section("content")
    <div class="content flex-column-fluid" id="kt_content">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">System Setting</h3>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <form method="post" class="form-horizontal" enctype="multipart/form-data" action="{{(route ('system.setting.update'))}}">
                    {{csrf_field()}}
                    <div class="card-body">
                        <div class="row">
                            <label class="form-control-label col-sm-6">Practical Exam Enabled : <span class="tx-danger">*</span></label>
                            <label class="radio radio-rounded radio-info" style="margin: 0 20px 0 0">
                                <input type="radio" name="practical_exam_enabled" class="radioSelect" value="1" {{isset($setting['practical_exam_enabled']) && $setting['practical_exam_enabled'] == '1' ? 'checked' : ''}} {{!empty(old('practical_exam_enabled')) && old('practical_exam_enabled') == '1' ? 'checked' : ''}} >Yes
                                <span></span>
                            </label>
                            <label class="radio radio-rounded radio-info">
                                <input type="radio" name="practical_exam_enabled" class="radioSelect" value="0" {{isset($setting['practical_exam_enabled']) && $setting['practical_exam_enabled'] == '0' ? 'checked' : ''}} {{!empty(old('practical_exam_enabled')) && old('practical_exam_enabled') == '0' ? 'checked' : ''}} {{!isset($setting['practical_exam_enabled']) ? 'checked' : ''}}>No
                                <span></span>
                            </label>
                            <br/>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info mr-2">Submit</button>
                        <a href="{{ route('exams.index') }}"><button type="button" class="btn btn-secondary">Cancel</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
