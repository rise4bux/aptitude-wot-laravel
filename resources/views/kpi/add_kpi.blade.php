@extends("layouts.app_demo")

@section("css")
    <style>
        #form { display: none }
    </style>
@endsection

@section("content")
    <div class="content flex-column-fluid" id="kt_content">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">@if(isset($kpiValue)) Edit KPI @else Create KPI @endif</h3>
            </div>
            <!--begin::Form-->
            <form method="POST" action="@if(isset($kpiValue)) {{route('kpi.update',$id)}} @else {{route('kpi.save')}} @endif">
                @csrf
                @if(isset($kpiValue))
                    @method('PUT')
                @endif
                <div class="card-body">
                    <div class="form-group">
                        <label>Select Openings</label>
                        <?php $data = session()->get('KPIValue'); ?>
                        <select class="form-control form-control-solid" name="openings" id="openings">
                            <option value="">Select</option>
                            @if(isset($kpiValue))
                                @foreach($openings as $opening)
                                    @if($kpi->opening_id == $opening->id)
                                        <option value="{{ $opening->id }}" selected>{{ $opening->name }}</option>
                                    @else
                                        <option value="{{ $opening->id }}">{{ $opening->name }}</option>
                                    @endif
                                @endforeach
                            @else
                                @foreach($openings as $opening)
                                    @if(isset($data) && $data == $opening->id)
                                        <option value="{{ $opening->id }}" selected>{{ $opening->name }}</option>
                                    @elseif(old('openings') == $opening->id)
                                        <option value="{{ $opening->id }}" selected>{{ $opening->name }}</option>
                                    @else
                                        <option value="{{ $opening->id }}">{{ $opening->name }}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                        @error('openings')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="formClass">
                        @if(isset($kpiValue))
                            @for($i = 0; $i < count($kpiValue); $i++)
                                <?php $j = $i + 1; $name = $kpiValue[$i]->name; $weightage = $kpiValue[$i]->weightage;?>
                                @if($j == 1)
                                    <div class="col-sm-12 row form" id="form_1">
                                        <div class="form-group col-sm-5 ">
                                            <label>Name</label>
                                            <input type="text" class="form-control form-control-solid" placeholder="Enter name" value="{{ $name }}" name="value[{{$j}}][name]" id="name_{{$j}}">
                                        </div>
                                        <div class="form-group col-sm-5">
                                            <label>Weightage</label>
                                            <input type="text" class="form-control form-control-solid" placeholder="Enter weightage" name="value[1][weightage]" id="weightage_1" value="{{ $weightage }}">
                                        </div>
                                        <button style="margin-top: 26px" type="button"
                                                class="btn btn-clean col-sm-1 btn-icon btn-icon-md addMore btn-info">Add More</button>
                                    </div>
                                @else
                                    <div class="col-sm-12 row form" id="form_{{$j}}">
                                        <div class="form-group col-sm-5 name">
                                            <label>Name</label>
                                            <input type="text" class="form-control form-control-solid" placeholder="Enter name" name="value[{{$j}}][name]" id="name" value="{{ $name }}">
                                        </div>
                                        <div class="form-group col-sm-5 weightage">
                                            <label>Weightage</label>
                                            <input type="text" class="form-control form-control-solid" placeholder="Enter weightage" name="value[{{$j}}][weightage]" id="weightage" value="{{$weightage}}">
                                        </div>
                                        <button style="float: right;margin-top: 26px" type="button"
                                                class="btn btn-sm btn-clean btn-icon btn-icon-md optionRemove" id="{{$j}}"><i class="la la-trash" style="color:red"></i></button>
                                    </div>
                                @endif
                            @endfor
                        @else
                            <div class="col-sm-12 row form" id="form_1">
                                <div class="form-group col-sm-5 ">
                                    <label>Name</label>
                                    <input type="text" class="form-control form-control-solid" placeholder="Enter name" name="value[1][name]" id="name_1">
                                </div>
                                <div class="form-group col-sm-5">
                                    <label>Weightage</label>
                                    <input type="text" class="form-control form-control-solid" placeholder="Enter weightage" name="value[1][weightage]" id="weightage_1">
                                </div>
                                <button style="margin-top: 26px" type="button" class="btn btn-clean col-sm-1 btn-icon btn-icon-md addMore btn-info">Add More</button>
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-12 row form" id="form">
                        <div class="form-group col-sm-5 name">
                            <label>Default Input</label>
                            <input type="email" class="form-control" placeholder="Large input">
                        </div>
                        <div class="form-group col-sm-5 weightage">
                            <label>Weightage</label>
                            <input type="text" class="form-control form-control-solid" placeholder="Enter weightage" name="sample2" id="weightage">
                        </div>
                        <button style="float: right;margin-top: 26px" type="button"
                                class="btn btn-sm btn-clean btn-icon btn-icon-md optionRemove" id="1"><i class="la la-trash" style="color:red"></i></button>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info mr-2" name="save">@if(isset($kpiValue)) Update @else Submit @endif</button>
                    @if(!isset($kpiValue))
                        <button type="submit" class="btn btn-info mr-2" name="saveNew">Save & New</button>
                    @endif
                    <a href="{{ route('kpi.index') }}"><button type="button" class="btn btn-secondary">Cancel</button></a>
                </div>
            </form>
        </div>

    </div>
@endsection

@section("popForm")
    <div id="deleteValue" data-keyboard="true" data-backdrop="static" class="modal fade in" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px">
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="padding: 20px">
                    <p>Do you really want to delete this Option?</p>
                </div>
                <div class="modal-footer" style="padding: 15px">
                    <a href="javascript:void(0);" id="deleteOptionBtn" class="btn btn-danger">Delete</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        // click on add more button add new name and Weightage fields
        $('.addMore').on('click', function() {
            var i = $('.formClass .form').length + 1;
            var cloneObj = $('#form').clone().attr('id','form_'+i);
            $(cloneObj).find('.name input').attr('id','name_'+i);
            $(cloneObj).find('.name input').attr('name','value['+i+'][name]');
            $(cloneObj).find('.weightage input').attr('id','weightage_'+i);
            $(cloneObj).find('.weightage input').attr('name','value['+i+'][weightage]');
            $(cloneObj).find('.optionRemove').attr('id',i);
            $('.formClass').append(cloneObj);
            i++;
        });

        // click on remove button show confirmation modal and delete this kpi value
        $(document).on('click','.optionRemove', function() {
            var id = $(this).attr('id');
            $('#deleteOptionBtn').attr('name',id);
            $('#deleteValue').modal({
                backdrop: 'static',
                keyboard: true
            }).on('click', '#deleteOptionBtn', function(e) {
                var id = $(this).attr('name');
                $('#form_'+id).remove();
                $("#deleteValue").modal('hide');
            });
            formClassFunction();
        });

        // after delete change the id and field name
        function formClassFunction() {
            var count = 1;
            $('.formClass .form').each(function() {
                $(this).attr('id','form_'+count);
                $(this).find('.name input').attr('id','name_'+count);
                $(this).find('.name input').attr('name','value['+count+'][name]');
                $(this).find('.weightage input').attr('id','weightage_'+count);
                 $(this).find('.weightage input').attr('name','value['+count+'][weightage]');
                $(this).find('.optionRemove').attr('id',count);
                count++;
            });
        }

    });
</script>
@endsection
