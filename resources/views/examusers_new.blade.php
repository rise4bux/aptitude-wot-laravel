@extends('layouts.app_demo')

@section('css')
    <style>
        .dataTables_wrapper .dataTable th.sorting_asc, .dataTables_wrapper .dataTable td.sorting_asc {
            color: cornflowerblue !important;
        }
        #questionModal .modal-body{
            max-height: 70vh;
            overflow-y: scroll;
            /*top: 20px;*/
            /*width: 100%;*/
            /*height :90%; // what ever you like*/
            /*z-index: 101;*/
            /*border-radius: 5px;*/
        }
        .activeClass { font-weight: 800;}
    </style>
@endsection

@section('content')
    <div class="content flex-column-fluid" id="kt_content">

                                <!--begin::Card-->
                                <div class="card card-custom">
                                    <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                        <div class="card-title">
                                            <h3 class="card-label">Exam Users</h3>
                                        </div>

                                        <div class="card-toolbar">
                                            <!--begin::Button-->
                                            <a href="javascript:void(0)" class="exportButton btn btn-info font-weight-bolder"  data-id="{{ $exam_id }}" >
                                            <span class="svg-icon svg-icon-md">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <circle fill="#000000" cx="9" cy="15" r="6"></circle>
                                                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>Export</a>
                                            <!--end::Button-->
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <!--begin::Search Form-->
                                        <div class="form_Class col-lg-12" style="margin-bottom: 20px">
                                            <form class="m-form m-form--fit m--margin-bottom-20" id="filterForm" action="{{ route('exams.exportstudent',$exam_id) }}" method="post">
                                                @csrf
                                                <input type="hidden" name="filter" value="" id="filterData"/>
                                                <div class="row m--margin-bottom-20">

                                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                                        <label class=""><b>Applied for</b></label>
                                                        <select class="form-control " name="openings[]" id="openings"
                                                                multiple>
                                                           <option value="">Select</option>
                                                           @foreach($openings as $opening)
                                                           <option value="{{ $opening->id }}">{{ $opening->name }}</option>
                                                           @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                                           <label class=""><b>College</b></label>
                                                           <select class="form-control select2 select2-hidden-accessible form-control-solid collegeSelect" id="kt_select2_3" name="college_id[]" multiple data-select2-id="kt_select2_3" tabindex="-1" aria-hidden="true">
                                                               <option value="">Select College</option>
                                                               @foreach($colleges as $college)
                                                                   <option value="{{ $college->id }}">{{ $college->name }}</option>
                                                               @endforeach
                                                           </select>
                                                       </div>
                                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                                        <label class=""><b>Education Stream</b></label>
                                                        <select class="form-control" name="stream[]" id="stream" multiple>
                                                            <option value="">Select</option>
                                                            @foreach($streams as $stream)
                                                                <option value="{{ $stream->id }}">{{ $stream->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                       <div class="col-lg-2 m--margin-bottom-10-tablet-and-mobile">
                                                           <label class=""><b>Marks</b></label>
                                                          <input type="number" class="form-control" id="marks" name="marks" min="1" placeholder="Eg. 7 meaning >= 7">
                                                       </div>
                                                </div>
                                                <div class="row m--margin-bottom-20" style="margin-top: 20px;">

                                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                                        <label class=""><b>Aptitude Status</b></label>
                                                        <select class="form-control" name="exam_status" id="exam_status">
                                                            <option value="">Select Status</option>
                                                            <option value="completed">Completed</option>
                                                            <option value="OnGoing">On Going</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                                        <label class=""><b>Eligible For Practical</b></label>
                                                       <select class="form-control" name="eligible_filter" id="eligible_filter">
                                                           <option value="">Select Status</option>
                                                           <option value="1">Yes</option>
                                                           <option value="0">No</option>
                                                       </select>
                                                    </div>



                                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                                        <label class=""><b>Practical Status</b></label>
                                                       <select class="form-control" name="practical_status" id="practical_status">
                                                           <option value="">Select Status</option>
                                                           <option value="notStarted">Not Started</option>
                                                           <option value="Completed">Completed</option>
                                                           <option value="on_going">On Going</option>
                                                       </select>
                                                    </div>

                                                    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                                                        <label class=""><b>Video Generated </b></label>
                                                        <select class="form-control" name="video_status" id="video_status">
                                                            <option value="">Select Status</option>
                                                            <option value="1">Yes</option>
                                                            <option value="0">No</option>
                                                        </select>
                                                    </div>


{{--                                                    <div class="col-lg-8 m--margin-bottom-10-tablet-and-mobile--}}
{{--                                                    float-right text-right" style="margin-top: 25px;">--}}

{{--                                                    </div>--}}

{{--                                                    <div class="col-lg-12 float-right text-right--}}
{{--                                                    m--margin-bottom-10-tablet-and-mobile--}}
{{--                                                    m--margin-top-25" style="padding-top:25px; padding-right: 0px">--}}
{{--                                                       --}}
{{--                                                        --}}
{{--                                                    </div>--}}
                                                </div>

                                            <div class="mb-7" style="margin-top: 25px;">
											        <div class="row align-items-center">
												        <div class="col-lg-12 col-xl-12">
													        <div class="row align-items-center">
														        <div class="col-md-3 my-2 my-md-0">
                                                                    <label><b>Firstname & Lastname</b></label>
															        <div class="input-icon">
																        <input type="text" class="form-control"
                                                                               placeholder="Search..."
                                                                               id="firstlastname"
                                                                               name="firstlastname">
																        <span>
																	        <i class="flaticon2-search-1 text-muted"></i>
																        </span>
															        </div>
														        </div>
                                                                <div class="col-md-2 my-2 my-md-0">
                                                                    <label><b>Enrollment No.</b></label>
															        <div class="input-icon">
																        <input type="text" class="form-control"
                                                                               placeholder="Search..."
                                                                               id="enrollmentno" name="enrollmentno">
																        <span>
																	        <i class="flaticon2-search-1 text-muted"></i>
																        </span>
															        </div>
														        </div>
                                                                <div class="col-md-2 my-2 my-md-0">
                                                                    <label><b>Mobile No.</b></label>
															        <div class="input-icon">
																        <input type="text" class="form-control"
                                                                               placeholder="Search..."
                                                                               id="mobileno" name="mobileno">
																        <span>
																	        <i class="flaticon2-search-1 text-muted"></i>
																        </span>
															        </div>
														        </div>
                                                                <div class="col-md-2 my-2 my-md-0">
                                                                    <label><b>Email</b></label>
															        <div class="input-icon">
																        <input type="text" class="form-control"
                                                                               placeholder="Search..."
                                                                               id="email" name="email">
																        <span>
																	        <i class="flaticon2-search-1 text-muted"></i>
																        </span>
															        </div>
														        </div>
                                                                <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile
                                                                float-right text-right" style="margin-top: 25px;">
                                                                    <button type="button" class="btn btn-brand m-btn m-btn--icon btn-info" id="m_search" style="margin-right:10px">
                                                                                  <span>
                                                                                      <i class="la la-search"></i>
                                                                                      <span>Search</span>
                                                                                  </span>
                                                                      </button>
                                                                    <button type="reset" class="btn btn-secondary m-btn m-btn--icon btn-default" id="m_reset" >
                                                                                <span>
                                                                                    <i class="la la-close"></i>
                                                                                    <span>Reset</span>
                                                                                </span>
                                                                    </button>
                                                                </div>
													        </div>
												        </div>
											        </div>
										        </div>
                                            </form>
                                        </div>
                                        <!--end::Search Form-->
                                        <!--begin: Datatable-->
                                        <div class="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" style="">
                                            <table class="m_datatable datatable-table" style="display: block;" id="ajax_data">

                                            </table>

                                        <!--end: Datatable-->
                                            <div class="datatable-pager datatable-paging-loaded">
                                                <div class="datatable-pager-info"><div class="dropdown bootstrap-select datatable-pager-size" style="width: 60px;"><button type="button" tabindex="-1" class="btn dropdown-toggle btn-light" data-toggle="dropdown" role="combobox" aria-owns="bs-select-3" aria-haspopup="listbox" aria-expanded="false" title="Select page size"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner"></div></div> </div></button><div class="dropdown-menu "><div class="inner show" role="listbox" id="bs-select-3" tabindex="-1"><ul class="dropdown-menu inner show" role="presentation"></ul></div></div></div><span class="datatable-pager-detail"></span></div></div></div>
                                            </div>
                                        </div>
                                        <!--end: Datatable-->
                                    </div>
                                </div>
                                <!--end::Card-->
                            </div>

@endsection

@section('popForm')
<div id="examModal" data-keyboard="true" data-backdrop="static" class="modal fade in" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-confirm modal-lg">
            <div class="modal-content">
                <div class="col-sm-12">
                    <div class="modal-header" style="padding: 15px">
                        <h4 class="modal-title">Student Detail</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body studentDetail text-center" style="padding: 20px;text-align: center">

                    </div>
                </div>
                <div class="col-sm-12 row">

                       {{--do not remove programDetails--}}
                   <div class="programDetails"></div>
                </div>

                <div class="col-sm-12 row">


                    <div class="col-sm-6">
                        <div class="modal-header" style="padding: 15px">
                            <h4 class="modal-title">Marks Detail</h4>
                        </div>
                        <div class="modal-body categoryResult text-center" style="padding: 0px;text-align: center">

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="modal-header" style="padding: 15px">
                            <h4 class="modal-title">Exam Time</h4>
                        </div>
                        <div class="modal-body examTime text-center" style="padding: 0px;text-align: center">

                        </div>
                    </div>


                <div class="col-sm-12 row">
                    <div class="col-sm-12">
                        <div class="modal-header" style="padding: 15px">
                                                <h4 class="modal-title">KPI Table</h4>
                                            </div>
                        <div class="modal-body text-center kpiData" style="padding: 0px;text-align: center"></div>

                    </div>
                </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleTextarea">Personal Interview Notes</label>
                        <textarea class="form-control form-control-solid" rows="3" id="piNotes"></textarea>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="exampleTextarea">HR Notes</label>
                        <textarea class="form-control form-control-solid" rows="3" id="hrNotes"></textarea>
                    </div>
                </div>

                <div class="modal-footer" style="padding: 15px">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
<div id="questionModal" data-keyboard="true" data-backdrop="static" class="modal fade in" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-confirm modal-lg">
        <div class="modal-content">
            <div class="col-sm-12">
                <div class="modal-header" style="padding: 15px">
                    <h4 class="modal-title">Exam User Answer Sheet</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="col-sm-12" style="margin-top: 15px">
                    <div class="tabParent">
                        <div class="categoryDetail">

                        </div>
                    </div>
                </div>
                <table class="table table-responsive  table-striped text-left" style="padding-left: 10px">
                    <thead>
                    <tr>
                        <th style="width: 46%;padding-left: 20px">Question</th>
                        <th style="width: 31%;padding-left: 20px">Options</th>
                        <th style="width: 174px;padding-left: 20px">Answer</th>
                    </tr>
                    </thead>
                </table>
                <div class="modal-body questionDetail text-center" style="padding: 20px;text-align: center">

                </div>
            </div>
            <div class="modal-footer" style="padding: 15px">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div id="VideoModal" data-keyboard="true" data-backdrop="static" class="modal fade in" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-confirm modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="col-sm-12">
                    <div class="modal-header" style="padding: 15px">
                        <h4 class="modal-title">Video</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                </div>
                <div class="col-sm-12 row" style="padding: 0; margin: 0;">
                    <div id="div_video" style="    width: 100%; position: relative; text-align: center; padding: 15px;"></div>
                </div>

                <div class="modal-footer" style="padding: 15px">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<!-- <script src="assets/js/pages/crud/ktdatatable/base/data-ajax.js?v=7.0.3"></script> -->
 <script>
        var examID = '{{$exam_id}}';
        var AjaxUrl = '{{URL::to('/')}}';
        var userId = '';
      /*  $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });*/
        var DatatableRemoteAjaxDemo = {

            init: function () {

                $("#exam_date").datepicker().datepicker("setDate", new Date());
                $("#kt_select2_3").select2({placeholder:"Select College"});
                $('#openings').select2({placeholder: "Select a Openings"});
                $('#stream').select2({placeholder: "Select a Education Steam"});


                var t;
                atable = $(".m_datatable").KTDatatable({
                    data: {
                        saveState :false,
                        type: "remote",
                        source: {
                            read: {
                                type: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: AjaxUrl + '/admin/exams/participants/examusers-ajax/'+examID,
                               /* params:{'exam_date':$("#exam_date").val()},*/
                                map: function (t) {
                                    console.log(t);
                                    var e = t;
                                    return void 0 !== t.data && (e = t.data), e
                                }
                            }
                        },
                        pageSize: 10,
                        serverPaging: !0,
                        serverFiltering: !0,
                        serverSorting: false,

                    },

                    layout: {
                        scroll: false,
                        footer: false
                    },
                    sortable: true,
                    pagination: !0,
                    toolbar: {
                        items: {
                            pagination: {
                                pageSizeSelect: [10, 20, 30, 50, 100]
                            }
                        }
                    },
                    // search: {
                    //     input: $("#generalSearch"),
                    //     key:'generalSearch'
                    // },
                    columns: [{
                        field: "id",
                        title: "#",
                        sortable: !1,
                        width: 50,
                        selector: !1,
                        textAlign: "center"
                    }, {
                        field: "first_name",
                        title: "User",
                        sortable: true,
                        width: 100,
                        template: function (t) {
                            return t.users.first_name+' '+t.users.last_name;
                        }
                    }, {
                        field: "enrollment_no",
                        title: "EnrollNo",
                        width: 130,
                        template: function (t) {
                            return t.users.enrollment_no;
                        }
                    },
                        {
                        field: "mobile",
                        title: "Mobile No.",
                        width: 100,
                        template: function (t) {
                            return t.users.mobile_no;
                        }
                    },
                        {
                        field: "applied_for",
                        title: "Applied For",
                        width: 100,
                        template: function (t) {
                            return t.users.opening.name;
                        }
                    },

                    //     {
                    //     field: "total_questions",
                    //     title: "Total Questions",
                    //     width: 80,
                    //     template: function (t) {
                    //         return t.total_questions;
                    //     }
                    //
                    // },
                        {
                        field: "total_correct_answers",
                        title: "Correct Answers",
                        width: 75,
                        template: function (t) {
                            return t.total_correct_answers;
                        }
                    },
                    //     {
                    //     field: "percentage",
                    //     title: "%",
                    //     width: 20,
                    //     template: function (t) {
                    //         return t.percentage;
                    //     }
                    // },
                        {
                        field: "eligibal",
                        title: "Eligible for Practical",
                        width: 100,
                        template: function (t) {
                            if(t.eligibal == '0') {
                                var buttonHTML = '';
                                buttonHTML += '<span class="switch switch-info"><label><input type="checkbox" name="select" id="'+t.id+'" class="eligibalClass unchecked"><span></span></label></span>'
                                return buttonHTML;
                            } else {
                                var buttonHTML = '';
                                buttonHTML += '<span class="switch switch-info"><label><input type="checkbox" checked name="select" id="'+t.id+'" class="eligibalClass checked"><span></span></label></span>'
                                return buttonHTML;
                            }
                        }
                    },
                    {
                        field: "status",
                        title: "Status",
                        width: 95,
                        template: function (t) {
                            if(t.status==='Completed'){
                                return `<span class="label font-weight-bold label-lg label-light-info label-inline">`+ t.status +`</span>`
                            }
                            else if(t.status==='OnGoing') {
                                return `<span class="label font-weight-bold label-lg label-light-danger label-inline">On Going</span>`
                            }
                            else{
                                return `<span class="label font-weight-bold label-lg label-light-danger label-inline">`+ t.status +`</span>`
                            }
                        }
                    }, {
                        field: "Actions",
                        width: 70,
                        title: "Actions",
                        sortable: !1,
                        overflow: "visible",
                        template: function (t, e, a) {
                            var buttonHTML = '';
                            buttonHTML += `<a href="javascript:void(0)"><button data-exam-user-id="`+t.id+`" data-id="`+t.user_id+`" class="btn
                            btn-sm btn-clean btn-icon btn-icon-md markDetail" title='Marks Detail'><i class="la la-file-text text-info"></i></button></a>`;
                            buttonHTML += `<a href="javascript:void(0)"><button data-exam-user-id="`+t.id+`" data-id="`+t.user_id+`" class="btn
                            btn-sm btn-clean btn-icon btn-icon-md answerSheet" title='Answer Sheet'><i class="la la-file-text text-primary"></i></button></a>`;
                            // buttonHTML += `<a href="https://wotcampus.s3-us-west-2.amazonaws.com/`+t.users.enrollment_no+`/video.avi" target="_blank"><button class="btn
                            // btn-sm btn-clean btn-icon btn-icon-md" title='video'><i class="fa fa-play-circle" style="color: deepskyblue"></i></button></a>`;
                            if(t.users.cv != '') {
                                buttonHTML += `<a href="/admin/users/download/`+t.users.cv+`" class="btn btn-sm btn-clean btn-icon" title="Download CV"><i class="la la-file-pdf-o text-danger"></i></a>`;
                            }
                            if(t.practical && t.practical.video_status == 1) {
                                buttonHTML += `<a href="javascript:void(0)"><button data-vidoe-url="https://wotcampus.s3-us-west-2.amazonaws.com/`+t.users.enrollment_no+`/video.webm" class="btn btn-sm btn-clean btn-icon btn-icon-md videoTrigger" title='Video'><i class="fa fa-play-circle" style="color: deepskyblue"></i></button></a>`;
                            }
                            return buttonHTML;
                        }
                    },
                        {
                           field: "university",
                           title: "University",
                           width: 100,
                           template: function (t) {
                               return t.users.university.name;
                           }
                       },
                        {
                           field: "college",
                           title: "College",
                           width: 100,
                           template: function (t) {
                               return t.users.college.name;
                           }
                       },
                        {
                           field: "education_stream",
                           title: "Education Stream",
                           width: 100,
                           template: function (t) {
                               return t.users.applystream.name;
                           }
                       },
                        {
                            field: "email",
                            title: "Email",
                            width: 120,
                            template: function (t) {
                                return t.users.email;
                            }
                        },
                        {
                            field: 'started_at',
                            title: 'Start Time (Aptitude Test)',
                            width: 75,
                        },
                        {
                            field: 'completed_at',
                            title: 'Complete Time (Aptitude Test)',
                            width: 75,
                        },{
                            field: 'practical_start',
                            title: 'Start Time (Practical Test)',
                            width: 75,
                            template: function(t) {
                                if(t.practical != null) {
                                    return t.practical.started_at;
                                } else {
                                    return t.practical;
                                }

                            }
                        },
                        {
                            field: 'practical_complete',
                            title: 'Complete Time (Practical Test)',
                            width: 75,
                            template: function(t) {
                                if(t.practical != null) {
                                    return t.practical.completed_at;
                                } else {
                                    return t.practical;
                                }
                            }
                        },

                    ]
                }), $("#m_form_status").on("change", function () {
                    atable.search($(this).val(), "Status")
                }), $("#m_form_type").on("change", function () {
                    atable.search($(this).val(), "Type")
                }), $("#m_form_status, #m_form_type").selectpicker(),

                $(document).on('click','#m_search',function (e) {
                    atable.setDataSourceParam('college_id',$(".collegeSelect").val());
                    atable.setDataSourceParam('exam_date',$('#exam_date').val());
                    atable.setDataSourceParam('stream',$('#stream').val());
                    atable.setDataSourceParam('openings',$('#openings').val());
                    atable.setDataSourceParam('exam_status',$('#exam_status').val());
                    atable.setDataSourceParam('practical_status',$('#practical_status').val());
                    atable.setDataSourceParam('video_status',$('#video_status').val());
                    atable.setDataSourceParam('eligible_filter',$('#eligible_filter').val());
                    atable.setDataSourceParam('marks',$('#marks').val());
                    atable.setDataSourceParam('filter','1');

                    atable.setDataSourceParam('firstlastname', $('#firstlastname').val());
                    atable.setDataSourceParam('enrollmentno', $('#enrollmentno').val());
                    atable.setDataSourceParam('mobileno', $('#mobileno').val());
                    atable.setDataSourceParam('email', $('#email').val());
                    $("#filterData").val(1);
                    //$('.m_datatable').mDatatable('reload');
                   // atable.search($(".collegeSelect").val(), 'college_id');
                    atable.reload();
                });

            }
        };
        jQuery(document).ready(function () {

            var callback = function(event) {
                event.preventDefault();
                atable.setDataSourceParam('college_id',$(".collegeSelect").val());
                atable.setDataSourceParam('exam_date',$('#exam_date').val());
                atable.setDataSourceParam('stream',$('#stream').val());
                atable.setDataSourceParam('openings',$('#openings').val());
                atable.setDataSourceParam('exam_status',$('#exam_status').val());
                atable.setDataSourceParam('practical_status',$('#practical_status').val());
                atable.setDataSourceParam('video_status',$('#video_status').val());
                atable.setDataSourceParam('eligible_filter',$('#eligible_filter').val());
                atable.setDataSourceParam('marks',$('#marks').val());
                atable.setDataSourceParam('filter','1');

                atable.setDataSourceParam('firstlastname', $('#firstlastname').val());
                atable.setDataSourceParam('enrollmentno', $('#enrollmentno').val());
                atable.setDataSourceParam('mobileno', $('#mobileno').val());
                atable.setDataSourceParam('email', $('#email').val());
                $("#filterData").val(1);
                //$('.m_datatable').mDatatable('reload');
               // atable.search($(".collegeSelect").val(), 'college_id');
                atable.reload();
                // Do exciting things here.
            };

            $('#firstlastname, #enrollmentno, #mobileno, #email, #marks').on({
                keyup:  $.debounce(350, callback)
            });

            $('#stream, #openings, .collegeSelect, #exam_status, #practical_status, #video_status, #eligible_filter').on({
                change:  $.debounce(350, callback)
            });

            DatatableRemoteAjaxDemo.init();
            $(document).on('click', '#m_reset', function (e) {
                $('#exam_date').val('');
                // $("#kt_select2_3").val([]);
                // $('#kt_select2_3').val(null);
                // $("#kt_select2_3").select2("val", "");
                // $("#stream").select2("val", "");
                // $("#openings").select2("val", "");
                $("#kt_select2_3").val([]).trigger('change');
                $("#stream").val([]).trigger('change');
                $("#openings").val([]).trigger('change');
                atable.setDataSourceParam('college_id','');
                atable.setDataSourceParam('exam_date','');
                atable.setDataSourceParam('exam_status','');
                atable.setDataSourceParam('practical_status','');
                atable.setDataSourceParam('video_status','');
                atable.setDataSourceParam('eligible_filter','');
                atable.setDataSourceParam('marks','');
                atable.setDataSourceParam('filter','1');
                atable.setDataSourceParam('firstlastname','');
                atable.setDataSourceParam('enrollmentno','');
                atable.setDataSourceParam('mobileno','');
                atable.setDataSourceParam('email','');
                $("#filterData").val(1);
                atable.reload();
            });

        });



        $(document).on('click', '.eligibalClass', function() {
            var id = $(this).attr('id');
            var status;
            if($(this).hasClass('checked')) {
                $(this).removeClass('checked');
                $(this).addClass('unchecked');
                status = 0;
            } else {
                $(this).removeClass('unchecked');
                $(this).addClass('checked');
                status = 1;
            }
            $.ajax({
                    url: AjaxUrl + '/admin/exams/participants/eligibal',
                    type: 'POST',
                    data:{ id:id, status :status },
                    success: function (response) {

                    },
                });
        });

        $(document).on('click', '.videoTrigger', function (e) {
            var videoURL = $(this).attr('data-vidoe-url');
            // var videoURL = 'https://intellyscan-local.s3.eu-central-1.amazonaws.com/8/videos/179/1599483563967.avi';
            // var videoURL = 'https://wotcampus.s3-us-west-2.amazonaws.com/89050/video.webm';
            // $(document).find('#videoTag source').attr('src', videoURL);
            $(document).find('#videoTag source').remove();

                var videoHTML = `<video id="videoTag" width="100%" height="100%" controls>
                    <source src="${videoURL}" type="video/webm">
                </video>`;
            document.getElementById('div_video').innerHTML = videoHTML;

            $('#VideoModal').modal()
        });

        $(document).on('click', '.markDetail', function (e) {
            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/users-details',
                type: 'POST',
                data:{ exam_id:examID,user_id:$(this).data('id')},
                success: function (response) {
                    /*console.log(response);*/
                    $(".studentDetail").html(response);
                },
            });

            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/exam-time',
                type: 'POST',
                data:{ exam_id:examID,user_id:$(this).data('id')},
                success: function (response) {
                    /*console.log(response);*/
                    $(".examTime").html(response);
                },
            });

            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/users-marks',
                type: 'POST',
                data:{ exam_id:examID,user_id:$(this).data('id')},
                success: function (response) {
                    /*console.log(response);*/
                    $(".categoryResult").html(response);
                },
            });
            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/get-kpi-data',
                type: 'POST',
                data:{ exam_user_id:$(this).data('exam-user-id')},
                success: function (response) {
                    /*console.log(response);*/
                    $(".kpiData").html(response);
                },
            });
            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/get-practical-data',
                type: 'POST',
                data:{ exam_user_id:$(this).data('exam-user-id')},
                success: function (response) {
                    /*console.log(response);*/
                    $(".programDetails").html(response);
                },
            });
            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/get-notes-data',
                type: 'POST',
                data:{ exam_user_id:$(this).data('exam-user-id')},
                success: function (response) {
                    console.log(response);
                    $(document).find("#piNotes").val(response.data.pi_notes);
                    $(document).find("#hrNotes").val(response.data.hr_notes);
                },
            });
            console.log('cc', $(this).data('exam-user-id'))
            $(document).find('#piNotes').attr('exam-user-id', $(this).data('exam-user-id'));
            $(document).find('#hrNotes').attr('exam-user-id', $(this).data('exam-user-id'));
            $('#examModal').modal();

        });

        $(document).on('click', '.tabClass', function(e) {
            $('.tab-container').find('button').removeClass('activeClass');
            $(this).addClass('activeClass');
            var categoryId = $(this).attr('id');
            userId = userId;
            showQuestions(categoryId,userId);
        });

        $(document).on('click', '.answerSheet', function (e) {
            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/categoryDetail',
                type: 'POST',
                data:{ exam_id:examID,user_id:$(this).data('id')},
                success: function (response) {
                    $(".categoryDetail").html(response);
                },
            });
            var categoryId = 0;
            userId = $(this).data('id');
            showQuestions(categoryId,userId);
            $('#questionModal').modal();
        });

        function showQuestions(id,userId) {
            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/question-answer-details',
                type: 'POST',
                data:{ exam_id:examID,user_id:userId,category_id:id},
                success: function (response) {
                    /*console.log(response);*/
                    $(".questionDetail").html(response);
                },
            });
        }

        $.fn.getForm2obj = function() {
          var _ = {};
          $.map(this.serializeArray(), function(n) {
            const keys = n.name.match(/[a-zA-Z0-9_]+|(?=\[\])/g);
            if (keys.length > 1) {
              let tmp = _;
              pop = keys.pop();
              for (let i = 0; i < keys.length, j = keys[i]; i++) {
                tmp[j] = (!tmp[j] ? (pop == '') ? [] : {} : tmp[j]), tmp = tmp[j];
              }
              if (pop == '') tmp = (!Array.isArray(tmp) ? [] : tmp), tmp.push(n.value);
              else tmp[pop] = n.value;
            } else _[keys.pop()] = n.value;
          });
          return _;
        }

        function updatekpiData() {
            var kpiData =  $(document).find('#kpiDataForm').getForm2obj();
            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/update-kpi-data',
                type: 'POST',
                data:{ exam_user_id:$(document).find('#updateKpiData').attr('exam-user-id'), kpiData: kpiData},
                success: function (response) {
                    /*console.log(response);*/
                    $(".kpiData").html(response);
                },
            });
        }

        $(document).on('focusout', '.userScoreInput', function(){
            updatekpiData();
        });

        function practicalCommentUpdate(id) {
            var comment_1 = $(document).find('input[name="comment_1"]').val();
            var comment_2 = $(document).find('input[name="comment_2"]').val();
            var comment_3 = $(document).find('input[name="comment_3"]').val();

            var comment_4 = $(document).find('input[name="comment_4"]').val();
            var comment_5 = $(document).find('input[name="comment_5"]').val();
            var comment_6 = $(document).find('input[name="comment_6"]').val();
            var comment_7 = $(document).find('input[name="comment_7"]').val();
            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/update-practical-comment',
                type: 'POST',
                data:{ practical_id:id,
                    comment_1: comment_1,
                    comment_2: comment_2,
                    comment_3: comment_3,
                    comment_4: comment_4,
                    comment_5: comment_5,
                    comment_6: comment_6,
                    comment_7: comment_7,
                },
                success: function (response) {
                    /*console.log(response);*/
                },
            });
        }

        $(document).on('click', '.practicalToggle', function(){
            console.log('value', )
            var value = $(this).is(":checked");
            var id = $(this).attr('data-practical-id');
            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/update-practical-status',
                type: 'POST',
                data:{ practical_id:id,
                    value: value,
                },
                success: function (response) {
                    /*console.log(response);*/
                },
            });
        });

        $(document).on('focusout', '.practicalInput', function(){
            practicalCommentUpdate($(this).attr('data-practical-id'));
        });

        function updateNotes(id) {
            var piNotes = $(document).find('#piNotes').val();
            var hrNotes = $(document).find('#hrNotes').val();
            $.ajax({
                url: AjaxUrl + '/admin/exams/participants/update-notes-data',
                type: 'POST',
                data:{ exam_user_id:id,
                    piNotes: piNotes,
                    hrNotes: hrNotes,
                },
                success: function (response) {
                    /*console.log(response);*/
                },
            });
        }

        $(document).on('focusout', '#piNotes, #hrNotes', function(){
            console.log('ZZZ', $(this).attr('exam-user-id'))
            updateNotes($(this).attr('exam-user-id'));
        });

        $(document).on('click', '#updateKpiData', function (e) {
            console.log('ZZZ', $(document).find('#updateKpiData').attr('exam-user-id'))
            updatekpiData();
        })

        $(document).on('click', '.exportButton', function (e) {
            $("#filterForm").submit();
        });
    </script>
@endsection
