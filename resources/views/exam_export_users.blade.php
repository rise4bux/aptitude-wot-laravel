<table>
    <thead>
    <tr>
        <th>
            #
        </th>
        <th>
            User
        </th>
        <th>
            University
        </th>
        <th>
            College
        </th>
        <th>
            Enrollment No
        </th>
        <th>
            Email
        </th>
        <th>
            Mobile No
        </th>
        <th>
            Address
        </th>
        <th>
            Percentage
        </th>
        <th>
            Total Questions
        </th>
        <th>
            Correct Answers
        </th>
        @if($data->count() > 0)
            @foreach($data[0]->categories as $userCat)
                <th>{{ $userCat->cat_name.'(Total Questions)' }}</th>
                <th>{{ $userCat->cat_name.'(Correct Answers)' }}</th>
            @endforeach
        @endif

        <th>
            Date
        </th>
        <th>
            Aptitude Status
        </th>
        <th>
            Apply For
        </th>
        <th>
            Education Stream
        </th>
        <th>
            Other Interest
        </th>
        <th>
            Eligible For Practical
        </th>
        <th>
            Practical Status
        </th>
        <th>
            Video Generated
        </th>
    </tr>
    </thead>
    <tbody>
    @if($data)
            @php $i=1; @endphp
            @foreach($data as $exuser)
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $exuser->users->first_name.' '.$exuser->users->last_name }}</td>
                    <td>{{ $exuser->users->university->name }}</td>
                    <td>{{ $exuser->users->college->name }}</td>
                    <td>{{ $exuser->users->enrollment_no }}</td>
                    <td>{{ $exuser->users->email }}</td>
                    <td>{{ $exuser->users->mobile_no }}</td>
                    <td>{{ $exuser->users->address }}</td>
                    <td>{{ $exuser->percentage }}</td>
                    <td>{{ $exuser->total_questions }}</td>
                    <td>{{ $exuser->total_correct_answers }}</td>
                    @foreach( $exuser->categories  as $cat)
                        <td>{{ $cat->total_question }}</td>
                        <td>{{ $cat->total_correct }}</td>
                    @endforeach


                    <td>{{ date("d/m/Y",strtotime($exuser->started_at)) }}</td>
                    <td>{{ $exuser->status }}</td>
                    <td>{{ $exuser->users->opening->name }}</td>
                    <td>{{ $exuser->users->applystream->name }}</td>
                    <td>{{ $exuser->users->other_interest }}</td>
                    <td>{{ $exuser->eligibal }}</td>
                    <td>{{ $exuser->practical ? $exuser->practical->status : ""}}</td>
                    <td>{{ $exuser->practical ? $exuser->practical->video_status : ""}}</td>
                </tr>
                @php $i++; @endphp
            @endforeach
        @endif
    </tbody>
</table>
