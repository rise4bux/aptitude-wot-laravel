<table class="table table-responsive  table-striped text-left" style="padding-left: 10px">
    <tbody>
        <tr>
            <th style="width: 150px">Name</th>
            <td style="width: 200px">{{ $user->first_name }} {{ $user->last_name }}</td>
            <th style="width: 150px">Email</th>
            <td style="width: 200px">{{ $user->email }}</td>
        </tr>
        <tr>
            <th style="width: 150px">Enrollment No.</th>
            <td style="width: 200px">{{ $user->enrollment_no }}</td>
            <th style="width: 150px">Mobile Number</th>
            <td style="width: 200px">{{ $user->mobile_no }}</td>
        </tr>
        <tr>
            <th style="width: 150px">University</th>
            <td style="width: 200px">{{ $user->university->name }}</td>
            <th style="width: 150px">College</th>
            <td style="width: 200px">{{ $user->college->name }}</td>

        </tr>
        <tr>
            <th style="width: 150px">Education Stream</th>
            <td style="width: 200px">{{ $user->educationstream->name }}</td>
            <th style="width: 150px">Applied for</th>
            <td style="width: 200px">{{ $user->opening->name }}</td>
        </tr>
        <tr>
            <th style="width: 150px">Other interest</th>
            <td style="width: 200px">{{ $user->other_interest }}</td>
            <th style="width: 150px">Address</th>
            <td style="width: 200px">{{ $user->address }}</td>
        </tr>
    </tbody>
</table>
