<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			Aptitude test
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.js"></script>
		<script>


            WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }

          });

		</script>




		<!--end::Web font -->
        <!--begin::Base Styles -->
        <!--begin::Page Vendors -->

		<!--end::Page Vendors -->
        <link href="{{asset('assets/app/css/dataTables.css')}}">
		<link href={{asset('assets/vendors/base/vendors.bundle.css')}} rel="stylesheet" type="text/css"/>
		<link href={{asset("assets/demo/demo9/base/style.bundle.css")}} rel="stylesheet" type="text/css" />
		<link href={{asset("assets/app/css/dashboard.css")}} rel="stylesheet" type="text/css" />
        <!--end::Base Styles -->
		<link rel="shortcut icon" href={{asset("assets/app/media/img/logos/weblogo.png")}} />
        <link rel="stylesheet" href="{{asset('assets/app/css/custom.css')}}">
        <link rel="stylesheet" href="{{asset('assets/app/css/style.bundle.css')}}" >
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
        @if(Request::segment(3) != 'add-questions' && Request::segment(3) != 'edit-question')
            <link href="{{asset('assets/app/toastr/toastr.min.css')}}" rel="stylesheet">
        @endif
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin::Page loader -->
		<div class="m-page-loader m-page-loader--base">
			<div class="m-blockui">
				<span>
					Please wait...
				</span>
				<span>
					<div class="m-loader m-loader--brand"></div>
				</span>
			</div>
		</div>
		<!-- end::Page Loader -->
<!-- begin:: Page -->

			<!-- BEGIN: Header -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
			<header id="m_header" class="m-grid__item    m-header "  m-minimize="minimize" m-minimize-mobile="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200" >
                <div class="m-container m-container--fluid m-container--full-height">
                    <div class="m-stack m-stack--ver m-stack--desktop">
						<!-- BEGIN: Brand -->

						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                            <div class="m-stack m-stack--ver m-stack--desktop">
                                <div class="m-stack__item m-stack__item--middle m-stack__item--fit">

                                    <!-- BEGIN: Aside Left Toggle -->
                                    <a href="javascript:;" id="m_aside_left_toggle" class="m-aside-left-toggler m-aside-left-toggler--left m_aside_left_toggler">
                                        <span></span>
                                    </a>

                                    <!-- END: Aside Left Toggle -->
                                </div>
                                <div class="m-stack__item m-stack__item--fluid">

                                    <!-- BEGIN: Horizontal Menu -->
                                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                                    <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
                                        <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
                                                <a href="{{route('user.list')}}" class="m-menu__link ">
                                                    <span class="m-menu__link-text">Users</span>
                                                </a>
                                            </li>
                                            <li class="m-menu__item  m-menu__item--rel" m-menu-link-redirect="1" aria-haspopup="true">
                                                <a href="{{route('university.index')}}" class="m-menu__link ">
                                                    <span class="m-menu__link-text">Universities</span>

                                                </a>
                                            </li>
                                            <li class="m-menu__item  m-menu__item--rel"  m-menu-link-redirect="1" aria-haspopup="true">
                                                <a href="{{route('college.index')}}" class="m-menu__link ">
                                                    <span class="m-menu__link-text">Colleges</span>
                                                </a>
                                            </li>
                                            <li class="m-menu__item m-menu__item--rel"  m-menu-link-redirect="1" aria-haspopup="true">
                                                <a href="{{route('technology.index')}}" class="m-menu__link">
                                                    <span class="m-menu__link-text">
                                                        Technology
                                                </span>
                                                </a>
                                            </li>
                                            <li class="m-menu__item  m-menu__item--rel" m-menu-link-redirect="1" aria-haspopup="true">
                                                <a href="{{route('category.list')}}" class="m-menu__link ">
                                                    <span class="m-menu__link-text">Categories</span>
                                                </a>
                                            </li>
                                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
                                                <a href="{{route('questions.index')}}" class="m-menu__link ">
                                                    <span class="m-menu__link-text">Questions</span>
                                                </a>
                                            </li>

                                            <li class="m-menu__item m-menu__item--rel"  m-menu-link-redirect="1" aria-haspopup="true">
                                                <a href="{{route('exams.index')}}" class="m-menu__link ">
                                                    <span class="m-menu__link-text">Exams</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <!-- END: Horizontal Menu -->
                                </div>
                            </div>
                        </div>
                        {{--<div class="m-stack__item m-stack__item--middle m-stack__item--center">

                    <!-- BEGIN: Brand -->
                    <a href="{{ route('home') }}" class="m-brand m-brand--desktop">
                        <img alt="" src="{{ asset('assets/app/media/img/logos/weblogo.png') }}" width="41" height="31" />
                    </a>

                </div>--}}
                        <div class="m-stack__item m-stack__item--right">

                            <!-- BEGIN: Topbar -->
                            <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                                <div class="m-stack__item m-topbar__nav-wrapper">
                                    <ul class="m-topbar__nav m-nav m-nav--inline">
                                        <li class="m-nav__item m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                            <a href="#" class="m-nav__link m-dropdown__toggle">
                                        <span class="m-topbar__username m--hidden-mobile">
                                            {{auth()->user()->first_name}}
                                            <span>
                                                <i class="la la-angle-down" style=" font-size: 15px"></i>
                                            </span>
                                        </span>


                                            </a>
                                            <div class="m-dropdown__wrapper">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__header m--align-center">
                                                        <div class="m-card-user m-card-user--skin-light">
                                                            <div >
                                                                <img src={{asset('assets/app/media/img/logos/weboccult.png')}}  alt=""/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">
                                                            <ul class="m-nav m-nav--skin-light">

                                                                <li class="m-nav__separator m-nav__separator--fit"></li>
                                                                <li class="m-nav__item">

                                                                    <form method="post" action="{{route('logout')}}">
                                                                        @csrf
                                                                        <input type="submit" name="logout"b value="logout" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                                                    </form>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!-- END: Topbar -->
                        </div>


                </div>
                <div>
                </div>
                </div>
            </header>
                <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
                    <i class="la la-close"></i>
                </button>
                    <div id="m_aside_left" class="m-aside-left  m-aside-left--skin-dark ">
                        <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
                            <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                                <li>
                                    <a href="{{ route('home') }}" class="m-brand m-brand--desktop">
                                        <img alt="" src="{{ asset('assets/app/media/img/logos/weboccult.png') }}" />
                                    </a>
                                </li>
                                <li class="m-menu__section">
                                    <h4 class="m-menu__section-text">
                                        Examportal
                                    </h4>
                                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                                </li>
                                <li class="m-menu__item">
                                    <a href="{{route('user.list')}}" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon la la-users"></i>
                                        <span class="m-menu__link-text">
									Users
								</span>
                                    </a>
                                </li>
                                <li class="m-menu__item"><a href="{{route('exams.index')}}" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon la la-check-square"></i><span class="m-menu__link-text">
                                    Exams
                                </span>
                                    </a>
                                </li>
                                <li class="m-menu__item"><a href="{{route('category.list')}}" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon la la-th-list"></i>
                                        <span class="m-menu__link-text">
                                    Categories
                                </span>
                                    </a>
                                </li>
                                <li class="m-menu__item">
                                    <a href="{{route('questions.index')}}" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon la la-question"></i>
                                        <span class="m-menu__link-text">
                                    Questions
                                </span>
                                    </a>
                                </li>
                                <li class="m-menu__item">
                                    <a href="{{route('university.index')}}" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon la la-university"></i>
                                        <span class="m-menu__link-text">
                                        Universities
                                </span>
                                    </a>
                                </li>
                                <li class="m-menu__item">
                                    <a href="{{route('college.index')}}" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon la la-institution"></i>
                                        <span class="m-menu__link-text">
                                        Colleges
                                </span>
                                    </a>
                                </li>
                                <li class="m-menu__item">
                                    <a href="{{route('technology.index')}}" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon la-code la"></i>
                                        <span class="m-menu__link-text">
                                        Technology
                                </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        <div class="m-content">
        @yield('content')
        </div>
            @extends('partials.footer')
    </body >
   	<!-- end::Body -->
   </html>
