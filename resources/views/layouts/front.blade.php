<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title>
        Aptitude test
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Add new css for the instruction part -->
    @yield('css')
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }

        });
    </script>




    <!--end::Web font -->
    <!--begin::Base Styles -->
    <!--begin::Page Vendors -->

    <!--end::Page Vendors -->
    <link href="{{asset('assets/app/css/dataTables.css')}}">
    <link href={{asset('assets/vendors/base/vendors.bundle.css')}} rel="stylesheet" type="text/css" />
    <link href={{asset("assets/demo/demo9/base/style.bundle.css")}} rel="stylesheet" type="text/css" />
    <link href={{asset("assets/app/css/dashboard.css")}} rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href={{asset("assets/app/media/img/logos/weblogo.png")}} />
    <link rel="stylesheet" href="{{asset('assets/app/css/custom.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
    <link rel="stylesheet" href="{{asset('assets/app/css/style.bundle.css')}}">
    <link href="{{asset('assets/app/toastr/toastr.min.css')}}" rel="stylesheet">
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="m--skin- m-page--loading-enabled m-page--loading m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default">
    <!-- begin::Page loader -->
    <div class="m-page-loader m-page-loader--base">
        <div class="m-blockui">
            <span> Please wait... </span>
            <span>
                <div class="m-loader m-loader--brand"></div>
            </span>
        </div>
    </div>
    <!-- end::Page Loader -->
    <!-- begin:: Page -->

    <!-- BEGIN: Header -->
    @if(Request::segment(2) == 'instruction')
    @elseif(Request::segment(2) == "login" && Request::segment(3) == "save")
    @else
    <div> 
        <header id="m_header" class="m-grid__item    m-header " m-minimize="minimize" m-minimize-mobile="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200">
            <div class="m-container m-container--fluid m-container--full-height">

                <div class="m-stack m-stack--ver m-stack--desktop  m-header__wrapper">
                    <!-- BEGIN: Brand -->
                    <div class="m-stack__item m-brand m-brand--mobile">
                        <div class="m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                <a href="{{route('home')}}" class="m-brand__logo-wrapper">
                                    <img alt="" src={{asset("assets/app/media/img/logos/weblogo.png")}} />
                                </a>
                            </div>

                            <div class="m-stack__item m-stack__item--middle m-brand__tools">
                                <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                <a href="javascript:;" id="m_aside_left_toggle_mobile" class="m-brand__icon m-brand__toggler m-brand__toggler--left">
                                    <span></span>
                                </a>
                                <!-- END -->
                                <!-- BEGIN: Responsive Header Menu Toggler -->
                                <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler">
                                    <span></span>
                                </a>
                                <!-- END -->
                                <!-- BEGIN: Topbar Toggler -->
                                <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon">
                                    <i class="flaticon-more"></i>
                                </a>
                                <!-- BEGIN: Topbar Toggler -->
                            </div>
                        </div>
                    </div>
                    <!-- END: Brand -->

                    <div class="m-stack__item m-stack__item--middle m-stack__item--center">
                        <!-- BEGIN: Brand -->
                        <a href="{{ route('welcome') }}" class="m-brand m-brand--desktop">
                            <img alt="" src="{{asset('assets/app/media/img/logos/weboccult.png')}}" />
                        </a>
                        <!-- END: Brand -->
                    </div>

                </div>

            </div>
        </header>
    </div>
    @endif
    <div class="m-content">
        @yield('content')
    </div>
    <script src={{asset("assets/vendors/base/vendors.bundle.js")}} type="text/javascript"></script>
        @extends('partials.footer')

</body>
<!-- end::Body -->

</html>