<!DOCTYPE html>
<!--
Template Name: Metronic - Bootstrap 4 HTML, React, Angular 9 & VueJS Admin Dashboard Theme
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: https://1.envato.market/EA4JP
Renew Support: https://1.envato.market/EA4JP
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">
	<!--begin::Head-->
	<head>
		<base href="">
		<meta charset="utf-8" />
		<title>Aptitude Test</title>
		<meta name="description" content="Updates and statistics" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!--begin::Fonts-->
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
		<!--end::Fonts-->
		<!--begin::Page Vendors Styles(used by this page)-->
		<link href="{{asset('new_assets/plugins/fullcalender.bundle.css?v=7.0.3')}}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors Styles-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="{{asset('new_assets/plugins/plugins.bundle.css?v=7.0.3')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('new_assets/plugins/prismjs.bundle.css?v=7.0.3')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('new_assets/css/style.bundle.css?v=7.0.3')}}" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<!--end::Layout Themes-->
		<link rel="shortcut icon" href="{{asset('assets/app/media/img/logos/weblogo.png')}}" />
		@yield('css')
        <style>
            .breadcrumb .breadcrumb-item.active i, .breadcrumb .breadcrumb-item.active a, .breadcrumb .breadcrumb-item:hover i, .breadcrumb .breadcrumb-item:hover a {color: cornflowerblue !important; font-weight: 500}
            .dataTables_wrapper .dataTable th.sorting_desc, .dataTables_wrapper .dataTable td.sorting_desc {color: cornflowerblue !important;}
            .switch.switch-info:not(.switch-outline) input:empty ~ span:before {background-color: #ffffff;border: 1px solid cornflowerblue;}
            .switch.switch-info:not(.switch-outline) input:empty ~ span:after {background-color: cornflowerblue;opacity: 0.2;}
            .switch.switch-info:not(.switch-outline) input:checked ~ span:after {opacity: 1;color: #6993FF;background-color: cornflowerblue;}
        </style>

	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed subheader-enabled page-loading">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="d-flex flex-row flex-column-fluid page">
				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
					<!--begin::Header Mobile-->
					<div id="kt_header_mobile" class="header-mobile">
						<!--begin::Logo-->
						<a href="index.html">
							<img alt="Logo" src="{{asset('assets/app/media/img/logos/weblogo.png')}}" class="max-h-30px" />
						</a>
						<!--end::Logo-->

						<!--begin::Toolbar-->
						<div class="d-flex align-items-center">
							<button class="btn p-0 burger-icon burger-icon-left ml-4" id="kt_header_mobile_toggle">
								<span></span>
							</button>
							<button class="btn p-0 ml-2" id="kt_header_mobile_topbar_toggle">
								<span class="svg-icon svg-icon-xl">
									<!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon points="0 0 24 0 24 24 0 24" />
											<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
											<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
										</g>
									</svg>
									<!--end::Svg Icon-->
								</span>
							</button>
						</div>
						<!--end::Toolbar-->
					</div>
					<!--end::Header Mobile-->
					<!--begin::Header-->
					<div id="kt_header" class="header header-fixed">
						<!--begin::Container-->
						<div class="container">
							<!--begin::Left-->
							<div class="d-none d-lg-flex align-items-center mr-3 bg-white p-3" style="border-radius: 0.42rem">
								<!--begin::Logo-->
								<a href="{{route('home')}}" class=" ">
									<img alt="Logo" src="{{asset('assets/app/media/img/logos/weboccult.png')}}" class="logo-default max-h-35px" />
								</a>
								<!--end::Logo-->
							</div>
							<!--end::Left-->
							<!--begin::Topbar-->
							<div class="topbar topbar-minimize">
								<!--begin::User-->
								<div class="dropdown">
									<!--begin::Toggle-->
									<div class="topbar-item" data-toggle="dropdown" data-offset="0px,0px">
										<div class="btn btn-icon btn-clean h-40px w-40px btn-dropdown">
											<span class="svg-icon svg-icon-lg">
												<!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24" />
														<path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
														<path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
													</g>
												</svg>
												<!--end::Svg Icon-->
											</span>
										</div>
									</div>
									<!--end::Toggle-->
									<!--begin::Dropdown-->
									<div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg p-0">
										<!--begin::Header-->
										<div class="d-flex align-items-center p-4 rounded-top">
											<!--begin::Symbol-->
											<div class="symbol symbol-md bg-light-primary mr-3 flex-shrink-0">
												<img src="{{asset('assets/app/media/img/logos/weblogo.png')}}" alt="" />
											</div>
											<!--end::Symbol-->
											<!--begin::Text-->

											<div class="text-dark m-0 flex-grow-1 mr-3 font-size-h5">
												<a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="color: black">
                                    {{auth()->user()->first_name}}
                                </a>
											</div>

											<!--end::Text-->
										</div>


										<!--end::Header-->
										<!--begin::Nav-->
										<div class="navi navi-spacer-x-0 pt-5">
											<!--begin::Item-->
											<a href="{{ route('edit.profile',auth()->user()->id) }}" class="navi-item px-8">
												<div class="navi-link">
													<div class="navi-icon mr-2">
														<i class="flaticon2-calendar-3 text-success"></i>
													</div>
													<div class="navi-text">
														<div class="font-weight-bold">My Profile</div>
														<div class="text-muted">Account settings and more
														</div>
													</div>
												</div>
											</a>
											<!--end::Item-->


										</div>
										<div class="navi navi-spacer-x-0 ">
											<div class="navi-footer px-5 py-5">
												<a class="btn btn-light-info font-weight-bold" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        			Sign Out
                                    			</a>

                                    			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        		@csrf
                                    			</form>
											</div>
											<!--end::Footer-->


											<!--end::Footer-->
										</div>
										<!--end::Nav-->
									</div>
									<!--end::Dropdown-->
								</div>
								<!--end::User-->
							</div>
							<!--end::Topbar-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header-->
					<!--begin::Header Menu Wrapper-->
					<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
						<div class="container">
							<!--begin::Header Menu-->
							<div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default header-menu-root-arrow">
								<!--begin::Header Nav-->
								<ul class="menu-nav">
									<li class="menu-item  menu-item-submenu menu-item-rel @if(Request::segment(1) == 'home') menu-item-open menu-item-here menu-item-open menu-item-here @endif ">
										<a href="{{route('home')}}" class="menu-link">
											<span class="menu-text">Dashboard</span>
										</a>
									</li>
									<li class="menu-item  menu-item-submenu menu-item-rel @if(Request::segment(2) == 'exams') menu-item-open menu-item-here menu-item-open menu-item-here @endif" >
										<a href="{{route('exams.index')}}" class="menu-link ">
											<span class="menu-text">Exams</span>
										</a>
									</li>
                                    <li class="menu-item  menu-item-submenu menu-item-rel @if(Request::segment(2) == 'users') menu-item-open menu-item-here menu-item-open menu-item-here @endif" >
                                        <a href="{{route('user.list')}}" class="menu-link ">
                                            <span class="menu-text">Users</span>

                                        </a>
                                    </li>
									<li class="menu-item menu-item-submenu menu-item-rel @if(Request::segment(2) == 'category' || Request::segment(2) == 'questions' || Request::segment(2) == 'practical-questions' ) menu-item-open menu-item-here menu-item-open menu-item-here @endif" data-menu-toggle="click" aria-haspopup="true">
										<a href="javascript:;" class="menu-link menu-toggle">
											<span class="menu-text">Question Management</span>
											<span class="menu-desc"></span>
											<i class="menu-arrow"></i>
										</a>
										<div class="menu-submenu menu-submenu-classic menu-submenu-left">
											<ul class="menu-subnav">
												<li class="menu-item" aria-haspopup="true">
													<a href="{{route('category.list')}}" class="menu-link">
														<i class="menu-bullet menu-bullet-dot">
															<span></span>
														</i>
														<span class="menu-text">Question Categories</span>
													</a>
												</li>
												<li class="menu-item" aria-haspopup="true">
													<a href="{{route('questions.index')}}" class="menu-link">
														<i class="menu-bullet menu-bullet-dot">
															<span></span>
														</i>
														<span class="menu-text">Questions</span>
													</a>
												</li>
                                                <li class="menu-item" aria-haspopup="true">
                                                    <a href="{{route('practical.questions.index')}}" class="menu-link">
                                                        <i class="menu-bullet menu-bullet-dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="menu-text">Practical Questions</span>
                                                    </a>
                                                </li>
											</ul>
										</div>
									</li>
                                    <li class="menu-item menu-item-submenu menu-item-rel @if(Request::segment(2) == 'college' || Request::segment(2) == 'university' || Request::segment(2) == 'technology' || Request::segment(2) == 'openings' || Request::segment(2) == 'educationstream') menu-item-open menu-item-here menu-item-open menu-item-here @endif" data-menu-toggle="click" aria-haspopup="true">
                                    										<a href="javascript:;" class="menu-link menu-toggle">
                                    											<span class="menu-text">Masters</span>
                                    											<span class="menu-desc"></span>
                                    											<i class="menu-arrow"></i>
                                    										</a>
                                    										<div class="menu-submenu menu-submenu-classic menu-submenu-left">
                                    											<ul class="menu-subnav">
                                    												<li class="menu-item" aria-haspopup="true">
                                    													<a href="{{route('college.index')}}" class="menu-link">
                                    														<i class="menu-bullet menu-bullet-dot">
                                    															<span></span>
                                    														</i>
                                    														<span class="menu-text">Colleges</span>
                                    													</a>
                                    												</li>
                                    												<li class="menu-item" aria-haspopup="true">
                                    													<a href="{{route('university.index')}}" class="menu-link">
                                    														<i class="menu-bullet menu-bullet-dot">
                                    															<span></span>
                                    														</i>
                                    														<span class="menu-text">Universities</span>
                                    													</a>
                                    												</li>
                                    												<li class="menu-item" aria-haspopup="true">
                                    													<a href="{{route('technology.index')}}" class="menu-link">
                                    														<i class="menu-bullet menu-bullet-dot">
                                    															<span></span>
                                    														</i>
                                    														<span class="menu-text">Technologies</span>
                                    													</a>
                                    												</li>
                                                                                    <li class="menu-item" aria-haspopup="true">
                                    													<a href="{{route('openings.index')}}" class="menu-link">
                                    														<i class="menu-bullet menu-bullet-dot">
                                    															<span></span>
                                    														</i>
                                    														<span class="menu-text">Openings</span>
                                    													</a>
                                    												</li>
                                                                                    <li class="menu-item" aria-haspopup="true">
                                    													<a href="{{route('education.index')}}" class="menu-link">
                                    														<i class="menu-bullet menu-bullet-dot">
                                    															<span></span>
                                    														</i>
                                    														<span class="menu-text">Education Streams</span>
                                    													</a>
                                    												</li>
                                    											</ul>
                                    										</div>
                                    									</li>

                                    <li class="menu-item  menu-item-submenu menu-item-rel @if(Request::segment(2) == 'kpi') menu-item-open menu-item-here menu-item-open menu-item-here @endif" >
                                        <a href="{{route('kpi.index')}}" class="menu-link ">
                                            <span class="menu-text">KPI</span>
                                        </a>
                                    </li>
                                    {{--<li class="menu-item  menu-item-submenu menu-item-rel @if(Request::segment(2) == 'educationstream') menu-item-open menu-item-here menu-item-open menu-item-here @endif" >
										<a href="{{route('education.index')}}" class="menu-link ">
											<span class="menu-text">Education Stream Management</span>

										</a>
									</li>--}}
									<li class="menu-item  menu-item-submenu menu-item-rel @if(Request::segment(2) == 'system-setting') menu-item-open menu-item-here menu-item-open menu-item-here @endif" >
										<a href="{{route('system.setting.edit')}}" class="menu-link ">
											<span class="menu-text">Settings</span>
										</a>
									</li>
								</ul>
								<!--end::Header Nav-->
							</div>
							<!--end::Header Menu-->
						</div>
					</div>
					<!--end::Header Menu Wrapper-->
					<!--begin::Container-->
					<div class="d-flex flex-row flex-column-fluid container">
						<!--begin::Content Wrapper-->
						<div class="main d-flex flex-column flex-row-fluid">
							<!--begin::Subheader-->
							<div class="subheader py-2 py-lg-4" id="kt_subheader">
								<div class="w-100 d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
									<!--begin::Info-->
									<div class="d-flex align-items-center flex-wrap mr-1">
										<!--begin::Page Heading-->
										<div class="d-flex align-items-baseline mr-5">
											<!--begin::Page Title-->
											<h5 class="text-dark font-weight-bold my-2 mr-5">Dashboard</h5>
											<!--end::Page Title-->
											<!--begin::Breadcrumb-->
											<ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
												<li class="breadcrumb-item">
													<a href="{{route('home')}}" class="text-muted">Dashboard</a>
												</li>
												@if(Request::segment(2) == "technology")
													<li class="breadcrumb-item">
														<a href="{{route('technology.index')}}" class="text-muted">Technologies</a>
													</li>
												@endif
												@if(Request::segment(2) == "college")
													<li class="breadcrumb-item">
														<a href="{{route('college.index')}}" class="text-muted">Colleges</a>
													</li>
												@endif
												@if(Request::segment(2) == "university")
													<li class="breadcrumb-item">
														<a href="{{route('university.index')}}" class="text-muted">Universities</a>
													</li>
												@endif
												@if(Request::segment(2) == "category")
													<li class="breadcrumb-item">
														<a href="{{route('category.list')}}" class="text-muted">Question Categories</a>
													</li>
												@endif
												@if(Request::segment(2) == "questions")
													<li class="breadcrumb-item">
														<a href="{{route('questions.index')}}" class="text-muted">Questions</a>
													</li>
												@endif
												@if(Request::segment(2) == "users")
													<li class="breadcrumb-item">
														<a href="{{route('user.list')}}" class="text-muted">Users</a>
													</li>
												@endif
												@if(Request::segment(2) == "exams")
                                                    <li class="breadcrumb-item">
														<a href="{{route('exams.index')}}" class="text-muted">Exams</a>
													</li>
												@endif
												@if(Request::segment(2) == "openings")
													<li class="breadcrumb-item">
														<a href="{{route('openings.index')}}" class="text-muted">Opening Management</a>
													</li>
												@endif
												@if(Request::segment(2) == "educationstream")
													<li class="breadcrumb-item">
														<a href="{{route('education.index')}}" class="text-muted">Education Streams</a>
													</li>
												@endif
                                                @if(Request::segment(2) == "practical-questions")
                                                    <li class="breadcrumb-item">
                                                        <a href="{{route('practical.questions.index')}}" class="text-muted">Practical Questions</a>
                                                    </li>
                                                @endif
                                                @if(Request::segment(3) == 'add-practical-questions' || Request::segment(3) == 'edit-practicalQuestions')
                                                    <li class="breadcrumb-item">
                                                        <a href="" class="text-muted">{{ Request::segment(3) }}</a>
                                                    </li>
                                                @endif
											</ul>
											<!--end::Breadcrumb-->
										</div>
										<!--end::Page Heading-->
									</div>
                                    @if(Request::segment(2) == 'exams' && (Request::segment(3) == 'create' || Request::segment(3) == 'edit-exam'))
                                        <div class="col-sm-3 text-right" style="">
                                            <button type="button" class="btn btn-sm btn-info" id="statistics" data-toggle="modal" data-target="#kt_modal_KTDatatable_remote">
                                                Questions/Categories Statistics
                                            </button>
                                        </div>
                                    @endif

									<!--end::Info-->
									<!--begin::Toolbar-->

									<!--end::Toolbar-->
								</div>
							</div>
							<!--end::Subheader-->
							<div class=" flex-column-fluid " id="kt_content">
								<!-- begin:Content -->
								@yield('content')
								<!-- end:Content -->
							</div>
							<!--end::Content-->
						</div>
						<!--begin::Content Wrapper-->
					</div>
					@yield('popForm')
					<!--end::Container-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<span class="text-muted font-weight-bold mr-2">2020©</span>
								<a href="https://www.weboccult.com" target="_blank" class="text-muted text-hover-primary">Weboccult Technologies.</a>
							</div>
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Main-->
		<script>var HOST_URL = "https://keenthemes.com/metronic/tools/preview";</script>
		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#EEE5FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->

		<script src="{{asset('new_assets/plugins/plugins.bundle.js?v=7.0.3')}}"></script>
		<script src="{{asset('new_assets/plugins/prismjs.bundle.js?v=7.0.3')}}"></script>
		<script src="{{asset('new_assets/js/scripts.bundle.js?v=7.0.3')}}"></script>
		<script src="{{asset('new_assets/js/select2.js?v=7.0.3')}}"></script>
		<script src="{{asset('new_assets/js/select2.min.js?v=7.0.3')}}"></script>
		<!--end::Global Theme Bundle-->
		<!--begin::Page Vendors(used by this page)-->
		<script src="{{asset('new_assets/plugins/fullcalender.bundle.js?v=7.0.3')}}"></script>
		<script src="{{asset('new_assets/js/bootstrap-datepicker.js?v=7.0.3')}}"></script>
		<!--end::Page Vendors-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="{{asset('new_assets/js/widgets.js?v=7.0.3')}}"></script>
		<script src="{{asset('new_assets/js/html-table.js?v=7.0.3')}}"></script>
		<script src="{{asset('new_assets/js/bootstrap-timepicker.js?v=7.0.3')}}"></script>
		<script src="{{asset('new_assets/js/jquery.ba-throttle-debounce.js')}}"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
		@extends('partials.footer')
		@yield('script')
		<!--end::Page Scripts-->
	</body>
	<!--end::Body-->
</html>
