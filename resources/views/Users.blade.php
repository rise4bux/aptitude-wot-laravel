 @extends('layouts.app')
    <!-- END: Header -->
    <!-- BEGIN: Left Aside -->
@section('content')

    <!-- END: Left Aside -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">

        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                       <a href="{{route('home')}}">
                            <h3 class="m-subheader__title m-subheader__title--separator">
                                Dashboard
                            </h3>
                       </a>
                           <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                <li class="m-nav__item m-nav__item--home">
                                    <a href="{{route('home')}}" class="m-nav__link m-nav__link--icon">
                                        <i class="m-nav__link-icon la la-home"></i>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{route('user.list')}}" class="m-nav__link">
												<span class="m-nav__link-text">
													Users
												</span>
                                    </a>
                                </li>
                            </ul>
                        </div>  
                    </div>
                </div>

                <div class="m-content">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <span class="kt-portlet__head-icon">
                                                        <i class="la la-users kt-font-brand"></i>
                                                    </span>
                                        </span>Users </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <a href="javascript:void(0)" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" data-toggle="modal" id="adduserbutton" data-target="#exampleModalLongInner">
												<span>
													<i class="la la-user"></i>
													<span> Create User</span>
												</span>
                                </a>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-form m-form--label-align-right m--margin-bottom-30">
                                <div class="row ">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch" name="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
															<span><i class="la la-search"></i></span>
														</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="usersDatatable" id="ajax_data"></div>

                            {{--<table class="table" id="usersdatatable" colspan='0' cellspacing='0'>
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        First name
                                    </th>
                                    <th>
                                        Last name
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Contact number
                                    </th>
                                    <th>
                                        Enrollment No.
                                    </th>
                                    <th>
                                       College
                                    </th>
                                    <th>
                                        Role
                                    </th>
                                    <th colspan="">
                                        Actions
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>--}}


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalLongInner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                    <div class="modal-body">
                            <form class="kt-form"  data-scroll="true" method="POST" id="user_form" action="JavaScript:void(0)" style="height: 250px; overflow: scroll;" >
                                @csrf
                                <div class="form-group">
                                    <label class="form-check-label">First name</label>
                                    <input class="form-control" type="text" placeholder="First name" name="f_name" value="{{ old('f_name') }}" id="f_name">
                                    <span id="f_nameError" style=" font-size: 10px;color: red; display: none;"></span>
                                </div>

                                <div class="form-group">
                                    <label class="form-check-label">Last name</label>
                                    <input class="form-control" type="text" placeholder="Last name" name="l_name" value="{{ old('l_name') }}" id="l_name" >
                                    <span id="l_nameError" style=" font-size: 10px;color: red; display: none;"></span>
                                </div>

                                <div class="form-group">
                                    <label class="form-check-label">Enrollment No.</label>
                                    <input class="form-control" type="text" placeholder="Enrollment number" name="enrollment" value="{{ old('enrollment') }}" id="enrollment">
                                    <span id="enrollmentError" style=" font-size: 10px;color: red; display: none;"></span>
                                </div>
                                <div class="form-group">
                                    <label class="form-check-label">Email</label>
                                    <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}" id="email">
                                    <span id="emailError" style=" font-size: 10px;color: red; display: none;"></span>
                                </div>
                                <div class="form-group">
                                    <label class="form-check-label">College</label>
                                    <select class="form-control" name="college" id="college">
                                        <option value="">Select</option>
                                        @if(!empty($colleges))
                                            @foreach($colleges as $college)
                                                <option value="{{ $college->id }}">{{ $college->name }}</option>
                                            @endforeach
                                        @endif    
                                    </select>
                                    <span id="collegeError" style=" font-size: 10px;color: red; display: none;"></span>
                                </div>
                                <div class="form-group">
                                    <label class="form-check-label">Mobile No.</label>
                                    <input class="form-control" type="text" placeholder="Contact number" name="mobile_no" id="mobile_no">
                                    <span id="mobile_noError" style=" font-size: 10px;color: red; display: none;"></span>
                                </div>
{{--                                <div class="form-group">--}}
{{--                                    <label class="form-check-label">Role</label>--}}
{{--                                    <select class="form-control" id="roles" name="role">--}}
{{--                                        <option value="admin" id="admin">Admin</option>--}}
{{--                                        <option value="user" id="user">User</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
                                <div class="form-group" >
                                    <label class="form-check-label">Password</label>
                                    <input class="form-control" type="password" placeholder="Password"
                                           name="password" id="password">
                                    <span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-password" style="position: relative; top: -28px;left: 415px;"></span>

                                    <span id="passwordError" style=" font-size: 10px;color: red; display: none;"></span>
                                </div>
                                <div class="form-group" >
                                    <label class="form-check-label">Confirm Password</label>
                                    <input class="form-control" type="password" id="rpassword" placeholder="Confirm Password"
                                           name="password_confirmation">
                                           <span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-password" style="position: relative; top: -28px;left: 415px;"></span>
                                </div>

                            </form>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save" id="adduser">Add</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Close</button>
                </div>
   
            </div>
        </div>
    </div>
    @include('partials.delete')
@endsection
@section('scripts')
     <script src="{{asset('assets/app/js/users.js')}}"></script>
    <script>
        var users = {!! $users !!};
    </script>
@endsection