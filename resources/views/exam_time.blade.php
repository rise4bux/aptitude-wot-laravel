<table class="table table-responsive  table-striped text-left">
    <thead>
    <tr>
        <th colspan="2">Aptitude Exam</th>
        <th colspan="2">Practical Exam</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Start Time</td>
        @if($user->started_at != null)
            @php
         $datestartedAt_app = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->started_at, 'UTC')->setTimezone('Asia/Kolkata')->format('h:i:s A');
         @endphp
        @else
            <?php $datestartedAt_app = ' - '; ?>
        @endif
        <td>{{ $datestartedAt_app }}</td>

        <td>Start Time</td>
        @if($user->practical != null)
            @php
                $datestartedAtP = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->practical->started_at, 'UTC')->setTimezone('Asia/Kolkata')->format('h:i:s A');
           @endphp
            <td>{{ $datestartedAtP }}</td>
        @else
            <td> - </td>
        @endif
    </tr>

        <tr>
            <td>Complete Time</td>
            @if($user->completed_at != null)
                @php
                   $datecompletedAt_app = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->completed_at, 'UTC')->setTimezone('Asia/Kolkata')->format('h:i:s A');
              @endphp
            @else
                @php $datecompletedAt_app = "-"; @endphp
            @endif
            <td>{{ $datecompletedAt_app }}</td>

            <td>Complete Time</td>
            @if($user->practical != null)
                @php
                      $datecompletedAtP = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->practical->completed_at, 'UTC')->setTimezone('Asia/Kolkata')->format('h:i:s A');
                 @endphp
<!--                --><?php
//                $time = explode(' ', $user->practical->completed_at);
//                $time = $time[1];
//                ?>
                <td>{{ $datecompletedAtP }}</td>
            @else
                <td> - </td>
            @endif
        </tr>

    </tbody>
</table>
