
<!DOCTYPE html>
<!--
Template Name: Metronic - Bootstrap 4 HTML, React, Angular 9 & VueJS Admin Dashboard Theme
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: https://1.envato.market/EA4JP
Renew Support: https://1.envato.market/EA4JP
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">
    <!--begin::Head-->
    <head><base href="">
        <meta charset="utf-8" />
        <title>Aptitue test | Login</title>
        <meta name="description" content="Updates and statistics" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
        <!--end::Fonts-->
        <!--begin::Page Vendors Styles(used by this page)-->
        <link href="{{asset('new_assets/plugins/fullcalender.bundle.css')}}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles-->
        <!--begin::Global Theme Styles(used by all pages)-->
        <link href="{{asset('new_assets/plugins/plugins.bundle.css?v=7.0.3')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('new_assets/plugins/prismjs.bundle.css?v=7.0.3')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('new_assets/css/style.bundle.css?v=7.0.3')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('new_assets/css/login-2.css?v=7.0.3')}}" rel="stylesheet" type="text/css" />
        <!--end::Global Theme Styles-->
        <!--begin::Layout Themes(used by all pages)-->
        <!--end::Layout Themes-->
        <link rel="shortcut icon" href="{{asset('assets/app/media/img/logos/weblogo.png')}}" />
        {{--    <script>--}}
{{--        (function(h,o,t,j,a,r){--}}
{{--            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};--}}
{{--            h._hjSettings={hjid:1070954,hjsv:6};--}}
{{--            a=o.getElementsByTagName('head')[0];--}}
{{--            r=o.createElement('script');r.async=1;--}}
{{--            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;--}}
{{--            a.appendChild(r);--}}
{{--        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');--}}
{{--    </script>--}}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-37564768-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-37564768-1');
    </script>
    </head>
    <!--end::Head-->
    <!--begin::Body-->
    <body id="kt_body" class="header-fixed subheader-enabled page-loading">
        <!--begin::Main-->
        <div class="d-flex flex-column flex-root">
            <!--begin::Login-->
            <div class="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white" id="kt_login">
                <!--begin::Aside-->
                <div class="login-aside order-2 order-lg-1 d-flex flex-column-fluid flex-lg-row-auto bgi-size-cover bgi-no-repeat p-7 p-lg-10">
                    <!--begin: Aside Container-->
                    <div class="d-flex flex-row-fluid flex-column justify-content-between">
                        <!--begin::Aside body-->
                        <div class="d-flex flex-column-fluid flex-column flex-center mt-5 mt-lg-0">
                            <a href="#" class="mb-7 text-center">
                                <img src="{{asset('assets/app/media/img/logos/weblogo.png')}}" class="max-h-75px" alt="" />
                            </a>
                                <!--begin::Form-->
                                <div class="login-form login-signin">
                                    <div class="text-center mb-10 mb-lg-20">
                                        <h2 class="font-weight-bold">Sign In</h2>
                                        <p class="text-muted font-weight-bold">Enter your username and password</p>
                                    </div>
                                    <!--begin::Form-->
                                    <form action="{{ route('login') }}" method='POST'>
                                        @csrf
                                        <div class="form-group py-3 m-0">
                                            <input class="form-control h-auto placeholder-dark-75 @error('email') is-invalid @enderror" type="email" placeholder="Email" id='email' name="email" autocomplete="email" autofocus />
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group py-3  m-0">
                                            <input class="form-control h-auto placeholder-dark-75 @error('password') is-invalid @enderror" id='password' type="password" placeholder="Password" name="password" />
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group d-flex flex-wrap justify-content-between align-items-center mt-3">
                                            <label class="checkbox checkbox-outline m-0 text-muted">
                                            <input type="checkbox" name="remember" />Remember me
                                            <span></span></label>
                                            <a href="javascript:;" id="kt_login_forgot" class="text-muted text-hover-primary">Forgot Password ?</a>
                                        </div>
                                        <div class="form-group d-flex flex-wrap justify-content-between align-items-center mt-2">
                                            <div class="my-3 mr-2">
                                                <!-- <span class="text-muted mr-2">Don't have an account?</span> -->
                                                <!-- <a href="/register" class="font-weight-bold">Signup</a> -->
                                            </div>
                                            <button type="submit" class="btn btn-info font-weight-bold px-9 py-4 my-3 ">Sign In</button>
                                        </div>
                                    </form>
                                    <!--end::Form-->
                                </div>
                                <!--end::Form-->
                        </div>
                        <!--end::Aside body-->
                        <!--begin: Aside footer for desktop-->
                        <div class="d-flex flex-column-auto justify-content-center mt-15">
                            <div class="text-dark-50 font-weight-bold order-2 order-sm-1 my-2">© 2020 Weboccult Technologies Pvt. Ltd.</div>
                        </div>
                        <!--end: Aside footer for desktop-->
                    </div>
                    <!--end: Aside Container-->
                </div>
                <!--begin::Aside-->
                <!--begin::Content-->
                <div class="order-1 order-lg-2 flex-column-auto flex-lg-row-fluid d-flex flex-column p-7" style="background-image: url('{{asset('new_assets/media/bg/bg-004.jpg')}}');     background-position: center; background-size: cover; background-repeat: no-repeat;">
                    <!--begin::Content body-->
                    <div class="d-flex flex-column-fluid flex-lg-center">
                        <div class="d-flex flex-column justify-content-center align-items-center text-center">
                            <h3 class="display-3 font-weight-bold my-7 text-white ">Welcome to Weboccult Technologies Pvt. Ltd.!</h3>

                        </div>
                    </div>
                    <!--end::Content body-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Login-->
        </div>
        <!--end::Main-->
        <script>var HOST_URL = "https://keenthemes.com/metronic/tools/preview";</script>
        <!--begin::Global Config(global config for global JS scripts)-->
        <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#EEE5FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
        <!--end::Global Config-->
        <!--begin::Global Theme Bundle(used by all pages)-->
        <script src="{{asset('new_assets/plugins/plugins.bundle.js')}}"></script>
        <script src="{{asset('new_assets/plugins/prismjs.bundle.js')}}"></script>
        <script src="{{asset('new_assets/js/scripts.bundle.js')}}"></script>
        <!--end::Global Theme Bundle-->
        <!--begin::Page Scripts(used by this page)-->
        <!-- <script src="{{asset('new_assets/js/login-general.js')}}"></script> -->
        <!--end::Page Scripts-->
    </body>
    <!--end::Body-->
</html>
