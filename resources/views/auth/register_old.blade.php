
<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>

    <title>Aptitue test | Register</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">        <!--end::Fonts -->


    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{asset('assets/app/Login/css/login-page.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{asset('assets/app/Login/plugins/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/app/Login/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/app/toastr/toastr.min.css')}}" rel="stylesheet">



    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <!--end::Layout Skins -->

    <link rel="shortcut icon" href="{{asset("assets/app/media/img/logos/weblogo.png")}}" />

    <!-- Hotjar Tracking Code for keenthemes.com -->
{{--    <script>--}}
{{--        (function(h,o,t,j,a,r){--}}
{{--            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};--}}
{{--            h._hjSettings={hjid:1070954,hjsv:6};--}}
{{--            a=o.getElementsByTagName('head')[0];--}}
{{--            r=o.createElement('script');r.async=1;--}}
{{--            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;--}}
{{--            a.appendChild(r);--}}
{{--        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');--}}
{{--    </script>--}}
<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-37564768-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-37564768-1');
    </script>    </head>
<!-- end::Head -->

<!-- begin::Body -->
<body  style="background-image: url({{asset('assets/app/media/img/bg/bg-2.jpg')}}); background-position: center top; background-size: 100% 350px;"  class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >

<!-- begin::Page loader -->

<!-- end::Page Loader -->
<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v4 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{asset('/assets/app/media/img/bg/bg-2.jpg')}});">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img src="{{asset('assets/app/media/img/logos/weblogo.png')}}" height="50px" width="50px">
                        </a>
                    </div>
                    <div class="">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">Sign Up</h3>
                            <div class="kt-login__desc">Enter your details to create your account:</div>
                        </div>
                        <form class="kt-form" method="POST" action="{{route('user.register')}}">
                            @csrf
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="First name" name="f_name" value="{{old('f_name')}}">
                                @error('f_name')
                                <span class="invalid-feedback" role="alert" >
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Last name" name="l_name" value="{{old('l_name')}}">
                                @error('l_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Enrollment number" name="enrollment" value="{{old('enrollment')}}">
                                @error('enrollment')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off" value="{{old('email')}}">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Contact number" name="mobile_no" value="{{old('mobile_no')}}">
                                @error('mobile_no')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="password" placeholder="Password" name="password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group">
                                <input class="form-control" type="password" placeholder="Confirm Password" name="password_confirmation">
                                @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="kt-login__actions">
                                <input type="submit" class="btn btn-brand btn-pill kt-login__btn-primary" value="Sign Up">&nbsp;&nbsp;
                                <a href="{{route('login')}}"><button type="button" class="btn btn-secondary btn-pill kt-login__btn-secondary">Cancel</button></a>
                            </div>
                        </form>
                    </div>
                    <div class="kt-login__account">
					<span class="kt-login__account-msg">
						Already have an account ?
					</span>
                        &nbsp;&nbsp;
                        <a href="{{route('login')}}" id="kt_login_signup" class="kt-login__account-link">Sign In!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Page -->


<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {"colors":{"state":{"brand":"#366cf3","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};
</script>
<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{asset('assets/app/Login/plugins/plugins.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/app/Login/js/scripts.bundle.js')}}" type="text/javascript"></script>
<!--end::Global Theme Bundle -->
<script src="{{asset('assets/app/toastr/toastr.min.js')}}"></script>
<script>
    @if(session()->has('error'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ session()->get('error') }}", "Error");
    @php
        session()->forget('error');
    @endphp
            @endif
            @if(session()->has('info'))
        toastr.options = {
        "progressBar": true,
    };
    toastr.error("{{ session()->get('info') }}", "Info");
    @php
        session()->forget('info');
    @endphp
    @endif
</script>
<!--begin::Page Scripts(used by this page) -->
<!--end::Page Scripts -->
</body>
<!-- end::Body -->
</html>
