@extends("layouts.app_demo")

@section("css")
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <style>
        .dataTables_info { float: right }
        .dataTables_paginate, .dataTables_info, .dataTables_length { display: inline-block}
        .questionClass { display: none}
        .dataTables_length { float: right}
        .dataTables_wrapper .dataTables_paginate .pagination .page-item.active > .page-link {background-color: #3699FF;color: #ffffff;}
        .dataTables_wrapper .dataTables_paginate .pagination .page-item:hover:not(.disabled) > .page-link {background-color: #3699FF;color: #ffffff;}
        .dataTables_processing {margin-top: -60px !important;background: #ffffff !important;color:  cornflowerblue !important;border-radius: 5px !important;font-size: 16px !important;opacity : 1 !important;text-decoration: none;padding-left: 10px;margin: -60px 45% 0px 45%;}
        .dataTables_wrapper .dataTable th.sorting_asc, .dataTables_wrapper .dataTable td.sorting_asc {color: cornflowerblue !important;}
        #DataTables_Table_0_length {width: 144px;padding-right: 0px;margin-right: 10px;}
        .custom-select.custom-select-sm.form-control.form-control-sm {width: 57px;}
        #DataTables_Table_0_info {padding-top: 6px;}
        .custom-select:focus {border-color: cornflowerblue;outline: 0;}
    </style>
@endsection

@section("content")
    <div class="content flex-column-fluid" id="kt_content">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Openings</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="javascript:void(0)" class="btn btn-info font-weight-bolder"   data-toggle="modal" id="addCollegeButton" data-target="#exampleModalLongInner">
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <circle fill="#000000" cx="9" cy="15" r="6"></circle>
                                        <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>Create Opening
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <div class="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" style="">
                    <table class="table datatable-table openingTableClass">
                        <thead>
                        <tr>
                            <th>NO</th>
                            <th>NAME</th>
                            <th>PRACTICAL EXAM TIME</th>
                            <th>STATUS</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!--end: Datatable-->
            </div>
        </div>
    </div>
@endsection

@section("popForm")
    <div class="modal fade" data-keyboard="true" id="exampleModalLongInner" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" style="display: none;" aria-hidden="true" style="overflow-y: scroll; max-height:55%;  margin-top: 50px; margin-bottom:50px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New Opening</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form class="kt-form"  data-scroll="true" method="POST" id="create_form" action="JavaScript:void(0)">
                        @csrf
                        <div class="form-group">
                            <label for="name"><strong>Name</strong></label>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Enter opening name" name="name" value="{{ old('name') }}" id="name">
                                <span id="openingsError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name"><strong>Practical Exam Time</strong><span style="font-size: 10px"> (Time in Minutes)</span></label>
                            <div class="form-group">
                                <input class="form-control" type="number" placeholder="Enter practical exam time" name="examTime" value="{{ old('examTime') }}" id="examTime">
                                <span id="examError" style=" font-size: 10px;color: red; display: none;"></span>
                            </div>
                        </div>

                        <div class="questionClass">
                            <input type="hidden" name="opening_id" id="opening_id">
                                <label for="status"><strong><strong>Total Exam Question</strong> ( <span id="total"></span> )</strong> </label>
                            <br><br>
                                <label for="status"><strong>Easy</strong> - <span id="easyTotal"></span></label>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Enter total number of easy question" name="easy" value="0" id="easy">
                                    <span id="easyError" style=" font-size: 10px;color: red; display: none;"></span>
                                </div>
                                <label for="status"><strong>Medium</strong> - <span id="mediumTotal"></span></label>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Enter total number of medium question" name="medium" value="0" id="medium">
                                    <span id="mediumError" style=" font-size: 10px;color: red; display: none;"></span>
                                </div>
                                <label for="status"><strong>Hard</strong> - <span id="hardTotal"></span></label>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Enter total nuber of hard question" name="hard" value="0" id="hard">
                                    <span id="hardError" style=" font-size: 10px;color: red; display: none;"></span>
                                </div>

                        </div>
                        <label for="status"><strong>Status</strong></label>
                        <div class="form-group">
                            <select class="form-control" name="status" id="status">
                                <option class="form-control" value="1" selected>Active</option>
                                <option class="form-control" value="0">Inactive</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info save" id="addButton">Add</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal"  data-keyboard="true" data-backdrop="static" class="modal fade in" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px">
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="padding: 20px">
                    <p>Do you really want to delete this record?</p>
                </div>
                <div class="modal-footer" style="padding: 15px">
                    <a href="javascript:void(0);" id="delete_btn" class="btn btn-danger">Delete</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <!-- Server side pagination -->
    <script type="text/javascript">
        $(function () {
            var table = $(".openingTableClass").DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route("openings.index") }}",
                "columnDefs": [
                    { "width": "8%", "targets": 0 },
                    { "width": "45%", "targets": 1 },
                    { "width": "13%", "targets": 3 },
                    { "width": "13%", "targets": 4 },
                    { "className": "dt-center", "targets": "_all"}
                ],
                dom: '<"top"f>rt<"bottom"pil><"clear">',
                columns: [
                    {data: "DT_RowIndex", name: "DT_RowIndex"},
                    {data: "name", name: "name"},
                    {data: "examTime", name: "examTime"},
                    {data: "status", name: "status"},
                    {data: "action", name: "action"},
                ]
            });
        });
    </script>
    <!-- All crud operation in this js -->
    <script src="{{asset('assets/app/js/openings.js')}}"></script>
@endsection

