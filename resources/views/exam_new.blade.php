@extends('layouts.app_demo')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <style>
        .dataTables_info { float: right }
        .dataTables_paginate, .dataTables_info, .dataTables_length { display: inline-block}
        .questionClass { display: none}
        .dataTables_length { float: right}
        .dataTables_wrapper .dataTables_paginate .pagination .page-item.active > .page-link {background-color: #3699FF;color: #ffffff;}
        .dataTables_wrapper .dataTables_paginate .pagination .page-item:hover:not(.disabled) > .page-link {background-color: #3699FF;color: #ffffff;}
        .dataTables_processing {margin-top: -60px !important;background: #ffffff !important;color:  cornflowerblue !important;border-radius: 5px !important;font-size: 16px !important;opacity : 1 !important;text-decoration: none;padding-left: 10px;margin: -60px 45% 0px 45%;}
        .dataTables_wrapper .dataTable th.sorting_asc, .dataTables_wrapper .dataTable td.sorting_asc {
            color: cornflowerblue !important;
        }
        #DataTables_Table_0_length {width: 144px;padding-right: 0px;margin-right: 10px;}
        .custom-select.custom-select-sm.form-control.form-control-sm {width: 57px;}
        #DataTables_Table_0_info {padding-top: 6px;}
        .custom-select:focus {border-color: cornflowerblue;outline: 0;}
    </style>
@endsection

@section('content')
	<div class="content flex-column-fluid" id="kt_content">

								<!--begin::Card-->
								<div class="card card-custom">
									<div class="card-header flex-wrap border-0 pt-6 pb-0">
										<div class="card-title">
											<h3 class="card-label">
                                                Exams</h3>
										</div>
										<div class="card-toolbar">
											<!--begin::Button-->
											<a href="{{route('exams.add')}}" class="btn btn-info font-weight-bolder">
                                                <i class="la la-check-square"></i>Create Exam</a>
											<!--end::Button-->
										</div>
									</div>
									<div class="card-body">
										<!--begin::Search Form-->
										<!--end::Search Form-->
										<!--begin: Datatable-->
										<div class="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" style="">
											<table class="table datatable-table examsDatatable">
                                                <thead>
                                                <tr style="width: 1000px">

                                                    <th>NO</th>
                                                    <th>TITLE</th>
                                                    <th>DESCRIPTION</th>
                                                    <th>QUESTION COUNT</th>
                                                    <th>TOTAL TIME</th>
                                                    <th>CUT OFF</th>
                                                    <th>STATUS</th>
                                                    <th>LINK</th>
                                                    <th>ACTION</th>
                                                </tr>
                                                </thead>
                                            </table>

										<!--end: Datatable-->
										</div>
										<!--end: Datatable-->
									</div>
								</div>
								<!--end::Card-->
							</div>
@endsection

@section('popForm')
<div id="myModal" data-keyboard="true" data-backdrop="static" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header" style="padding: 15px">
                <h4 class="modal-title">Are you sure?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="padding: 20px">
                <p>Do you really want to delete this record?</p>
            </div>
            <div class="modal-footer" style="padding: 15px">
                <a href="javascript:void(0);" id="delete_btn" class="btn btn-danger">Delete</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function () {
            var table = $('.examsDatatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('exams.index') }}",
                "columnDefs": [
                    { "width": "8%", "targets": 0 },
                    { "width": "13%", "targets": 7 },
                    { "width": "13%", "targets": 8 }
                ],
                dom: '<"top"f>rt<"bottom"pil><"clear">',

                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'question', name: 'question'},
                    {data: 'time', name: 'time'},
                    {data: 'cut_off_percentage', name: 'cut_off_percentage'},
                    {data: 'status', name: 'status'},
                    {data: 'link', name: 'link'},
                    {data: 'action', name: 'action'},
                ]
            });
        });
    </script>
<script src="{{asset('assets/app/js/exams.js')}}"></script>
<script>
        $(document).on('click','.Copylink',function () {
            var link=$(this).attr('title');
            var link="{{env('APP_URL')}}/exam/"+link;
            var tempInput = document.createElement("input");
            tempInput.style = "position: absolute; left: -1000px; top: -1000px";
            tempInput.value = link;
            console.log(tempInput.value);
            document.body.appendChild(tempInput);
            tempInput.select();
            document.execCommand("copy");
            document.body.removeChild(tempInput);
            toastr.success("Exam Link Copied", "Success");
        });
        $(document).on('click','.LoginLink',function () {
            var link=$(this).attr('title');
            var link="{{env('APP_URL')}}/exam/login/"+link;
            var tempInput = document.createElement("input");
            tempInput.style = "position: absolute; left: -1000px; top: -1000px";
            tempInput.value = link;
            console.log(tempInput.value);
            document.body.appendChild(tempInput);
            tempInput.select();
            document.execCommand("copy");
            document.body.removeChild(tempInput);
            toastr.success("Exam Login Link Copied", "Success");
        });
        $(document).on('click','.start-exam',function () {
            var link=$(this).attr('title');
           link= link.split('start').join("");
            console.log(link);
            var url="{{env('APP_URL')}}/exam/"+link;
           window.open(url, "_private");
            // chrome.windows.create({ incognito: true });
            // browser.windows.create({ incognito: true }).then(onSuccess, onError);
		});

    </script>
@endsection
