@extends('layouts.app_demo')

@section('content')
    <div class="content flex-column-fluid" id="kt_content">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">Update Admin Profile</h3>
            </div>
            <!--begin::Form-->
            <div class="card-body">
                <form method="POST" action="{{ route('update.profile',$id) }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control form-control-solid" placeholder="Enter First Name" name='fname' value="{{$data->first_name}}">
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control form-control-solid" placeholder="Enter Last Name" name="lname" value="{{ $data->last_name }}">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control form-control-solid" name="email" value="{{ $data->email }}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control form-control-solid" placeholder="Enter Password" name="password">
                        </div>
                        <div class="form-group">
                            <label>Conform Password</label>
                            <input type="password" class="form-control form-control-solid" placeholder="Enter confirm password" name="c_password">
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info mr-2">Update</button>
                        <a href="{{route('exams.index')}}"><button type="button" class="btn btn-secondary">Cancel</button></a>
                    </div>
                </form>
            </div>
            <!--end::Form-->
        </div>
    </div>

@endsection
