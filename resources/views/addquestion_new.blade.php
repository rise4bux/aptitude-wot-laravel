@extends('layouts.app_demo')
@section('content')
    <script src="{{ asset('assets/app/js/ckeditor5/build/ckeditor.js') }}"></script>
    <style>
        .alert.alert-danger {
            background: #f66e84;
            border: 1px solid #f66e84;
            color: #ffffff;
        }
        .radio.radio-info > span {
            background-color: #ECF0F3;
            border: 1px solid cornflowerblue;
        }
    </style>
    <div class="content flex-column-fluid" id="kt_content">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="la la-question kt-font-brand" style="color:black; font-size:20px"></i>
                    {{!empty($question) ? 'Edit' : 'Create'}} Question
                </h3>
            </div>
            <!--begin::Form-->
            <div class="card-body col-sm-9" style="margin: 0 auto">
                <form id="form" class="kt-form row" action="{{!empty($question) ? route('questions.update',$question->id) : route('questions.save')}}"  method="post">
                    @csrf
                    <div class="col-sm-12 form-group">
                        <label class="form-check-label" for="title"><strong>Title</strong></label>
                        <input id="title" @if($question!==null) value="{{$question->title}}" @endif type="hidden"
                               name="title">
                        <textarea name="title" class=" form-control titleeditor"  rows="3">{!!($question!==null) ? $question->title : old('title')!!}</textarea>
                        <trix-editor input="title"></trix-editor>
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                            <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group col-sm-4">
                        <label class="form-check-label" for="category"><strong>Category</strong></label>
                        @if(Session::has('category'))
                            <?php
                            $data = session()->get('category');
                            $difficulty = session()->get('difficulty');
                            ?>
                        @endif

                        <select class="form-control" id="category" name="category">
                            @foreach($category as $cat)
                                @if(isset($data) && $data == $cat->id)
                                    <option class="form-control" value="{{$cat->id}}" selected>{{$cat->name}}</option>
                                @else
                                    <option class="form-control" value="{{$cat->id}}" @if($question!==null) @if($cat->id==$question->category_id) selected @endif @endif>{{$cat->name}}</option>
                                @endif

                            @endforeach
                        </select>
                        @error('category')
                        <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group col-sm-4">
                        <label class="form-check-label" for="difficulty"><strong>Difficulty</strong></label>
                        <select class="form-control" id="difficulty" name="difficulty">
                            @if(isset($difficulty))
                                <option class="form-control" value="Easy" @if($difficulty=="Easy") selected @endif>Easy</option>
                                <option class="form-control" value="Medium" @if($difficulty=="Medium") selected @endif>Medium</option>
                                <option class="form-control" value="Hard" @if($difficulty=="Hard") selected @endif>Hard</option>
                            @else
                                <option class="form-control" value="Easy" @if($question!==null && $question->difficulty=="Easy") selected @endif>Easy</option>
                                <option class="form-control" value="Medium" @if($question!==null && $question->difficulty=="Medium") selected @endif>Medium</option>
                                <option class="form-control" value="Hard" @if($question!==null && $question->difficulty=="Hard") selected @endif>Hard</option>
                            @endif

                       </select>
                        @error('difficulty')
                        <span class="invalid-feedback" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="status" class="form-check-label"><strong>Select Status</strong></label>
                        <select name="status" id="status" class="form-control">
                            <option value="1" @if($question!==null && $question->status=='1') selected @endif>Active</option>
                            <option value="0" @if($question!==null &&  $question->status=='0') selected @endif>Inactive</option>
                        </select>
                        @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong style="color: #f4516c !important;">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-sm-12">
                        @error('options')
                        <span class="invalid-feedback" style="display: block !important" role="alert">
                                        <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="kt-portlet__head kt-portlet__head--lg"
                             style=" border: none ;border-top: 1px solid #ebedf3;">
                            <div class="kt-portlet__head-label">
                                <!-- <h3 class="kt-portlet__head-title">
                                    Options
                                </h3> -->
                                <label for="status" class="form-check-label" style="font-size: 18px;font-weight:600;padding:20px 0"><strong>Options</strong></label>
                                <button type="button" class="btn btn-sm btn-info"
                                        id="addOptionButton" style="float: right;margin:17px 0">
                                    Add Options
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="option-container" class="w-100">
                        @if($question !==null)
                            @foreach($question->options as $option)
                                <div class="form-group">
                                    <label class="form-check-label" for="title">{{$loop->iteration.'.'}}</label>
                                    <input id="option{{$loop->iteration-1}}" value="{{$option->content}}" type="hidden"
                                           name="options[{{$loop->iteration-1}}][option]">
                                    <textarea name="option{{$loop->iteration-1}}" class="form-control"  rows="5" id="option{{$loop->iteration}}" style="display: none">{{$option->content}}</textarea>
                                    <script type="text/javascript">
                                        CKEDITOR.replace( 'options{{$loop->iteration}}' );
                                        CKEDITOR.add
                                    </script>
                                    <trix-editor input="option{{$loop->iteration-1}}"></trix-editor>
                                    <textarea name="options[{{$loop->iteration-1}}][option]" class="kt-ckeditor-{{ $loop->iteration-1 }}">{{$option->content}}</textarea>
                                    <script>
                                        ClassicEditor
                                            .create( document.querySelector( '.kt-ckeditor-{{ $loop->iteration-1 }}' ), {

                                                toolbar: {
                                                    items: [
                                                        'heading',
                                                        '|',
                                                        'bold',
                                                        'italic',
                                                        'link',
                                                        'bulletedList',
                                                        'numberedList',
                                                        '|',
                                                        'indent',
                                                        'outdent',
                                                        '|',
                                                        'imageUpload',
                                                        'blockQuote',
                                                        'insertTable',
                                                        'mediaEmbed',
                                                        'undo',
                                                        'redo'
                                                    ]
                                                },
                                                language: 'en',
                                                image: {
                                                    toolbar: [
                                                        'imageTextAlternative',
                                                        'imageStyle:full',
                                                        'imageStyle:side'
                                                    ]
                                                },
                                                table: {
                                                    contentToolbar: [
                                                        'tableColumn',
                                                        'tableRow',
                                                        'mergeTableCells'
                                                    ]
                                                },
                                                licenseKey: '',

                                            } )
                                            .then( editor => {
                                                window.editor = editor;
                                            } )
                                            .catch( error => {
                                                console.error( 'Oops, something gone wrong!' );
                                                console.error( 'Please, report the following error in the https://github.com/ckeditor/ckeditor5 with the build id and the error stack trace:' );
                                                console.warn( 'Build id: ia47t6hg9pi0-8o65j7c6blw0' );
                                                console.error( error );
                                            } );

                                    </script>
                                    <div class="clearfix m-1">
{{--                                        <label class="mt-checkbox">--}}
{{--                                            <input type="radio" class="radioSelect"--}}
{{--                                                   name="options[{{$loop->iteration-1}}][is_correct]" @if($option->is_correct=='1') value="1" checked @else value="0" @endif>--}}
{{--                                            <span class="text-muted" style="color:black"> Correct</span>--}}

{{--                                        </label>--}}
                                        <label class="radio radio-rounded radio-info">
                                            <input type="radio" name="options[{{$loop->iteration-1}}][is_correct]" class="radioSelect" @if($option->is_correct=='1') value="1" checked @else value="0" @endif>Correct
                                            <span></span></label>
                                        <button style="float: right" type="button"
                                                class="btn btn-sm btn-clean btn-icon btn-icon-md optionRemove"><i class="la la-trash" style="color:red"></i>
                                        </button>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div  class="form-group">
                                <label class="form-check-label" for="title">1.</label>
                                <input id="option" value="" type="hidden"
                                       name="options[0][option]">
                                <textarea name="options[0][option]" class="form-control" rows="3" style="display: none"></textarea>
                                <trix-editor input="option"></trix-editor>
                                <textarea name="options[0][option]" class="kt-ckeditor-0" id="options1"></textarea>
                                <script>
                                    ClassicEditor
                                        .create( document.querySelector( '.kt-ckeditor-0' ), {

                                            toolbar: {
                                                items: [
                                                    'heading',
                                                    '|',
                                                    'bold',
                                                    'italic',
                                                    'link',
                                                    'bulletedList',
                                                    'numberedList',
                                                    '|',
                                                    'indent',
                                                    'outdent',
                                                    '|',
                                                    'imageUpload',
                                                    'blockQuote',
                                                    'insertTable',
                                                    'mediaEmbed',
                                                    'undo',
                                                    'redo'
                                                ]
                                            },
                                            language: 'en',
                                            image: {
                                                toolbar: [
                                                    'imageTextAlternative',
                                                    'imageStyle:full',
                                                    'imageStyle:side'
                                                ]
                                            },
                                            table: {
                                                contentToolbar: [
                                                    'tableColumn',
                                                    'tableRow',
                                                    'mergeTableCells'
                                                ]
                                            },
                                            licenseKey: '',

                                        } )
                                        .then( editor => {
                                            window.editor = editor;
                                        } )
                                        .catch( error => {
                                            console.error( 'Oops, something gone wrong!' );
                                            console.error( 'Please, report the following error in the https://github.com/ckeditor/ckeditor5 with the build id and the error stack trace:' );
                                            console.warn( 'Build id: ia47t6hg9pi0-8o65j7c6blw0' );
                                            console.error( error );
                                        } );

                                </script>
                                <div class="clearfix m-1">
                                    <label class="radio radio-rounded radio-info">
                                        <input type="radio" name="options[0][is_correct]" class="radioSelect" value="0">Correct
                                        <span></span></label>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="submit" value="{{!empty($question) ? 'Update' : 'Save'}}" name="save" class="btn btn-info">&nbsp;
                        @if(!isset($question))
                            <input type="submit" value="Save & New" name="saveNew" class="btn btn-info">&nbsp;
                        @endif
                        <a href="{{route('questions.index')}}" class="btn btn-secondary">Cancel</a>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div id="optionGroup" class="form-group" style="display: none">
        <label class="form-check-label" for="title" id="phaseCount">1.</label>
     {{--   <input id="option" value="" type="hidden"
               name="options[0][option]">
        --}}{{--<textarea name="options[0][option]" class="form-control" rows="3"></textarea>--}}{{--
        <trix-editor input="options[0][option]"></trix-editor>--}}
        <textarea name="options[0][option]" class="kt-ckeditor-new" id="options0"></textarea>
        <div class="clearfix m-1">
{{--            <label class="mt-checkbox">--}}
{{--                <input type="radio" class="radioSelect"--}}
{{--                       name="options[0][is_correct]" value="0">--}}
{{--                <span class="text-muted">correct</span>--}}
{{--            </label>--}}
            <label class="radio radio-rounded radio-info">
                <input type="radio" name="options[0][is_correct]" class="radioSelect" value="0">Correct
                <span></span></label>
            <button style="float: right" type="button"
                    class="btn btn-sm btn-clean btn-icon btn-icon-md optionRemove"><i class="la la-trash" style="color:red"></i>
            </button>
        </div>
    </div>
    <div id="myModal" data-keyboard="true" data-backdrop="static" class="modal fade in">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px">
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="padding: 20px">
                    <p>Do you really want to delete this record?</p>
                </div>
                <div class="modal-footer" style="padding: 15px">
                    <a href="javascript:void(0);" id="delete_btn" class="btn btn-danger">Delete</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div id="deleteOption" data-keyboard="true" data-backdrop="static" class="modal fade in" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px">
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="padding: 20px">
                    <p>Do you really want to delete this Option?</p>
                </div>
                <div class="modal-footer" style="padding: 15px">
                    <a href="javascript:void(0);" id="deleteOptionBtn" class="btn btn-danger">Delete</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var question=`@php echo !empty($question) ? $question : null @endphp`;
        if (question !== null){
            var count =`@php echo !empty($count) ? $count : 1 @endphp`;
        }
    </script>

    <script src="{{asset('assets/app/js/jquery.validate.js')}}"></script>

    <script>
        $(document).ready(function () {
            $(document).on('click', '#addOptionButton', function() {
                resetIndexNumber();
            });

            // click on delete button show confirmation modal
            $(document).on('click', '.optionRemove', function() {
                var name = $(this).parent().siblings('textarea').attr('name');
                $('#deleteOptionBtn').attr('name',name);
                $('#deleteOption').modal({
                    backdrop: 'static',
                    keyboard: true
                }).on('click', '#deleteOptionBtn', function(e) {
                    var divName = $(this).attr('name');
                    $('#option-container').find('textarea[name="'+divName+'"]').parent().remove();
                    resetIndexNumber();
                    $("#deleteOption").modal('hide');
                });
            });

            // reset index number after delete option
            function resetIndexNumber() {
                var i = 1;
                $('#option-container .form-check-label').each(function() {
                    $(this).text(i+".");
                    i++;
                });
            }
            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });
        });
    </script>
    <script src="{{asset('assets/app/js/addQuestions.js')}}"></script>

@endsection
