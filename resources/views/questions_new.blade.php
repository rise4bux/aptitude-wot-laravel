@extends("layouts.app_demo")

@section("css")
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <style>
        .dataTables_info { float: right }
        .dataTables_paginate, .dataTables_info, .dataTables_length { display: inline-block}
        .questionClass { display: none}
        .dataTables_length { float: right}
        .dataTables_wrapper .dataTables_paginate .pagination .page-item.active > .page-link {background-color: #3699FF;color: #ffffff;}
        .dataTables_wrapper .dataTables_paginate .pagination .page-item:hover:not(.disabled) > .page-link {background-color: #3699FF;color: #ffffff;}
        .dataTables_processing {margin-top: -60px !important;background: #ffffff !important;color:  cornflowerblue !important;border-radius: 5px !important;font-size: 16px !important;opacity : 1 !important;text-decoration: none;padding-left: 10px;margin: -60px 45% 0px 45%;}
        .dataTables_wrapper .dataTable th.sorting_asc, .dataTables_wrapper .dataTable td.sorting_asc {
            color: cornflowerblue !important;
        }
        #DataTables_Table_0_length {width: 144px;padding-right: 0px;margin-right: 10px;}
        .custom-select.custom-select-sm.form-control.form-control-sm {width: 57px;}
        #DataTables_Table_0_info {padding-top: 6px;}
        .custom-select:focus {border-color: cornflowerblue;outline: 0;}
    </style>
@endsection


@section("content")
    <div class="content flex-column-fluid" id="kt_content">

        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Questions</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{route('questions.add')}}" class="btn btn-info font-weight-bolder">
                        <i class="fa fa-question-circle" style="font-size:18px"></i>Create Question</a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin::Search Form-->

                <!--end::Search Form-->
                <!--begin: Datatable-->
                <form id="filterForm" action="POST" action="/">
                    <div class="row m--margin-bottom-20" style="margin-top: 20px;">
                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                            <label class=""><b>Category</b></label>
                            <select class="form-control" name="category" id="category">
                                <option>Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                            <label class=""><b>Difficulty</b></label>
                            <select class="form-control" name="difficulty" id="difficulty">
                                <option value="">Select Status</option>
                                <option value="Easy">Easy</option>
                                <option value="Medium">Medium</option>
                                <option value="Hard">Hard</option>
                            </select>
                        </div>
                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
                            <label class=""><b>Status</b></label>
                            <select class="form-control" name="status" id="status">
                                <option value="">Select Status</option>
                                <option value="1">Active</option>
                                <option value="2">Inactive</option>
                            </select>
                        </div>

                        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile" style="margin-top: 25px;margin-bottom: 25px;">
                            <button type="reset" class="btn btn-secondary m-btn m-btn--icon btn-default" id="m_reset">
                                                    <span>
                                                        <i class="la la-close"></i>
                                                        <span>Reset</span>
                                                    </span>
                            </button>
                        </div>
                    </div>
                </form>

                <div class="datatable datatable-bordered datatable-head-custom datatable-default datatable-primary datatable-loaded" style="">
                    <table class="table datatable-table questionsDatatable">
                        <thead>
                        <tr>
                            <th>NO</th>
                            <th>TITLE</th>
                            <th>CATEGORY</th>
                            <th>DIFFICULTY</th>
                            <th>STATUS</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                    </table>
                    <!--end: Datatable-->
                </div>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
    </div>
@endsection

@section("popForm")
    <div id="myModal" data-keyboard="true" data-backdrop="static" class="modal fade in" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px">
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="padding: 20px">
                    <p>Do you really want to delete this record?</p>
                </div>
                <div class="modal-footer" style="padding: 15px">
                    <a href="javascript:void(0);" id="delete_btn" class="btn btn-danger">Delete</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        $(function () {
            var table = $(".questionsDatatable").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('questions.index') }}",
                    data: function(d) {
                        d.status = $('#status option:selected').val();
                        d.difficulty = $('#difficulty option:selected').val();
                        d.category = $('#category option:selected').val();
                    }
                },
                columnDefs: [
                    { "width": "8%", "targets": 0 },
                    { "width": "10%", "targets": 4 },
                    { "width": "13%", "targets": 5 },
                ],
                dom: '<"top"f>rt<"bottom"pil><"clear">',
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'title', name: 'title'},
                    {data: 'language', name: 'language'},
                    {data: 'difficulty', name: 'difficulty'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'},
                ]
            });
            $('#status').change(function(){
                table.draw();
            });
            $('#difficulty').change(function(){
                table.draw();
            });
            $('#category').change(function(){
                table.draw();
            });
            $('#m_reset').on('click',function() {
                $("#category").val($("#category option:first").val());
                $("#difficulty").val($("#difficulty option:first").val());
                $("#status").val($("#status option:first").val());
                table.draw();
            });
        });
    </script>
    <script src="{{asset('assets/app/js/questions.js')}}"></script>
@endsection

