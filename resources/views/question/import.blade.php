@extends('layouts.app')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body"
         style="background: rgb(249, 249, 252);">

        <div
                class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <a href="{{route('home')}}">
                                <h3 class="m-subheader__title m-subheader__title--separator">
                                    Dashboard
                                </h3>
                            </a>
                            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                                <li class="m-nav__item ">
                                    <a href="{{route('questions.index')}}" class="m-nav__link">
												<span class="m-nav__link-text">
													Questions
												</span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
												<span class="m-nav__link-text">
										>
												</span>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{route('questions.add')}}" class="m-nav__link">
												<span class="m-nav__link-text">
											Import Question

												</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                     id="kt_content" style="padding-top:0">
                    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                        <div class="kt-portlet kt-portlet--mobile">
                            <div class="kt-portlet__head kt-portlet__head--lg">
                                <div class="kt-portlet__head-label">
                                                    <span class="kt-portlet__head-icon">
                                                        <i class="flaticon-layers kt-font-brand"></i>
                                                    </span>
                                    <h3 class="kt-portlet__head-title">
                                        Import Question
                                    </h3>
                                </div>

                            </div>
                            <div class="kt-portlet--tabs row" style="justify-content: center">
                                <div class="card-block p-4 col-sm-6">
                                    <form id="form" class="kt-form row" action="{{ route('questions.import_data') }}"  method="post" enctype="multipart/form-data">
                                        @csrf

                                            <div class="form-group col-sm-12 w-100" >
                                                <label class="form-check-label" for="category">Import File</label>
                                                <div class="custom-file">
                                                    <input type="file" name="import_file" class="custom-file-input" id="customFile">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                </div>

                                                <span class="invalid-feedback" role="alert">
                                                {{-- <strong style="color: #f4516c !important;">{{ $message }}</strong>--}}
                                                </span>

                                            </div>

                                            <div class="form-group col-sm-12">
                                                <input type="submit" value="Submit" class="btn btn-info"> &nbsp;
                                                <a href="{{route('questions.index')}}"></a><button class="btn
                                        btn-secondary"
                                                >Cancel</button></div>



                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <!--begin::Page Vendors(used by this page)-->

    <!--end::Page Vendors-->
    <!--begin::Page Scripts(used by this page)-->
    {{-- <script src="assets/js/pages/crud/forms/editors/ckeditor-classic.js?v=7.0.3"></script>--}}

    {{--   <script src="{{ asset('assets/app/js/ckeditor/ckeditor.js') }}"></script>
       <script src="{{ asset('assets/app/js/ckeditor/samples/js/sample.js') }}"></script>--}}
    {{--
        <link rel="stylesheet" href="{{ asset('assets/app/js/ckeditor/samples/css/samples.css') }}">
    --}}

    <script src="{{asset('assets/app/js/jquery.validate.js')}}"></script>

    <script>
        $(document).ready(function () {

            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });

            /*$('#form').validate({
                rules: {
                    title: "required",
                    category: "required",
                    difficulty: "required",
                    status: "required",
                },
                messages: {},
                submitHandler: function (form) {
                    //form.submit();
                }, errorPlacement: function (error, element) {
                    console.log(element);
                    element.parent().append(error);
                }
            });*/

        });
    </script>

@endsection
