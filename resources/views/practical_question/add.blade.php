@extends("layouts.app_demo")

@section("content")
    <script src="{{ asset('assets/app/js/ckeditor5/build/ckeditor.js') }}"></script>
    <style>
        .alert.alert-danger {background: #f66e84;border: 1px solid #f66e84;color: #ffffff;}
    </style>
    <div class="content flex-column-fluid" id="kt_content">
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="la la-question kt-font-brand" style="color:black; font-size:20px"></i>
                    {{!empty($question) ? 'Edit' : 'Create'}} Practical Question<Question></Question>
                </h3>
            </div>
            <!--begin::Form-->
            <div class="card-body col-sm-9" style="margin: 0 auto">
                <form id="form" class="kt-form row" action="{{!empty($question) ? route('practical.questions.update',$question->id) : route('practical.questions.save')}}"  method="post">
                    @csrf
                    <div class="col-sm-12 form-group">
                        <label class="form-check-label" for="title"><strong>Defination</strong></label>
                        @if(isset($question->defination))
                            <input id="title" @if($question!==null) value="{{$question->defination}}" @endif type="hidden" name="title">
                            <textarea name="title" class=" form-control titleeditor" rows="3">{!!($question!==null) ? $question->defination : old('title')!!}</textarea>
                        @else
                            <input id="title" type="hidden" name="title">
                            <textarea name="title" class=" form-control titleeditor" rows="3">{!!old('title')!!}</textarea>
                        @endif
                        {{--<trix-editor input="title"></trix-editor>--}}
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                                    <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                                </span>
                        @enderror
                    </div>
                    <div class="col-sm-12 form-group">
                        <label class="form-check-label" for="title"><strong>Predifind Answer</strong></label>
                        @if(isset($question))
                            <input id="answer" @if($question!==null) value="{{$question->predefined_answer}}" @endif  type="hidden" name="answer">
                            <textarea name="answer" class=" form-control answerEditor" rows="3">{!!($question!==null) ? $question->predefined_answer : old('answer')!!}</textarea>
                        @else
                            <input id="answer" type="hidden" name="answer">
                            <textarea name="answer" class=" form-control answerEditor" rows="3">{!!old('answer')!!}</textarea>
                        @endif
                        {{--<trix-editor input="title"></trix-editor>--}}
                        @error('answer')
                        <span class="invalid-feedback" role="alert">
                            <strong style="color: #f4516c !important;">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group col-sm-4">
                        <label class="form-check-label" for="opaning"><strong>Opening</strong></label>
                        <select class="form-control" id="opaning" name="opaning">
                            @if(Session::has('practicalDifficulty'))
                                <?php
                                    $data = session()->get('opening');
                                    $difficulty = session()->get('practicalDifficulty');
                                ?>
                            @endif
                            @foreach($openings as $opaning)
                                @if(isset($data) && $data == $opaning->id)
                                    <option class="form-control" value="{{$opaning->id}}" selected>{{$opaning->name}}</option>
                                @else
                                    <option class="form-control" value="{{$opaning->id}}" @if(isset($question->defination)) @if($opaning->id==$question->opening_id) selected @endif @endif>{{$opaning->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('opaning')
                        <span class="invalid-feedback" role="alert">
                            <strong style="color: #f4516c !important;">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-sm-4">
                        <label class="form-check-label" for="difficulty"><strong>Difficulty</strong></label>
                        <select class="form-control" id="difficulty" name="difficulty">
                            @if(isset($data))
                                <option class="form-control" value="Easy" @if($difficulty =="Easy") selected @endif>Easy</option>
                                <option class="form-control" value="Medium" @if($difficulty=="Medium") selected @endif>Medium</option>
                                <option class="form-control" value="Hard" @if($difficulty=="Hard") selected @endif>Hard</option>

                            @else
                                <option class="form-control" value="Easy" @if(isset($question->difficulty) && $question->difficulty =="Easy") selected @endif>Easy</option>
                                <option class="form-control" value="Medium" @if(isset($question->difficulty) && $question->difficulty=="Medium") selected @endif>Medium</option>
                                <option class="form-control" value="Hard" @if(isset($question->difficulty) && $question->difficulty=="Hard") selected @endif>Hard</option>
                            @endif

                        </select>
                        @error('difficulty')
                        <span class="invalid-feedback" role="alert">
                                                    <strong style="color: #f4516c !important;">{{ $message }}</strong>
                                                </span>
                        @enderror
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="status" class="form-check-label"><strong>Select Status</strong></label>
                        <select name="status" id="status" class="form-control">
                            <option value="1" @if(isset($question->defination) && $question->status=='1') selected @endif>Active</option>
                            <option value="0" @if(isset($question->defination) &&  $question->status=='0') selected @endif>Inactive</option>
                        </select>
                        @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong style="color: #f4516c !important;">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" value="{{!empty($question) ?'Update' : 'Save'}}" name="save" class="btn btn-info"> &nbsp;
                        @if(!isset($question))
                            <input type="submit" value="Save & New" class="btn btn-info" name="saveNew">&nbsp;
                        @endif
                        <a href="{{route('practical.questions.index')}}" class="btn btn-secondary">Cancel</a>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
@section("script")
    <script src="{{asset('assets/app/js/jquery.validate.js')}}"></script>
    <script>
        $(document).ready(function () {

            $.validator.setDefaults({
                debug: true,
                success: "valid"
            });

        });
    </script>
    <script src="{{asset('assets/app/js/practical-question.js')}}"></script>
@endsection
