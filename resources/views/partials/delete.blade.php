<div id="myModal" class="modal fade in">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header" style="padding: 15px">
                <h4 class="modal-title">Are you sure?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="padding: 20px">
                <p>Do you really want to delete this record?</p>
            </div>
            <div class="modal-footer" style="padding: 15px">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                <a href="javascript:void(0);" id="delete_btn" class="btn btn-danger">Delete</a>
            </div>
        </div>
    </div>
</div>