@if(Request::segment(2) == 'instruction')
@elseif(Request::segment(2) == "login" && Request::segment(3) == "save")
@else
<footer class="m-grid__item  m-footer ">

</footer>
@endif
			<!-- end::Footer -->
		<!-- end:: Page -->
    		        <!-- begin::Quick Sidebar -->

		<!-- end::Quick Sidebar -->
	    <!-- begin::Scroll Top -->
		<!-- <div id="m_scroll_top" class="m-scroll-top">

			<i class="la la-arrow-up"></i>
		</div> -->
		<!-- end::Scroll Top -->		    <!-- begin::Quick Nav -->

		<!-- begin::Quick Nav -->
    	<!--begin::Base Scripts -->
@if(Request::segment(2) == 'instruction')
    <script src={{asset("assets/vendors/base/vendors.bundle.js")}} type="text/javascript"></script>
@endif
		<script src={{asset("assets/demo/demo9/base/scripts.bundle.js")}} type="text/javascript"></script>

		<!--end::Base Scripts -->
        <!--begin::Page Vendors -->
		<script src={{asset("assets/vendors/custom/fullcalendar/fullcalendar.bundle.js")}} type="text/javascript"></script>
{{--        @if(Request::segment(3) != 'add-questions' && Request::segment(3) != 'edit-question')--}}
            <script src="{{asset('assets/app/toastr/toastr.min.js')}}"></script>
{{--        @endif--}}
		<!--end::Page Vendors -->
        <!--begin::Page Snippets -->
		<script src={{asset("assets/app/js/dashboard.js")}} type="text/javascript"></script>
		<!--end::Page Snippets -->
        <!-- begin::Page Loader -->

        <script src="{{asset('assets/app/js/datatables.bundle.js')}}" ></script>
{{--@if(Request::segment(3) != 'add-questions' && Request::segment(3) != 'edit-question')--}}
    @include('partials.notify')
{{--@endif--}}

@yield('scripts')

		<script>
            function showNotification(type, message, title) {
                Command: toastr[type](message, title)
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            }
            $(window).on('load', function() {
                $('body').removeClass('m-page--loading');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });
		</script>
