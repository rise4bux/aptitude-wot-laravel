<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoulmnToExamUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam_users', function (Blueprint $table) {
            $table->float('remaining_time')->after('started_at')->nullable();
            $table->integer('total_attempted')->after('total_questions')->nullable();
            $table->integer('total_not_attempted')->after('total_attempted')->nullable();
            $table->integer('total_incorrect')->after('percentage')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_users', function (Blueprint $table) {
            $table->dropColumn('remaining_time');
            $table->dropColumn('total_attempted');
            $table->dropColumn('total_not_attempted');
            $table->dropColumn('total_incorrect');
        });
    }
}
