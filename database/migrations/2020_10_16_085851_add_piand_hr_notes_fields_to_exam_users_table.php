<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPiandHrNotesFieldsToExamUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam_users', function (Blueprint $table) {
            $table->longText('pi_notes')->nullable();
            $table->longText('hr_notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_users', function (Blueprint $table) {
            $table->dropColumn('pi_notes');
            $table->dropColumn('hr_notes');
        });
    }
}
