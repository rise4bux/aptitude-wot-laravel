<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsInUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('university_id')->after('password');
            $table->integer('college_id')->after('university_id');
            $table->string('future_studies')->after('college_id');
            $table->integer('primary_interest')->after('future_studies');
            $table->integer('secondary_interest')->after('primary_interest');
            $table->string('other_interest')->after('secondary_interest');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('university_id');
            $table->dropColumn('college_id');
            $table->dropColumn('future_studies');
            $table->dropColumn('primary_interest');
            $table->dropColumn('secondary_interest');
            $table->dropColumn('other_interest');
        });
    }   
}
