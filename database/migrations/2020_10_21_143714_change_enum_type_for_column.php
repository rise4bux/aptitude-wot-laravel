<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeEnumTypeForColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('colleges', function (Blueprint $table) {
            $table->string('status')->default('1')->change();
        });

        Schema::table('universities', function (Blueprint $table) {
            $table->string('status')->default('1')->change();
        });

        Schema::table('technologies', function (Blueprint $table) {
            $table->string('status')->default('1')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colleges', function (Blueprint $table) {
            $table->enum('status',['Active','Inactive']);
        });

        Schema::table('universities', function (Blueprint $table) {
            $table->enum('status',['Active','Inactive']);
        });

        Schema::table('technologies', function (Blueprint $table) {
            $table->enum('status',['Active','Inactive']);
        });
    }
}
