<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommentsToPracticalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('practicals', function (Blueprint $table) {
            $table->longText('comment_1')->nullable();
            $table->longText('comment_2')->nullable();
            $table->longText('comment_3')->nullable();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('practicals', function (Blueprint $table) {
            $table->dropColumn('comment_1');
            $table->dropColumn('comment_2');
            $table->dropColumn('comment_3');
        });
    }
}
