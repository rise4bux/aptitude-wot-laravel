<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraFieldsToPracticalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('practicals', function (Blueprint $table) {
            $table->string('result_4')->after('result_3')->nullable();
            $table->string('result_5')->after('result_4')->nullable();
            $table->string('result_6')->after('result_5')->nullable();
            $table->string('result_7')->after('result_6')->nullable();

            $table->longText('comment_4')->after('comment_3')->nullable();
            $table->longText('comment_5')->after('comment_4')->nullable();
            $table->longText('comment_6')->after('comment_5')->nullable();
            $table->longText('comment_7')->after('comment_6')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('practicals', function (Blueprint $table) {
            $table->dropColumn('result_4');
            $table->dropColumn('result_5');
            $table->dropColumn('result_6');
            $table->dropColumn('result_7');

            $table->dropColumn('comment_4');
            $table->dropColumn('comment_5');
            $table->dropColumn('comment_6');
            $table->dropColumn('comment_7');
        });
    }
}
