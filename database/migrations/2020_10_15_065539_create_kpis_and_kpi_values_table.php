<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKpisAndKpiValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpis', function (Blueprint $table) {
            $table->id();
            $table->integer('opening_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('kpi_values', function (Blueprint $table) {
            $table->id();
            $table->integer('kpi_id');
            $table->string('name');
            $table->float('weightage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpis');
        Schema::dropIfExists('kpi_values');
    }
}
