<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateStartedAtColumnInExamUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam_users', function (Blueprint $table) {
            $table->dropColumn(['started_at']);
        });

        Schema::table('exam_users', function (Blueprint $table) {
            $table->timestamp('started_at')->nullable()->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_users', function (Blueprint $table) {
            $table->dropColumn(['started_at']);
        });
        Schema::table('exam_users', function (Blueprint $table) {
            $table->timestamp('started_at')->after('user_id');
        });
    }
}
