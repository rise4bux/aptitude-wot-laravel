<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;class AddDifficultyColumnInOpening extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opening_masters', function (Blueprint $table) {
            $table->string('easy')->default('0');
            $table->string('medium')->default('0');
            $table->string('hard')->default('0');
        });
    }    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opening_masters', function (Blueprint $table) {
            $table->dropColumn('easy');
            $table->dropColumn('medium');
            $table->dropColumn('hard');
        });
    }
}
