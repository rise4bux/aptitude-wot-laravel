<?php

use Illuminate\Database\Seeder;
USE App\OpeningMasters;

class OpeningMastersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // OpeningMasters::truncate();
        $UserTypes = [
            [
                'name' => 'AI/ML Developer',
                'status'  => '1',
            ],
            [
                'name' => 'IOT Developer',
                'status'  => '1',
            ],
            [
                'name' => 'Backend Developer (Node/PHP-Laravel)',
                'status'  => '1',
            ],
            [
                'name' => 'Frontend Developer',
                'status'  => '1',
            ],
            [
                'name' => 'Graphic Designer',
                'status'  => '1',
            ],
            [
                'name' => 'Business Analyst',
                'status'  => '1',
            ],
            [
                'name' => 'Business Development Executive',
                'status'  => '1',
            ],
            [
                'name' => 'Digital Marketing Executive',
                'status'  => '1',
            ],
            [
                'name' => 'Quality Analyst',
                'status'  => '1',
            ]
        ];
        foreach ($UserTypes as $key => $value) {
            $user = OpeningMasters::create([
                'name' => $value['name'],
                'status'  => $value['status'],

            ]);
        }
    }
}
