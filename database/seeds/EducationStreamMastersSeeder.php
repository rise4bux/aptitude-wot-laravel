<?php

use Illuminate\Database\Seeder;
use App\EducationStreamMasters;

class EducationStreamMastersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // EducationStreamMasters::truncate();
        $UserTypes = [
            [
                'name' => 'BE/BTech CE/CSE',
                'status'  => '1',
            ],
            [
                'name' => 'BE/BTech EC',
                'status'  => '1',
            ],
            [
                'name' => 'BE/BTech IT',
                'status'  => '1',
            ],
            [
                'name' => 'ME/Mtech CE/CSE',
                'status'  => '1',
            ],
            [
                'name' => 'ME/Mtech EC',
                'status'  => '1',
            ],
            [
                'name' => 'ME/Mtech IT',
                'status'  => '1',
            ],
            [
                'name' => 'BCA',
                'status'  => '1',
            ],
            [
                'name' => 'BSC IT',
                'status'  => '1',
            ],
            [
                'name' => 'MBA',
                'status'  => '1',
            ],
            [
                'name' => 'MCA',
                'status'  => '1',
            ],
            [
                'name' => 'MSC IT',
                'status'  => '1',
            ],
            [
                'name' => 'Other',
                'status'  => '1',
            ],
        ];
        foreach ($UserTypes as $key => $value) {
            $user = EducationStreamMasters::create([
                'name' => $value['name'],
                'status'  => $value['status'],

            ]);
        }

    }
}
