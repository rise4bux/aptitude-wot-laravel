<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExamUserDelete extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('exam_users')->truncate();
        DB::table('question_answers')->truncate();
    }
}
