<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'practical-exam'], function () {
    Route::get('{practicalId}/details', 'PracticalController@practicalDetails');
    Route::post('verify-eligibility', 'PracticalController@verifyEligibility');
    Route::post('start-exam', 'PracticalController@startExam');
    Route::post('complete-exam', 'PracticalController@completeExam');
    Route::post('video-processing', 'PracticalController@videoProcessing');
});
Route::get('exam/{id}/get-all-enrollment-of-unprocessed-video', 'PracticalController@getAllEnrollmentOfUnProcessedVideo');
