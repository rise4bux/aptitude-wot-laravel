<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\Technology;
use App\Models\College;
use App\Models\University;

//Route::get('/year', function() {
//   $users = \App\User::all();
//   foreach($users as $user) {
//       if($user->year == 1) {
//           $user->update(['year'=>'2021']);
//       } else if($user->year == 2) {
//           $user->update(['year'=>'2020']);
//       } else if($user->year == 3) {
//           $user->update(['year'=>'2019']);
//       }else if($user->year == 4) {
//           $user->update(['year'=>'2018']);
//       }else if($user->year == 5) {
//           $user->update(['year'=>'2017']);
//       }else if($user->year == 6) {
//           $user->update(['year'=>'2016']);
//       }else if($user->year == 7) {
//           $user->update(['year'=>'2015']);
//       }else if($user->year == 8) {
//           $user->update(['year'=>'2014']);
//       }else if($user->year == 9) {
//           $user->update(['year'=>'2013']);
//       }else if($user->year == 10) {
//           $user->update(['year'=>'2012']);
//       }else if($user->year ==11) {
//           $user->update(['year'=>'2011']);
//       }
//   }
//});

//Route::get('/temp',function(){
//    $technology = Technology::all();
//    foreach ($technology as $t) {
//        if($t->status == 'Active') {
//            Technology::find($t->id)->update(['status' => '1']);
//        } else if ($t->status == 'InActive') {
//            Technology::find($t->id)->update(['status' => '0']);
//        }
//    }
//
//    $colleges = College::all();
//    foreach ($colleges as $c) {
//        if($c->status == 'Active') {
//           College::find($c->id)->update(['status' => '1']);
//       }
//       else if($c->status == 'InActive') {
//           College::find($c->id)->update(['status' => '0']);
//       }
//    }
//
//    $univercities = University::all();
//    foreach ($univercities as $u) {
//        if($u->status == 'Active') {
//            University::find($u->id)->update(['status' => '1']);
//        } else if($u->status == 'InActive') {
//            University::find($u->id)->update(['status' => '0']);
//        }
//    }
//});

Route::get('/sampale', function() {
  $technology = Technology::all();
    return view('technology.index_new', compact('technology'));
});

Route::get('/', function () {
    return view('frontend.candidate');
});

Route::get('/login',function (){
    return view('frontend.candidate');
});

Route::get('welcome',function(){
    return redirect()->away('https://www.weboccult.com/');
})->name('welcome');

Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
    Route::group(['middleware' => ['role:admin','auth']],function(){
    Route::prefix('/users')->middleware('auth')->group(function () {
        Route::get('/', 'UserController@index')->name('user.list');
        Route::get('/table-ajax', 'UserController@getUsers');
        Route::post('register', 'UserController@registerUser')->name('user.register');
        Route::get('edit-user/{id}', 'UserController@editUser')->name('user.edit');
        Route::post('update-user/{id}', 'UserController@updateUser')->name('user.update');
        Route::post('delete-user', 'UserController@deleteUser')->name('user.delete');
        Route::get('download/{file}', 'UserController@downloadFile');
        Route::get('ajaxCollege/{id}', 'UserController@ajaxCollege');
    });
    Route::group(['prefix'=>'/profile','middleware'=>'auth'],function(){
        Route::get('edit/{id}', 'UserController@editProfile')->name('edit.profile');
        Route::post('update/{id}', 'UserController@updateProfile')->name('update.profile');
    });
    Route::group(['prefix'=>'/category','middleware'=>'auth'],function(){
       Route::get('/','CategoryController@index')->name('category.list');
       Route::post('/status','CategoryController@statusChange');
       Route::get('/table-ajax','CategoryController@categoryList');
       Route::post('/create','CategoryController@create');
       Route::get('/editCategory/{id}','CategoryController@editCategory');
       Route::post('/updateCategory/{id}','CategoryController@updateCategory');
       Route::post('/deleteCategory','CategoryController@deleteCategory');
       Route::get('/count-question/{id}','CategoryController@countQuestion');
    });
    Route::group(['prefix'=>'/university','middleware'=>'auth'],function(){
       Route::get('/','UniversityController@index')->name('university.index');
       Route::post('/status','UniversityController@statusChange');
       Route::get('/uniLists','UniversityController@uniLists');
       Route::post('/store','UniversityController@store');
       Route::get('/edit-university/{id}','UniversityController@edit');
       Route::post('/update-university/{id}','UniversityController@update');
       Route::post('/delete-university','UniversityController@delete');
        Route::post('/university-ajax', 'UniversityController@ajaxData');
    });
    Route::group(['prefix'=>'/college','middleware'=>'auth'],function(){
       Route::get('/','CollegeController@index')->name('college.index');
       Route::post('/status','CollegeController@statusChange');
       Route::get('/collegeLists','CollegeController@collegeLists');
       Route::post('/store','CollegeController@store');
       Route::get('/edit/{id}','CollegeController@edit');
       Route::post('/update/{id}','CollegeController@update');
       Route::post('/delete','CollegeController@delete');
       Route::post('/college-ajax', 'CollegeController@ajaxData');
    });
    Route::group(['prefix'=>'/technology','middleware'=>'auth'],function(){
       Route::get('/','TechnologyController@index')->name('technology.index');
       Route::post('/status','TechnologyController@statusChange');
       Route::get('/technologyLists','TechnologyController@technologyLists');
       Route::post('/store','TechnologyController@store');
       Route::get('/edit/{id}','TechnologyController@edit');
       Route::post('/update/{id}','TechnologyController@update');
       Route::post('/delete','TechnologyController@delete');
    });
    Route::group(['prefix'=>'/questions','middleware'=>'auth'],function(){
        Route::get('/','QuestionController@index')->name('questions.index');
        Route::post('/status','QuestionController@statusChange');
        Route::get('/table-ajax','QuestionController@questionList');
        Route::get('/add-questions','QuestionController@addQuestion')->name('questions.add');
        Route::post('/save','QuestionController@save')->name('questions.save');
        Route::get('/edit-question/{id}','QuestionController@edit')->name('questions.edit');
        Route::post('/update-question/{id}','QuestionController@update')->name('questions.update');
        Route::get('/delete-question/{id}','QuestionController@delete')->name('questions.delete');
        Route::get('/import/','QuestionController@import')->name('questions.import');
        Route::post('/import_data','QuestionController@import_data')->name('questions.import_data');
        Route::post('/filter', 'QuestionController@filter')->name('questions.filter');
    });

    Route::group(['prefix' => '/practical-questions', 'middleware' => 'auth'], function() {
        Route::get('/', 'PracticalQuestionController@index')->name('practical.questions.index');
        Route::post('/status','PracticalQuestionController@statusChange');
        Route::get('/create-questions','PracticalQuestionController@addQuestion')->name('practical.questions.add');
        Route::post('/save','PracticalQuestionController@save')->name('practical.questions.save');
        Route::get('/edit-question/{id}','PracticalQuestionController@edit')->name('practical.questions.edit');
        Route::post('/update-question/{id}','PracticalQuestionController@update')->name('practical.questions.update');
        Route::post('/delete-question', 'PracticalQuestionController@delete');
        Route::post('status/','PracticalQuestionController@statusChange');
    });

    Route::group(['prefix'=>'/openings', 'middleware'=>'auth'], function() {
        Route::get('/', 'OpeningMastersController@index')->name('openings.index');
        Route::post('/status','OpeningMastersController@statusChange');
        Route::get('/add', 'OpeningMastersController@add')->name('openings.add');
        Route::get('edit/{id}', 'OpeningMastersController@edit')->name('openings.edit');
        Route::get('questions/{id}', 'OpeningMastersController@totalQuestion')->name('openings.question');
        Route::post('/save', 'OpeningMastersController@save')->name('openings.save');
        Route::post('update/{id}', 'OpeningMastersController@update')->name('openings.update');
        Route::post('delete', 'OpeningMastersController@delete')->name('openings.delete');
        Route::get('/ajaxData', 'OpeningMastersController@ajaxData')->name('openings.sample');
    });

    Route::group(['prefix'=>'/educationstream', 'middleware'=>'auth'], function() {
        Route::get('/', 'EducationStreamMastersController@index')->name('education.index');
        Route::post('/status','EducationStreamMastersController@statusChange');
         Route::get('/add', 'EducationStreamMastersController@add')->name('openings.add');
        Route::get('edit/{id}', 'EducationStreamMastersController@edit')->name('openings.edit');
        Route::post('/save', 'EducationStreamMastersController@save')->name('openings.save');
        Route::post('update/{id}', 'EducationStreamMastersController@update')->name('openings.update');
        Route::post('delete', 'EducationStreamMastersController@delete')->name('openings.delete');
    });

    Route::group(['prefix'=>'/system-setting', 'middleware'=>'auth'], function() {
        Route::get('/', 'SystemSettingController@editSystemSettings')->name('system.setting.edit');
        Route::post('/update', 'SystemSettingController@updateSystemSettings')->name('system.setting.update');
    });
    Route::prefix('kpi')->group(function () {
        Route::get('/', 'KpiController@index')->name('kpi.index');
        Route::get('/add', 'KpiController@add')->name('kpi.add');
        Route::get('{id}/edit', 'KpiController@edit')->name('kpi.edit');
        Route::post('/save', 'KpiController@save')->name('kpi.save');
        Route::put('{id}/update', 'KpiController@update')->name('kpi.update');
        Route::get('delete/{id}', 'KpiController@delete')->name('kpi.delete');
    });
    Route::group(['prefix'=>'/exams','middleware'=>'auth'],function (){
        Route::get('/','ExamController@index')->name('exams.index');
        Route::get('/table-ajax','ExamController@examList');
        Route::post('/status','ExamController@statusChange');
        Route::get('/create','ExamController@create')->name('exams.add');
        Route::post('/save','ExamController@save')->name('exams.save');
        Route::post('/get-category-difficulty','ExamController@getCategoryDiff');
        Route::get('edit-exam/{id}','ExamController@edit')->name('exams.edit');
        Route::post('update-exam/{id}','ExamController@update')->name('exams.update');
        Route::post('participants/users-marks','ExamController@examUsersMarks');
        Route::post('participants/users-details','ExamController@userDetail');
        Route::post('participants/question-answer-details','ExamController@questionDetail');
        Route::post('participants/categoryDetail','ExamController@categoryDetail');

        Route::post('participants/exam-time','ExamController@examTime');
        Route::post('participants/get-practical-data','ExamController@getPracticalData');
        Route::post('participants/update-practical-comment','ExamController@UpdatePracticalComment');
        Route::post('participants/update-practical-status','ExamController@UpdatePracticalStatus');
        Route::post('participants/get-kpi-data','ExamController@getKpiData');
        Route::post('participants/update-kpi-data','ExamController@updateKpiData');

        Route::post('participants/get-notes-data','ExamController@getNotesData');
        Route::post('participants/update-notes-data','ExamController@updateNotesData');

        Route::post('participants/eligibal','ExamController@eligibalValue');
        Route::post('participants/examusers-ajax/{id}','ExamController@examuserslist');
        Route::get('participants/{id}','ExamController@getParticipants')->name('exams.participants');

        Route::post('/export-student/{id}', 'ExamController@exportStudent')->name('exams.exportstudent');


        Route::get('delete-exam/{id}','ExamController@delete')->name('exams.delete');
    Route::get('stats-data','ExamController@statsData');
    });
    });
});

Route::group(['prefix'=>'exam'],function (){
    Route::get('/instruction','CandidateController@instruction')->name('candidate.instruction');
    Route::get('/{slug}','CandidateController@registerView')->middleware('candidate.auth')->name('candidate.exam');
    //Route::get('/{slug}','CandidateController@registerView')->name('candidate.exam');
    Route::post('/checkUser','CandidateController@checkUser')->name('candidate.checkUser');
    Route::post('/getColleges','CandidateController@getColleges')->name('candidate.getColleges');
    Route::post('/candidate_register','CandidateController@register')->name('candidate.register');
    Route::get('questions/{slug}/{id}','CandidateController@start')->name('candidate.questions');
    Route::get('start/{slug}','CandidateController@getQuestions')->middleware('start.auth')->name('candidate.start');
    Route::post('save-next','CandidateController@saveAnswer');
    Route::post('save-timer','CandidateController@saveTimer');
    Route::post('clear-session','CandidateController@clearSession');
    Route::get('submit/{exam_id}/{user_id}/{slug}','CandidateController@submit')->name('exam.submit');
    Route::post('save-submit','CandidateController@submitAjax')->name('exam.save-submit');
    Route::get('/login/{slug}','CandidateController@loginView')->name('candidate.login');
    Route::post('login/save','CandidateController@login')->name('candidate.save');
    Route::get('finished/thankyou','CandidateController@finish')->name('exam.finish');
});



Route::get('/home', 'HomeController@index')->name('home');
Route::get('test/{slug}','CandidateController@getQuestions');
Route::get('bypass/{id}',function($id){
    auth()->loginUsingId($id);
    echo "login email".auth()->user()->email;
});
